import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';

import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:provider/provider.dart';
import 'package:reparet/models/project_model.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';

import 'providers/auth_provider.dart';
import 'providers/categories_provider.dart';
import 'providers/chat_provider.dart';
import 'providers/contractor_details_provider.dart';
import 'providers/contractor_listings_provider.dart';
import 'providers/contractor_project_details_provider.dart';
import 'providers/link_account_provider.dart';
import 'providers/owner_details_provider.dart';
import 'providers/payment_methods_provider.dart';
import 'providers/project_bids_provider.dart';
import 'providers/project_chats_provider.dart';
import 'providers/user_listings_provider.dart';
import 'providers/user_project_details_provider.dart';
import 'providers/user_search_contractors_provider.dart';
import 'screens/common/forgot_password_screen.dart';
import 'screens/common/images_details_screen.dart';
import 'screens/common/login_screen.dart';
import 'screens/common/main_chat_screen.dart';
import 'screens/common/notifications_screen.dart';
import 'screens/common/play_video_screen.dart';
import 'screens/common/splash_screen.dart';
import 'screens/common/webview_screen.dart';
import 'screens/contractor/contractor_dashboard_screen.dart';
import 'screens/contractor/contractor_self_profile_screen.dart';
import 'screens/contractor/contractor_signup_screen.dart';
import 'screens/contractor/link_bank_account_screen.dart';
import 'screens/contractor/project_details_for_contractor_screen.dart';
import 'screens/contractor/user_details_screen.dart';
import 'screens/contractor/wallet_balance_screen.dart';
import 'screens/contractor/webview_screen_payment.dart';
import 'screens/user/contractor_profile_screen.dart';
import 'screens/user/edit_project_screen.dart';
import 'screens/user/favourite_list_screen.dart';
import 'screens/user/manage_payment_methods_screen.dart';
import 'screens/user/project_details_for_user_screen.dart';
import 'screens/user/user_dashboard_sreen.dart';
import 'screens/user/user_profile_screen.dart';
import 'screens/user/user_signup_screen.dart';
import 'utils/constants.dart';
import 'utils/custom_colors.dart';
import 'utils/custom_route.dart';
import 'utils/my_http_overrides.dart';
import 'utils/notifications_helper.dart';

NotificationsHelper? _notificationsHelper;

String getScaReturnUrl() {
  return "stripesdk://3ds.stripesdk.io";
}

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  _notificationsHelper = NotificationsHelper();
  await _notificationsHelper?.initialize();

  Stripe.publishableKey = Constants.stripePublishableKey;
  runApp(const ReparetApp());
}

class ReparetApp extends StatefulWidget {
  const ReparetApp({Key? key}) : super(key: key);

  @override
  _ReparetAppState createState() => _ReparetAppState();
}

class _ReparetAppState extends State<ReparetApp> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initState() {
    _notificationsHelper!.setNavigatorKey(navigatorKey);
    super.initState();
    UploadFileS3.configureAmplify();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
    );

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ProjectMedia()),
        ChangeNotifierProvider(create: (context) => ProjectModel()),
        ChangeNotifierProvider(create: (context) => CategoriesProvider()),
        ChangeNotifierProvider(
            create: (context) => UserSearchContractorsProvider()),
        ChangeNotifierProvider(create: (context) => AuthProvider()),
        ChangeNotifierProvider(create: (context) => ChatProvider()),
      ],
      child: MaterialApp(
        navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        title: "Reparet",
        theme: ThemeData(
          backgroundColor: Colors.white,
          fontFamily: "segoe",
          primaryColor: CustomColors.colorPrimary,
          accentColor: CustomColors.colorAccent,
          primaryColorDark: CustomColors.colorPrimary,
          primaryColorLight: CustomColors.colorPrimary,
          textTheme: const TextTheme(
            bodyText1: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w400,
              color: CustomColors.colorGrayDark2,
            ),
            headline1: TextStyle(
              fontSize: 19.0,
              fontWeight: FontWeight.w600,
              color: CustomColors.colorWhite,
            ),
            headline2: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w500,
              color: CustomColors.colorAccent,
            ),
          ),
          pageTransitionsTheme: PageTransitionsTheme(
            builders: {
              TargetPlatform.android: CustomPageTransitionBuilder(),
              TargetPlatform.iOS: CustomPageTransitionBuilder(),
            },
          ),
        ),
        routes: {
          SplashScreen.routeName: (ctx) => const SplashScreen(),
          LoginScreen.routeName: (ctx) => LoginScreen(),
          ForgotPasswordScreen.routeName: (ctx) => ForgotPasswordScreen(),
          UserSignUpScreen.routeName: (ctx) => UserSignUpScreen(),
          ContractorSignUpScreen.routeName: (ctx) => ContractorSignUpScreen(),
          ContractorProfileScreen.routeName: (ctx) => ChangeNotifierProvider(
                create: (context) => ContractorDetailsProvider(),
                child: const ContractorProfileScreen(),
              ),
          WebViewScreen.routeName: (ctx) => WebViewScreen(),
          FavouriteListScreen.routeName: (ctx) => const FavouriteListScreen(),
          UserProfileScreen.routeName: (ctx) => const UserProfileScreen(),
          EditProjectScreen.routeName: (ctx) => ChangeNotifierProvider(
                create: (context) => UserProjectDetailsProvider(),
                child: const EditProjectScreen(),
              ),
          ManagePaymentMethodsScreen.routeName: (ctx) => ChangeNotifierProvider(
                create: (context) => PaymentMethodsProvider(),
                child: const ManagePaymentMethodsScreen(),
              ),
          ProjectDetailsForUserScreen.routeName: (ctx) {
            Map<String, dynamic> data =
                ModalRoute.of(ctx)!.settings.arguments as Map<String, dynamic>;

            UserListingProvider? provider = data["PROVIDER"];
            return MultiProvider(
              providers: [
                ChangeNotifierProvider(
                  create: (context) => ProjectBidsProvider(),
                ),
                ChangeNotifierProvider(
                  create: (context) => ProjectChatsProvider(),
                ),
                ChangeNotifierProvider(
                  create: (context) => UserProjectDetailsProvider(),
                ),
                if (provider != null)
                  ChangeNotifierProvider.value(value: provider),
                if (provider == null)
                  ChangeNotifierProvider(
                      create: (context) => UserListingProvider())
              ],
              child: const ProjectDetailsForUserScreen(),
            );
          },
          ProjectDetailsForContractorScreen.routeName: (ctx) {
            Map<String, dynamic> data =
                ModalRoute.of(ctx)!.settings.arguments as Map<String, dynamic>;

            ContractorListingProvider? provider = data["PROVIDER"];
            return MultiProvider(
              providers: [
                ChangeNotifierProvider(
                  create: (context) => ContractorProjectDetailsProvider(),
                ),
                if (provider != null)
                  ChangeNotifierProvider.value(value: provider),
                if (provider == null)
                  ChangeNotifierProvider(
                      create: (context) => ContractorListingProvider()),
              ],
              child: const ProjectDetailsForContractorScreen(),
            );
          },
          MainChatScreen.routeName: (ctx) => MultiProvider(
                providers: [
                  ChangeNotifierProvider(
                      create: (context) => UserProjectDetailsProvider()),
                  ChangeNotifierProvider(
                      create: (context) => ContractorProjectDetailsProvider()),
                ],
                child: const MainChatScreen(),
              ),
          UserDetailsScreen.routeName: (ctx) => ChangeNotifierProvider(
                create: (context) => OwnerDetailsProvider(),
                child: const UserDetailsScreen(),
              ),
          WebViewScreenPayment.routeName: (ctx) => const WebViewScreenPayment(),
          NotificationsScreen.routeName: (ctx) => NotificationsScreen(),
          ImagesDetailsScreen.routeName: (ctx) => const ImagesDetailsScreen(),
          PlayVideoScreen.routeName: (ctx) => const PlayVideoScreen(),
          ContractorSelfProfileScreen.routeName: (ctx) =>
              ContractorSelfProfileScreen(),
          UserDashboardScreen.routeName: (ctx) => const UserDashboardScreen(),
          WalletBalanceScreen.routeName: (ctx) => const WalletBalanceScreen(),
          ContractorDashboardScreen.routeName: (ctx) => MultiProvider(
                providers: [
                  ChangeNotifierProvider(
                    create: (context) => LinkAccountProvider(),
                  ),
                ],
                child: ContractorDashboardScreen(),
              ),
          LinkBankAccountScreen.routeName: (ctx) => ChangeNotifierProvider(
                create: (context) => LinkAccountProvider(),
                child: const LinkBankAccountScreen(),
              ),
        },
        initialRoute: SplashScreen.routeName,
      ),
    );
  }
}
