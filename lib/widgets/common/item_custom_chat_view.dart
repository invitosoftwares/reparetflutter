import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../models/previous_chat_people_model.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/badge_count_text.dart';

class ItemCustomChatView extends StatefulWidget {
  final PreviousChatPeopleModel _model;
  final Size _size;
  final bool showHeader;
  final bool _isUser;

  const ItemCustomChatView(
      this._model, this._size, this.showHeader, this._isUser,
      {Key? key})
      : super(key: key);

  @override
  State<ItemCustomChatView> createState() => _ItemCustomChatViewState();
}

class _ItemCustomChatViewState extends State<ItemCustomChatView> {
  bool isFirst = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (isFirst) {
      isFirst = false;
      String temp = await UploadFileS3.getImageUrl(widget._model.image);
      widget._model.image = temp;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        if (widget.showHeader && !widget._isUser) ...[
          const SizedBox(height: 4),
          Text(
            ((widget._model.projectStatus ?? 0) == Constants.PROJECT_POSTED ||
                    ((widget._model.projectStatus ?? 0) ==
                        Constants.PROJECT_ACCEPTED) ||
                    ((widget._model.projectStatus ?? 0) ==
                        Constants.PROJECT_COMPLETED))
                ? "-------- Open Chats --------"
                : "-------- Closed Chats --------",
            style: const TextStyle(
              color: CustomColors.colorPrimary,
              fontSize: 13.0,
              fontWeight: FontWeight.normal,
            ),
          ),
          const SizedBox(height: 4),
        ],
        InkWell(
          onTap: () {
            if (!((widget._model.projectStatus ?? 0) ==
                    Constants.PROJECT_POSTED ||
                ((widget._model.projectStatus ?? 0) ==
                    Constants.PROJECT_ACCEPTED) ||
                ((widget._model.projectStatus ?? 0) ==
                    Constants.PROJECT_COMPLETED))) {
              return;
            }
            Common.getChatScreen(
              context,
              consumerId: widget._model.consumerId ?? "",
              providerId: widget._model.providerId ?? "",
              projectId: widget._model.projectId ?? 0,
              name:
                  "${widget._model.firstName ?? ""} ${widget._model.lastName ?? ""}",
            );
          },
          child: Card(
            elevation: 1,
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: CachedNetworkImage(
                      imageUrl: widget._model.image ?? "",
                      fit: BoxFit.cover,
                      height: widget._size.width * 0.144,
                      width: widget._size.width * 0.128,
                      placeholder: (context, url) => Image.asset(
                        "assets/images/no_pic_image.jpeg",
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, error) => Container(
                        padding: const EdgeInsets.all(4),
                        color: CustomColors.colorAccent.withOpacity(0.1),
                        height: widget._size.width * 0.144,
                        width: widget._size.width * 0.128,
                        child: const Icon(
                          Icons.error,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  "${widget._model.firstName} ${widget._model.lastName}",
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline1!
                                      .copyWith(
                                        color: widget._isUser
                                            ? CustomColors.colorAccent
                                            : CustomColors.colorPrimary,
                                        fontSize: 18.0,
                                      ),
                                ),
                              ),
                              const SizedBox(width: 4),
                              (((widget._model.projectStatus ?? 0) ==
                                          Constants.PROJECT_POSTED ||
                                      ((widget._model.projectStatus ?? 0) ==
                                          Constants.PROJECT_ACCEPTED) ||
                                      ((widget._model.projectStatus ?? 0) ==
                                          Constants.PROJECT_COMPLETED)))
                                  ? ((widget._model.unread ?? 0) != 0)
                                      ? BadgeCountText(
                                          count: widget._model.unread ?? 0,
                                          badgeCountColor: widget._isUser
                                              ? CustomColors.colorAccent
                                              : CustomColors.colorPrimary,
                                        )
                                      : Container()
                                  : Text(
                                      "Closed",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            color: CustomColors.colorGrayDark3,
                                            fontSize: 10,
                                          ),
                                    ),
                            ],
                          ),
                          const SizedBox(height: 2),
                          Text(
                            widget._model.lastMessage ?? "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style:
                                Theme.of(context).textTheme.headline1!.copyWith(
                                      color: CustomColors.colorGrayDark3,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12.0,
                                    ),
                          ),
                          const SizedBox(height: 2),
                          Row(
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    Image.asset(
                                      "assets/images/icon_briefcase_filled.png",
                                      color: widget._isUser
                                          ? CustomColors.colorAccent
                                          : CustomColors.colorPrimary,
                                      height: 12,
                                      width: 12,
                                    ),
                                    const SizedBox(width: 4),
                                    Expanded(
                                      child: Text(
                                        widget._model.projectTitle ?? "",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline1!
                                            .copyWith(
                                              color:
                                                  CustomColors.colorGrayDark3,
                                              fontWeight: FontWeight.normal,
                                              fontSize: 12.0,
                                            ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              ((widget._model.projectStatus ?? 0) ==
                                          Constants.PROJECT_POSTED ||
                                      ((widget._model.projectStatus ?? 0) ==
                                          Constants.PROJECT_ACCEPTED) ||
                                      ((widget._model.projectStatus ?? 0) ==
                                          Constants.PROJECT_COMPLETED))
                                  ? Text(
                                      Common.getConvertedDate(
                                        widget._model.projectTime ?? "",
                                        Constants.DATE_FORMAT_4,
                                        Constants.DATE_FORMAT_5,
                                        isUtc: true,
                                      ),
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText1!
                                          .copyWith(
                                            color: CustomColors.colorGrayDark3,
                                            fontSize: 10,
                                          ),
                                    )
                                  : Row(
                                      children: [
                                        const Icon(
                                          Icons.done,
                                          color: CustomColors.colorGreen,
                                          size: 10,
                                        ),
                                        const SizedBox(width: 2),
                                        Text(
                                          "Project Completed",
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyText1!
                                              .copyWith(
                                                color: CustomColors.colorGreen,
                                                fontSize: 10,
                                              ),
                                        ),
                                      ],
                                    ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
