import 'package:flutter/material.dart';

class BadgeCountText extends StatelessWidget {
  final int count;
  final Color? badgeCountColor;
  final Color? outLineColor;

  BadgeCountText({
    this.count = 0,
    @required this.badgeCountColor,
    this.outLineColor,
  });

  @override
  Widget build(BuildContext context) {
    return count < 0
        ? Container()
        : Container(
            padding: EdgeInsets.only(bottom: 2, right: 2),
            decoration: BoxDecoration(
              color: outLineColor != null ? outLineColor : Colors.transparent,
              shape: BoxShape.circle,
            ),
            child: Container(
              padding: EdgeInsets.all(count < 10 ? 4 : 2),
              decoration: BoxDecoration(
                color: badgeCountColor,
                shape: BoxShape.circle,
              ),
              child: Text(
                (count < 10 ? "$count" : "9+"),
                style: TextStyle(
                  color: Colors.white,
                  height: 1,
                  fontSize: 8,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          );
  }
}
