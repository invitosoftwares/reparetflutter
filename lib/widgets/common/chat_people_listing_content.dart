import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/chat_provider.dart';
import '../../utils/constants.dart';
import '../../widgets/common/item_custom_chat_view.dart';

class ChatPeopleListingContent extends StatelessWidget {
  final Size _size;
  final bool _isUser;

  const ChatPeopleListingContent(this._isUser, this._size, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ChatProvider>(
      builder: (context, value, child) => ListView.builder(
        padding: EdgeInsets.all(_size.width * 0.04),
        itemBuilder: (context, index) => Padding(
          padding: EdgeInsets.only(top: index == 0 ? 0.0 : 4.0),
          child: ItemCustomChatView(
            value.chatPeople[index],
            _size,
            index == 0 ||
                (((value.chatPeople[index].projectStatus ?? 0) ==
                            Constants.PROJECT_POSTED ||
                        ((value.chatPeople[index].projectStatus ?? 0) ==
                            Constants.PROJECT_ACCEPTED) ||
                        ((value.chatPeople[index].projectStatus ?? 0) ==
                            Constants.PROJECT_COMPLETED)) !=
                    ((value.chatPeople[index - 1].projectStatus ?? 0) ==
                            Constants.PROJECT_POSTED ||
                        ((value.chatPeople[index - 1].projectStatus ?? 0) ==
                            Constants.PROJECT_ACCEPTED) ||
                        ((value.chatPeople[index - 1].projectStatus ?? 0) ==
                            Constants.PROJECT_COMPLETED))),
            _isUser,
          ),
        ),
        itemCount: value.chatPeople.length,
      ),
    );
  }
}
