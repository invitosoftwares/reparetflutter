import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class MyOutlinedButton extends StatelessWidget {
  final String _text;
  final Color? color;
  final String? icon;
  final bool isExpanded;
  final double textSize;
  final double verticalPadding;
  final double horizontalPadding;

  MyOutlinedButton(
    this._text, {
    this.color,
    this.icon,
    this.isExpanded = false,
    this.textSize = 18.0,
    this.verticalPadding = 6.0,
    this.horizontalPadding = 12.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
          vertical: verticalPadding, horizontal: horizontalPadding),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: color == null ? CustomColors.colorAccent : color!,
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: isExpanded ? MainAxisSize.max : MainAxisSize.min,
        children: [
          if (icon != null)
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Image.asset(
                icon!,
                color: color == null ? CustomColors.colorAccent : color,
                width: textSize,
              ),
            ),
          Text(
            _text,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: color == null ? CustomColors.colorAccent : color,
                  fontSize: textSize,
                ),
          ),
        ],
      ),
    );
  }
}
