import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class ToolbarWithBackAndText extends StatelessWidget
    implements PreferredSizeWidget {
  final Color _color;
  final Size _screenSize;
  final String _text;
  final bool isCenter;
  final VoidCallback? onBackPressed;

  ToolbarWithBackAndText(
    this._color,
    this._screenSize,
    this._text, {
    this.isCenter = true,
    this.onBackPressed,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.dark,
      backgroundColor: _color,
      automaticallyImplyLeading: false,
      leading: InkWell(
        child: Padding(
          padding: EdgeInsets.only(
            left: _screenSize.height * 0.02,
            right: _screenSize.height * 0.02,
          ),
          child: Image.asset(
            "assets/images/icon_back.png",
            color: CustomColors.colorWhite,
          ),
        ),
        onTap: () =>
            onBackPressed == null ? Navigator.pop(context) : onBackPressed!(),
      ),
      titleSpacing: 0.0,
      leadingWidth: _screenSize.height * 0.065,
      title: Text(
        _text,
        style: Theme.of(context).textTheme.headline1!.copyWith(
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
      ),
      centerTitle: isCenter,
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}
