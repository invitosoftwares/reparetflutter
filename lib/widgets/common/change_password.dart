import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/forgot_password_model.dart';
import '../../models/form_field_data.dart';
import '../../providers/auth_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/password_text_form_field.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/show_success_dialog.dart';

class ChangePasswordView extends StatefulWidget {
  final int role;

  const ChangePasswordView(
    this.role,
  );

  @override
  _ChangePasswordViewState createState() => _ChangePasswordViewState();
}

class _ChangePasswordViewState extends State<ChangePasswordView> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "CURRENT_PASSWORD": null,
    "NEW_PASSWORD": null,
    "CONFIRM_NEW_PASSWORD": null,
  };
  bool _isLoading = false;
  Size? _size;

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(
            _size!.width * 0.05,
            30,
            _size!.width * 0.05,
            0,
          ),
          decoration: const BoxDecoration(
            color: CustomColors.colorGrayLight2,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(_size!.width * 0.05),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: _size!.width * 0.05),
                    Text(
                      "Change Password",
                      style: Theme.of(context).textTheme.headline2!.copyWith(
                            color: widget.role == Constants.USER
                                ? CustomColors.colorAccent
                                : CustomColors.colorPrimary,
                          ),
                    ),
                    SizedBox(height: _size!.width * 0.05),
                    getElevatedWidget(
                      PasswordTextFormField(
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            _data["CURRENT_PASSWORD"] = FormFieldData(
                                true, "Please enter current password");
                          } else {
                            _data["CURRENT_PASSWORD"] =
                                FormFieldData(false, value);
                          }
                          return null;
                        },
                        placeholder: "Current password",
                        prefixIconString: "assets/images/icon_key.png",
                        cursorColor: widget.role == Constants.USER
                            ? CustomColors.colorAccent
                            : CustomColors.colorPrimary,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.04),
                    getElevatedWidget(
                      PasswordTextFormField(
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            _data["NEW_PASSWORD"] = FormFieldData(
                                true, "Please enter new password");
                          } else if (value.trim().length < 6) {
                            _data["NEW_PASSWORD"] = FormFieldData(true,
                                "New password must be at least 6 characters long");
                          } else {
                            _data["NEW_PASSWORD"] = FormFieldData(false, value);
                          }
                          return null;
                        },
                        placeholder: "New password",
                        prefixIconString: "assets/images/icon_key.png",
                        cursorColor: widget.role == Constants.USER
                            ? CustomColors.colorAccent
                            : CustomColors.colorPrimary,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.04),
                    getElevatedWidget(
                      PasswordTextFormField(
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            _data["CONFIRM_NEW_PASSWORD"] =
                                FormFieldData(true, "Please confirm password");
                          } else if (!_data["NEW_PASSWORD"]!.isError &&
                              ((_data["NEW_PASSWORD"]?.data ?? "") !=
                                  value.trim())) {
                            _data["CONFIRM_NEW_PASSWORD"] = FormFieldData(
                                true, "Password does not confirm");
                          } else {
                            _data["CONFIRM_NEW_PASSWORD"] =
                                FormFieldData(false, value);
                          }
                          return null;
                        },
                        placeholder: "Confirm new password",
                        prefixIconString: "assets/images/icon_key.png",
                        cursorColor: widget.role == Constants.USER
                            ? CustomColors.colorAccent
                            : CustomColors.colorPrimary,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.04),
                    Align(
                      alignment: Alignment.topRight,
                      child: PrimaryRaisedButton(
                        "Save new password",
                        () {
                          _validateData(context);
                        },
                        size: _size!,
                        color: widget.role == Constants.USER
                            ? CustomColors.colorAccent
                            : CustomColors.colorPrimary,
                        isLoading: _isLoading,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.04),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(3),
          decoration: const BoxDecoration(
            color: CustomColors.colorGrayLight2,
            shape: BoxShape.circle,
          ),
          child: Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: widget.role == Constants.USER
                  ? CustomColors.colorAccent
                  : CustomColors.colorPrimary,
              shape: BoxShape.circle,
            ),
            child: Image.asset(
              "assets/images/icon_edit.png",
              width: 20,
              height: 20,
              color: CustomColors.colorWhite,
              fit: BoxFit.fill,
            ),
          ),
        ),
      ],
    );
  }

  Widget getElevatedWidget(Widget widget) {
    return Material(
      elevation: 1,
      borderRadius: const BorderRadius.all(Radius.circular(5)),
      child: widget,
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);
    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",

      );
      return;
    }

    setState(() {
      _isLoading = true;
    });

    final _authProvider = Provider.of<AuthProvider>(context, listen: false);

    try {
      dynamic response = await _authProvider.changePasswordApi(
        context,
        _data["CURRENT_PASSWORD"]?.data ?? "",
        _data["NEW_PASSWORD"]?.data ?? "",
      );

      setState(() {
        _isLoading = false;
      });

      if (response is ForgotPasswordModel) {
        FocusManager.instance.primaryFocus?.unfocus();
        Navigator.pop(context);
        showDialog(
          context: context,
          builder: (_) => ShowSuccessDialog(
            "Password changed successfully",
            null,
            color: widget.role == Constants.USER
                ? CustomColors.colorAccent
                : CustomColors.colorPrimary,
          ),
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (e) {
      print(e);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
