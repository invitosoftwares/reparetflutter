import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import '../../utils/common.dart';
import '../../utils/my_permission_handler.dart';

enum MyFileType {
  storage,
  camera,
}

class BottomSheetImagePicker extends StatefulWidget {
  final Color color;
  final bool shouldCrop;

  const BottomSheetImagePicker({
    this.color = Colors.black,
    this.shouldCrop = true,
  });

  @override
  _BottomSheetImagePickerState createState() => _BottomSheetImagePickerState();
}

class _BottomSheetImagePickerState extends State<BottomSheetImagePicker> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _renderBottomMenuItem(
            Icons.image,
            "Gallery photo",
            context,
            type: MyFileType.storage,
          ),
          const Divider(height: 1),
          _renderBottomMenuItem(
            Icons.camera_alt,
            "Camera",
            context,
            type: MyFileType.camera,
          ),
          const SizedBox(height: 10)
        ],
      ),
    );
  }

  _renderBottomMenuItem(
    icon,
    title,
    BuildContext context, {
    MyFileType? type,
  }) {
    var item = SizedBox(
      height: 60.0,
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Icon(icon, size: 30.0, color: widget.color),
          ),
          Center(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 20.0),
              child: Text(
                title,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          ),
        ],
      ),
    );
    return InkWell(
      child: item,
      onTap: () async {
        File? file = await _checkPermissions(type);
        if (file != null) Navigator.pop(context, file);
      },
    );
  }

  Future<File?> _checkPermissions(MyFileType? type) async {
    List<Permission> allPermissions = [
      Permission.camera,
      Platform.isIOS ? Permission.photos : Permission.storage
    ];

    MyPermissionHandler myPermissionHandler = MyPermissionHandler();
    if (!await myPermissionHandler.checkPermissions(allPermissions)) {
      bool status =
          await myPermissionHandler.requestPermissions(allPermissions);
      if (status) {
        if (type == MyFileType.camera) {
          return _openCamera();
        } else {
          return _openFilePicker();
        }
      } else {
        Common.showToast("Requested permissions are required to proceed.");
        openAppSettings();
        return null;
      }
    } else {
      if (type == MyFileType.camera) {
        return _openCamera();
      } else {
        return _openFilePicker();
      }
    }
  }

  Future<File?> _openCamera() async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(
      source: ImageSource.camera,
    );
    if (pickedFile == null) return null;
    if (widget.shouldCrop) {
      return _cropImage(File(pickedFile.path));
    } else {
      return File(pickedFile.path);
    }
  }

  Future<File?> _openFilePicker() async {
    try {
      FilePickerResult? filePickerResult =
          await FilePicker.platform.pickFiles(type: FileType.image);
      if (filePickerResult == null || filePickerResult.count == 0) {
        return null;
      }
      if (widget.shouldCrop) {
        return _cropImage(File(filePickerResult.files[0].path!));
      } else {
        return File(filePickerResult.files[0].path!);
      }
    } on PlatformException catch (e) {
      Common.showToast("Error while picking the file: " + e.toString());
      return null;
    }
  }

  Future<File?> _cropImage(File file) async {
    File? croppedFile = await ImageCropper().cropImage(
      sourcePath: file.path,
      aspectRatioPresets: [CropAspectRatioPreset.square],
      maxWidth: 1080,
      maxHeight: 1080,
      aspectRatio: const CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
      cropStyle: CropStyle.circle,
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: "",
        toolbarColor: widget.color,
        toolbarWidgetColor: Colors.white,
        initAspectRatio: CropAspectRatioPreset.original,
        lockAspectRatio: false,
      ),
      iosUiSettings: const IOSUiSettings(
        title: "",
        aspectRatioLockEnabled: false,
      ),
    );
    return croppedFile;
  }
}
