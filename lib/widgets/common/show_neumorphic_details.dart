import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/neumorphic_view.dart';

class ShowNeumorphicDetails extends StatelessWidget {
  final String _text;
  final String? icon;
  final Color color;

  ShowNeumorphicDetails(
    this._text, {
    this.icon,
    this.color = CustomColors.colorAccent,
  });

  @override
  Widget build(BuildContext context) {
    return NeumorphicContainer(
      child: Padding(
        padding: const EdgeInsets.all(12),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (icon != null)
              Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: Image.asset(
                  icon!,
                  height: 12,
                  color: color,
                ),
              ),
            SizedBox(width: 10),
            Expanded(
              child: Text(
                _text,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
