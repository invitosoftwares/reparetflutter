import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import 'package:shimmer/shimmer.dart';

class GetShimmerBox extends StatelessWidget {
  final double width;
  final double height;
  final double? radius;
  final double? opacity;

  const GetShimmerBox(
    this.width,
    this.height, {
    this.radius,
    this.opacity,
  });

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: CustomColors.colorShimmerBase
          .withOpacity(opacity == null ? 1.0 : opacity!),
      highlightColor: CustomColors.colorShimmerOverlay,
      period: const Duration(seconds: 1),
      child: Container(
        decoration: BoxDecoration(
          color: CustomColors.colorWhite,
          borderRadius: BorderRadius.circular(radius == null ? 5 : radius!),
        ),
        height: height,
        width: width,
      ),
    );
  }
}
