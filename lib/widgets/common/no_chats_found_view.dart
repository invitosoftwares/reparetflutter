import 'package:flutter/material.dart';

class NoChatsFoundView extends StatelessWidget {
  final Color color;

  NoChatsFoundView(this.color);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            "assets/images/empty_white_box.png",
            width: _screenSize.width * 0.5,
            color: color,
          ),
          Text(
            "No chats available!",
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600,
                ),
          ),
        ],
      ),
    );
  }
}
