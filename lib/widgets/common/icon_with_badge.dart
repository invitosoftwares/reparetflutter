import 'package:flutter/material.dart';

class IconWithBadge extends StatelessWidget {
  final int _count;
  final Widget _icon;
  final Color _color;

  IconWithBadge(this._count, this._icon, this._color);

  @override
  Widget build(BuildContext context) {
    return _count == 0
        ? _icon
        : Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Stack(
              children: [
                _icon,
                Positioned(
                  right: 10,
                  top: 10,
                  child: Container(
                    padding: EdgeInsets.all(_count < 10 ? 4 : 2),
                    decoration: BoxDecoration(
                      color: _color,
                      shape: BoxShape.circle,
                    ),
                    child: new Text(
                      (_count < 10 ? "$_count" : "9+"),
                      style: new TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
