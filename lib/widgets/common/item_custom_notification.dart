import 'package:flutter/material.dart';
import '../../screens/common/notifications_screen.dart';
import '../../utils/custom_colors.dart';

class ItemCustomNotification extends StatelessWidget {
  final NotificationModel _notificationModel;
  final bool _isUser;

  ItemCustomNotification(this._notificationModel, this._isUser);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              _notificationModel.notificationTitle,
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    color: _isUser ? CustomColors.colorAccent : CustomColors.colorPrimary,
                    fontSize: 15.0,
                  ),
            ),
            SizedBox(height: 4),
            Row(
              children: [
                Image.asset(
                  "assets/images/icon_briefcase_filled.png",
                  color: _isUser ? CustomColors.colorAccent : CustomColors.colorPrimary,
                  height: 12,
                  width: 12,
                ),
                SizedBox(width: 4),
                Expanded(
                  child: Text(
                    _notificationModel.projectTitle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.headline1!.copyWith(
                          color: CustomColors.colorGrayDark3,
                          fontWeight: FontWeight.normal,
                          fontSize: 12.0,
                        ),
                  ),
                ),
                SizedBox(width: 4),
                Text(
                  _notificationModel.time,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                        color: CustomColors.colorGrayDark3,
                        fontWeight: FontWeight.normal,
                        fontSize: 12.0,
                      ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
