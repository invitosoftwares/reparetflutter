import 'package:flutter/material.dart';

class NoInternetView extends StatelessWidget {
  final VoidCallback retryClick;

  const NoInternetView(this.retryClick, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Image.asset(
              "assets/images/no_listing.png",
              width: _screenSize.width * 0.85,
            ),
          ),
          SizedBox(height: _screenSize.height * 0.025),
          Padding(
            padding: EdgeInsets.all(_screenSize.width * 0.035),
            child: Text(
              "An internet connection is required to proceed. Please check your internet and retry.",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontSize: 12,
                  ),
            ),
          ),
          InkWell(
            onTap: () {
              retryClick();
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Retry",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1!
                    .copyWith(fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
