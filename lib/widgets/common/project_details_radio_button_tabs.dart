import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../models/tab_model.dart';
import '../../utils/constants.dart';
import '../../widgets/common/radio_button_tab.dart';

class ProjectDetailsRadioButtonTabs extends StatefulWidget {
  static final int PROJECT_DETAILS = 0;
  static final int BIDS = 1;
  static final int MESSAGES = 2;
  static final int CONTRACTOR_DETAILS = 3;

  final Function _onValueChanged;
  final Color _selectedColor;
  final Color? badgeCountColor;
  final bool _isProjectCompleted;
  final int? bidCount;
  final int? unreadMessagesCount;

  const ProjectDetailsRadioButtonTabs(
    this._onValueChanged,
    this._selectedColor,
    this._isProjectCompleted, {
    Key? key,
    this.badgeCountColor,
    required this.bidCount,
    required this.unreadMessagesCount,
  }) : super(key: key);

  @override
  _ProjectDetailsRadioButtonTabsState createState() =>
      _ProjectDetailsRadioButtonTabsState();
}

class _ProjectDetailsRadioButtonTabsState
    extends State<ProjectDetailsRadioButtonTabs> {
  List<TabModel> _allModels = [];
  bool _isFirstTime = true;
  int _selectedIndex = ProjectDetailsRadioButtonTabs.PROJECT_DETAILS;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_isFirstTime) {
      _isFirstTime = false;

      _allModels = [
        TabModel(
          ProjectDetailsRadioButtonTabs.PROJECT_DETAILS,
          _selectedIndex == ProjectDetailsRadioButtonTabs.PROJECT_DETAILS,
          "Project Details",
          "assets/images/icon_description.png",
        ),
        if (!widget._isProjectCompleted)
          TabModel(
            ProjectDetailsRadioButtonTabs.BIDS,
            _selectedIndex == ProjectDetailsRadioButtonTabs.BIDS,
            "Bids",
            "assets/images/icon_bid.png",
            badgeCount: widget.bidCount ?? 0,
          ),
        if (!widget._isProjectCompleted)
          TabModel(
            ProjectDetailsRadioButtonTabs.MESSAGES,
            _selectedIndex == ProjectDetailsRadioButtonTabs.MESSAGES,
            "Messages",
            "assets/images/icon_chat.png",
            badgeCount: widget.unreadMessagesCount ?? 0,
          ),
        if (widget._isProjectCompleted)
          TabModel(
            ProjectDetailsRadioButtonTabs.CONTRACTOR_DETAILS,
            _selectedIndex == ProjectDetailsRadioButtonTabs.CONTRACTOR_DETAILS,
            "Contractor details",
            "assets/images/icon_bid.png",
          ),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    updateBadgeCountIfApplicable();
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ..._allModels.map(
            (e) => InkWell(
              onTap: () {
                if (e.isSelected) return;
                widget._onValueChanged(e.type);
                setState(() {
                  for (var element in _allModels) {
                    element.isSelected = false;
                  }
                  e.isSelected = true;
                  _selectedIndex = e.type;
                });
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: RadioButtonTab(
                  e,
                  widget._selectedColor,
                  showNotch: true,
                  badgeCountColor: widget.badgeCountColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void updateBadgeCountIfApplicable() {
    if (widget._isProjectCompleted) {
      return;
    }

    _allModels.clear();
    _allModels = [
      TabModel(
        ProjectDetailsRadioButtonTabs.PROJECT_DETAILS,
        _selectedIndex == ProjectDetailsRadioButtonTabs.PROJECT_DETAILS,
        "Project Details",
        "assets/images/icon_description.png",
      ),
      if (!widget._isProjectCompleted)
        TabModel(
          ProjectDetailsRadioButtonTabs.BIDS,
          _selectedIndex == ProjectDetailsRadioButtonTabs.BIDS,
          "Bids",
          "assets/images/icon_bid.png",
          badgeCount: widget.bidCount ?? 0,
        ),
      if (!widget._isProjectCompleted)
        TabModel(
          ProjectDetailsRadioButtonTabs.MESSAGES,
          _selectedIndex == ProjectDetailsRadioButtonTabs.MESSAGES,
          "Messages",
          "assets/images/icon_chat.png",
          badgeCount: widget.unreadMessagesCount ?? 0,
        ),
      if (widget._isProjectCompleted)
        TabModel(
          ProjectDetailsRadioButtonTabs.CONTRACTOR_DETAILS,
          _selectedIndex == ProjectDetailsRadioButtonTabs.CONTRACTOR_DETAILS,
          "Contractor details",
          "assets/images/icon_bid.png",
        ),
    ];
  }
}
