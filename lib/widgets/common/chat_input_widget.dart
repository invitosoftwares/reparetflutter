import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/chat_provider.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';

class ChatInputWidget extends StatefulWidget {
  final String receiverId;
  final String projectId;
  final bool _isUser;

  ChatInputWidget(this.receiverId, this.projectId, this._isUser);

  @override
  _ChatInputWidgetState createState() => _ChatInputWidgetState();
}

class _ChatInputWidgetState extends State<ChatInputWidget> {
  final TextEditingController _textEditingController =
      new TextEditingController();
  bool _isDisabled = true;

  _checkStatus(String s) {
    s = s.trim();
    if ((s.isEmpty && !_isDisabled) || (s.isNotEmpty && _isDisabled)) {
      setState(() {
        _isDisabled = !_isDisabled;
      });
    }
  }

  @override
  dispose() {
    super.dispose();
    if (_textEditingController != null) _textEditingController.dispose();
  }

  _clearAll() {
    _textEditingController.clear();
    _checkStatus("");
  }

  @override
  Widget build(BuildContext context) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Expanded(
            child: Material(
              elevation: 1,
              color: CustomColors.colorWhite,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
              child: Container(
                padding: const EdgeInsets.all(8.0),
                child: IntrinsicHeight(
                  child: Row(
                    children: [
                      Expanded(
                        child: TextField(
                          onChanged: _checkStatus,
                          style: Theme.of(context).textTheme.bodyText1,
                          controller: _textEditingController,
                          textInputAction: TextInputAction.done,
                          maxLength: 1000,
                          cursorColor: widget._isUser
                              ? CustomColors.colorAccent
                              : CustomColors.colorPrimary,
                          decoration: const InputDecoration(
                            counterText: "",
                            hintText: "Type here ...",
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.symmetric(vertical: 5),
                            hintStyle: TextStyle(
                              color: CustomColors.colorGrayDark,
                            ),
                          ),
                          keyboardType: TextInputType.multiline,
                          maxLines: null,
                          expands: true,
                        ),
                      ),
                      // SizedBox(
                      //   height: double.infinity,
                      //   child: InkWell(
                      //     onTap: () {
                      //       Common.showToast("text");
                      //     },
                      //     child: const Icon(
                      //       Icons.attach_file,
                      //       color: CustomColors.colorGrayDark,
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(width: 5),
          Material(
            elevation: 1,
            color: CustomColors.colorWhite,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            child: Consumer<ChatProvider>(
              builder: (context, value, child) => IconButton(
                icon: const Icon(
                  Icons.send,
                ),
                onPressed: () {
                  if (_isDisabled) return;
                  value.sendNewMessage(
                    _textEditingController.text,
                    widget.receiverId,
                    widget.projectId,
                  );
                  _clearAll();
                },
                color: _isDisabled
                    ? widget._isUser
                        ? CustomColors.colorAccent.withOpacity(0.10)
                        : CustomColors.colorPrimary.withOpacity(0.10)
                    : widget._isUser
                        ? CustomColors.colorAccent
                        : CustomColors.colorPrimary,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
