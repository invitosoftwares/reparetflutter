import 'dart:io';
import 'dart:math';

//import 'package:aws_s3/aws_s3.dart';
import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:amplify_storage_s3/amplify_storage_s3.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../amplifyconfiguration.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
//import 'package:simple_s3/simple_s3.dart';

class UploadFileS3 {
  final File file;
  final String? title;
  final BuildContext context;
  static bool _isAmplifyConfigured = false;
  static bool _isAmplifyBeingConfigured = false;
  //SimpleS3? _simpleS3;

  UploadFileS3(this.context, this.file, this.title);

  /*Future displayUploadDialog(SimpleS3 awsS3) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => StreamBuilder(
        stream: awsS3.getUploadPercentage,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return buildFileUploadDialog(snapshot, context);
        },
      ),
    );
  }*/

  /*WillPopScope buildFileUploadDialog(
      AsyncSnapshot snapshot, BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: AlertDialog(
        title: Container(
          padding: const EdgeInsets.all(6),
          child: Consumer<AuthProvider>(
            builder: (context, value, child) => LinearProgressIndicator(
              value: (snapshot.data != null) ? snapshot.data / 100 : 0,
              backgroundColor: (value.userModel!.role == Constants.USER)
                  ? CustomColors.colorAccent.withOpacity(0.2)
                  : CustomColors.colorPrimary.withOpacity(0.2),
              valueColor: AlwaysStoppedAnimation<Color>(
                (value.userModel!.role == Constants.USER)
                    ? CustomColors.colorAccent
                    : CustomColors.colorPrimary,
              ),
            ),
          ),
        ),
        content: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 6),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  'Uploading $title',
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              Text(
                "${snapshot.data ?? 0}%",
                style: Theme.of(context).textTheme.bodyText1,
              ),
            ],
          ),
        ),
      ),
    );
  }*/

  Future<String?> uploadFile() async {
    if (!_isAmplifyConfigured) {
      await configureAmplify();
    }
    try {
      final key = DateTime.now().toString();
      Map<String, String> metadata = <String, String>{};
      metadata['name'] = 'filename';
      metadata['desc'] = 'A test file';
      S3UploadFileOptions options = S3UploadFileOptions(
          accessLevel: StorageAccessLevel.guest, metadata: metadata);
      UploadFileResult result = await Amplify.Storage.uploadFile(
          key: key,
          local: file,
          options: options,
          onProgress: (progress) {
            print("PROGRESS: " + progress.getFractionCompleted().toString());
          });
      print('upload success result: ${result.key}');
      getImageUrl(result.key);
      return result.key;
    } catch (e) {
      print('UploadFile Err: ' + e.toString());
    }
    return "";
  }

  static Future<String> getImageUrl(key) async {
    try {
      if (key.contains("https")) {
        return key;
      }
      if (!_isAmplifyConfigured) {
        await configureAmplify();
      }

      S3GetUrlOptions options = S3GetUrlOptions(
          accessLevel: StorageAccessLevel.guest, expires: 90000);
      GetUrlResult result =
          await Amplify.Storage.getUrl(key: key, options: options);
      print('uploaded image url: ${result.url}');
      return result.url;
    } catch (e) {
      print('GetUrl Err: ' + e.toString());
      return e.toString();
    }
  }

  static Future<void> configureAmplify() async {
    AmplifyStorageS3 storage = new AmplifyStorageS3();
    AmplifyAuthCognito auth = new AmplifyAuthCognito();
    await Amplify.addPlugins([auth, storage]);

    try {
      // Configure
      await Amplify.configure(amplifyconfig);
      _isAmplifyConfigured = true;
    } on AmplifyAlreadyConfiguredException {
      print(
          'Amplify was already configured. Looks like app restarted on android.');
    }
  }

  /*Future<String?> uploadFile() async {
    _simpleS3 = SimpleS3();
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();

    String random = String.fromCharCodes(
      Iterable.generate(
        50,
        (_) => _chars.codeUnitAt(
          _rnd.nextInt(_chars.length),
        ),
      ),
    );

    String extension = file.path.substring(file.path.lastIndexOf("."));
    String fileName =
        //"$random\_${DateTime.now().millisecondsSinceEpoch}"
        "filename$extension";

    String? result;
    try {
      result = await _simpleS3!.uploadFile(
        file,
        Constants.BUCKET_NAME,
        Constants.POOL_ID,
        AWSRegions.apSouth1,
//        debugLog: true,
        s3FolderPath: Constants.FOLDER_PATH,
        accessControl: S3AccessControl.publicRead,
      );
      print('upload result: $result');
    } catch (e) {
      print(e);
    }

    */ /*AwsS3 awsS3 = AwsS3(
      awsFolderPath: Constants.FOLDER_PATH,
      file: file,
      fileNameWithExt: fileName,
      poolId: Constants.POOL_ID,
      region: Constants.REGION,
      bucketName: Constants.BUCKET_NAME,
    );*/ /*

    try {
      if (title != null) displayUploadDialog(_simpleS3!);
      //String result = await _simpleS3.uploadFile;
      if (title != null) Navigator.pop(context);
      //result = "${Constants.UPLOADED_URL_PREFIX}$result";
      result = "$result";
      print(result);
      return result;
    } on PlatformException catch (e) {
      debugPrint("Failed :'${e.message}'");
    }
    return null;
  }*/
}
