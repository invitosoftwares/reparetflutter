import 'dart:ui' show lerpDouble;

import 'package:flutter/material.dart';

class TitledBottomNavigationBar extends StatefulWidget {
  int currentIndex;
  final ValueChanged<int>? onTap;
  final List<TitledNavigationBarItem>? items;
  final Size? screenSize;
  final bool showIndicator;
  final Color? selectedColor;
  final Color? unSelectedColor;

  TitledBottomNavigationBar({
    Key? key,
    @required this.onTap,
    @required this.items,
    this.currentIndex = 0,
    this.screenSize,
    this.selectedColor,
    this.unSelectedColor,
    this.showIndicator = false,
  })  : assert(items != null),
        assert(items!.length >= 2 && items.length <= 5),
        assert(onTap != null),
        super(key: key);

  @override
  State createState() => _TitledBottomNavigationBarState();
}

class _TitledBottomNavigationBarState extends State<TitledBottomNavigationBar> {
  static double BAR_HEIGHT = 60;
  static const double INDICATOR_HEIGHT = 2;

  List<TitledNavigationBarItem>? get items => widget.items;

  double width = 0;
  Duration duration = Duration(milliseconds: 200);

  double? _getIndicatorPosition(int index) {
    return lerpDouble(-1.0, 1.0, index / (items!.length - 1));
  }

  @override
  Widget build(BuildContext context) {
    width = widget.screenSize!.width;
    BAR_HEIGHT = widget.screenSize!.height * 0.075;

    return Material(
      elevation: 10,
      child: Container(
        height: BAR_HEIGHT + MediaQuery.of(context).viewPadding.bottom,
        width: width,
        color: Colors.white,
        child: Stack(
          clipBehavior: Clip.none, children: <Widget>[
            Positioned(
              top: widget.showIndicator ? INDICATOR_HEIGHT : 0,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: items!.map((item) {
                  var index = items!.indexOf(item);
                  return InkWell(
                    onTap: () => _select(index),
                    child: _buildItemWidget(item, index == widget.currentIndex),
                  );
                }).toList(),
              ),
            ),
            if (widget.showIndicator)
              Positioned(
                top: 0,
                width: width,
                child: AnimatedAlign(
                  alignment: Alignment(
                    _getIndicatorPosition(widget.currentIndex) ?? 0.0,
                    0,
                  ),
                  curve: Curves.easeIn,
                  duration: duration,
                  child: Container(
                    color: widget.selectedColor,
                    width: width / items!.length,
                    height: INDICATOR_HEIGHT,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  _select(int index) {
    widget.currentIndex = index;
    if (widget.onTap != null) widget.onTap!(widget.currentIndex);

    setState(() {});
  }

  Widget _buildItemWidget(TitledNavigationBarItem item, bool isSelected) {
    return Container(
      height: BAR_HEIGHT,
      width: width / items!.length,
      color: Colors.white,
      child: Center(child: getIcon(item, width, isSelected)),
    );
  }

  Widget getIcon(TitledNavigationBarItem item, double width, bool isActive) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        item._badgeCount == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 6.0, right: 6.0, left: 6.0),
                child: getIconPrepared(item._image, isActive),
              )
            : Stack(
                alignment: Alignment.center,
                children: [
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 6.0, right: 6.0, left: 6.0),
                    child: getIconPrepared(item._image, isActive),
                  ),
                  Positioned(
                    right: 0,
                    top: 0,
                    child: Container(
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                      ),
                      child: Container(
                        padding: EdgeInsets.all(item._badgeCount < 10 ? 2 : 1),
                        decoration: BoxDecoration(
                          color: isActive
                              ? widget.unSelectedColor
                              : widget.selectedColor,
                          shape: BoxShape.circle,
                        ),
                        child: new Text(
                          (item._badgeCount < 10
                              ? "${item._badgeCount}"
                              : "9+"),
                          style: new TextStyle(
                            color: Colors.white,
                            fontSize: 8,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
        SizedBox(
          height: item._badgeCount == 0 ? 5 : 5,
        ),
        Text(
          item._text,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 10.0,
                color: isActive ? widget.selectedColor : widget.unSelectedColor,
              ),
        ),
      ],
    );
  }

  Widget getIconPrepared(String image, bool isActive) {
    return Image.asset(
      image,
      width: width * 0.05,
      color: isActive ? widget.selectedColor : widget.unSelectedColor,
    );
  }
}

class TitledNavigationBarItem {
  final String _image;
  final String _text;
  final int _badgeCount;

  TitledNavigationBarItem(
    this._image,
    this._text,
    this._badgeCount,
  );
}
