import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class SpinKitLoading extends StatelessWidget {
  final int _role;
  final bool _isTransparent;

  SpinKitLoading(this._role, this._isTransparent);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: _isTransparent ? Colors.transparent : CustomColors.colorBlack.withOpacity(0.15),
      child: Center(
        child: SpinKitThreeBounce(
          color: _role == Constants.USER ? CustomColors.colorAccent : CustomColors.colorPrimary,
          size: 25.0,
        ),
      ),
    );
  }
}
