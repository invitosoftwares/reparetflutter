import 'package:flutter/material.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';

class PasswordTextFormField extends StatefulWidget {
  final FormFieldValidator<String>? validator;
  final String? placeholder;
  final String? prefixIconString;
  final Color? cursorColor;

  const PasswordTextFormField({
    this.validator,
    this.placeholder,
    this.prefixIconString,
    this.cursorColor,
  });

  @override
  _PasswordTextFormFieldState createState() => _PasswordTextFormFieldState();
}

class _PasswordTextFormFieldState extends State<PasswordTextFormField> {
  bool _passwordVisible = false;

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      validator: widget.validator,
      obscureText: !_passwordVisible,
      style: Theme.of(context).textTheme.bodyText1,
      keyboardType: TextInputType.text,
      maxLines: 1,
      textAlign: TextAlign.start,
      cursorColor: widget.cursorColor != null
          ? widget.cursorColor
          : Theme.of(context).primaryColor,
      maxLength: 30,
      inputFormatters: [
        MyLengthLimitingTextInputFormatter(30),
      ],
      decoration: CustomDecorators(context).getInputDecorationPasswordToggle(
        widget.placeholder ?? "",
        () {
          setState(() {
            _passwordVisible = !_passwordVisible;
          });
        },
        _passwordVisible,
        prefixIconString: widget.prefixIconString,
        prefixIconColor: widget.cursorColor,
      ),
    );
  }
}
