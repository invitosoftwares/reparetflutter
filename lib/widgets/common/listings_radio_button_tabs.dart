import 'package:flutter/material.dart';
import '../../models/tab_model.dart';
import '../../widgets/common/radio_button_tab.dart';

class ListingsRadioButtonTabs extends StatefulWidget {
  static const int NEW = 0;
  static const int ONGOING = 1;
  static const int COMPLETED = 2;

  final Function _onValueChanged;
  final Color _selectedColor;

  const ListingsRadioButtonTabs(this._onValueChanged, this._selectedColor, {Key? key}) : super(key: key);

  @override
  _ListingsRadioButtonTabsState createState() =>
      _ListingsRadioButtonTabsState();
}

class _ListingsRadioButtonTabsState extends State<ListingsRadioButtonTabs> {
  List<TabModel> _allModels = [];
  bool _isFirstTime = true;
  final int _selectedIndex = ListingsRadioButtonTabs.NEW;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_isFirstTime) {
      _isFirstTime = false;
      _allModels = [
        TabModel(
          ListingsRadioButtonTabs.NEW,
          _selectedIndex == ListingsRadioButtonTabs.NEW,
          "New",
          "assets/images/icon_new.png",
        ),
        TabModel(
          ListingsRadioButtonTabs.ONGOING,
          _selectedIndex == ListingsRadioButtonTabs.ONGOING,
          "Ongoing",
          "assets/images/icon_refresh.png",
        ),
        TabModel(
          ListingsRadioButtonTabs.COMPLETED,
          _selectedIndex == ListingsRadioButtonTabs.COMPLETED,
          "Completed",
          "assets/images/icon_tick.png",
        ),
      ];
    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ..._allModels.map(
            (e) => InkWell(
              onTap: () {
                if (e.isSelected) return;
                widget._onValueChanged(e.type);
                setState(() {
                  for (var element in _allModels) {
                    element.isSelected = false;
                  }
                  e.isSelected = true;
                });
              },
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 8.0,
                  right: 8.0,
                  bottom: 4,
                ),
                child: RadioButtonTab(e, widget._selectedColor),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
