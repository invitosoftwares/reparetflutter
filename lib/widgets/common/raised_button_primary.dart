import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class PrimaryRaisedButton extends StatelessWidget {
  final String _text;
  final Function _onPressed;
  final Function? onDisablePressed;
  final bool isLoading;
  final bool isDisabled;
  final bool isTextCaps;
  final Size? size;
  final Color? color;
  final String? leadingIcon;

  PrimaryRaisedButton(
    this._text,
    this._onPressed, {
    this.isLoading = false,
    this.isDisabled = false,
    this.isTextCaps = false,
    this.size,
    this.color,
    this.leadingIcon,
    this.onDisablePressed,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        textStyle: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: CustomColors.colorWhite,
            ),
        primary: (isDisabled || isLoading)
            ? CustomColors.colorGrayDark3
            : (color ?? CustomColors.colorPrimary),
        padding: EdgeInsets.symmetric(
          vertical: size == null ? 8 : size!.height * 0.01,
          horizontal: size == null ? 20 : size!.width * 0.05,
        ),
      ),
      onPressed: () {
        if (!(isDisabled || isLoading)) _onPressed();
        if (isDisabled && !isLoading && onDisablePressed != null) {
          onDisablePressed!();
        }
      },
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (isLoading)
            Container(
              child: const CircularProgressIndicator(
                backgroundColor: Colors.transparent,
                valueColor:
                    AlwaysStoppedAnimation<Color>(CustomColors.colorWhite),
                strokeWidth: 2,
              ),
              margin: const EdgeInsets.only(
                right: 10,
              ),
              width: 20,
              height: 20,
            ),
          if (leadingIcon != null)
            Padding(
              padding: const EdgeInsets.only(right: 6.0),
              child: Image.asset(
                leadingIcon!,
                height: 12,
                width: 12,
                color: CustomColors.colorWhite,
              ),
            ),
          Text(
            isTextCaps ? _text.toUpperCase() : _text,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: CustomColors.colorWhite,
                ),
          ),
        ],
      ),
    );
  }
}
