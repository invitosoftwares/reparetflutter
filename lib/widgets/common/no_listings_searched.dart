import 'package:flutter/material.dart';

class NoListingsSearched extends StatelessWidget {
  final String _icon;
  final String? text;

  const NoListingsSearched(
    this._icon, {
    this.text,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50.0),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              AspectRatio(
                aspectRatio: 1,
                child: Image.asset(
                  _icon,
                  width: double.infinity,
                ),
              ),
              const SizedBox(height: 20),
              Text(
                text ?? "Sorry, no projects found related to your search",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      fontWeight: FontWeight.w600,
                    ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
