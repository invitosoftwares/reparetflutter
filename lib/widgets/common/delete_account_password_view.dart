import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../providers/auth_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/password_text_form_field.dart';
import '../../widgets/common/raised_button_primary.dart';

class DeleteAccountPasswordView extends StatefulWidget {
  final int role;

  const DeleteAccountPasswordView(
    this.role,
  );

  @override
  _DeleteAccountPasswordViewState createState() =>
      _DeleteAccountPasswordViewState();
}

class _DeleteAccountPasswordViewState extends State<DeleteAccountPasswordView> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "CURRENT_PASSWORD": null,
  };
  bool _isLoading = false;
  Size? _size;

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return Stack(
      alignment: Alignment.topCenter,
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(
            _size!.width * 0.05,
            30,
            _size!.width * 0.05,
            0,
          ),
          decoration: const BoxDecoration(
            color: CustomColors.colorGrayLight2,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.all(_size!.width * 0.05),
            child: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: _size!.width * 0.05),
                    Text(
                      "Confirm your password",
                      style: Theme.of(context).textTheme.headline2!.copyWith(
                            color: widget.role == Constants.USER
                                ? CustomColors.colorAccent
                                : CustomColors.colorPrimary,
                          ),
                    ),
                    SizedBox(height: _size!.width * 0.05),
                    getElevatedWidget(
                      PasswordTextFormField(
                        validator: (value) {
                          if (value == null || value.trim().isEmpty) {
                            _data["CURRENT_PASSWORD"] = FormFieldData(
                                true, "Please enter current password");
                          } else {
                            _data["CURRENT_PASSWORD"] =
                                FormFieldData(false, value);
                          }
                          return null;
                        },
                        placeholder: "Current password",
                        prefixIconString: "assets/images/icon_key.png",
                        cursorColor: widget.role == Constants.USER
                            ? CustomColors.colorAccent
                            : CustomColors.colorPrimary,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.04),
                    Align(
                      alignment: Alignment.topRight,
                      child: PrimaryRaisedButton(
                        "Delete my account",
                        () {
                          _validateData(context);
                        },
                        size: _size!,
                        color: widget.role == Constants.USER
                            ? CustomColors.colorAccent
                            : CustomColors.colorPrimary,
                        isLoading: _isLoading,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.04),
                  ],
                ),
              ),
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.all(3),
          decoration: const BoxDecoration(
            color: CustomColors.colorGrayLight2,
            shape: BoxShape.circle,
          ),
          child: Container(
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
              color: widget.role == Constants.USER
                  ? CustomColors.colorAccent
                  : CustomColors.colorPrimary,
              shape: BoxShape.circle,
            ),
            child: const Icon(
              Icons.delete,
              size: 20,
              color: CustomColors.colorWhite,
            ),
          ),
        ),
      ],
    );
  }

  Widget getElevatedWidget(Widget widget) {
    return Material(
      elevation: 1,
      borderRadius: const BorderRadius.all(Radius.circular(5)),
      child: widget,
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);
    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    setState(() {
      _isLoading = true;
    });

    final _authProvider = Provider.of<AuthProvider>(context, listen: false);

    try {
      dynamic response = await _authProvider.deleteAccountAPI(
        context,
        _data["CURRENT_PASSWORD"]?.data ?? "",
      );

      setState(() {
        _isLoading = false;
      });

      if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (e) {
      print(e);
    }

  }
}
