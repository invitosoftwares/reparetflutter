import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class ShowSuccessDialog extends StatefulWidget {
  final String _text;
  final VoidCallback? _onClicked;
  final Color color;

  const ShowSuccessDialog(
    this._text,
    this._onClicked, {
    Key? key,
    this.color = CustomColors.colorAccent,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => ShowSuccessDialogState();
}

class ShowSuccessDialogState extends State<ShowSuccessDialog>
    with SingleTickerProviderStateMixin {
  AnimationController? controller;
  Animation<double>? scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller!, curve: Curves.elasticInOut);

    controller!.addListener(() {
      setState(() {});
    });

    controller!.forward();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return widget._onClicked == null;
      },
      child: Center(
        child: Material(
          color: Colors.transparent,
          child: ScaleTransition(
            scale: scaleAnimation!,
            child: Stack(
              alignment: Alignment.topCenter,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 30.5),
                  child: Material(
                    color: CustomColors.colorGrayLight2,
                    borderRadius: BorderRadius.circular(5),
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 10),
                      child: IntrinsicWidth(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            SizedBox(height: 25),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              child: Text(
                                widget._text,
                                style: TextStyle(
                                  color: CustomColors.colorGrayDark2,
                                  fontSize: 16.0,
                                ),
                              ),
                            ),
                            SizedBox(height: 5),
                            Container(
                              width: double.infinity,
                              child: Align(
                                alignment: Alignment.topRight,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                    if (widget._onClicked != null)
                                      widget._onClicked!();
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 8.0, left: 8.0),
                                    child: Text(
                                      "OK",
                                      style: TextStyle(
                                        color: widget.color,
                                        fontSize: 16.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(3),
                  decoration: BoxDecoration(
                    color: CustomColors.colorGrayLight2,
                    shape: BoxShape.circle,
                  ),
                  child: Container(
                    padding: EdgeInsets.all(15),
                    decoration: BoxDecoration(
                      color: widget.color,
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset(
                      "assets/images/icon_tick.png",
                      width: 25,
                      height: 25,
                      color: CustomColors.colorWhite,
                      fit: BoxFit.fill,
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
