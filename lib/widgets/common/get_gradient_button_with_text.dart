import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class GradientButtonWithText extends StatelessWidget {
  final String _text;
  final Function _onClick;

  GradientButtonWithText(this._text, this._onClick);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: CustomColors.colorAccent,
      borderRadius: BorderRadius.circular(5),
      elevation: 1,
      child: InkWell(
        onTap: () {
          _onClick();
        },
        child: Ink(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                CustomColors.colorAccent,
                CustomColors.colorRed,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 5),
            child: Text(
              "$_text",
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    fontSize: 14.0,
                  ),
            ),
          ),
        ),
      ),
    );
  }
}
