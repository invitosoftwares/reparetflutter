import 'package:flutter/material.dart';
import '../../widgets/common/shimmer_box.dart';

class ProjectListShimmer extends StatelessWidget {
  const ProjectListShimmer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.only(top: 4.0),
      itemBuilder: (context, index) => _getView(context),
      itemCount: 10,
    );
  }

  _getView(context) {
    Size _size = MediaQuery.of(context).size;
    return Card(
      elevation: 1,
      child: Padding(
        padding: const EdgeInsets.all(4.0),
        child: IntrinsicHeight(
          child: Row(
            children: [
              GetShimmerBox(_size.width * 0.2, _size.width * 0.225),
              const SizedBox(width: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const GetShimmerBox(double.infinity, 18),
                      const SizedBox(height: 5),
                      GetShimmerBox(_size.width * 0.2, 12),
                      const SizedBox(height: 5),
                      GetShimmerBox(_size.width * 0.4, 12),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
