import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/chat_provider.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/chat_people_listing_content.dart';
import '../../widgets/common/no_chats_found_view.dart';

class ChatListingView extends StatefulWidget {
  final bool _isUser;

  const ChatListingView(this._isUser, {Key? key}) : super(key: key);

  @override
  _ChatListingViewState createState() => _ChatListingViewState();
}

class _ChatListingViewState extends State<ChatListingView> {
  Size? _size;
  ChatProvider? chatProvider;

  @override
  void initState() {
    chatProvider = Provider.of<ChatProvider>(context, listen: false);
    chatProvider!.turnOnChatPeopleListeners();
    super.initState();
  }

  @override
  void dispose() {
    if (chatProvider != null) {
      chatProvider!.turnOffChatPeopleListeners();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    return Consumer<ChatProvider>(
      builder: (context, value, child) => value.chatPeople.isEmpty
          ? NoChatsFoundView(
              widget._isUser
                  ? CustomColors.colorAccent
                  : CustomColors.colorPrimary,
            )
          : ChatPeopleListingContent(
              widget._isUser,
              _size!,
            ),
    );
  }
}
