import 'package:flutter/material.dart';
import '../../models/chat_message_model.dart';
import '../../screens/common/main_chat_screen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class ItemChatMessageSelf extends StatelessWidget {
  final ChatMessageModel _chatModel;
  final bool _isUser;

  const ItemChatMessageSelf(this._chatModel, this._isUser, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(
        end: 0,
        start: 60,
        top: 3,
        bottom: 3,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Flexible(
            child: Container(
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: _isUser
                    ? CustomColors.colorAccent.withOpacity(0.18)
                    : CustomColors.colorPrimary.withOpacity(0.22),
              ),
              child: IntrinsicWidth(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      _chatModel.message ?? "",
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontSize: 15.0,
                          ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        Common.getConvertedDate(
                          _chatModel.createdAt ?? "",
                          Constants.DATE_FORMAT_4,
                          Constants.DATE_FORMAT_5,
                          isUtc: true,
                        ),
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontSize: 10.0,
                            ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
