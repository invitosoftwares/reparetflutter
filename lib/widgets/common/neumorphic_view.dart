import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class NeumorphicContainer extends StatefulWidget {
  final Widget? child;
  final double bevel;
  final Offset blurOffset;
  final Color? color;
  final double radius;
  final VoidCallback? onPressed;

  NeumorphicContainer({
    Key? key,
    this.child,
    this.bevel = 3.0,
    this.color,
    this.radius = 5.0,
    this.onPressed,
  })  : this.blurOffset = Offset(bevel / 2, bevel / 2),
        super(key: key);

  @override
  _NeumorphicContainerState createState() => _NeumorphicContainerState();
}

class _NeumorphicContainerState extends State<NeumorphicContainer> {
  bool _isPressed = false;

  void _onPointerDown(PointerDownEvent event) {
    if (widget.onPressed == null) return;
    setState(() {
      _isPressed = true;
    });
  }

  void _onPointerUp(PointerUpEvent event) async {
    if (widget.onPressed == null) return;
    setState(() {
      _isPressed = false;
    });
    await Future.delayed(Duration(milliseconds: 150));
    widget.onPressed!();
  }

  @override
  Widget build(BuildContext context) {
    final color = this.widget.color ?? CustomColors.colorGrayLight2;

    return Listener(
      onPointerDown: _onPointerDown,
      onPointerUp: _onPointerUp,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 150),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(widget.radius),
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              _isPressed ? color : color.mix(color, .1),
              _isPressed ? color.mix(Colors.black, .05) : color,
              _isPressed ? color.mix(Colors.black, .05) : color,
              color.mix(Colors.white, _isPressed ? .2 : .5),
            ],
            stops: [
              0.0,
              .3,
              .6,
              1.0,
            ],
          ),
          boxShadow: _isPressed
              ? null
              : [
                  BoxShadow(
                    blurRadius: widget.bevel,
                    offset: -widget.blurOffset,
                    color: color.mix(Colors.white, .1),
                  ),
                  BoxShadow(
                    blurRadius: widget.bevel,
                    offset: widget.blurOffset,
                    color: color.mix(Colors.black, .3),
                  )
                ],
        ),
        child: widget.child,
      ),
    );
  }
}

extension ColorUtils on Color {
  Color mix(Color another, double amount) {
    return Color.lerp(this, another, amount) ?? another;
  }
}
