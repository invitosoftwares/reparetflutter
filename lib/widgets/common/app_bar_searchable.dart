import 'dart:async';

import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/common/icon_with_badge.dart';

class DashboardAppBarWithSearch extends StatefulWidget
    implements PreferredSizeWidget {
  final Function? searchQuery;
  final int notificationCount;
  final VoidCallback? notificationClick;
  final VoidCallback? sideMenuClick;
  final String title;
  final Color mainColor;
  final Color accentColor;
  final bool isLoading;
  final StreamController? clearQuery;

  DashboardAppBarWithSearch({
    this.searchQuery,
    this.notificationCount = 0,
    this.notificationClick,
    this.sideMenuClick,
    this.title = "",
    this.mainColor = CustomColors.colorAccent,
    this.accentColor = CustomColors.colorPrimary,
    this.isLoading = false,
    this.clearQuery,
  });

  @override
  _DashboardAppBarWithSearchState createState() =>
      _DashboardAppBarWithSearchState();

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

class _DashboardAppBarWithSearchState extends State<DashboardAppBarWithSearch> {
  bool _isSearching = false;
  final TextEditingController _textEditingController = TextEditingController();

  @override
  void dispose() {
    super.dispose();
    _textEditingController.dispose();
    if (widget.clearQuery != null) {
      widget.clearQuery!.stream.listen((event) {}).cancel();
    }
  }

  @override
  initState() {
    super.initState();
    if (widget.clearQuery != null) {
      widget.clearQuery!.stream.listen((event) {
        if (event != null && (event is String) && (event == "1")) {
          if (_textEditingController.text.isNotEmpty) {
            _textEditingController.text = "";
          }
          if (_isSearching) {
            setState(() {
              _isSearching = false;
            });
          }
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      brightness: Brightness.dark,
      backgroundColor: widget.mainColor,
      title: _isSearching
          ? TextField(
              onChanged: (value) {
                if (widget.searchQuery != null) {
                  widget.searchQuery!(value);
                }
              },
              controller: _textEditingController,
              cursorColor: widget.mainColor,
              style: Theme.of(context).textTheme.bodyText1,
              decoration: CustomDecorators(context).getInputDecoration(
                "Search...",
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              ),
              autofocus: true,
              maxLines: 1,
              maxLength: 20,
              inputFormatters: [
                MyLengthLimitingTextInputFormatter(20),
              ],
            )
          : Text(
              widget.title,
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
            ),
      actions: [
        if (widget.searchQuery != null)
          IconButton(
            icon: !_isSearching
                ? Image.asset(
                    "assets/images/icon_search.png",
                    height: 20,
                    width: 20,
                  )
                : const Icon(Icons.close),
            onPressed: () {
              if (!widget.isLoading) {
                setState(
                  () {
                    _isSearching = !_isSearching;
                    if (!_isSearching) {
                      widget.searchQuery!("");
                    }
                  },
                );
              }
            },
          ),
        if (!_isSearching && widget.notificationClick != null)
          IconWithBadge(
            widget.notificationCount,
            IconButton(
              icon: Image.asset(
                "assets/images/icon_notification.png",
                height: 20,
                width: 20,
              ),
              onPressed: () {
                if (!widget.isLoading && widget.notificationClick != null) {
                  widget.notificationClick!();
                }
              },
            ),
            widget.accentColor,
          ),
        if (!_isSearching)
          IconButton(
            icon: Image.asset(
              "assets/images/icon_side_menu.png",
              height: 25,
              width: 25,
            ),
            onPressed: () {
              if (!widget.isLoading && widget.sideMenuClick != null) {
                widget.sideMenuClick!();
              }
            },
          )
      ],
    );
  }
}
