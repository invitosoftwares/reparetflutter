import 'package:flutter/material.dart';
import '../../models/tab_model.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/badge_count_text.dart';
import '../../widgets/common/triangle.dart';

class RadioButtonTab extends StatelessWidget {
  final TabModel _tabModel;
  final Color _selectedColor;
  final bool showNotch;
  final Color? badgeCountColor;

  RadioButtonTab(
    this._tabModel,
    this._selectedColor, {
    this.showNotch = false,
    this.badgeCountColor,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 4.0, left: 4.0),
              child: Material(
                color: _tabModel.isSelected
                    ? _selectedColor
                    : CustomColors.colorWhite,
                elevation: _tabModel.isSelected ? 4 : 0,
                borderRadius: BorderRadius.circular(6),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 4, horizontal: 12),
                  child: Row(
                    children: [
                      Image.asset(
                        _tabModel.icon,
                        width: 15,
                        height: 15,
                        color: _tabModel.isSelected
                            ? CustomColors.colorWhite
                            : _selectedColor,
                      ),
                      SizedBox(width: 5),
                      Text(
                        _tabModel.text,
                        style: TextStyle(
                          fontSize: 15.0,
                          color: _tabModel.isSelected
                              ? CustomColors.colorWhite
                              : CustomColors.colorGrayDark2,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            if (_tabModel.badgeCount != 0 && !_tabModel.isSelected)
              getBadgeCount()
          ],
        ),
        if (showNotch && _tabModel.isSelected)
          ClipPath(
            clipper: TriangleClipper(),
            child: Container(
              color: _selectedColor,
              height: 7,
              width: 14,
            ),
          )
      ],
    );
  }

  Widget getBadgeCount() {
    return BadgeCountText(
      count: _tabModel.badgeCount,
      badgeCountColor: badgeCountColor,
      outLineColor: CustomColors.colorGrayLight2,
    );
  }
}
