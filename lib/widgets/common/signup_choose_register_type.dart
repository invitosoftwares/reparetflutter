import 'dart:ui';

import 'package:flutter/material.dart';

class SignUpChooseRegisterType extends StatelessWidget {
  final Function _onClick;
  final Size _screenSize;

  static final int USER = 0;
  static final int CONTRACTOR = 1;

  SignUpChooseRegisterType(this._screenSize, this._onClick);

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 5,
        sigmaY: 5,
      ),
      child: Padding(
        padding: EdgeInsets.all(_screenSize.width * 0.1),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              getWidget(USER, "Property\nOwner", "assets/images/signup_user.png", context),
              SizedBox(height: _screenSize.width * 0.1),
              getWidget(CONTRACTOR, "Service Provider/\nContractor",
                  "assets/images/signup_contractor.png", context),
            ],
          ),
        ),
      ),
    );
  }

  Widget getWidget(int type, String text, String image, BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.pop(context);
          _onClick(type);
        },
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image.asset(
              image,
            ),
            Positioned(
              bottom: 20,
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline1,
              ),
            )
          ],
        ),
      ),
    );
  }
}
