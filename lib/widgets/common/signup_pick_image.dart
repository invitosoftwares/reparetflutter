import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../utils/custom_colors.dart';

class SignUpPickImage extends StatefulWidget {
  final Size _screenSize;
  final Color _color;
  final Function _onClicked;
  final File? file;
  String? url;

  SignUpPickImage(
    this._screenSize,
    this._color,
    this._onClicked, {
    this.file,
    this.url,
  });

  @override
  _SignUpPickImageState createState() => _SignUpPickImageState();
}

class _SignUpPickImageState extends State<SignUpPickImage> {
  bool isFirst = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    // if (isFirst) {
    //   isFirst = false;
    //   if (widget.url != null) {
    //     String temp = await UploadFileS3.getImageUrl(widget.url);
    //     widget.url = temp;
    //     setState(() {});
    //   }
    // }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => widget._onClicked(),
      borderRadius: BorderRadius.circular(100),
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Material(
            elevation: 2,
            shape: const CircleBorder(),
            child: (widget.file == null &&
                    (widget.url == null || widget.url!.isEmpty))
                ? Container(
                    width: widget._screenSize.width * 0.35,
                    decoration: const BoxDecoration(
                      color: CustomColors.colorWhite,
                      shape: BoxShape.circle,
                    ),
                    child: Image.asset(
                      "assets/images/icon_user.png",
                      color: widget._color,
                    ),
                  )
                : widget.file != null
                    ? ClipRRect(
                        borderRadius: BorderRadius.circular(
                          widget._screenSize.width * 0.175,
                        ),
                        child: Image.file(
                          widget.file!,
                          width: widget._screenSize.width * 0.35,
                          height: widget._screenSize.width * 0.35,
                          fit: BoxFit.cover,
                        ),
                      )
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(
                          widget._screenSize.width * 0.175,
                        ),
                        child: CachedNetworkImage(
                          width: widget._screenSize.width * 0.35,
                          height: widget._screenSize.width * 0.35,
                          fit: BoxFit.cover,
                          imageUrl: widget.url!,
                          errorWidget: (context, url, error) => Container(
                            padding: const EdgeInsets.all(4),
                            color: CustomColors.colorAccent.withOpacity(0.1),
                            width: widget._screenSize.width * 0.35,
                            height: widget._screenSize.width * 0.35,
                            child: const Icon(
                              Icons.error,
                            ),
                          ),
                        ),
                      ),
          ),
          Container(
            margin: EdgeInsets.only(
              bottom: widget._screenSize.width * 0.015,
              right: widget._screenSize.width * 0.015,
            ),
            padding: const EdgeInsets.all(2),
            decoration: const BoxDecoration(
              color: CustomColors.colorGrayLight2,
              shape: BoxShape.circle,
            ),
            child: Material(
              elevation: 2,
              shape: const CircleBorder(),
              child: Container(
                width: widget._screenSize.width * 0.065,
                decoration: const BoxDecoration(
                  color: CustomColors.colorWhite,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  Icons.add,
                  color: widget._color,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
