import 'package:flutter/material.dart';
import '../../models/chat_message_model.dart';
import '../../screens/common/main_chat_screen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class ItemChatMessageOthers extends StatelessWidget {
  final bool _isUser;
  final ChatMessageModel _chatModel;

  ItemChatMessageOthers(this._chatModel, this._isUser);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsetsDirectional.only(
        end: 60,
        start: 0,
        top: 3,
        bottom: 3,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Container(
              padding: const EdgeInsets.all(4),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: _isUser
                    ? CustomColors.colorPrimary.withOpacity(0.22)
                    : CustomColors.colorAccent.withOpacity(0.18),
              ),
              child: IntrinsicWidth(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _chatModel.message ?? "",
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            fontSize: 15.0,
                          ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        Common.getConvertedDate(
                          _chatModel.createdAt ?? "",
                          Constants.DATE_FORMAT_4,
                          Constants.DATE_FORMAT_5,
                          isUtc: true,
                        ),
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontSize: 10.0,
                            ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
