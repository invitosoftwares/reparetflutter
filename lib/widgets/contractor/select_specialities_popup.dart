import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/get_categories_parent_model.dart';
import '../../providers/categories_provider.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/raised_button_primary.dart';

class SelectSpecialitiesPopup extends StatefulWidget {
  final Function _onSelected;
  final List<CategoryModel> _preSelected;

  SelectSpecialitiesPopup(this._onSelected, this._preSelected);

  @override
  _SelectSpecialitiesPopupState createState() =>
      _SelectSpecialitiesPopupState();
}

class _SelectSpecialitiesPopupState extends State<SelectSpecialitiesPopup> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Center(
      child: SizedBox(
        width: size.width * 0.75,
        child: Material(
          elevation: 5,
          borderRadius: BorderRadius.circular(5),
          color: CustomColors.colorGrayLight2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: CustomColors.colorPrimary,
                ),
                child: const Text(
                  "Select specialities",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: CustomColors.colorGrayLight3,
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0,
                  ),
                ),
              ),
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: size.height * 0.7,
                ),
                child: Consumer<CategoriesProvider>(
                  builder: (context, categoriesProvider, child) =>
                      ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, index) => buildItem(index, context),
                    itemCount: categoriesProvider.allCategories.length,
                  ),
                ),
              ),
              PrimaryRaisedButton(
                "Done",
                () {
                  widget._onSelected(widget._preSelected);
                  Navigator.pop(context);
                },
                isDisabled: widget._preSelected.isEmpty,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildItem(int index, BuildContext context) {
    return Consumer<CategoriesProvider>(
      builder: (context, categoriesProvider, child) => InkWell(
        onTap: () {
          if (widget._preSelected
              .contains(categoriesProvider.allCategories[index])) {
            widget._preSelected.remove(categoriesProvider.allCategories[index]);
          } else {
            widget._preSelected.add(categoriesProvider.allCategories[index]);
          }
          setState(() {});
        },
        child: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        categoriesProvider.allCategories[index].toString(),
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 15),
                      ),
                    ),
                    const SizedBox(width: 10),
                    Icon(
                      Icons.done,
                      color: widget._preSelected
                              .contains(categoriesProvider.allCategories[index])
                          ? CustomColors.colorPrimary
                          : Colors.transparent,
                    ),
                  ],
                ),
              ),
              if (index != categoriesProvider.allCategories.length)
                Container(
                  width: double.infinity,
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  height: 1,
                  color: CustomColors.colorGrayDark,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
