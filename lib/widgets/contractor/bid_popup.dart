import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/contractor/get_gradient_button_with_bid_icon.dart';

class BidPopup extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "BID_AMOUNT": null,
  };

  final String projectId;
  final Function onBidAdded;

  BidPopup({
    Key? key,
    required this.onBidAdded,
    required this.projectId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: size.width * 0.1),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 30.5),
              child: Material(
                color: CustomColors.colorGrayLight2,
                borderRadius: BorderRadius.circular(5),
                elevation: 5,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 30, horizontal: 50),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const SizedBox(height: 10),
                      const Text(
                        "Bid amount",
                        style: TextStyle(
                          color: CustomColors.colorGrayDark2,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const SizedBox(height: 15),
                      Form(
                        key: _formKey,
                        child: SizedBox(
                          width: size.width * 0.4,
                          child: TextFormField(
                            validator: (value) {
                              if (value == null || value.trim().isEmpty) {
                                _data["BID_AMOUNT"] = FormFieldData(
                                    true, "Please enter some amount");
                              } else if (int.parse(value) == 0) {
                                _data["BID_AMOUNT"] =
                                    FormFieldData(true, "Amount can't be zero");
                              } else {
                                _data["BID_AMOUNT"] =
                                    FormFieldData(false, value);
                              }
                              return null;
                            },
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.0,
                                    ),
                            keyboardType: TextInputType.number,
                            maxLines: 1,
                            textAlign: TextAlign.start,
                            cursorColor: Theme.of(context).primaryColor,
                            maxLength: 6,
                            inputFormatters: [
                              MyLengthLimitingTextInputFormatter(6),
                            ],
                            decoration:
                                CustomDecorators(context).getInputDecoration(
                              "----",
                              contentPadding: const EdgeInsets.all(8),
                              prefixIconString: "\$",
                              isPrefixStringText: true,
                              prefixIconColor: CustomColors.colorPrimary,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 15),
                      GradientButtonWithBidIcon(
                        "Make a bid",
                        () {
                          _validateData(context);
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(3),
              decoration: const BoxDecoration(
                color: CustomColors.colorGrayLight2,
                shape: BoxShape.circle,
              ),
              child: Container(
                padding: const EdgeInsets.all(15),
                decoration: const BoxDecoration(
                  color: CustomColors.colorPrimary,
                  shape: BoxShape.circle,
                ),
                child: Transform.rotate(
                  angle: pi / -10,
                  child: Image.asset(
                    "assets/images/icon_bid.png",
                    width: 25,
                    height: 25,
                    color: CustomColors.colorWhite,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    FormFieldData? formFieldData = _data.values.firstWhere(
      (element) => (element != null && element.isError),
      orElse: () => null,
    );

    if (formFieldData != null) {
      Common.showToast(formFieldData.data);
      return;
    }

    if (!await Common.checkInternetConnection()) {
      Common.showToast(
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    ContractorListingProvider provider = Provider.of<ContractorListingProvider>(
      context,
      listen: false,
    );

    Common.showLoaderDialog("please wait", context);

    try {
      var res = await provider.makeBid(
        context,
        projectId,
        _data["BID_AMOUNT"]!.data,
      );
      Navigator.pop(context);

      if (res is bool && res) {
        onBidAdded(int.parse(_data["BID_AMOUNT"]!.data));
        Navigator.pop(context);
      } else if (res is String) {
        AlertDialogs.showAlertDialogWithOk(context, res);
      }
    } catch (err) {
      print(err);
    }
  }
}
