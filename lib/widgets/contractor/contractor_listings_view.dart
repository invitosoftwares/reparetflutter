import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/auth_provider.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/listings_radio_button_tabs.dart';
import '../../widgets/contractor/contractor_completed_listings_data.dart';
import '../../widgets/contractor/contractor_new_listings_data.dart';
import '../../widgets/contractor/contractor_ongoing_listings_data.dart';
import '../../widgets/contractor/no_speciality_view.dart';
import '../../widgets/contractor/onboard_pending_view.dart';
import '../../widgets/contractor/verification_pending_view.dart';

class ContractorListingsView extends StatefulWidget {
  final Size _screenSize;
  final VoidCallback onInternalTabChanged;
  final StreamController queryStreamController;

  const ContractorListingsView(
    this._screenSize, {
    Key? key,
    required this.onInternalTabChanged,
    required this.queryStreamController,
  }) : super(key: key);

  @override
  _ContractorListingsViewState createState() => _ContractorListingsViewState();
}

class _ContractorListingsViewState extends State<ContractorListingsView> {
  int _currentView = ListingsRadioButtonTabs.NEW;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        widget._screenSize.width * 0.04,
        widget._screenSize.width * 0.04,
        widget._screenSize.width * 0.04,
        0,
      ),
      child: Column(
        children: [
          Consumer<AuthProvider>(builder: (context, value, child) {
            return (value.userModel?.categories?.isEmpty ?? false)
                ? const NoSpecialityView()
                : Container();
          }),
          Consumer<AuthProvider>(
            builder: (context, value, child) =>
                (value.userModel?.onboardingStatus ?? false)
                    ? Container()
                    : const OnBoardPendingView(),
          ),
          Consumer<AuthProvider>(
            builder: (context, value, child) =>
                !(value.userModel?.providerVerified ?? false)
                    ? const VerificationPendingView()
                    : Container(),
          ),
          ListingsRadioButtonTabs(
            (int type) {
              if (_currentView == type) {
                return;
              }
              widget.onInternalTabChanged();
              setState(() {
                _currentView = type;
              });
            },
            CustomColors.colorAccent,
          ),
          const SizedBox(height: 2),
          Expanded(
            child: _currentView == ListingsRadioButtonTabs.NEW
                ? ContractorNewListingsData(
                    widget._screenSize,
                    queryStreamController: widget.queryStreamController,
                  )
                : _currentView == ListingsRadioButtonTabs.ONGOING
                    ? ContractorOnGoingListingsData(
                        widget._screenSize,
                        queryStreamController: widget.queryStreamController,
                      )
                    : ContractorCompletedListingsData(
                        widget._screenSize,
                        queryStreamController: widget.queryStreamController,
                      ),
          )
        ],
      ),
    );
  }
}
