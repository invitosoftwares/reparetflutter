import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../screens/contractor/project_details_for_contractor_screen.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/contractor/contractor_no_listing_view.dart';
import '../../widgets/contractor/item_new_listing_contractor.dart';

class ContractorShowInvitationsData extends StatefulWidget {
  final Size _size;

  const ContractorShowInvitationsData(
    this._size, {
    Key? key,
  }) : super(key: key);

  @override
  State<ContractorShowInvitationsData> createState() =>
      _ContractorShowInvitationsDataState();
}

class _ContractorShowInvitationsDataState
    extends State<ContractorShowInvitationsData> {
  int _loaderType = 0;
  bool _isFirstTime = true;
  ContractorListingProvider? _providerListingProvider;
  int _page = 0;
  bool _isNoInternet = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _providerListingProvider = Provider.of<ContractorListingProvider>(
        context,
        listen: false,
      );

      _loaderType = _providerListingProvider!.allInvitations.isEmpty ? 1 : 0;
      _page = 0;
      _getAllInvitations(
        showLoader: _providerListingProvider!.allInvitations.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllInvitations(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<ContractorListingProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllInvitations(showLoader: false);
                      },
                      child: (provider.allInvitations.isEmpty)
                          ? ContractorNoListingView(
                              retry: () {
                                _page = 0;
                                _getAllInvitations();
                              },
                              text:
                                  "No invitations yet\nPlease check after some time",
                            )
                          : ListView.builder(
                              itemBuilder: (context, index) {
                                if (index ==
                                        provider.allInvitations.length -
                                            min(
                                                3,
                                                provider
                                                    .allInvitations.length) &&
                                    provider.hasMoreInvitations &&
                                    _loaderType == 0) {
                                  _page++;
                                  _getAllInvitations();
                                }
                                if (index == provider.allInvitations.length &&
                                    provider.hasMoreInvitations) {
                                  return Container();
                                }
                                return Padding(
                                  padding: const EdgeInsets.only(top: 4.0),
                                  child: ChangeNotifierProvider.value(
                                    value: provider.allInvitations[index],
                                    child: ItemNewListingContractor(
                                      widget._size,
                                      contractorListingProvider: provider,
                                      click: () {
                                        Navigator.pushNamed(
                                          context,
                                          ProjectDetailsForContractorScreen
                                              .routeName,
                                          arguments: {
                                            "PROJECT_ID":
                                                "${provider.allInvitations[index].projectId}",
                                            "PROVIDER": provider,
                                          },
                                        );
                                      },
                                    ),
                                  ),
                                );
                              },
                              itemCount: provider.allInvitations.length,
                            ),
                    ),
                  ),
        if (_loaderType == 2)
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: SizedBox(
              height: 10,
              width: 10,
              child: CircularProgressIndicator(
                color: CustomColors.colorPrimary,
                strokeWidth: 2,
              ),
            ),
          ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllInvitations({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _providerListingProvider!.getAllInvitations(
        context,
        "$_page",
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }
}
