import 'package:flutter/material.dart';
import '../../screens/contractor/link_bank_account_screen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class VerificationPendingView extends StatelessWidget {
  const VerificationPendingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Common.launchUrl(Constants.verificationUrl);
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        decoration: BoxDecoration(
          color: CustomColors.colorPrimary,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          children: [
            Text(
              "Your background hasn't been verified yet, Click here to get verified as verified contractors gets jobs more easily.",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    color: CustomColors.colorWhite,
                    fontSize: 12,
                  ),
            ),
            const SizedBox(height: 10),
            Text(
              "(Please note: Verification process may take some time so if you have already filled your details, we suggest you to wait for few days or contact us via support.)",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    color: CustomColors.colorWhite,
                    fontSize: 9,
                  ),
            ),
          ],
        ),
      ),
    );
  }
}
