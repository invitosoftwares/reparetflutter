import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/change_password.dart';
import '../../widgets/common/my_outlined_button.dart';
import '../../widgets/common/neumorphic_view.dart';
import '../../widgets/common/show_neumorphic_details.dart';

class ContractorShowProfileView extends StatelessWidget {
  Size? _screenSize;
  final Function _onEditClicked;

  ContractorShowProfileView(this._onEditClicked);

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return SingleChildScrollView(
      padding: EdgeInsets.all(_screenSize!.width * 0.05),
      child: Consumer<AuthProvider>(
        builder: (context, authValue, child) => Column(
          children: [
            _getEditButton(),
            SizedBox(height: _screenSize!.width * 0.05),
            _getUserPicView(authValue.userModel?.image ?? ""),
            SizedBox(height: _screenSize!.width * 0.1),
            ShowNeumorphicDetails(
              "${authValue.userModel?.firstName} ${authValue.userModel?.lastName}",
              icon: "assets/images/icon_name.png",
              color: CustomColors.colorPrimary,
            ),
            SizedBox(height: _screenSize!.width * 0.02),
            ShowNeumorphicDetails(
              authValue.userModel?.companyName ?? "",
              icon: "assets/images/icon_company.png",
              color: CustomColors.colorPrimary,
            ),
            SizedBox(height: _screenSize!.width * 0.02),
            ShowNeumorphicDetails(
              authValue.userModel?.phone ?? "",
              icon: "assets/images/icon_phone.png",
              color: CustomColors.colorPrimary,
            ),
            SizedBox(height: _screenSize!.width * 0.02),
            ShowNeumorphicDetails(
              authValue.userModel?.location ?? "",
              icon: "assets/images/icon_location.png",
              color: CustomColors.colorPrimary,
            ),
            SizedBox(height: _screenSize!.width * 0.02),
            ShowNeumorphicDetails(
              authValue.userModel!.categories!.isEmpty
                  ? "No speciality selected"
                  : authValue.userModel?.categories
                          ?.map((e) => e.name)
                          .join(", ") ??
                      "No speciality selected",
              icon: "assets/images/icon_listings.png",
              color: CustomColors.colorPrimary,
            ),
            SizedBox(height: _screenSize!.width * 0.02),
            ShowNeumorphicDetails(
              authValue.userModel?.bio ?? "N/A",
              icon: "assets/images/icon_info.png",
              color: CustomColors.colorPrimary,
            ),
            SizedBox(height: _screenSize!.width * 0.02),
            _getChangePasswordButton(context),
            SizedBox(height: _screenSize!.width * 0.05),
          ],
        ),
      ),
    );
  }

  _getEditButton() {
    return Align(
      alignment: Alignment.centerRight,
      child: NeumorphicContainer(
        onPressed: () {
          _onEditClicked();
        },
        child: MyOutlinedButton(
          "Edit",
          icon: "assets/images/icon_edit.png",
          color: CustomColors.colorPrimary,
        ),
      ),
    );
  }

  _getChangePasswordButton(BuildContext context) {
    return NeumorphicContainer(
      onPressed: () {
        showModalBottomSheet<void>(
          context: context,
          builder: (context) => Padding(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: ChangePasswordView(Constants.CONTRACTOR),
          ),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
        );
      },
      child: MyOutlinedButton(
        "Change password",
        isExpanded: true,
        color: CustomColors.colorPrimary,
      ),
    );
  }

  _getUserPicView(String image) {
    Widget child = (image.isEmpty)
        ? Container(
            width: _screenSize!.width * 0.35,
            decoration: const BoxDecoration(
              color: CustomColors.colorGrayLight2,
              shape: BoxShape.circle,
            ),
            child: Image.asset(
              "assets/images/icon_user.png",
              color: CustomColors.colorPrimary,
            ),
          )
        : ClipRRect(
            borderRadius: BorderRadius.circular(_screenSize!.width * 0.175),
            child: FadeInImage.assetNetwork(
              placeholder: "assets/images/profile-user.png",
              image: image,
              width: _screenSize!.width * 0.35,
              height: _screenSize!.width * 0.35,
              fit: BoxFit.cover,
            ),
          );
    return NeumorphicContainer(
      child: child,
      radius: _screenSize!.width * 0.175,
      onPressed: () {},
    );
  }
}
