import 'package:flutter/material.dart';
import '../../screens/contractor/link_bank_account_screen.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';

class OnBoardPendingView extends StatelessWidget {
  const OnBoardPendingView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, LinkBankAccountScreen.routeName);
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        decoration: BoxDecoration(
          color: CustomColors.colorBlue2,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Text(
          "You haven't provided your bank details yet, Click here to add your bank details.",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: CustomColors.colorWhite,
                fontSize: 12,
              ),
        ),
      ),
    );
  }
}
