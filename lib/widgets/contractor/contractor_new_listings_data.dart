import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../screens/contractor/project_details_for_contractor_screen.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/no_listings_searched.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/contractor/contractor_no_listing_view.dart';
import '../../widgets/contractor/item_new_listing_contractor.dart';

class ContractorNewListingsData extends StatefulWidget {
  final Size _size;
  final StreamController queryStreamController;

  const ContractorNewListingsData(
    this._size, {
    Key? key,
    required this.queryStreamController,
  }) : super(key: key);

  @override
  State<ContractorNewListingsData> createState() =>
      _ContractorNewListingsDataState();
}

class _ContractorNewListingsDataState extends State<ContractorNewListingsData> {
  int _loaderType = 0;
  bool _isFirstTime = true;
  ContractorListingProvider? _providerListingProvider;
  int _page = 0;
  bool _isNoInternet = false;
  String _searchQuery = "";

  @override
  void dispose() {
    super.dispose();
    widget.queryStreamController.stream.listen((event) {}).cancel();
  }

  @override
  initState() {
    super.initState();
    widget.queryStreamController.stream.listen((event) {
      if (_searchQuery == event) {
        return;
      }
      _page = 0;
      if (event == null || (event is! String) || (event).isEmpty) {
        _searchQuery = "";
      } else {
        _searchQuery = event;
      }
      _getAllNewProjects();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _providerListingProvider = Provider.of<ContractorListingProvider>(
        context,
        listen: false,
      );

      _loaderType = _providerListingProvider!.allNewProjects.isEmpty ? 1 : 0;
      _page = 0;
      _getAllNewProjects(
        showLoader: _providerListingProvider!.allNewProjects.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllNewProjects(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<ContractorListingProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllNewProjects(showLoader: false);
                      },
                      child: (provider.allNewProjects.isEmpty &&
                              _searchQuery.isEmpty)
                          ? ContractorNoListingView(
                              retry: () {
                                _page = 0;
                                _getAllNewProjects();
                              },
                            )
                          : (provider.allNewProjects.isEmpty &&
                                  _searchQuery.isNotEmpty)
                              ? const NoListingsSearched(
                                  "assets/images/empty_search_contractor_listings.png",
                                )
                              : ListView.builder(
                                  itemBuilder: (context, index) {
                                    if (index ==
                                            provider.allNewProjects.length -
                                                min(
                                                    3,
                                                    provider.allNewProjects
                                                        .length) &&
                                        provider.hasMoreNewProjects &&
                                        _loaderType == 0) {
                                      _page++;
                                      _getAllNewProjects();
                                    }
                                    if (index ==
                                            provider.allNewProjects.length &&
                                        provider.hasMoreNewProjects) {
                                      return Container();
                                    }
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: ChangeNotifierProvider.value(
                                        value: provider.allNewProjects[index],
                                        child: ItemNewListingContractor(
                                          widget._size,
                                          contractorListingProvider: provider,
                                          click: () {
                                            Navigator.pushNamed(
                                              context,
                                              ProjectDetailsForContractorScreen
                                                  .routeName,
                                              arguments: {
                                                "PROJECT_ID":
                                                    "${provider.allNewProjects[index].projectId}",
                                                "PROVIDER": provider,
                                              },
                                            );
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: provider.allNewProjects.length,
                                ),
                    ),
                  ),
        if (_loaderType == 2)
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: SizedBox(
              height: 10,
              width: 10,
              child: CircularProgressIndicator(
                color: CustomColors.colorPrimary,
                strokeWidth: 2,
              ),
            ),
          ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllNewProjects({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _providerListingProvider!.getAllNewProjects(
        context,
        "$_page",
        _searchQuery,
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }
}
