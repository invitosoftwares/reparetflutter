import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/bottom_bar_with_indicator.dart';

class ContractorDashboardNavigationBar extends StatefulWidget {
  final Function(int) _onTap;
  final int _currentIndex;
  final Size _screenSize;

  const ContractorDashboardNavigationBar(
      this._onTap, this._currentIndex, this._screenSize,
      {Key? key})
      : super(key: key);

  @override
  _ContractorDashboardNavigationBarState createState() =>
      _ContractorDashboardNavigationBarState();
}

class _ContractorDashboardNavigationBarState
    extends State<ContractorDashboardNavigationBar> {
  @override
  Widget build(BuildContext context) {
    final List<TitledNavigationBarItem> items = [
      TitledNavigationBarItem(
        "assets/images/icon_listings.png",
        "Listings",
        0,
      ),
      TitledNavigationBarItem(
        "assets/images/icon_briefcase.png",
        "Invitations",
        0,
      ),
      TitledNavigationBarItem(
        "assets/images/icon_chat.png",
        "Chat",
        0,
      ),
    ];

    return TitledBottomNavigationBar(
      onTap: widget._onTap,
      items: items,
      currentIndex: widget._currentIndex,
      screenSize: widget._screenSize,
      selectedColor: CustomColors.colorPrimary,
      unSelectedColor: CustomColors.colorGrayDark2,
      showIndicator: true,
    );
  }
}
