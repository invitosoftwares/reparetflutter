import 'package:flutter/material.dart';
import '../../screens/contractor/contractor_self_profile_screen.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';

class NoSpecialityView extends StatelessWidget {
  const NoSpecialityView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, ContractorSelfProfileScreen.routeName);
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        decoration: BoxDecoration(
          color: CustomColors.colorAccent,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Text(
          "You haven't selected any speciality yet, Click here to add your specialities.",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: CustomColors.colorWhite,
                fontSize: 12,
              ),
        ),
      ),
    );
  }
}
