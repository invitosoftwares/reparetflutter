import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../models/get_categories_parent_model.dart';
import '../../models/user_model.dart';
import '../../providers/auth_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/common/bottom_sheet_image_picker.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/show_success_dialog.dart';
import '../../widgets/common/signup_pick_image.dart';
import '../../widgets/common/upload_file_s3.dart';
import '../../widgets/contractor/multi_clickable_spinner_leading_trailing.dart';

class ContractorEditProfileView extends StatefulWidget {
  final Function _editDone;

  ContractorEditProfileView(this._editDone);

  @override
  _ContractorEditProfileViewState createState() =>
      _ContractorEditProfileViewState();
}

class _ContractorEditProfileViewState extends State<ContractorEditProfileView> {
  Size? _screenSize;
  bool _isLoading = false;
  File? _profileImageFile;
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "FIRST_NAME": null,
    "LAST_NAME": null,
    "COMPANY_NAME": null,
    "PHONE": null,
    "LOCATION": null,
    "BIO": null,
  };
  Position? _currentPosition;
  TextEditingController? _currentAddress;
  final List<CategoryModel> _selectedCategories = [];
  bool _isFirstTime = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
    }
  }

  @override
  void initState() {
    super.initState();
    _currentAddress = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    if (_currentAddress != null) _currentAddress!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    var _customDecorators = CustomDecorators(context);

    return Consumer<AuthProvider>(
      builder: (context, authValue, child) {
        if (_currentAddress!.text.isEmpty) {
          _currentAddress!.text = authValue.userModel?.location ?? "";
        }
        if (_selectedCategories.isEmpty) {
          _selectedCategories.addAll(authValue.userModel?.categories ?? []);
        }
        return Form(
          key: _formKey,
          child: SingleChildScrollView(
            padding: EdgeInsets.all(_screenSize!.width * 0.05),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: _screenSize!.width * 0.145),
                SignUpPickImage(
                  _screenSize!,
                  CustomColors.colorPrimary,
                  _showImagePickerModal,
                  url: authValue.userModel?.image ?? "",
                  file: _profileImageFile,
                ),
                SizedBox(height: _screenSize!.width * 0.1),
                getElevatedWidget(
                  _customDecorators,
                  "First Name",
                  "assets/images/icon_name.png",
                  50,
                  CustomColors.colorPrimary,
                  TextInputType.text,
                  (value) {
                    if (value == null || value.trim().isEmpty) {
                      _data["FIRST_NAME"] =
                          FormFieldData(true, "Please enter first name");
                    } else {
                      _data["FIRST_NAME"] = FormFieldData(false, value);
                    }
                    return null;
                  },
                  initialValue: authValue.userModel?.firstName ?? "",
                ),
                getElevatedWidget(
                  _customDecorators,
                  "Last Name",
                  "assets/images/icon_name.png",
                  50,
                  CustomColors.colorPrimary,
                  TextInputType.text,
                  (value) {
                    if (value == null || value.trim().isEmpty) {
                      _data["LAST_NAME"] =
                          FormFieldData(true, "Please enter last name");
                    } else {
                      _data["LAST_NAME"] = FormFieldData(false, value);
                    }
                    return null;
                  },
                  initialValue: authValue.userModel?.lastName ?? "",
                ),
                getElevatedWidget(
                  _customDecorators,
                  "Company name",
                  "assets/images/icon_company.png",
                  50,
                  CustomColors.colorPrimary,
                  TextInputType.text,
                  (value) {
                    if (value == null || value.trim().isEmpty) {
                      _data["COMPANY_NAME"] =
                          FormFieldData(true, "Please enter company name");
                    } else {
                      _data["COMPANY_NAME"] = FormFieldData(false, value);
                    }
                    return null;
                  },
                  initialValue: authValue.userModel?.companyName ?? "",
                ),
                getElevatedWidget(
                  _customDecorators,
                  "Phone number",
                  "assets/images/icon_phone.png",
                  10,
                  CustomColors.colorPrimary,
                  TextInputType.number,
                  (value) {
                    if (value == null || value.trim().isEmpty) {
                      _data["PHONE"] =
                          FormFieldData(true, "Please enter phone");
                    } else if (value.trim().length < 10) {
                      _data["PHONE"] =
                          FormFieldData(true, "Please enter some valid phone");
                    } else {
                      _data["PHONE"] = FormFieldData(false, value);
                    }
                    return null;
                  },
                  initialValue: authValue.userModel?.phone ?? "",
                ),
                IntrinsicHeight(
                  child: Row(
                    children: [
                      Expanded(
                        child: getElevatedWidgetLocation(
                          _customDecorators,
                          "Location",
                          "assets/images/icon_location.png",
                          300,
                          CustomColors.colorPrimary,
                          TextInputType.text,
                          (value) {
                            if (value == null || value.trim().isEmpty) {
                              _data["LOCATION"] = FormFieldData(
                                true,
                                "Please pick your location",
                              );
                            } else {
                              _data["LOCATION"] = FormFieldData(false, value);
                            }
                            return null;
                          },
                          isReadOnly: true,
                          onClick: () async {
                            _currentPosition =
                                await Common.getCurrentLocation();
                            if (_currentPosition != null) {
                              _currentAddress!.text =
                                  (await Common.getAddressFromLatLng(
                                          _currentPosition!)) ??
                                      "";
                            }
                          },
                          controller: _currentAddress!,
                        ),
                      ),
                      InkWell(
                        onTap: () async {
                          _currentPosition = await Common.getCurrentLocation();
                          if (_currentPosition != null) {
                            _currentAddress!.text =
                                (await Common.getAddressFromLatLng(
                                        _currentPosition!)) ??
                                    "";
                          }
                        },
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: Container(
                            height: double.infinity,
                            margin: const EdgeInsets.only(left: 8.0, top: 8.0),
                            child: Material(
                              elevation: 1,
                              color: CustomColors.colorPrimary,
                              borderRadius: BorderRadius.circular(5),
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Image.asset(
                                  "assets/images/pick_location.png",
                                  height: 20,
                                  width: 20,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 8),
                MultiSpinnerWithLeadingTrailing(
                  (List<CategoryModel> values) {
                    _selectedCategories.clear();
                    _selectedCategories.addAll(values);
                  },
                  _selectedCategories,
                ),
                const SizedBox(height: 8),
                Material(
                  elevation: 1,
                  borderRadius: const BorderRadius.all(Radius.circular(5)),
                  child: TextFormField(
                    validator: (value) {
                      _data["BIO"] = FormFieldData(false, value ?? "");
                      return null;
                    },
                    style: Theme.of(context).textTheme.bodyText1,
                    keyboardType: TextInputType.text,
                    maxLines: 4,
                    initialValue: authValue.userModel?.bio ?? "",
                    textAlign: TextAlign.start,
                    cursorColor: Theme.of(context).primaryColor,
                    maxLength: 500,
                    inputFormatters: [
                      MyLengthLimitingTextInputFormatter(500),
                    ],
                    decoration: _customDecorators.getInputDecorationMultiline(
                      "Write about yourself...",
                      maxLines: 4,
                      prefixIconString: "assets/images/icon_info.png",
                      prefixIconColor: CustomColors.colorPrimary,
                    ),
                  ),
                ),
                const SizedBox(height: 8),
                Align(
                  alignment: Alignment.topRight,
                  child: PrimaryRaisedButton(
                    "Save Details",
                    () {
                      _validateData(context);
                    },
                    size: _screenSize,
                    color: CustomColors.colorPrimary,
                    isLoading: _isLoading,
                  ),
                ),
                SizedBox(height: _screenSize!.width * 0.02),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget getElevatedWidgetLocation(
    CustomDecorators _customDecorators,
    String hint,
    String icon,
    int maxLength,
    Color cursorColor,
    TextInputType textInputType,
    Function validator, {
    bool? isReadOnly = false,
    Function? onClick,
    TextEditingController? controller,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 5.0),
      child: Material(
        elevation: 1,
        borderRadius: const BorderRadius.all(Radius.circular(5)),
        child: TextFormField(
          readOnly: isReadOnly ?? false,
          validator: (value) => validator(value),
          style: Theme.of(context).textTheme.bodyText1,
          keyboardType: textInputType,
          maxLines: 1,
          textAlign: TextAlign.start,
          cursorColor: cursorColor,
          maxLength: maxLength,
          controller: controller,
          inputFormatters: [
            MyLengthLimitingTextInputFormatter(maxLength),
            if (textInputType == TextInputType.number)
              FilteringTextInputFormatter.digitsOnly,
          ],
          decoration: _customDecorators.getInputDecoration(
            hint,
            prefixIconString: icon,
            prefixIconColor: cursorColor,
          ),
          onTap: () {
            if ((isReadOnly ?? false) && (onClick != null)) onClick();
          },
        ),
      ),
    );
  }

  Widget getElevatedWidget(
    CustomDecorators _customDecorators,
    String hint,
    String icon,
    int maxLength,
    Color cursorColor,
    TextInputType textInputType,
    Function validator, {
    String initialValue = "",
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Material(
        elevation: 1,
        borderRadius: const BorderRadius.all(Radius.circular(5)),
        child: TextFormField(
          validator: (value) => validator(value),
          style: Theme.of(context).textTheme.bodyText1,
          keyboardType: textInputType,
          maxLines: 1,
          textAlign: TextAlign.start,
          cursorColor: cursorColor,
          maxLength: maxLength,
          initialValue: initialValue,
          inputFormatters: [
            MyLengthLimitingTextInputFormatter(maxLength),
            if (textInputType == TextInputType.number)
              FilteringTextInputFormatter.digitsOnly,
          ],
          decoration: _customDecorators.getInputDecoration(
            hint,
            prefixIconString: icon,
            prefixIconColor: cursorColor,
          ),
        ),
      ),
    );
  }

  void _showImagePickerModal() async {
    File? pickedFile = await showModalBottomSheet<File>(
      context: context,
      builder: (context) => const BottomSheetImagePicker(
        color: CustomColors.colorPrimary,
      ),
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = pickedFile;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);

    if (_selectedCategories.isEmpty) {
      errors.add("Please select at least one category");
    }

    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    setState(() {
      _isLoading = true;
    });
    final _authProvider = Provider.of<AuthProvider>(context, listen: false);

    String? image = _authProvider.userModel?.image ?? "";
    if (_profileImageFile != null) {
      image = await UploadFileS3(
        context,
        _profileImageFile!,
        null,
      ).uploadFile();
    }
    print("file -------------- " + (image ?? ""));

    try {
      dynamic response = await _authProvider.contractorUpdateProfileAPI(
        context,
        _data["FIRST_NAME"]?.data ?? "",
        _data["LAST_NAME"]?.data ?? "",
        _data["PHONE"]?.data ?? "",
        image ?? "",
        _selectedCategories.map((e) => e.id).join(","),
        _data["COMPANY_NAME"]?.data ?? "",
        "${_currentPosition?.latitude ?? _authProvider.userModel?.lat}",
        "${_currentPosition?.longitude ?? _authProvider.userModel?.lng}",
        _data["LOCATION"]?.data ?? "",
        _data["BIO"]?.data ?? "",
      );

      setState(() {
        _isLoading = false;
      });

      if (response is UserModel) {
        FocusManager.instance.primaryFocus?.unfocus();
        widget._editDone();
        showDialog(
          context: context,
          builder: (_) => ShowSuccessDialog(
            "Changes saved successfully",
            null,
            color: CustomColors.colorPrimary,
          ),
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (e) {
      print(e);
    }
  }
}
