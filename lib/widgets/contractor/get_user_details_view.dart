import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../../utils/custom_colors.dart';
import '../common/upload_file_s3.dart';

class GetUserDetailsView extends StatefulWidget {
  String image;
  final String name;
  final dynamic rating;
  final int totalReviews;
  final Size size;

  GetUserDetailsView({
    required this.image,
    required this.name,
    required this.rating,
    required this.totalReviews,
    required this.size,
    Key? key,
  }) : super(key: key);

  @override
  State<GetUserDetailsView> createState() => _GetUserDetailsViewState();
}

class _GetUserDetailsViewState extends State<GetUserDetailsView> {
  bool isFirst = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    // if (isFirst) {
    //   isFirst = false;
    //   String temp = await UploadFileS3.getImageUrl(widget.image);
    //   widget.image = temp;
    //   setState(() {});
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: (widget.image.isEmpty)
              ? Container(
                  color: CustomColors.colorPrimary.withOpacity(0.15),
                  height: widget.size.width * 0.291,
                  width: widget.size.width * 0.256,
                  child: Image.asset(
                    "assets/images/icon_user.png",
                  ),
                )
              : CachedNetworkImage(
                  imageUrl: widget.image,
                  fit: BoxFit.cover,
                  height: widget.size.width * 0.291,
                  width: widget.size.width * 0.256,
                  placeholder: (context, url) => Image.asset(
                    "assets/images/no_pic_image.jpeg",
                    fit: BoxFit.cover,
                  ),
                  errorWidget: (context, url, error) => Container(
                    padding: const EdgeInsets.all(4),
                    color: CustomColors.colorAccent.withOpacity(0.1),
                    height: widget.size.width * 0.291,
                    width: widget.size.width * 0.256,
                    child: const Icon(
                      Icons.error,
                    ),
                  ),
                ),
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getNameRow(context),
              getRatingBar(context),
            ],
          ),
        ),
      ],
    );
  }

  Widget getRatingBar(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        RatingBarIndicator(
          rating: widget.rating.toDouble(),
          itemBuilder: (context, index) => const Icon(
            Icons.star,
            color: CustomColors.colorAccent,
          ),
          itemCount: 5,
          itemSize: 18.0,
          direction: Axis.horizontal,
        ),
        const SizedBox(width: 5),
        Text(
          "(${widget.totalReviews})",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: CustomColors.colorGrayDark3,
                fontSize: 12.0,
              ),
        ),
      ],
    );
  }

  Widget getNameRow(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Text(
            widget.name,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorPrimary,
                  fontSize: 20.0,
                ),
          ),
        ),
        const SizedBox(width: 8),
      ],
    );
  }
}
