import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../screens/contractor/project_details_for_contractor_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/no_listings_searched.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/contractor/item_on_going_listing_contractor.dart';

import 'contractor_no_listing_view.dart';

class ContractorOnGoingListingsData extends StatefulWidget {
  final Size _size;
  final StreamController queryStreamController;

  const ContractorOnGoingListingsData(
    this._size, {
    Key? key,
    required this.queryStreamController,
  }) : super(key: key);

  @override
  State<ContractorOnGoingListingsData> createState() =>
      _ContractorOnGoingListingsDataState();
}

class _ContractorOnGoingListingsDataState
    extends State<ContractorOnGoingListingsData> {
  int _loaderType = 0;
  bool _isFirstTime = true;
  ContractorListingProvider? _providerListingProvider;
  int _page = 0;
  bool _isNoInternet = false;
  String _searchQuery = "";

  @override
  void dispose() {
    super.dispose();
    widget.queryStreamController.stream.listen((event) {}).cancel();
  }

  @override
  initState() {
    super.initState();
    widget.queryStreamController.stream.listen((event) {
      if (_searchQuery == event) {
        return;
      }
      _page = 0;
      if (event == null || (event is! String) || (event).isEmpty) {
        _searchQuery = "";
      } else {
        _searchQuery = event;
      }
      _getAllOngoingProjects();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _providerListingProvider = Provider.of<ContractorListingProvider>(
        context,
        listen: false,
      );

      _loaderType =
          _providerListingProvider!.allOngoingProjects.isEmpty ? 1 : 0;
      _page = 0;
      _getAllOngoingProjects(
        showLoader: _providerListingProvider!.allOngoingProjects.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllOngoingProjects(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<ContractorListingProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllOngoingProjects(showLoader: false);
                      },
                      child: (provider.allOngoingProjects.isEmpty &&
                              _searchQuery.isEmpty)
                          ? ContractorNoListingView(
                              retry: () {
                                _page = 0;
                                _getAllOngoingProjects();
                              },
                            )
                          : (provider.allOngoingProjects.isEmpty &&
                                  _searchQuery.isNotEmpty)
                              ? const NoListingsSearched(
                                  "assets/images/empty_search_contractor_listings.png",
                                )
                              : ListView.builder(
                                  itemBuilder: (context, index) {
                                    if (index ==
                                            provider.allOngoingProjects.length -
                                                min(
                                                    3,
                                                    provider.allOngoingProjects
                                                        .length) &&
                                        provider.hasMoreOngoingProjects &&
                                        _loaderType == 0) {
                                      _page++;
                                      _getAllOngoingProjects();
                                    }
                                    if (index ==
                                            provider
                                                .allOngoingProjects.length &&
                                        provider.hasMoreOngoingProjects) {
                                      return Container();
                                    }
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: ChangeNotifierProvider.value(
                                        value:
                                            provider.allOngoingProjects[index],
                                        child: ItemOnGoingListingContractor(
                                          widget._size,
                                          contractorListingProvider: provider,
                                          click: () {
                                            Navigator.pushNamed(
                                              context,
                                              ProjectDetailsForContractorScreen
                                                  .routeName,
                                              arguments: {
                                                "PROJECT_ID":
                                                    "${provider.allOngoingProjects[index].projectId}",
                                                "PROVIDER": provider,
                                              },
                                            );
                                          },
                                          markAsDoneClick: () {
                                            _markAsDone(
                                              "${provider.allOngoingProjects[index].projectId}",
                                            );
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: provider.allOngoingProjects.length,
                                ),
                    ),
                  ),
        if (_loaderType == 2)
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: SizedBox(
              height: 10,
              width: 10,
              child: CircularProgressIndicator(
                color: CustomColors.colorPrimary,
                strokeWidth: 2,
              ),
            ),
          ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllOngoingProjects({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _providerListingProvider!.getAllOngoingProjects(
        context,
        "$_page",
        _searchQuery,
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }

  Future<dynamic> _markAsDone(String projectId) async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);
    try {
      dynamic response = await _providerListingProvider!.markProjectAsDone(
        context,
        projectId,
      );
      Navigator.pop(context);

      if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (err) {
      print(err);
    }
  }
}
