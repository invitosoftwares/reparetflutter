import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';

class ContractorNoListingView extends StatelessWidget {
  final VoidCallback retry;
  final String? text;

  const ContractorNoListingView({
    Key? key,
    required this.retry,
    this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              "assets/images/no_listing_contractor.png",
              width: _screenSize.width,
            ),
            Text(
              text ?? "No jobs available\nPlease check after some time",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w600,
                  ),
            ),
            const SizedBox(height: 10),
            InkWell(
              onTap: () {
                retry();
              },
              borderRadius: BorderRadius.circular(10),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: CustomColors.colorPrimary,
                    width: 1,
                  ),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 1.0,
                  ),
                  child: Text(
                    "Retry",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: 12.0,
                          fontWeight: FontWeight.w600,
                        ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
