import 'package:flutter/material.dart';
import '../../models/get_categories_parent_model.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/contractor/select_specialities_popup.dart';

class MultiSpinnerWithLeadingTrailing extends StatefulWidget {
  final Function onChanged;
  final List<CategoryModel> selectedValues;

  const MultiSpinnerWithLeadingTrailing(this.onChanged, this.selectedValues,
      {Key? key})
      : super(key: key);

  @override
  _MultiSpinnerWithLeadingTrailingState createState() =>
      _MultiSpinnerWithLeadingTrailingState();
}

class _MultiSpinnerWithLeadingTrailingState
    extends State<MultiSpinnerWithLeadingTrailing> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return SelectSpecialitiesPopup(
              (value) {
                widget.onChanged(value);
                setState(() {
                  widget.selectedValues.clear();
                  widget.selectedValues.addAll(value);
                });
              },
              [...widget.selectedValues],
            );
          },
        );
      },
      child: Material(
        elevation: 1,
        color: CustomColors.colorWhite,
        borderRadius: BorderRadius.circular(5),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 5, bottom: 5, left: 14, right: 14),
          child: IntrinsicHeight(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                Image.asset(
                  "assets/images/icon_listings.png",
                  color: CustomColors.colorPrimary,
                  height: 40,
                  width: 15,
                ),
                const SizedBox(width: 10),
                Expanded(
                  child: Center(
                    child: SizedBox(
                      width: double.infinity,
                      child: Text(
                        (widget.selectedValues.isEmpty)
                            ? "Select your specialities"
                            : widget.selectedValues.join(", "),
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: (widget.selectedValues.isEmpty)
                                  ? CustomColors.colorGrayDark
                                  : CustomColors.colorGrayDark2,
                            ),
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Image.asset(
                  "assets/images/icon_dropdown.png",
                  color: CustomColors.colorPrimary,
                  height: 40,
                  width: 15,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
