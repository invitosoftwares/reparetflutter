import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class ItemCompletedListingContractor extends StatelessWidget {
  final Size _size;
  final VoidCallback click;

  const ItemCompletedListingContractor(
    this._size, {
    Key? key,
    required this.click,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProjectModel>(
      builder: (context, model, child) => InkWell(
        onTap: () {
          click();
        },
        child: Card(
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: CachedNetworkImage(
                    imageUrl: model.projectimages?[0].image ?? "",
                    fit: BoxFit.cover,
                    width: _size.width * 0.2,
                    height: _size.width * 0.225,
                    placeholder: (context, url) => Image.asset(
                      "assets/images/no_pic_image.jpeg",
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, error) => Container(
                      padding: const EdgeInsets.all(4),
                      color: CustomColors.colorAccent.withOpacity(0.1),
                      width: _size.width * 0.2,
                      height: _size.width * 0.225,
                      child: const Icon(
                        Icons.error,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          model.projectTitle ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: CustomColors.colorPrimary,
                                    fontSize: 18.0,
                                  ),
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/icon_date_range.png",
                              height: 10,
                              width: 10,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              Common.getConvertedDate(
                                model.projectTime ?? "",
                                Constants.DATE_FORMAT_4,
                                Constants.DATE_FORMAT_3,
                                isUtc: true,
                              ),
                              style: const TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0, top: 3.0),
                          child: Row(
                            mainAxisAlignment: (model.reviewAdded ?? false)
                                ? MainAxisAlignment.spaceBetween
                                : MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              if (model.projectStatus ==
                                  Constants.PROJECT_COMPLETED)
                                getNotPaidView(),
                              if (model.reviewAdded ?? false)
                                getRatingBar(model.rating ?? 0.0, context),
                              if ((model.projectStatus ==
                                      Constants.PROJECT_PAID || model.projectStatus ==
                                  Constants.PROJECT_PAID_REVIEWED_BY_USER) ||
                                  (model.reviewAdded ?? false))
                                getPaidView(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getNotPaidView() {
    return Image.asset(
      "assets/images/icon_not_paid.png",
      width: 50,
      height: 25,
    );
  }

  Widget getPaidView() {
    return Image.asset(
      "assets/images/icon_paid.png",
      width: 50,
      height: 25,
    );
  }

  Widget getRatingBar(dynamic rating, BuildContext context) {
    return RatingBarIndicator(
      rating: rating.toDouble(),
      itemBuilder: (context, index) => const Icon(
        Icons.star,
        color: CustomColors.colorAccent,
      ),
      itemCount: 5,
      itemSize: 15.0,
      direction: Axis.horizontal,
    );
  }
}
