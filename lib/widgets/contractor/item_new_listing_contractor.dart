import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/contractor/bid_popup.dart';
import '../../widgets/contractor/get_gradient_button_with_bid_icon.dart';

class ItemNewListingContractor extends StatelessWidget {
  final Size _size;
  final VoidCallback click;
  final ContractorListingProvider contractorListingProvider;

  const ItemNewListingContractor(
    this._size, {
    required this.click,
    required this.contractorListingProvider,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<ProjectModel>(
      builder: (context, model, child) => InkWell(
        onTap: () {
          click();
        },
        child: Card(
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: CachedNetworkImage(
                    imageUrl: model.projectimages?[0].image ?? "",
                    fit: BoxFit.cover,
                    width: _size.width * 0.2,
                    height: _size.width * 0.225,
                    placeholder: (context, url) => Image.asset(
                      "assets/images/no_pic_image.jpeg",
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, error) => Container(
                      padding: const EdgeInsets.all(4),
                      color: CustomColors.colorAccent.withOpacity(0.1),
                      width: _size.width * 0.2,
                      height: _size.width * 0.225,
                      child: const Icon(
                        Icons.error,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          model.projectTitle ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: CustomColors.colorPrimary,
                                    fontSize: 18.0,
                                  ),
                        ),
                        Text(
                          model.category ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: CustomColors.colorAccent,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12.0,
                                  ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0, top: 2),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Row(
                                  children: [
                                    Image.asset(
                                      "assets/images/icon_date_range.png",
                                      height: 10,
                                      width: 10,
                                    ),
                                    const SizedBox(width: 4),
                                    Text(
                                      Common.getConvertedDate(
                                        model.projectTime ?? "",
                                        Constants.DATE_FORMAT_4,
                                        Constants.DATE_FORMAT_3,
                                        isUtc: true,
                                      ),
                                      style: const TextStyle(
                                        fontSize: 10,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              (model.bidAmount ?? 0) == 0
                                  ? GradientButtonWithBidIcon(
                                      "BID",
                                      () => showDialog(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return ChangeNotifierProvider.value(
                                            value: contractorListingProvider,
                                            child: BidPopup(
                                              projectId:
                                                  "${model.projectId ?? ""}",
                                              onBidAdded: (int value) {
                                                if (model.bidAmount != value) {
                                                  model.makeBidLocally(value);
                                                }
                                              },
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  : showBid(
                                      model.bidAmount ?? 0,
                                      context,
                                    ),
                              const SizedBox(width: 12),
                              getImageWithBadge(
                                "assets/images/icon_chat.png",
                                CustomColors.colorPrimary,
                                (model.unreadMessages ?? 0),
                                CustomColors.colorAccent,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget showBid(dynamic amount, BuildContext context) {
    return Material(
      color: CustomColors.colorAccent,
      borderRadius: BorderRadius.circular(5),
      elevation: 1,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Transform.rotate(
            angle: pi / -10,
            child: Image.asset(
              "assets/images/icon_bid.png",
              width: 15,
              height: 15,
              color: CustomColors.colorWhite,
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(width: 5),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 2.0, horizontal: 5),
            child: Text(
              "\$$amount",
              style: Theme.of(context).textTheme.headline1!.copyWith(
                    fontSize: 14.0,
                  ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getImageWithBadge(
      String icon, Color imageColor, int count, Color badgeColor) {
    Widget myIcon = Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 8.0),
      child: Image.asset(
        icon,
        height: 18,
        width: 18,
        color: imageColor,
      ),
    );

    return count == 0
        ? myIcon
        : Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: Stack(
              children: [
                myIcon,
                Positioned(
                  right: 0,
                  top: 0,
                  child: Container(
                    padding: EdgeInsets.all(count < 10 ? 4 : 2),
                    decoration: BoxDecoration(
                      color: badgeColor,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      (count < 10 ? "$count" : "9+"),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
