import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../../models/project_model.dart';
import '../../screens/common/images_details_screen.dart';
import '../../screens/common/play_video_screen.dart';
import '../../screens/contractor/user_details_screen.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/my_outlined_button.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class ProjectDetailsBasicDetailsContractorView extends StatefulWidget {
  final Size _size;
  final ProjectModel? projectModel;

  const ProjectDetailsBasicDetailsContractorView(
    this._size, {
    Key? key,
    required this.projectModel,
  }) : super(key: key);

  @override
  State<ProjectDetailsBasicDetailsContractorView> createState() =>
      _ProjectDetailsBasicDetailsContractorViewState();
}

class _ProjectDetailsBasicDetailsContractorViewState
    extends State<ProjectDetailsBasicDetailsContractorView> {
  Uint8List? uint8list;

  @override
  void initState() {
    super.initState();
    getVideoThumbnail();
  }

  void getVideoThumbnail() async {
    if (widget.projectModel!.projectvideos?.isNotEmpty ?? false) {
      uint8list = await VideoThumbnail.thumbnailData(
        video: widget.projectModel!.projectvideos?[0].video ?? "",
        imageFormat: ImageFormat.JPEG,
        maxWidth: 128,
        quality: 25,
      );
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _getTitleText("Location :", context),
        _getValueText(widget.projectModel?.address ?? "", context),
        _getTitleText("Description :", context),
        _getValueText(widget.projectModel?.projectDescription ?? "", context),
        _getTitleText("Attachments :", context),
        _getAttachments(context, widget.projectModel),
        ..._getOwnerDetailsButton(context),
      ],
    );
  }

  Widget _getAttachments(BuildContext context, ProjectModel? projectModel) {
    if ((projectModel?.projectimages?.isEmpty ?? true) &&
        (projectModel?.projectvideos?.isEmpty ?? true)) {
      _getValueText("N/A", context);
    }

    return SizedBox(
      height: widget._size.width * 0.2,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => (index <
                projectModel!.projectimages!.length)
            ? Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                          context, ImagesDetailsScreen.routeName,
                          arguments: {
                            "IMAGE_URL":
                                projectModel.projectimages?[index].image ?? "",
                          });
                    },
                    child: CachedNetworkImage(
                      imageUrl: projectModel.projectimages?[index].image ?? "",
                      fit: BoxFit.cover,
                      height: widget._size.width * 0.2,
                      width: widget._size.width * 0.18,
                      placeholder: (context, url) => Image.asset(
                        "assets/images/no_pic_image.jpeg",
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, error) => Container(
                        padding: const EdgeInsets.all(4),
                        color: CustomColors.colorAccent.withOpacity(0.1),
                        height: widget._size.width * 0.2,
                        width: widget._size.width * 0.18,
                        child: const Icon(
                          Icons.error,
                        ),
                      ),
                    ),
                  ),
                ),
              )
            : Padding(
                padding: const EdgeInsets.only(right: 12.0),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        PlayVideoScreen.routeName,
                        arguments: {
                          "VIDEO_URL":
                              projectModel.projectvideos?[0].video ?? "",
                        },
                      );
                    },
                    child: (uint8list != null && false)
                        ? SizedBox(
                            height: widget._size.width * 0.2,
                            width: widget._size.width * 0.18,
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Image.memory(
                                  uint8list!,
                                  fit: BoxFit.cover,
                                  height: widget._size.width * 0.2,
                                  width: widget._size.width * 0.18,
                                ),
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: CustomColors.colorPrimary,
                                  ),
                                  child: const Icon(
                                    Icons.play_arrow,
                                    color: CustomColors.colorWhite,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Image.asset(
                            "assets/images/default_thumbnail.png",
                            height: widget._size.width * 0.2,
                            width: widget._size.width * 0.18,
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
              ),
        itemCount: projectModel!.projectimages!.length +
            projectModel.projectvideos!.length,
      ),
    );
  }

  Widget _getTitleText(String title, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 2.0),
      child: Text(
        title,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: CustomColors.colorAccent,
            ),
      ),
    );
  }

  Widget _getValueText(String? value, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        value ?? "N/A",
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: CustomColors.colorGrayDark2,
              fontSize: 15.0,
            ),
      ),
    );
  }

  _getOwnerDetailsButton(BuildContext context) {
    return [
      const SizedBox(height: 10),
      Align(
        alignment: Alignment.centerRight,
        child: InkWell(
          onTap: () {
            Navigator.pushNamed(
              context,
              UserDetailsScreen.routeName,
              arguments: {
                "PROJECT_ID": "${widget.projectModel?.projectId ?? ""}",
              },
            );
          },
          child: MyOutlinedButton(
            "See owner details",
            color: CustomColors.colorPrimary,
            textSize: 12.0,
            horizontalPadding: 10,
            verticalPadding: 5,
          ),
        ),
      ),
      const SizedBox(height: 10),
      Container(
        height: 0.5,
        color: CustomColors.colorPrimary,
        width: double.infinity,
      ),
      const SizedBox(height: 10),
    ];
  }
}
