import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/get_gradient_button_with_text.dart';

class ItemOnGoingListingContractor extends StatelessWidget {
  final VoidCallback click;
  final VoidCallback markAsDoneClick;
  final ContractorListingProvider contractorListingProvider;
  final Size _size;

  const ItemOnGoingListingContractor(
    this._size, {
    Key? key,
    required this.click,
    required this.markAsDoneClick,
    required this.contractorListingProvider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProjectModel>(
      builder: (context, model, child) => InkWell(
        onTap: () {
          click();
        },
        child: Card(
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: CachedNetworkImage(
                    imageUrl: model.projectimages?[0].image ?? "",
                    fit: BoxFit.cover,
                    width: _size.width * 0.2,
                    height: _size.width * 0.225,
                    placeholder: (context, url) => Image.asset(
                      "assets/images/no_pic_image.jpeg",
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, error) => Container(
                      padding: const EdgeInsets.all(4),
                      color: CustomColors.colorAccent.withOpacity(0.1),
                      width: _size.width * 0.2,
                      height: _size.width * 0.225,
                      child: const Icon(
                        Icons.error,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          model.projectTitle ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: CustomColors.colorPrimary,
                                    fontSize: 18.0,
                                  ),
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/icon_date_range.png",
                              height: 10,
                              width: 10,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              Common.getConvertedDate(
                                model.projectTime ?? "",
                                Constants.DATE_FORMAT_4,
                                Constants.DATE_FORMAT_3,
                                isUtc: true,
                              ),
                              style: const TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0, top: 2),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Expanded(
                                child: Text(
                                  "\$${model.amount ?? "N/A"}",
                                  style: const TextStyle(
                                    fontSize: 13.0,
                                    color: CustomColors.colorAccent,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              GradientButtonWithText(
                                "Mark as done",
                                () {
                                  _confirmMarkAsDone(context);
                                },
                              ),
                              const SizedBox(width: 12),
                              getImageWithBadge(
                                "assets/images/icon_chat.png",
                                CustomColors.colorPrimary,
                                model.unreadMessages ?? 0,
                                CustomColors.colorAccent,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getImageWithBadge(
      String icon, Color imageColor, int count, Color badgeColor) {
    Widget myIcon = Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 8.0),
      child: Image.asset(
        icon,
        height: 18,
        width: 18,
        color: imageColor,
      ),
    );

    return count == 0
        ? myIcon
        : Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: Stack(
              children: [
                myIcon,
                Positioned(
                  right: 0,
                  top: 0,
                  child: Container(
                    padding: EdgeInsets.all(count < 10 ? 4 : 2),
                    decoration: BoxDecoration(
                      color: badgeColor,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      (count < 10 ? "$count" : "9+"),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          );
  }

  void _confirmMarkAsDone(BuildContext context) async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "No",
      "Yes",
      "Are you sure you want to mark this project as done?",
      () => Navigator.pop(context),
      () {
        Navigator.pop(context);
        markAsDoneClick();
      },
    );
  }
}
