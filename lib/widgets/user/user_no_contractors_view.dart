import 'package:flutter/material.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';

class UserNoContractorsView extends StatelessWidget {
  const UserNoContractorsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Image.asset(
                "assets/images/no_listing.png",
                width: _screenSize.width * 0.85,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
