import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/project_model.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class ItemOnGoingListingUser extends StatelessWidget {
  final Size _size;
  final VoidCallback click;

  const ItemOnGoingListingUser(
    this._size, {
    Key? key,
    required this.click,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProjectModel>(
      builder: (context, model, child) => InkWell(
        onTap: () {
          click();
        },
        child: Card(
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: CachedNetworkImage(
                      imageUrl: model.projectimages?[0].image ?? "",
                      fit: BoxFit.cover,
                      width: _size.width * 0.2,
                      height: _size.width * 0.225,
                      placeholder: (context, url) => Image.asset(
                        "assets/images/no_pic_image.jpeg",
                        fit: BoxFit.cover,
                      ),
                      errorWidget: (context, url, error) => Container(
                        padding: const EdgeInsets.all(4),
                        color: CustomColors.colorAccent.withOpacity(0.1),
                        width: _size.width * 0.2,
                        height: _size.width * 0.225,
                        child: const Icon(
                          Icons.error,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 4.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            model.projectTitle ?? "",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style:
                                Theme.of(context).textTheme.headline1!.copyWith(
                                      color: CustomColors.colorAccent,
                                      fontSize: 18.0,
                                    ),
                          ),
                          const SizedBox(height: 5),
                          Row(
                            children: [
                              Image.asset(
                                "assets/images/icon_date_range.png",
                                height: 10,
                                width: 10,
                              ),
                              const SizedBox(width: 4),
                              Text(
                                Common.getConvertedDate(
                                  model.projectTime ?? "",
                                  Constants.DATE_FORMAT_4,
                                  Constants.DATE_FORMAT_3,
                                  isUtc: true,
                                ),
                                style: const TextStyle(
                                  fontSize: 10,
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(right: 8.0, top: 5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Container(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 1.5, horizontal: 8),
                                  decoration: BoxDecoration(
                                    color: CustomColors.colorPrimary,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(
                                    "\$${model.amount}",
                                    style: const TextStyle(
                                      color: CustomColors.colorWhite,
                                      fontSize: 12,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 12),
                                getImageWithBadge(
                                  "assets/images/icon_chat.png",
                                  CustomColors.colorPrimary,
                                  model.unreadMessages ?? 0,
                                  CustomColors.colorAccent,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getImageWithBadge(
      String icon, Color imageColor, int count, Color badgeColor) {
    Widget myIcon = Padding(
      padding: const EdgeInsets.only(top: 8.0, right: 8.0),
      child: Image.asset(
        icon,
        height: 18,
        width: 18,
        color: imageColor,
      ),
    );

    return count == 0
        ? myIcon
        : Padding(
            padding: const EdgeInsets.only(top: 0.0),
            child: Stack(
              children: [
                myIcon,
                Positioned(
                  right: 0,
                  top: 0,
                  child: Container(
                    padding: EdgeInsets.all(count < 10 ? 4 : 2),
                    decoration: BoxDecoration(
                      color: badgeColor,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      (count < 10 ? "$count" : "9+"),
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          );
  }
}
