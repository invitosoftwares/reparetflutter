import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

import '../../models/contractor_details_model.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../common/upload_file_s3.dart';

class ItemContractorRecentProjects extends StatefulWidget {
  final Size _size;

  const ItemContractorRecentProjects(
    this._size, {
    Key? key,
  }) : super(key: key);

  @override
  State<ItemContractorRecentProjects> createState() =>
      _ItemContractorRecentProjectsState();
}

class _ItemContractorRecentProjectsState
    extends State<ItemContractorRecentProjects> {
  bool isFirst = true;

  // @override
  // void didChangeDependencies() async {
  //   super.didChangeDependencies();
  //   if (isFirst) {
  //     isFirst = false;
  //     RecentProjectModel recentProjectModel =
  //         Provider.of<RecentProjectModel>(context, listen: false);
  //     String temp = await UploadFileS3.getImageUrl(
  //         recentProjectModel.projectimages?[0].image);
  //     recentProjectModel.projectimages?[0].image = temp;
  //     setState(() {});
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Consumer<RecentProjectModel>(
      builder: (context, _model, child) => Card(
        elevation: 1,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: (_model.projectimages == null ||
                        _model.projectimages!.isEmpty ||
                        _model.projectimages![0].image == null ||
                        _model.projectimages![0].image!.isEmpty)
                    ? Image.asset(
                        "assets/images/no_pic_image.jpeg",
                        height: widget._size.width * 0.225,
                        width: widget._size.width * 0.2,
                        fit: BoxFit.cover,
                      )
                    : CachedNetworkImage(
                        imageUrl: _model.projectimages?[0].image ?? "",
                        height: widget._size.width * 0.225,
                        width: widget._size.width * 0.2,
                        fit: BoxFit.cover,
                        placeholder: (context, url) => Image.asset(
                          "assets/images/no_pic_image.jpeg",
                          fit: BoxFit.cover,
                        ),
                        errorWidget: (context, url, error) => Container(
                          padding: const EdgeInsets.all(4),
                          color: CustomColors.colorAccent.withOpacity(0.1),
                          height: widget._size.width * 0.225,
                          width: widget._size.width * 0.2,
                          child: const Icon(
                            Icons.error,
                          ),
                        ),
                      ),
              ),
              const SizedBox(width: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        _model.projectTitle ?? "",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.headline1!.copyWith(
                              color: CustomColors.colorAccent,
                              fontSize: 18.0,
                            ),
                      ),
                      const SizedBox(height: 5),
                      Row(
                        children: [
                          Image.asset(
                            "assets/images/icon_date_range.png",
                            height: 10,
                            width: 10,
                          ),
                          const SizedBox(width: 4),
                          Text(
                            Common.getConvertedDate(
                              _model.projectTime ?? "",
                              Constants.DATE_FORMAT_4,
                              Constants.DATE_FORMAT_3,
                              isUtc: true,
                            ),
                            style: const TextStyle(
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 8.0, top: 3.0),
                        child: Row(
                          mainAxisAlignment: (_model.projectStatus ==
                                  Constants.PROJECT_PAID_REVIEWED_BY_USER)
                              ? MainAxisAlignment.spaceBetween
                              : MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              (_model.projectStatus == Constants.PROJECT_PAID ||
                                      _model.projectStatus ==
                                          Constants
                                              .PROJECT_PAID_REVIEWED_BY_USER)
                                  ? "Paid"
                                  : "Ongoing",
                              style: const TextStyle(
                                color: CustomColors.colorGreen,
                                fontSize: 15.0,
                              ),
                            ),
                            if (_model.projectStatus ==
                                Constants.PROJECT_PAID_REVIEWED_BY_USER)
                              getRatingBar(
                                _model.projectReviews?.rating ?? 0,
                                context,
                              ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getRatingBar(dynamic rating, BuildContext context) {
    return RatingBarIndicator(
      rating: rating.toDouble(),
      itemBuilder: (context, index) => const Icon(
        Icons.star,
        color: CustomColors.colorPrimary,
      ),
      itemCount: 5,
      itemSize: 15.0,
      direction: Axis.horizontal,
    );
  }
}
