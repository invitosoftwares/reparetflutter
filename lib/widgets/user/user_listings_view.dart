import 'dart:async';

import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/listings_radio_button_tabs.dart';
import '../../widgets/user/user_completed_listings_data.dart';
import '../../widgets/user/user_new_listings_data.dart';
import '../../widgets/user/user_ongoing_listings_data.dart';

class UserListingsView extends StatefulWidget {
  final Size _screenSize;
  final VoidCallback newProjectClick;
  final VoidCallback onInternalTabChanged;
  final StreamController queryStreamController;

  const UserListingsView(
    this._screenSize, {
    Key? key,
    required this.newProjectClick,
    required this.onInternalTabChanged,
    required this.queryStreamController,
  }) : super(key: key);

  @override
  _UserListingsViewState createState() => _UserListingsViewState();
}

class _UserListingsViewState extends State<UserListingsView> {
  int _currentView = ListingsRadioButtonTabs.NEW;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
        widget._screenSize.width * 0.04,
        widget._screenSize.width * 0.04,
        widget._screenSize.width * 0.04,
        0,
      ),
      child: Column(
        children: [
          ListingsRadioButtonTabs(
            (int type) {
              if (_currentView == type) {
                return;
              }
              widget.onInternalTabChanged();
              setState(() {
                _currentView = type;
              });
            },
            CustomColors.colorPrimary,
          ),
          const SizedBox(height: 2),
          Expanded(
            child: _currentView == ListingsRadioButtonTabs.NEW
                ? UserNewListingsData(
                    widget._screenSize,
                    newProjectClick: widget.newProjectClick,
                    queryStreamController: widget.queryStreamController,
                  )
                : _currentView == ListingsRadioButtonTabs.ONGOING
                    ? UserOnGoingListingsData(
                        widget._screenSize,
                        newProjectClick: widget.newProjectClick,
                        queryStreamController: widget.queryStreamController,
                      )
                    : UserCompletedListingsData(
                        widget._screenSize,
                        newProjectClick: widget.newProjectClick,
                        queryStreamController: widget.queryStreamController,
                      ),
          )
        ],
      ),
    );
  }
}
