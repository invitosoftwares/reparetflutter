import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../providers/rating_provider.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../widgets/common/get_gradient_button_with_text.dart';

class RateContractorPopup extends StatefulWidget {
  final String projectId;

  const RateContractorPopup({
    Key? key,
    required this.projectId,
  }) : super(key: key);

  @override
  State<RateContractorPopup> createState() => _RateContractorPopupState();
}

class _RateContractorPopupState extends State<RateContractorPopup> {
  final _formKey = GlobalKey<FormState>();

  final Map<String, FormFieldData?> _data = {
    "REVIEW": null,
  };

  double? _rating = 0.0;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Center(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: size.width * 0.05),
        child: Material(
          color: CustomColors.colorGrayLight2,
          borderRadius: BorderRadius.circular(5),
          elevation: 5,
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 10),
                const Text(
                  "Give rating to the contractor",
                  style: TextStyle(
                    color: CustomColors.colorGrayDark2,
                    fontSize: 15.0,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(height: 15),
                RatingBar(
                  onRatingUpdate: (value) {
                    _rating = value;
                  },
                  ratingWidget: RatingWidget(
                    empty: const Icon(
                      Icons.star,
                      color: CustomColors.colorGrayDark,
                    ),
                    full: const Icon(
                      Icons.star,
                      color: CustomColors.colorAccent,
                    ),
                    half: const Icon(
                      Icons.star_half,
                      color: CustomColors.colorAccent,
                    ),
                  ),
                  itemCount: 5,
                  itemSize: 30.0,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  tapOnlyMode: false,
                ),
                const SizedBox(height: 15),
                Form(
                  key: _formKey,
                  child: TextFormField(
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        _data["REVIEW"] =
                            FormFieldData(true, "Please write some review");
                      } else {
                        _data["REVIEW"] = FormFieldData(false, value);
                      }
                      return null;
                    },
                    style: Theme.of(context).textTheme.bodyText1!.copyWith(
                          fontSize: 13.0,
                        ),
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: 4,
                    textAlign: TextAlign.start,
                    cursorColor: Theme.of(context).primaryColor,
                    maxLength: 200,
                    decoration: CustomDecorators(context).getInputDecoration(
                      "Write about your experience with them",
                    ),
                  ),
                ),
                const SizedBox(height: 15),
                GradientButtonWithText(
                  "Submit",
                  () {
                    _validateData(context);
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    if (_rating == null || _rating == 0.0) {
      Common.showToast("Provide some valid rating");
      return;
    }

    FormFieldData? formFieldData = _data.values.firstWhere(
      (element) => (element != null && element.isError),
      orElse: () => null,
    );
    if (formFieldData != null) {
      Common.showToast(formFieldData.data);
      return;
    }

    if (!await Common.checkInternetConnection()) {
      Common.showToast(
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    RatingProvider ratingProvider =
        Provider.of<RatingProvider>(context, listen: false);

    Common.showLoaderDialog("Please wait...", context);

    try {
      dynamic response = await ratingProvider.rateContractorByUser(
        context,
        widget.projectId,
        "$_rating",
        _data["REVIEW"]!.data,
      );

      Navigator.pop(context);

      if ((response is bool) && response) {
        Navigator.pop(context, _rating);
      }
    } catch (err) {
      print(err);
    }
  }
}
