import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../models/user_model.dart';
import '../../providers/auth_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/common/bottom_sheet_image_picker.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/show_success_dialog.dart';
import '../../widgets/common/signup_pick_image.dart';
import '../../widgets/common/upload_file_s3.dart';

class UserEditProfileView extends StatefulWidget {
  final Function _editDone;

  UserEditProfileView(this._editDone);

  @override
  _UserEditProfileViewState createState() => _UserEditProfileViewState();
}

class _UserEditProfileViewState extends State<UserEditProfileView> {
  Size? _screenSize;
  bool _isLoading = false;
  File? _profileImageFile;
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "FIRST_NAME": null,
    "LAST_NAME": null,
    "PHONE": null,
  };

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    var _customDecorators = CustomDecorators(context);

    return Padding(
      padding: EdgeInsets.all(_screenSize!.width * 0.05),
      child: Consumer<AuthProvider>(
        builder: (context, authValue, child) => Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(height: _screenSize!.width * 0.145),
                SignUpPickImage(
                  _screenSize!,
                  CustomColors.colorAccent,
                  _showImagePickerModal,
                  url: authValue.userModel?.image ?? "",
                  file: _profileImageFile,
                ),
                SizedBox(height: _screenSize!.width * 0.1),
                getElevatedWidget(
                  _customDecorators,
                  "First Name",
                  "assets/images/icon_name.png",
                  50,
                  CustomColors.colorAccent,
                  TextInputType.text,
                  (value) {
                    if (value == null || value.trim().isEmpty) {
                      _data["FIRST_NAME"] =
                          FormFieldData(true, "Please enter first name");
                    } else {
                      _data["FIRST_NAME"] = FormFieldData(false, value);
                    }
                    return null;
                  },
                  initialValue: authValue.userModel?.firstName ?? "",
                ),
                getElevatedWidget(
                  _customDecorators,
                  "Last Name",
                  "assets/images/icon_name.png",
                  50,
                  CustomColors.colorAccent,
                  TextInputType.text,
                  (value) {
                    if (value == null || value.trim().isEmpty) {
                      _data["LAST_NAME"] =
                          FormFieldData(true, "Please enter last name");
                    } else {
                      _data["LAST_NAME"] = FormFieldData(false, value);
                    }
                    return null;
                  },
                  initialValue: authValue.userModel?.lastName ?? "",
                ),
                Visibility(
                  visible: false,
                  child: getElevatedWidget(
                    _customDecorators,
                    "Phone number",
                    "assets/images/icon_phone.png",
                    10,
                    CustomColors.colorAccent,
                    TextInputType.number,
                    (value) {
                      if (value == null || value.trim().isEmpty) {
                        _data["PHONE"] =
                            FormFieldData(true, "Please enter phone");
                      } else if (value.trim().length < 10) {
                        _data["PHONE"] = FormFieldData(
                            true, "Please enter some valid phone");
                      } else {
                        _data["PHONE"] = FormFieldData(false, value);
                      }
                      return null;
                    },
                    initialValue: authValue.userModel?.phone ?? "",
                  ),
                ),
                SizedBox(height: _screenSize!.width * 0.05),
                Align(
                  alignment: Alignment.topRight,
                  child: PrimaryRaisedButton(
                    "Save Details",
                    () {
                      _validateData(context);
                    },
                    size: _screenSize,
                    color: CustomColors.colorAccent,
                    isLoading: _isLoading,
                  ),
                ),
                SizedBox(height: _screenSize!.width * 0.02),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getElevatedWidget(
    CustomDecorators _customDecorators,
    String hint,
    String icon,
    int maxLength,
    Color cursorColor,
    TextInputType textInputType,
    Function validator, {
    String? initialValue,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Material(
        elevation: 1,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: TextFormField(
          validator: (value) => validator(value),
          style: Theme.of(context).textTheme.bodyText1,
          keyboardType: textInputType,
          maxLines: 1,
          textAlign: TextAlign.start,
          cursorColor: cursorColor,
          maxLength: maxLength,
          initialValue: initialValue,
          inputFormatters: [
            MyLengthLimitingTextInputFormatter(maxLength),
            if (textInputType == TextInputType.number)
              FilteringTextInputFormatter.digitsOnly,
          ],
          decoration: _customDecorators.getInputDecoration(
            hint,
            prefixIconString: icon,
            prefixIconColor: cursorColor,
          ),
        ),
      ),
    );
  }

  void _showImagePickerModal() async {
    File? pickedFile = await showModalBottomSheet<File>(
      context: context,
      builder: (context) => BottomSheetImagePicker(
        color: CustomColors.colorAccent,
      ),
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = pickedFile;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);
    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    setState(() {
      _isLoading = true;
    });
    final _authProvider = Provider.of<AuthProvider>(context, listen: false);

    String? image = _authProvider.userModel?.image ?? "";
    if (_profileImageFile != null) {
      image = await UploadFileS3(
        context,
        _profileImageFile!,
        null,
      ).uploadFile();
    }
    print("file -------------- " + (image ?? ""));

    try {
      dynamic response = await _authProvider.userUpdateProfileAPI(
        context,
        _data["FIRST_NAME"]?.data ?? "",
        _data["LAST_NAME"]?.data ?? "",
        _data["PHONE"]?.data ?? "",
        image ?? "",
      );

      setState(() {
        _isLoading = false;
      });

      if (response is UserModel) {
        FocusManager.instance.primaryFocus?.unfocus();
        widget._editDone();
        showDialog(
          context: context,
          builder: (_) => ShowSuccessDialog(
            "Changes saved successfully",
            null,
          ),
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (e) {
      print(e);
    }
  }
}
