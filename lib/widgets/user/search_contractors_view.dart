import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/user_search_contractors_provider.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/no_listings_searched.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/user/item_contractor.dart';
import '../../widgets/user/user_no_contractors_view.dart';

class SearchContractorsView extends StatefulWidget {
  const SearchContractorsView({Key? key}) : super(key: key);

  @override
  _SearchContractorsViewState createState() => _SearchContractorsViewState();
}

class _SearchContractorsViewState extends State<SearchContractorsView> {
  Size? _screenSize;
  int _loaderType = 0;
  bool _isFirstTime = true;
  UserSearchContractorsProvider? _userSearchContractorsProvider;
  int _page = 0;
  bool _isNoInternet = false;
  String _searchQuery = "";

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _userSearchContractorsProvider =
          Provider.of<UserSearchContractorsProvider>(
        context,
        listen: false,
      );

      _loaderType =
          _userSearchContractorsProvider!.allContractors.isEmpty ? 1 : 0;
      _page = 0;
      _getAllContractors(
        showLoader: _userSearchContractorsProvider!.allContractors.isEmpty,
      );
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(_screenSize!.width * 0.035),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          _isNoInternet
              ? NoInternetView(
                  () => _getAllContractors(),
                )
              : Column(
                  children: [
                    buildSearchBar(),
                    Expanded(
                      child: (_loaderType == 1)
                          ? const ProjectListShimmer()
                          : Consumer<UserSearchContractorsProvider>(
                              builder: (context, provider, child) =>
                                  RefreshIndicator(
                                color: CustomColors.colorAccent,
                                onRefresh: () async {
                                  if (_loaderType != 0) return;
                                  _page = 0;
                                  await _getAllContractors(showLoader: false);
                                },
                                child: (provider.allContractors.isEmpty &&
                                        _searchQuery.isEmpty)
                                    ? const UserNoContractorsView()
                                    : (provider.allContractors.isEmpty &&
                                            _searchQuery.isNotEmpty)
                                        ? const NoListingsSearched(
                                            "assets/images/empty_search_user_listings.png",
                                            text:
                                                "Sorry, no contractors found related to your search",
                                          )
                                        : ListView.builder(
                                            itemBuilder: (context, index) {
                                              if (index ==
                                                      provider.allContractors
                                                              .length -
                                                          min(
                                                              3,
                                                              provider
                                                                  .allContractors
                                                                  .length) &&
                                                  provider.hasMoreProviders &&
                                                  _loaderType == 0) {
                                                _page++;
                                                _getAllContractors();
                                              }
                                              if (index ==
                                                      provider.allContractors
                                                          .length &&
                                                  provider.hasMoreProviders) {
                                                return Container();
                                              }
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                  top: 4.0,
                                                ),
                                                child: ChangeNotifierProvider.value(
                                                  value: provider
                                                      .allContractors[index],
                                                  child: ItemContractorSearch(
                                                    _screenSize!,
                                                    toggleFavourite: () {
                                                      _toggleFavouriteStatus(
                                                        provider
                                                                .allContractors[
                                                                    index]
                                                                .providerId ??
                                                            0,
                                                      );
                                                    },
                                                  ),
                                                ),
                                              );
                                            },
                                            itemCount:
                                                provider.allContractors.length,
                                          ),
                              ),
                            ),
                    ),
                  ],
                ),
          if (_loaderType == 2)
            const Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: SizedBox(
                height: 10,
                width: 10,
                child: CircularProgressIndicator(
                  color: CustomColors.colorAccent,
                  strokeWidth: 2,
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget buildSearchBar() {
    return IntrinsicHeight(
      child: Row(
        children: [
          Expanded(
            child: TextField(
              onChanged: (value) {
                _updateSearch(value);
              },
              cursorColor: CustomColors.colorAccent,
              style: Theme.of(context).textTheme.bodyText1,
              decoration: CustomDecorators(context).getInputDecoration(
                "Search...",
                contentPadding:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              ),
              maxLines: 1,
              maxLength: 20,
              inputFormatters: [
                MyLengthLimitingTextInputFormatter(20),
              ],
            ),
          ),
          const SizedBox(width: 10),
          getClickableView(
            "assets/images/icon_search.png",
            () {
              FocusScope.of(context).unfocus();
            },
            CustomColors.colorWhite,
            CustomColors.colorAccent,
          ),
        ],
      ),
    );
  }

  Widget getClickableView(
      String icon, Function click, Color iconColor, Color backgroundColor) {
    return InkWell(
      onTap: () {
        click();
      },
      highlightColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: AspectRatio(
        aspectRatio: 1,
        child: Container(
          height: double.infinity,
          color: Colors.transparent,
          child: Material(
            elevation: 1,
            borderRadius: BorderRadius.circular(5),
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Image.asset(
                  icon,
                  height: 20,
                  width: 20,
                  color: iconColor,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _updateSearch(String? event) {
    if (_searchQuery == event) {
      return;
    }
    _page = 0;
    if (event == null || (event).isEmpty) {
      _searchQuery = "";
    } else {
      _searchQuery = event;
    }
    _getAllContractors();
  }

  Future<dynamic> _getAllContractors({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _userSearchContractorsProvider!.searchProviders(
        context,
        "$_page",
        _searchQuery,
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }

  Future<dynamic> _toggleFavouriteStatus(int providerId) async {
    if (!await Common.checkInternetConnection()) {
      Common.showToast(
        "You are offline. Please make sure you have a stable internet connection to proceed",
      );
      return;
    }

    try {
      await _userSearchContractorsProvider!.toggleFavouriteStatus(
        context,
        providerId,
      );
    } catch (err) {
      print(err);
    }
  }
}
