import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:reparet/providers/user_project_details_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../models/form_field_data.dart';
import '../../models/get_categories_parent_model.dart';
import '../../models/project_model.dart';
import '../../place_picker/entities/location_result.dart';
import '../../providers/categories_provider.dart';
import '../../providers/post_project_provider.dart';
import '../../screens/common/images_details_screen.dart';
import '../../screens/common/play_video_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../common/bottom_sheet_image_picker.dart';
import '../common/bottom_sheet_video_picker.dart';
import '../common/raised_button_primary.dart';
import '../common/show_success_dialog.dart';
import '../common/upload_file_s3.dart';
import 'single_clickable_spinner_leading_trailing.dart';

class EditProject extends StatefulWidget {
  final VoidCallback onProjectEdited;
  final ProjectModel projectModel;

  const EditProject({
    Key? key,
    required this.onProjectEdited,
    required this.projectModel,
  }) : super(key: key);

  @override
  _EditProjectState createState() => _EditProjectState();
}

class MyFileModel {
  File? file;
  String? uploadedUrl;
  String? imageKey;
  bool isUrl;

  MyFileModel(
    this.file,
    this.uploadedUrl, /*this.imageKey,*/ {
    this.imageKey,
    this.isUrl = false,
  });
}

class _EditProjectState extends State<EditProject> {
  DateTime? _selectedDate;
  TimeOfDay? _selectedTime;

  bool _onlyVerifiedContractors = false;

  Position? _currentPosition;
  String? _currentAddress;
  double? lat, lng;

  final Map<String, FormFieldData?> _data = {
    "PROJECT_TITLE": null,
    "PROJECT_DESCRIPTION": null,
  };

  TextEditingController? _textEditingControllerTitle;
  TextEditingController? _textEditingControllerDescription;

  final List<MyFileModel> _projectImages = [];

  MyFileModel? _videoFile;

  Uint8List? uint8list;

  CategoryModel? _categoryModel;

  Size? _size;
  final _formKey = GlobalKey<FormState>();
  CustomDecorators? _customDecorators;
  bool _isLoading = false;
  bool _isFirst = true;
  late UserProjectDetailsProvider _projectDetailsProvider;

  @override
  void initState() {
    super.initState();

    //verified contractors
    _onlyVerifiedContractors =
        (widget.projectModel.allowVerifiedOnly ?? 0) == 1;

    //date and time
    DateTime dateTime = DateFormat(Constants.DATE_FORMAT_4)
        .parse(widget.projectModel.projectTime ?? "", true)
        .toLocal();
    _selectedDate = DateTime(dateTime.year, dateTime.month, dateTime.day);
    _selectedTime = TimeOfDay.fromDateTime(dateTime);

    //location
    _currentAddress = widget.projectModel.address ?? "";
    try {
      lat = double.parse(widget.projectModel.lat ?? "0.0");
      lng = double.parse(widget.projectModel.lng ?? "0.0");
    } catch (err) {
      print(err);
    }

    //project category
    _categoryModel = Provider.of<CategoriesProvider>(context, listen: false)
        .allCategories
        .firstWhere(
          (element) => element.id == widget.projectModel.categoryId,
        );

    //description and title
    _textEditingControllerTitle = TextEditingController(
      text: widget.projectModel.projectTitle ?? "",
    );
    _textEditingControllerDescription = TextEditingController(
      text: widget.projectModel.projectDescription ?? "",
    );
  }

  @override
  void didChangeDependencies() async {
    if (_isFirst) {
      _isFirst = false;
      _projectDetailsProvider =
          Provider.of<UserProjectDetailsProvider>(context, listen: false);
      await _getProjectDetails();
      //images
      await _loadImages();

      //video
      await _loadVideo();
      setState(() {});
    }
    super.didChangeDependencies();
  }

  Future<void> _loadImages() async {
    if (widget.projectModel.projectimages != null &&
        widget.projectModel.projectimages!.isNotEmpty) {
      _projectImages.clear();
      int i = 0;
      for (var element in widget.projectModel.projectimages!) {
        //String uploadedUrl=await UploadFileS3.getImageUrl(element.image??"");
        MyFileModel model = MyFileModel(
          null,
          element.image ?? "",
          isUrl: true,
          imageKey:
              _projectDetailsProvider.projectModel?.projectimages?[i].image,
        );
        _projectImages.add(model);
        i += 1;
      }
    }
  }

  Future<void> _loadVideo() async {
    if (widget.projectModel.projectvideos != null &&
        widget.projectModel.projectvideos!.isNotEmpty) {
      _videoFile = MyFileModel(
        null,
        widget.projectModel.projectvideos![0].video,
        isUrl: true,
        imageKey: _projectDetailsProvider.projectModel?.projectvideos?[0].video,
      );

      uint8list = await VideoThumbnail.thumbnailData(
        video: _videoFile?.uploadedUrl ?? "",
        imageFormat: ImageFormat.JPEG,
        maxWidth: 128,
        quality: 25,
      );

      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (_textEditingControllerTitle != null) {
      _textEditingControllerTitle!.dispose();
    }
    if (_textEditingControllerDescription != null) {
      _textEditingControllerDescription!.dispose();
    }
  }

  @override
  Widget build(BuildContext context) {
    _size = MediaQuery.of(context).size;
    _customDecorators = CustomDecorators(context);

    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(FocusNode());
          },
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: _size!.width * 0.04),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    getTitleWidget("Add Media", context),
                    getMediaView(context),
                    getTitleWidget("Project Details :", context),
                    getProjectDetailsView(context),
                    getTitleWidget("Timing and location :", context),
                    getTimingAndLocationView(context),
                    const SizedBox(height: 10),
                    Align(
                      alignment: Alignment.topRight,
                      child: PrimaryRaisedButton(
                        "Edit Project",
                        () {
                          _validateData();
                        },
                        size: _size,
                        color: CustomColors.colorAccent,
                      ),
                    ),
                    SizedBox(height: _size!.width * 0.03),
                  ],
                ),
              ),
            ),
          ),
        ),
        if (_isLoading)
          Container(
            color: CustomColors.colorBlack.withOpacity(0.6),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  SpinKitThreeBounce(
                    color: CustomColors.colorAccent,
                    size: 25.0,
                  ),
                  Text(
                    "Please wait while we are editing your project",
                    style: TextStyle(
                      fontSize: 12.0,
                      color: CustomColors.colorWhite,
                    ),
                  ),
                ],
              ),
            ),
          )
      ],
    );
  }

  Widget getTimingAndLocationView(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 6),
        IntrinsicHeight(
          child: Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () {
                    openLocationPicker();
                  },
                  child: getElevatedWidget(
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 12.0),
                            child: Image.asset(
                              "assets/images/icon_location.png",
                              fit: BoxFit.fill,
                              height: 15,
                              color: CustomColors.colorAccent,
                            ),
                          ),
                          SizedBox(
                            height: double.infinity,
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                _currentAddress ?? "Project location",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1!
                                    .copyWith(
                                      color: _currentAddress == null
                                          ? CustomColors.colorGrayDark
                                          : CustomColors.colorGrayDark2,
                                    ),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(width: 6),
              InkWell(
                onTap: () {
                  _pickLocation();
                },
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Container(
                    height: double.infinity,
                    color: Colors.transparent,
                    child: Material(
                      elevation: 1,
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.transparent,
                      child: Container(
                        decoration: BoxDecoration(
                          color: CustomColors.colorAccent,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(14.0),
                          child: Image.asset(
                            "assets/images/pick_location.png",
                            height: 20,
                            width: 20,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        const SizedBox(height: 10),
        Visibility(
          visible: false,
          child: Row(
            children: [
              Expanded(
                child: getClickableTextView(
                  "Date",
                  "assets/images/icon_date.png",
                  _selectedDate != null
                      ? DateFormat("dd/MM/yyyy").format(_selectedDate!)
                      : null,
                  "PICK_DATE",
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                  child: getClickableTextView(
                "Time",
                "assets/images/icon_time.png",
                _selectedTime != null ? _selectedTime!.format(context) : null,
                "PICK_TIME",
              )),
            ],
          ),
        ),
        CheckboxListTile(
          contentPadding: const EdgeInsets.all(0),
          activeColor: CustomColors.colorAccent,
          dense: true,
          title: RichText(
            text: TextSpan(
              text: "Allow only ",
              style:
                  Theme.of(context).textTheme.bodyText1!.copyWith(fontSize: 13),
              children: [
                TextSpan(
                  text: "verified contractors/service providers",
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontSize: 13,
                        color: CustomColors.colorAccent,
                      ),
                )
              ],
            ),
          ),
          value: _onlyVerifiedContractors,
          onChanged: (newVal) {
            setState(() {
              _onlyVerifiedContractors = newVal ?? false;
            });
          },
          controlAffinity: ListTileControlAffinity.leading,
        ),
        const SizedBox(height: 10),
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 5,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: CustomColors.colorRed,
          ),
          child: Text(
            "Note: Editing this project will cancel all the bids (if any).",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 13,
                  color: CustomColors.colorWhite,
                  height: 1,
                ),
          ),
        ),
      ],
    );
  }

  Future<void> _selectDate(BuildContext context) async {
    final DateTime? picked = await showDatePicker(
      context: context,
      initialDate:
          (_selectedDate != null && DateTime.now().isAfter(_selectedDate!))
              ? DateTime.now()
              : _selectedDate!,
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(const Duration(days: 30)),
      builder: (context, child) {
        return Theme(
          data: ThemeData(
            colorScheme: const ColorScheme.dark(
              primary: CustomColors.colorAccent,
              onPrimary: CustomColors.colorWhite,
              surface: CustomColors.colorAccent,
              onSurface: CustomColors.colorGrayDark2,
            ),
            dialogBackgroundColor: CustomColors.colorWhite,
          ),
          child: child!,
        );
      },
    );
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  Future<void> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: _selectedTime ?? TimeOfDay.now(),
      builder: (context, child) {
        return Theme(
          data: ThemeData(
            colorScheme: const ColorScheme.dark(
              primary: CustomColors.colorAccent,
              onPrimary: CustomColors.colorWhite,
              surface: CustomColors.colorWhite,
              onSurface: CustomColors.colorGrayDark2,
            ),
            dialogBackgroundColor: CustomColors.colorWhite,
          ),
          child: child!,
        );
      },
    );
    if (picked != null && picked != _selectedTime) {
      setState(() {
        _selectedTime = picked;
      });
    }
  }

  Widget getClickableTextView(
    String hintText,
    String icon,
    String? selectedItem,
    String action,
  ) {
    return InkWell(
      onTap: () {
        if (action == "PICK_DATE") {
          _selectDate(context);
        } else if (action == "PICK_TIME") {
          _selectTime(context);
        }
      },
      child: Material(
        elevation: 1,
        color: CustomColors.colorWhite,
        borderRadius: BorderRadius.circular(5),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 5, bottom: 5, left: 14, right: 14),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  selectedItem ?? hintText,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: selectedItem == null
                            ? CustomColors.colorGrayDark
                            : CustomColors.colorGrayDark2,
                      ),
                ),
              ),
              Image.asset(
                icon,
                color: CustomColors.colorAccent,
                height: 40,
                width: 19,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getUploadButton(
    BuildContext context,
    int count,
    String text,
    String action,
  ) {
    return getElevatedWidget(
      InkWell(
        onTap: () {
          if (action == "PICK_PHOTO") {
            showImagePicker();
          } else {
            showVideoPicker();
          }
        },
        borderRadius: BorderRadius.circular(5),
        child: SizedBox(
          width: 90,
          height: 90,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    padding: const EdgeInsets.all(4),
                    decoration: const BoxDecoration(
                      color: CustomColors.colorAccent,
                      shape: BoxShape.circle,
                    ),
                    child: Text(
                      "$count",
                      style: const TextStyle(
                        color: CustomColors.colorWhite,
                        fontSize: 10.0,
                      ),
                    ),
                  ),
                ),
                Image.asset(
                  "assets/images/icon_plus.png",
                  color: CustomColors.colorAccent,
                  height: 25,
                  width: 25,
                ),
                Text(
                  text,
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        fontSize: 14,
                      ),
                ),
                const SizedBox(height: 3),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getProjectDetailsView(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 6),
        getElevatedWidget(
          TextFormField(
            validator: (value) {
              if (value == null || value.trim().isEmpty) {
                _data["PROJECT_TITLE"] =
                    FormFieldData(true, "Please enter project title");
              } else {
                _data["PROJECT_TITLE"] = FormFieldData(false, value);
              }
              return null;
            },
            controller: _textEditingControllerTitle,
            style: Theme.of(context).textTheme.bodyText1,
            keyboardType: TextInputType.text,
            maxLines: 1,
            textAlign: TextAlign.start,
            cursorColor: CustomColors.colorAccent,
            maxLength: 100,
            inputFormatters: [
              MyLengthLimitingTextInputFormatter(100),
            ],
            decoration: _customDecorators!.getInputDecoration(
              "Project title",
              prefixIconString: "assets/images/icon_title.png",
              prefixIconColor: CustomColors.colorAccent,
            ),
          ),
        ),
        const SizedBox(height: 10),
        getElevatedWidget(
          TextFormField(
            validator: (value) {
              _data["PROJECT_DESCRIPTION"] = FormFieldData(false, value ?? "");
              return null;
            },
            controller: _textEditingControllerDescription,
            style: Theme.of(context).textTheme.bodyText1,
            keyboardType: TextInputType.text,
            maxLines: 3,
            textAlign: TextAlign.start,
            cursorColor: CustomColors.colorAccent,
            maxLength: 500,
            inputFormatters: [
              MyLengthLimitingTextInputFormatter(500),
            ],
            decoration: _customDecorators!.getInputDecorationMultiline(
              "Project description (optional)",
              maxLines: 3,
              prefixIconString: "assets/images/icon_description.png",
              prefixIconColor: CustomColors.colorAccent,
            ),
          ),
        ),
        const SizedBox(height: 10),
        Visibility(
          visible: true,
          child: SingleSpinnerWithLeadingTrailing(
            (value) {
              _categoryModel = value;
            },
            categoryModelForEdit: _categoryModel,
          ),
        ),
      ],
    );
  }

  Widget getMediaView(BuildContext context) {
    return Column(
      children: [
        const SizedBox(height: 6),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...getAllSelectedPhotos(),
              if (_projectImages.length < 4) ...[
                getUploadButton(
                  context,
                  (4 - _projectImages.length),
                  "Photos",
                  "PICK_PHOTO",
                ),
                const SizedBox(width: 10),
              ],
              _videoFile == null
                  ? getUploadButton(
                      context,
                      1,
                      "Video",
                      "PICK_VIDEO",
                    )
                  : getSelectedVideo(),
            ],
          ),
        ),
      ],
    );
  }

  Widget getTitleWidget(String title, BuildContext context) {
    return Column(
      children: [
        SizedBox(height: _size!.width * 0.025),
        Text(
          title,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }

  Widget getElevatedWidget(Widget widget) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 2.0),
      child: Material(
        elevation: 1,
        borderRadius: const BorderRadius.all(Radius.circular(5)),
        child: widget,
      ),
    );
  }

  getSelectedVideo() {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: getElevatedWidget(
        InkWell(
          onTap: () {
            Navigator.pushNamed(
              context,
              PlayVideoScreen.routeName,
              arguments: {
                "VIDEO_FILE": _videoFile?.file,
                "VIDEO_URL":
                    (_videoFile!.isUrl ? (_videoFile?.uploadedUrl ?? "") : ""),
              },
            );
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: SizedBox(
              width: 90,
              height: 90,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  if (uint8list != null) Image.memory(uint8list!),
                  if (uint8list == null)
                    Image.asset(
                      "assets/images/default_thumbnail.png",
                      height: _size!.width * 0.2,
                      width: _size!.width * 0.18,
                      fit: BoxFit.cover,
                    ),
                  Align(
                    alignment: Alignment.topRight,
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          _videoFile = null;
                        });
                      },
                      child: Container(
                        margin: const EdgeInsets.all(2),
                        padding: const EdgeInsets.all(2),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: CustomColors.colorRed,
                        ),
                        child: const Icon(
                          Icons.delete,
                          size: 15,
                          color: CustomColors.colorWhite,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  getAllSelectedPhotos() {
    if (_projectImages.isEmpty) {
      return [
        Container(),
      ];
    }
    return _projectImages.map(
      (e) => Padding(
        padding: const EdgeInsets.only(right: 10.0),
        child: getElevatedWidget(
          InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                ImagesDetailsScreen.routeName,
                arguments: {
                  "IMAGE_FILE": e.isUrl ? null : e.file,
                  "IMAGE_URL": e.uploadedUrl ?? "",
                },
              );
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: SizedBox(
                width: 90,
                height: 90,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    (e.isUrl)
                        ? CachedNetworkImage(
                            imageUrl: e.uploadedUrl!,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Image.asset(
                              "assets/images/no_pic_image.jpeg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, error) => Container(
                              padding: const EdgeInsets.all(4),
                              color: CustomColors.colorAccent.withOpacity(0.1),
                              child: const Icon(
                                Icons.error,
                              ),
                            ),
                          )
                        : Image.file(e.file!),
                    Align(
                      alignment: Alignment.topRight,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            _projectImages.remove(e);
                          });
                        },
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          padding: const EdgeInsets.all(2),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: CustomColors.colorRed,
                          ),
                          child: const Icon(
                            Icons.delete,
                            size: 15,
                            color: CustomColors.colorWhite,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showImagePicker() async {
    File? pickedFile = await showModalBottomSheet<File>(
      context: context,
      builder: (context) => const BottomSheetImagePicker(
        color: CustomColors.colorAccent,
        shouldCrop: false,
      ),
    );
    if (pickedFile != null) {
      setState(() {
        _projectImages.add(MyFileModel(pickedFile, null));
      });
    }
  }

  void showVideoPicker() async {
    File? pickedFile = await showModalBottomSheet<File>(
      context: context,
      builder: (context) => const BottomSheetVideoPicker(
        color: CustomColors.colorAccent,
      ),
    );
    if (pickedFile != null) {
      _videoFile = MyFileModel(pickedFile, null);
      uint8list = await VideoThumbnail.thumbnailData(
        video: _videoFile!.file!.path,
        imageFormat: ImageFormat.JPEG,
        maxWidth: 128,
        quality: 25,
      );

      setState(() {});
    }
  }

  void _pickLocation() async {
    _currentPosition = await Common.getCurrentLocation();
    print("_currentPosition $_currentPosition");
    if (_currentPosition != null) {
      lat = _currentPosition!.latitude;
      lng = _currentPosition!.longitude;
      _currentAddress =
          (await Common.getAddressFromLatLng(_currentPosition!)) ?? "";
      setState(() {});
    }
  }

  void openLocationPicker() async {
    LocationResult? result = await Common.checkLocationPermission(context);
    if (result == null ||
        result.latLng == null ||
        result.formattedAddress == null) return;
    print(result);
    setState(() {
      lat = result.latLng!.latitude;
      lng = result.latLng!.longitude;
      _currentAddress = result.formattedAddress;
    });
  }

  Future<void> uploadFilesToS3(
    int index,
    List<MyFileModel> files,
    String fileName,
  ) async {
    try {
      print("----------------------- $fileName");

      if (files[index].imageKey == null || files[index].imageKey!.isEmpty) {
        String? image = await UploadFileS3(
          context,
          files[index].file!,
          fileName,
        ).uploadFile();
        print("file -------------- " + (image ?? ""));
        files[index].imageKey = image;
      }

      index++;
      if (index == files.length) {
        return;
      } else {
        return await uploadFilesToS3(
          index,
          files,
          "image ${index + 1}/${_projectImages.length}",
        );
      }
    } catch (e) {
      print("error is: $e");
      return;
    }
  }

  void _validateData() async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);
    if (_projectImages.isEmpty) {
      errors.add("Please select at least one photo.");
    }
    /*if (_categoryModel == null) {
      errors.add("Please select project category.");
    }
    if (_selectedDate == null) {
      errors.add("Please select project date.");
    }
    if (_selectedTime == null) {
      errors.add("Please select project time.");
    }*/
    if (_currentAddress == null) {
      errors.add("Please enter project location or pick current location.");
    }

    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    String startTime = _selectedTime!.format(context);
    if (startTime.contains("AM") ||
        startTime.contains("PM") ||
        startTime.contains("am") ||
        startTime.contains("pm")) {
      startTime = Common.getConvertedDate(startTime, "hh:mm a", "HH:mm");
    }
    DateTime now = DateTime.now();
    DateTime jobStart = DateFormat("yyyy-MM-dd HH:mm").parse(
      "${_selectedDate!.year}"
      "-"
      "${_selectedDate!.month}"
      "-"
      "${_selectedDate!.day}"
      " "
      "$startTime",
    );

    /*if (now.isAfter(jobStart)) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "Project time has already passed.",
      );
      return;
    }*/

    jobStart = jobStart.toUtc();

    if (_videoFile != null &&
        (_videoFile!.imageKey == null || _videoFile!.imageKey!.isEmpty)) {
      _videoFile!.imageKey = await UploadFileS3(
        context,
        _videoFile!.file!,
        "video",
      ).uploadFile();

      print("file -------------- " + (_videoFile!.uploadedUrl ?? ""));
      _videoFile!.imageKey ??= "";
    }

    await uploadFilesToS3(
      0,
      _projectImages,
      "image 1/${_projectImages.length}",
    );

    setState(() {
      _isLoading = true;
    });

    PostProjectProvider provider = Provider.of<PostProjectProvider>(
      context,
      listen: false,
    );

    try {
      final response = await provider.editProject(
        context,
        "${widget.projectModel.projectId ?? ""}",
        _data["PROJECT_TITLE"]!.data,
        _data["PROJECT_DESCRIPTION"]!.data,
        "${_categoryModel?.id ?? ""}",
        _currentAddress ?? "",
        "${lat ?? ""}",
        "${lng ?? ""}",
        /*DateFormat(Constants.DATE_FORMAT_1).format(jobStart),
        DateFormat(Constants.DATE_FORMAT_2).format(jobStart),*/
        _onlyVerifiedContractors ? "1" : "0",
        _videoFile?.imageKey ?? "",
        _projectImages.map((e) => e.imageKey!).join(","),
      );

      setState(() {
        _isLoading = false;
      });

      if (response is bool && response) {
        showDialog(
          context: context,
          builder: (_) => ShowSuccessDialog("Your project is edited", () {
            widget.onProjectEdited();
          }),
          barrierDismissible: false,
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (err) {
      print(err);
    }
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) super.setState(fn);
  }

  _getProjectDetails() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed");
      return;
    }

    try {
      dynamic response = await _projectDetailsProvider.getProjectDetails(
        context,
        widget.projectModel.projectId!.toString(),
      );

      if ((response is bool) && response) {
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }
  }
}
