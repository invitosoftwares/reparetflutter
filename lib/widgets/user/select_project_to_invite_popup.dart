import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/project_invitation_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/common/show_success_dialog.dart';
import '../../widgets/user/item_invite_project.dart';
import '../../widgets/user/user_no_listing_view.dart';

class SelectProjectToInvitePopup extends StatefulWidget {
  final String providerId;

  const SelectProjectToInvitePopup({
    required this.providerId,
    Key? key,
  }) : super(key: key);

  @override
  State<SelectProjectToInvitePopup> createState() =>
      _SelectProjectToInvitePopupState();
}

class _SelectProjectToInvitePopupState
    extends State<SelectProjectToInvitePopup> {
  int _loaderType = 0;
  bool _isFirstTime = true;
  ProjectInvitationProvider? _projectInvitationProvider;
  int _page = 0;
  bool _isNoInternet = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _projectInvitationProvider = Provider.of<ProjectInvitationProvider>(
        context,
        listen: false,
      );

      _loaderType = _projectInvitationProvider!.allNewProjects.isEmpty ? 1 : 0;
      _page = 0;
      _getAllNewProjects(
        showLoader: _projectInvitationProvider!.allNewProjects.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Center(
      child: SizedBox(
        width: size.width * 0.8,
        child: Material(
          elevation: 5,
          borderRadius: BorderRadius.circular(5),
          color: CustomColors.colorGrayLight2,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  "Invite for project",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                        color: CustomColors.colorGrayDark2,
                      ),
                ),
                const SizedBox(height: 10),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    maxHeight: size.height * 0.7,
                  ),
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: [
                      _isNoInternet
                          ? NoInternetView(
                              () => _getAllNewProjects(),
                            )
                          : (_loaderType == 1)
                              ? const ProjectListShimmer()
                              : Consumer<ProjectInvitationProvider>(
                                  builder: (context, provider, child) =>
                                      RefreshIndicator(
                                    color: CustomColors.colorAccent,
                                    onRefresh: () async {
                                      if (_loaderType != 0) return;
                                      _page = 0;
                                      await _getAllNewProjects(
                                        showLoader: false,
                                      );
                                    },
                                    child: (provider.allNewProjects.isEmpty)
                                        ? UserNoListingView(
                                            () {},
                                            customText:
                                                "You don't have any project posted!",
                                          )
                                        : ListView.builder(
                                            itemBuilder: (context, index) {
                                              if (index ==
                                                      provider.allNewProjects
                                                              .length -
                                                          min(
                                                              3,
                                                              provider
                                                                  .allNewProjects
                                                                  .length) &&
                                                  provider.hasMoreNewListings &&
                                                  _loaderType == 0) {
                                                _page++;
                                                _getAllNewProjects();
                                              }
                                              if (index ==
                                                      provider.allNewProjects
                                                          .length &&
                                                  provider.hasMoreNewListings) {
                                                return Container();
                                              }
                                              return Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 4.0),
                                                child: InkWell(
                                                  onTap: () {
                                                    _sendInvitation(
                                                      "${provider.allNewProjects[index].projectId}",
                                                      widget.providerId,
                                                    );
                                                  },
                                                  child: ChangeNotifierProvider
                                                      .value(
                                                    value: provider
                                                        .allNewProjects[index],
                                                    child:
                                                        ItemInviteProject(size),
                                                  ),
                                                ),
                                              );
                                            },
                                            itemCount:
                                                provider.allNewProjects.length,
                                          ),
                                  ),
                                ),
                      if (_loaderType == 2)
                        const Padding(
                          padding: EdgeInsets.only(bottom: 8.0),
                          child: SizedBox(
                            height: 10,
                            width: 10,
                            child: CircularProgressIndicator(
                              color: CustomColors.colorAccent,
                              strokeWidth: 2,
                            ),
                          ),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllNewProjects({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _projectInvitationProvider!.getAllNewProjects(
        context,
        "$_page",
        "",
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }

  Future<dynamic> _sendInvitation(
    String projectId,
    String providerId,
  ) async {
    if (!await Common.checkInternetConnection()) {
      Common.showToast(
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);

    try {
      dynamic response = await _projectInvitationProvider!.sendInvitation(
        context,
        projectId,
        providerId,
      );

      Navigator.pop(context);

      if (response is bool && response) {
        Navigator.pop(context);
        _showSuccessDialog();
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (err) {
      print(err);
    }
  }

  _showSuccessDialog() {
    showDialog(
      context: context,
      builder: (_) => const ShowSuccessDialog(
        "Invitation sent to contractor",
        null,
      ),
    );
  }
}
