import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:provider/provider.dart';

import '../../models/setup_intent_parent_model.dart';
import '../../providers/payment_methods_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../common/raised_button_primary.dart';

class BottomSheetEnterCard extends StatefulWidget {
  const BottomSheetEnterCard();

  @override
  _BottomSheetEnterCardState createState() => _BottomSheetEnterCardState();
}

class _BottomSheetEnterCardState extends State<BottomSheetEnterCard> {
  bool _isLoading = false;
  String? _paymentError;
  CardFieldInputDetails? _card;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: CustomColors.colorWhite,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              const SizedBox(height: 20),
              _getCardWidget(),
              PrimaryRaisedButton(
                "Proceed",
                _validateCardData,
                isLoading: _isLoading,
              ),
              const SizedBox(height: 24),
            ],
          ),
        ),
      ),
    );
  }

  _getCardWidget() {
    const OutlineInputBorder border = OutlineInputBorder(
      borderSide: BorderSide(
        color: CustomColors.colorPrimary,
        width: 1,
      ),
    );
    final theme = ThemeData.light().copyWith(
      inputDecorationTheme: const InputDecorationTheme(
        border: border,
        focusedErrorBorder: border,
        errorBorder: border,
        focusedBorder: border,
        disabledBorder: border,
        enabledBorder: border,
        floatingLabelBehavior: FloatingLabelBehavior.always,
        contentPadding: EdgeInsets.zero,
      ),
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Please enter your card details",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        Text(
          "Your card details are fully secured by STRIPE payment gateway",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 12,
              ),
        ),
        const SizedBox(height: 15),
        Theme(
          data: theme,
          child: CardField(
            autofocus: true,
            enablePostalCode: true,
            style: Theme.of(context).textTheme.bodyText1,
            onCardChanged: (card) {
              setState(() {
                _card = card;
              });
            },
          ),
        ),
        const SizedBox(height: 20),
        if (_paymentError != null)
          Padding(
            padding: const EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              bottom: 20.0,
            ),
            child: Text(
              "$_paymentError",
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontSize: 12,
                    color: CustomColors.colorRed,
                  ),
            ),
          ),
      ],
    );
  }

  void _validateCardData() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed",
      );
      return;
    }
    if (_card == null || !_card!.complete) {
      setState(() {
        _paymentError = "Please complete your card details.";
      });
      return;
    }
    setState(() {
      _paymentError = null;
      _isLoading = true;
    });

    try {
      Common.showLoaderDialog("Please wait...", context);
      PaymentMethodsProvider paymentProvider =
          Provider.of<PaymentMethodsProvider>(context, listen: false);
      dynamic response = await paymentProvider.getSetupIntent(context);

      if (response is SetupIntentParentModel) {
        String setupIntent = "${response.data!.setupIntentId}";
        await handleCardSetupIntent(setupIntent);
      } else if (response is String) {
        Navigator.pop(context);
        _paymentError = response;
      }
    } catch (e) {
      Navigator.pop(context);
      print(e);
    }

    setState(() {
      _isLoading = false;
    });
  }

  handleCardSetupIntent(String setupIntent) async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed",
      );
      return;
    }
    try {
      final setupIntentResult = await Stripe.instance.confirmSetupIntent(
        setupIntent,
        const PaymentMethodParams.card(),
      );
      print("setup intent part done");
      print("$setupIntentResult");
      if (setupIntentResult.status == "Succeeded") {
        Common.showToast("Payment method added successfully.");
        Navigator.pop(context); //this is for loader
        Navigator.pop(context, true);
      }
    } on StripeException catch (stripeException) {
      print(stripeException);
      setState(() {
        _isLoading = false;
        _paymentError = stripeException.error.localizedMessage;
      });
    } catch (error) {
      print(error);
      setState(() {
        _isLoading = false;
        _paymentError = error.toString();
      });
    }
  }
}
