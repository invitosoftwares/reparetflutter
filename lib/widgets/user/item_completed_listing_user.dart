import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

import '../../models/project_model.dart';
import '../../providers/rating_provider.dart';
import '../../providers/user_listings_provider.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import 'rate_contractor_popup.dart';

class ItemCompletedListingUser extends StatelessWidget {
  final Size _size;
  final VoidCallback click;
  final VoidCallback onPaymentDone;
  final UserListingProvider userListingProvider;

  const ItemCompletedListingUser(
    this._size, {
    Key? key,
    required this.click,
    required this.onPaymentDone,
    required this.userListingProvider,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProjectModel>(
      builder: (context, model, child) => InkWell(
        onTap: () {
          click();
        },
        child: Card(
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(4.0),
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: CachedNetworkImage(
                    imageUrl: model.projectimages?[0].image ?? "",
                    fit: BoxFit.cover,
                    width: _size.width * 0.2,
                    height: _size.width * 0.225,
                    placeholder: (context, url) => Image.asset(
                      "assets/images/no_pic_image.jpeg",
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, error) => Container(
                      padding: const EdgeInsets.all(4),
                      color: CustomColors.colorAccent.withOpacity(0.1),
                      width: _size.width * 0.2,
                      height: _size.width * 0.225,
                      child: const Icon(
                        Icons.error,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          model.projectTitle ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: CustomColors.colorAccent,
                                    fontSize: 18.0,
                                  ),
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/icon_date_range.png",
                              height: 10,
                              width: 10,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              Common.getConvertedDate(
                                model.projectTime ?? "",
                                Constants.DATE_FORMAT_4,
                                Constants.DATE_FORMAT_3,
                                isUtc: true,
                              ),
                              style: const TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8.0, top: 3.0),
                          child: Row(
                            mainAxisAlignment:
                                (model.projectStatus == Constants.PROJECT_PAID)
                                    ? MainAxisAlignment.spaceBetween
                                    : MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              if (model.projectStatus == Constants.PROJECT_PAID)
                                const Text(
                                  "Paid",
                                  style: TextStyle(
                                    color: CustomColors.colorGreen,
                                    fontSize: 15.0,
                                  ),
                                ),
                              if (model.projectStatus ==
                                  Constants.PROJECT_COMPLETED)
                                getRightAlignedButton(
                                  "Pay \$${model.amount}",
                                  context,
                                  () async {
                                    dynamic response = await Common
                                        .startPaymentProcessForProject(
                                      context,
                                      "${model.projectId}",
                                      userListingProvider,
                                    );
                                    if (response is bool && response) {
                                      onPaymentDone();
                                    }
                                  },
                                ),
                              if (model.projectStatus == Constants.PROJECT_PAID)
                                getRightAlignedButton(
                                  "Leave review",
                                  context,
                                  () {
                                    _showLeaveReview(
                                      context,
                                      model,
                                    );
                                  },
                                ),
                              if (model.projectStatus ==
                                  Constants.PROJECT_PAID_REVIEWED_BY_USER)
                                getRatingBar(model.rating, context),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _showLeaveReview(BuildContext context, ProjectModel model) async {
    dynamic response = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return ChangeNotifierProvider(
          create: (context) => RatingProvider(),
          child: RateContractorPopup(
            projectId: "${model.projectId}",
          ),
        );
      },
    );

    if (response != null && (response is double)) {
      model.setRatingLocally(response);
    }
  }

  Widget getRightAlignedButton(
      String text, BuildContext context, VoidCallback click) {
    return Material(
      color: CustomColors.colorAccent,
      borderRadius: BorderRadius.circular(5),
      elevation: 1,
      child: InkWell(
        onTap: () {
          click();
        },
        child: Ink(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 8.0),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                CustomColors.colorAccent,
                CustomColors.colorRed,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: Text(
            text,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  fontSize: 14.0,
                ),
          ),
        ),
      ),
    );
  }

  Widget getRatingBar(dynamic rating, BuildContext context) {
    return RatingBarIndicator(
      rating: rating.toDouble(),
      itemBuilder: (context, index) => const Icon(
        Icons.star,
        color: CustomColors.colorPrimary,
      ),
      itemCount: 5,
      itemSize: 15.0,
      direction: Axis.horizontal,
    );
  }
}
