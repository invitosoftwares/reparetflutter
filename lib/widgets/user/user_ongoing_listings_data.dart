import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/user_listings_provider.dart';
import '../../screens/user/project_details_for_user_screen.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/no_listings_searched.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/user/item_on_going_listing_user.dart';
import '../../widgets/user/user_no_listing_view.dart';

class UserOnGoingListingsData extends StatefulWidget {
  final Size _size;
  final VoidCallback newProjectClick;
  final StreamController queryStreamController;

  const UserOnGoingListingsData(
    this._size, {
    Key? key,
    required this.newProjectClick,
    required this.queryStreamController,
  }) : super(key: key);

  @override
  State<UserOnGoingListingsData> createState() =>
      _UserOnGoingListingsDataState();
}

class _UserOnGoingListingsDataState extends State<UserOnGoingListingsData> {
  int _loaderType = 0;
  bool _isFirstTime = true;
  UserListingProvider? _userListingProvider;
  int _page = 0;
  bool _isNoInternet = false;
  String _searchQuery = "";

  @override
  void dispose() {
    super.dispose();
    widget.queryStreamController.stream.listen((event) {}).cancel();
  }

  @override
  initState() {
    super.initState();
    widget.queryStreamController.stream.listen((event) {
      if (_searchQuery == event) {
        return;
      }
      _page = 0;
      if (event == null || (event is! String) || (event).isEmpty) {
        _searchQuery = "";
      } else {
        _searchQuery = event;
      }
      _getAllOngoingProjects();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _userListingProvider = Provider.of<UserListingProvider>(
        context,
        listen: false,
      );

      _loaderType = _userListingProvider!.allOngoingProjects.isEmpty ? 1 : 0;
      _page = 0;
      _getAllOngoingProjects(
        showLoader: _userListingProvider!.allOngoingProjects.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllOngoingProjects(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<UserListingProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllOngoingProjects(showLoader: false);
                      },
                      child: (provider.allOngoingProjects.isEmpty &&
                              _searchQuery.isEmpty)
                          ? UserNoListingView(widget.newProjectClick)
                          : (provider.allOngoingProjects.isEmpty &&
                                  _searchQuery.isNotEmpty)
                              ? const NoListingsSearched(
                                  "assets/images/empty_search_user_listings.png",
                                )
                              : ListView.builder(
                                  itemBuilder: (context, index) {
                                    if (index ==
                                            provider.allOngoingProjects.length -
                                                min(
                                                    3,
                                                    provider.allOngoingProjects
                                                        .length) &&
                                        provider.hasMoreOngoingListings &&
                                        _loaderType == 0) {
                                      _page++;
                                      _getAllOngoingProjects();
                                    }
                                    if (index ==
                                            provider
                                                .allOngoingProjects.length &&
                                        provider.hasMoreOngoingListings) {
                                      return Container();
                                    }
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: ChangeNotifierProvider.value(
                                        value:
                                            provider.allOngoingProjects[index],
                                        child: ItemOnGoingListingUser(
                                          widget._size,
                                          click: () async {
                                            Navigator.pushNamed(
                                              context,
                                              ProjectDetailsForUserScreen
                                                  .routeName,
                                              arguments: {
                                                "PROJECT_ID":
                                                    "${provider.allOngoingProjects[index].projectId}",
                                              },
                                            );
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount: provider.allOngoingProjects.length,
                                ),
                    ),
                  ),
        if (_loaderType == 2)
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: SizedBox(
              height: 10,
              width: 10,
              child: CircularProgressIndicator(
                color: CustomColors.colorAccent,
                strokeWidth: 2,
              ),
            ),
          ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllOngoingProjects({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _userListingProvider!.getAllOngoingProjects(
        context,
        "$_page",
        _searchQuery,
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }
}
