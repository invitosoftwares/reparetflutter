import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import '../../models/project_bids_parent_model.dart';
import '../../providers/project_bids_provider.dart';
import '../../screens/user/contractor_profile_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../common/project_list_shimmer.dart';

class ProjectDetailsNewShowAllBids extends StatefulWidget {
  final Size _size;
  final String projectId;
  final VoidCallback onBidConfirmed;

  const ProjectDetailsNewShowAllBids(
    this._size, {
    Key? key,
    required this.projectId,
    required this.onBidConfirmed,
  }) : super(key: key);

  @override
  State<ProjectDetailsNewShowAllBids> createState() =>
      _ProjectDetailsNewShowAllBidsState();
}

class _ProjectDetailsNewShowAllBidsState
    extends State<ProjectDetailsNewShowAllBids> {
  ProjectBidsProvider? _projectBidsProvider;
  int _loaderType = 0;
  bool _isFirstTime = true;
  int _page = 0;
  bool _isNoInternet = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _projectBidsProvider = Provider.of<ProjectBidsProvider>(
        context,
        listen: false,
      );

      _loaderType = _projectBidsProvider!.allBids.isEmpty ? 1 : 0;
      _page = 0;

      _getAllBids(
        showLoader: _projectBidsProvider!.allBids.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllBids(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<ProjectBidsProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllBids(showLoader: false);
                      },
                      child: (provider.allBids.isEmpty)
                          ? Align(
                              alignment: Alignment.center,
                              child: InkWell(
                                onTap: () {
                                  _page = 0;
                                  _getAllBids();
                                },
                                child: Text(
                                  "No bids yet! Tap to retry.",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          : ListView.builder(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 2.0),
                              itemBuilder: (context, index) {
                                if (index ==
                                        provider.allBids.length -
                                            min(3, provider.allBids.length) &&
                                    provider.hasMore &&
                                    _loaderType == 0) {
                                  _page++;
                                  _getAllBids();
                                }
                                if (index == provider.allBids.length &&
                                    provider.hasMore) {
                                  return Container();
                                }
                                return _getContractorBidView(
                                  provider.allBids[index],
                                );
                              },
                              itemCount: provider.allBids.length,
                            ),
                    ),
                  ),
        if (_loaderType == 2)
          const Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: SizedBox(
                height: 10,
                width: 10,
                child: CircularProgressIndicator(
                  color: CustomColors.colorAccent,
                  strokeWidth: 2,
                ),
              ),
            ),
          ),
      ],
    );
  }

  _getContractorBidView(ProjectBidModel model) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(
            context,
            ContractorProfileScreen.routeName,
            arguments: {
              "PROVIDER_ID":
                  model.providerId == null ? "" : "${model.providerId}",
              "SHOW_INVITE": false,
            },
          );
        },
        child: Material(
          elevation: 1,
          color: CustomColors.colorWhite,
          borderRadius: BorderRadius.circular(5),
          child: Container(
            padding: const EdgeInsets.all(4.0),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: (model.providerImage?.isEmpty ?? true)
                        ? Container(
                            color: CustomColors.colorAccent.withOpacity(0.15),
                            height: widget._size.width * 0.2,
                            width: widget._size.width * 0.18,
                            child: Image.asset(
                              "assets/images/icon_user.png",
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl: model.providerImage!,
                            fit: BoxFit.cover,
                            height: widget._size.width * 0.2,
                            width: widget._size.width * 0.18,
                            placeholder: (context, url) => Image.asset(
                              "assets/images/no_pic_image.jpeg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, error) => Container(
                              padding: const EdgeInsets.all(4),
                              color: CustomColors.colorAccent.withOpacity(0.1),
                              height: widget._size.width * 0.2,
                              width: widget._size.width * 0.18,
                              child: const Icon(
                                Icons.error,
                              ),
                            ),
                          ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          getNameRow(context, model),
                          getRatingBar(context, model),
                          getAmountAndConfirmView(context, model)
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getAmountAndConfirmView(BuildContext context, ProjectBidModel model) {
    return Row(
      children: [
        Expanded(
          child: Text(
            "\$${model.bidAmount}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontSize: 18.0,
                ),
          ),
        ),
        (model.bidAccepted ?? false)
            ? Material(
                elevation: 0,
                borderRadius: BorderRadius.circular(5),
                color: CustomColors.colorGreen,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: Row(
                    children: [
                      const Icon(
                        Icons.done,
                        color: CustomColors.colorWhite,
                        size: 12.0,
                      ),
                      const SizedBox(width: 4),
                      Text(
                        "Confirmed",
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: CustomColors.colorWhite,
                              fontSize: 12.0,
                            ),
                      ),
                    ],
                  ),
                ),
              )
            : InkWell(
                onTap: () {
                  _confirmBidConfirmation(model.bidId!);
                },
                child: Material(
                  elevation: 1,
                  borderRadius: BorderRadius.circular(5),
                  color: CustomColors.colorPrimary,
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                    child: Text(
                      "Confirm",
                      style: Theme.of(context).textTheme.bodyText1!.copyWith(
                            color: CustomColors.colorWhite,
                            fontSize: 12.0,
                          ),
                    ),
                  ),
                ),
              ),
      ],
    );
  }

  Widget getNameRow(BuildContext context, ProjectBidModel model) {
    return Row(
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Text(
            "${model.providerFirstName ?? ""} ${model.providerLastName ?? ""}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontSize: 18.0,
                ),
          ),
        ),
        const SizedBox(width: 8),
        Image.asset(
          ((model.providerVerified ?? 0) == 1)
              ? "assets/images/icon_verified.png"
              : "assets/images/icon_not_verified.png",
          height: 15,
          width: 15,
        ),
        const SizedBox(width: 8),
      ],
    );
  }

  Widget getRatingBar(BuildContext context, ProjectBidModel model) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        RatingBarIndicator(
          rating: model.averageRating?.toDouble() ?? 0.0,
          itemBuilder: (context, index) => const Icon(
            Icons.star,
            color: CustomColors.colorPrimary,
          ),
          itemCount: 5,
          itemSize: 17.0,
          direction: Axis.horizontal,
        ),
        const SizedBox(width: 5),
        Text(
          "(${model.totalRatings ?? "N/A"})",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: CustomColors.colorGrayDark3,
                fontSize: 12.0,
              ),
        ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllBids({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _projectBidsProvider!.getProjectBids(
        context,
        widget.projectId,
        "$_page",
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }

  void _confirmBidConfirmation(int bidId) async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "No",
      "Yes",
      "Are you sure you want to confirm this bid?",
      () => Navigator.pop(context),
      () {
        _confirmBid(bidId);
        Navigator.pop(context);
      },
    );
  }

  Future<dynamic> _confirmBid(int bidId) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    Common.showLoaderDialog("confirming bid...", context);

    try {
      var res = await _projectBidsProvider!.confirmBid(
        context,
        bidId,
      );

      Navigator.pop(context);
      if (res is bool && res) {
        widget.onBidConfirmed();
      } else if (res is String) {
        AlertDialogs.showAlertDialogWithOk(context, res);
      }
    } catch (err) {
      print(err);
    }
  }
}
