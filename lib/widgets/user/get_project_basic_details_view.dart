import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../common/upload_file_s3.dart';

class GetProjectBasicDetailsView extends StatefulWidget {
  String _image;
  final String _projectTitle;
  final String _category;
  final String _projectTime;
  final String _postedOn;
  final int _status;
  final Size _size;

  GetProjectBasicDetailsView(
    this._image,
    this._projectTitle,
    this._category,
    this._projectTime,
    this._postedOn,
    this._status,
    this._size,
  );

  @override
  State<GetProjectBasicDetailsView> createState() =>
      _GetProjectBasicDetailsViewState();
}

class _GetProjectBasicDetailsViewState
    extends State<GetProjectBasicDetailsView> {
  bool isFirst = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (isFirst) {
      isFirst = false;
      //String temp = await UploadFileS3.getImageUrl(widget._image);
      //widget._image = temp;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Stack(
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: widget._status != Constants.PROJECT_POSTED ? 6.0 : 0.0,
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5),
                child: CachedNetworkImage(
                  imageUrl: widget._image,
                  fit: BoxFit.cover,
                  height: widget._size.width * 0.28125,
                  width: widget._size.width * 0.25,
                  placeholder: (context, url) => Image.asset(
                    "assets/images/no_pic_image.jpeg",
                    fit: BoxFit.cover,
                  ),
                  errorWidget: (context, url, error) => Container(
                    padding: const EdgeInsets.all(4),
                    color: CustomColors.colorAccent.withOpacity(0.1),
                    height: widget._size.width * 0.28125,
                    width: widget._size.width * 0.25,
                    child: const Icon(
                      Icons.error,
                    ),
                  ),
                ),
              ),
            ),
            if (widget._status != Constants.PROJECT_POSTED)
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Material(
                  color: widget._status == Constants.PROJECT_ACCEPTED
                      ? CustomColors.colorBlue2
                      : CustomColors.colorGreen,
                  borderRadius: BorderRadius.circular(5),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 6,
                      vertical: 2,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (widget._status == Constants.PROJECT_ACCEPTED) ...[
                          const Icon(
                            Icons.refresh,
                            color: CustomColors.colorWhite,
                            size: 12,
                          ),
                          const SizedBox(width: 2),
                        ],
                        Text(
                          widget._status == Constants.PROJECT_ACCEPTED
                              ? "Ongoing"
                              : "Completed",
                          style: const TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.bold,
                            color: CustomColors.colorWhite,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
          ],
        ),
        SizedBox(width: 8),
        Consumer<AuthProvider>(
          builder: (context, authProvider, child) => Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${widget._projectTitle}",
                  maxLines: 2,
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                        color:
                            ((authProvider.userModel?.role ?? Constants.USER) ==
                                    Constants.USER)
                                ? CustomColors.colorAccent
                                : CustomColors.colorPrimary,
                        fontSize: 20.0,
                        height: 1,
                      ),
                ),
                SizedBox(height: 5),
                Text(
                  "Category: ${widget._category}",
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color:
                            ((authProvider.userModel?.role ?? Constants.USER) ==
                                    Constants.USER)
                                ? CustomColors.colorPrimary
                                : CustomColors.colorAccent,
                        fontSize: 12.0,
                      ),
                ),
                const SizedBox(height: 2),
                Row(
                  children: [
                    Image.asset(
                      "assets/images/icon_date_range.png",
                      height: 10,
                      width: 10,
                    ),
                    const SizedBox(width: 4),
                    Expanded(
                      child: Text(
                        "Project time: ${widget._projectTime}",
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: CustomColors.colorGrayDark3,
                              fontSize: 12.0,
                            ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Image.asset(
                      "assets/images/icon_date_range.png",
                      height: 10,
                      width: 10,
                    ),
                    const SizedBox(width: 4),
                    Expanded(
                      child: Text(
                        "Posted on: ${widget._postedOn}",
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              color: CustomColors.colorGrayDark3,
                              fontStyle: FontStyle.italic,
                              fontSize: 12.0,
                            ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
