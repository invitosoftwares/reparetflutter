import 'package:flutter/material.dart';
import '../../models/get_categories_parent_model.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/user/select_project_category_popup.dart';

class SingleSpinnerWithLeadingTrailing extends StatefulWidget {
  final Function onSelected;
  final CategoryModel? categoryModelForEdit;

  const SingleSpinnerWithLeadingTrailing(
    this.onSelected, {
    Key? key,
    this.categoryModelForEdit,
  }) : super(key: key);

  @override
  _SingleSpinnerWithLeadingTrailingState createState() =>
      _SingleSpinnerWithLeadingTrailingState();
}

class _SingleSpinnerWithLeadingTrailingState
    extends State<SingleSpinnerWithLeadingTrailing> {
  CategoryModel? _selectedValue;

  @override
  void initState() {
    super.initState();
    if (widget.categoryModelForEdit != null && _selectedValue == null) {
      _selectedValue = widget.categoryModelForEdit;
    }
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return SelectProjectCategoryPopup((value) {
              widget.onSelected(value);
              setState(() {
                _selectedValue = value;
              });
            });
          },
        );
      },
      child: Material(
        elevation: 1,
        color: CustomColors.colorWhite,
        borderRadius: BorderRadius.circular(5),
        child: Padding(
          padding:
              const EdgeInsets.only(top: 5, bottom: 5, left: 14, right: 14),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Image.asset(
                "assets/images/icon_listings.png",
                color: CustomColors.colorAccent,
                height: 40,
                width: 15,
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Text(
                  _selectedValue == null
                      ? "Select project category (optional)"
                      : _selectedValue.toString(),
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: _selectedValue == null
                            ? CustomColors.colorGrayDark
                            : CustomColors.colorGrayDark2,
                      ),
                ),
              ),
              const SizedBox(width: 10),
              Image.asset(
                "assets/images/icon_dropdown.png",
                color: CustomColors.colorAccent,
                height: 40,
                width: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
