import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../utils/custom_colors.dart';
import '../common/upload_file_s3.dart';

class GetContractorDetailsView extends StatefulWidget {
  String _image;
  final String _name;
  final String _services;
  final bool _isVerified;
  final dynamic _rating;
  final int _totalReviews;
  final Size _size;

  GetContractorDetailsView(
    this._image,
    this._name,
    this._services,
    this._isVerified,
    this._rating,
    this._totalReviews,
    this._size, {
    Key? key,
  }) : super(key: key);

  @override
  State<GetContractorDetailsView> createState() =>
      _GetContractorDetailsViewState();
}

class _GetContractorDetailsViewState extends State<GetContractorDetailsView> {
  bool isFirst = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (isFirst) {
      isFirst = false;
      String temp = await UploadFileS3.getImageUrl(widget._image);
      widget._image = temp;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: widget._image.isEmpty
              ? Container(
                  color: CustomColors.colorAccent.withOpacity(0.15),
                  height: widget._size.width * 0.28125,
                  width: widget._size.width * 0.25,
                  child: Image.asset(
                    "assets/images/icon_user.png",
                  ),
                )
              : CachedNetworkImage(
                  imageUrl: widget._image,
                  height: widget._size.width * 0.28125,
                  width: widget._size.width * 0.25,
                  fit: BoxFit.cover,
                  placeholder: (context, url) => Image.asset(
                    "assets/images/no_pic_image.jpeg",
                    fit: BoxFit.cover,
                  ),
                  errorWidget: (context, url, error) => Container(
                    padding: const EdgeInsets.all(4),
                    color: CustomColors.colorAccent.withOpacity(0.1),
                    height: widget._size.width * 0.28125,
                    width: widget._size.width * 0.25,
                    child: const Icon(
                      Icons.error,
                    ),
                  ),
                ),
        ),
        const SizedBox(width: 8),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getNameRow(context),
              getRatingBar(context),
              Text(
                widget._services,
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: CustomColors.colorGrayDark3,
                      fontSize: 12.0,
                    ),
              )
            ],
          ),
        ),
      ],
    );
  }

  Widget getRatingBar(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        RatingBarIndicator(
          rating: widget._rating.toDouble(),
          itemBuilder: (context, index) => const Icon(
            Icons.star,
            color: CustomColors.colorPrimary,
          ),
          itemCount: 5,
          itemSize: 18.0,
          direction: Axis.horizontal,
        ),
        const SizedBox(width: 5),
        Text(
          "(${widget._totalReviews})",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: CustomColors.colorGrayDark3,
                fontSize: 12.0,
              ),
        ),
      ],
    );
  }

  Widget getNameRow(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Text(
            widget._name,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontSize: 20.0,
                ),
          ),
        ),
        const SizedBox(width: 8),
        Image.asset(
          widget._isVerified
              ? "assets/images/icon_verified.png"
              : "assets/images/icon_not_verified.png",
          height: 15,
          width: 15,
        ),
        const SizedBox(width: 8),
      ],
    );
  }
}
