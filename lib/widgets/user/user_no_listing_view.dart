import 'package:flutter/material.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';

class UserNoListingView extends StatelessWidget {
  final VoidCallback newProjectClick;
  final String? customText;

  const UserNoListingView(
    this.newProjectClick, {
    this.customText,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Image.asset(
                "assets/images/no_listing.png",
                width: _screenSize.width * 0.85,
              ),
            ),
            SizedBox(height: _screenSize.height * 0.025),
            if (customText == null)
              Padding(
                padding: EdgeInsets.all(_screenSize.width * 0.035),
                child: FloatingActionButton(
                  onPressed: () {
                    newProjectClick();
                  },
                  // mini: true,
                  child: Padding(
                    padding: EdgeInsets.all(_screenSize.width * 0.04),
                    child: Image.asset(
                      "assets/images/icon_plus.png",
                      color: CustomColors.colorWhite,
                    ),
                  ),
                ),
              ),
            Text(
              customText ?? "Post a new project",
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
          ],
        ),
      ),
    );
  }
}
