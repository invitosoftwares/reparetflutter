import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../models/project_model.dart';
import '../../screens/common/images_details_screen.dart';
import '../../screens/common/play_video_screen.dart';
import '../../screens/user/edit_project_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../common/raised_button_primary.dart';

class ProjectDetailsDetailsUserView extends StatefulWidget {
  final bool showActions;
  final bool enableActions;
  final ProjectModel _projectBasicDetails;
  final Size _size;
  final VoidCallback deleteProjectClick;
  final VoidCallback onProjectEdited;

  const ProjectDetailsDetailsUserView(
    this._projectBasicDetails,
    this._size, {
    Key? key,
    this.enableActions = false,
    this.showActions = false,
    required this.deleteProjectClick,
    required this.onProjectEdited,
  }) : super(key: key);

  @override
  State<ProjectDetailsDetailsUserView> createState() =>
      _ProjectDetailsDetailsUserViewState();
}

class _ProjectDetailsDetailsUserViewState
    extends State<ProjectDetailsDetailsUserView> {
  Uint8List? uint8list;

  @override
  void initState() {
    super.initState();
    getVideoThumbnail();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: SizedBox(
        width: double.infinity,
        child: Card(
          elevation: 1,
          child: Padding(
            padding: EdgeInsets.all(widget._size.width * 0.02),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _getTitleText("Location :", context),
                _getValueText(
                    widget._projectBasicDetails.address ?? "", context),
                _getTitleText("Description :", context),
                _getValueText(
                    widget._projectBasicDetails.projectDescription ?? "",
                    context),
                _getTitleText("Attachments :", context),
                _getAttachments(context),
                const SizedBox(height: 10),
                if (widget.showActions) ...[
                  _getActionButtons(context),
                  const SizedBox(height: 10),
                  if ((widget._projectBasicDetails.allowVerifiedOnly ?? 0) == 1)
                    _getContractorText(context),
                ],
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _getAttachments(BuildContext context) {
    if ((widget._projectBasicDetails.projectimages?.isEmpty ?? true) &&
        (widget._projectBasicDetails.projectvideos?.isEmpty ?? true)) {
      _getValueText("N/A", context);
    }
//    Common.showToast("${widget._projectBasicDetails.projectimages?[0].image}");
    return SizedBox(
      height: widget._size.width * 0.2,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) =>
            (index < widget._projectBasicDetails.projectimages!.length)
                ? Padding(
                    padding: const EdgeInsets.only(right: 12.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                              context, ImagesDetailsScreen.routeName,
                              arguments: {
                                "IMAGE_URL": widget._projectBasicDetails
                                        .projectimages?[index].image ??
                                    "",
                              });
                        },
                        child: CachedNetworkImage(
                          imageUrl: widget._projectBasicDetails
                                  .projectimages?[index].image ??
                              "",
                          fit: BoxFit.cover,
                          height: widget._size.width * 0.2,
                          width: widget._size.width * 0.18,
                          placeholder: (context, url) => Image.asset(
                            "assets/images/no_pic_image.jpeg",
                            fit: BoxFit.cover,
                          ),
                          errorWidget: (context, url, error) => Container(
                            padding: const EdgeInsets.all(4),
                            color: CustomColors.colorAccent.withOpacity(0.1),
                            height: widget._size.width * 0.2,
                            width: widget._size.width * 0.18,
                            child: const Icon(
                              Icons.error,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                : Padding(
                    padding: const EdgeInsets.only(right: 12.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                            context,
                            PlayVideoScreen.routeName,
                            arguments: {
                              "VIDEO_URL": widget._projectBasicDetails
                                      .projectvideos?[0].video ??
                                  "",
                            },
                          );
                        },
                        child: (uint8list != null)
                            ? SizedBox(
                                height: widget._size.width * 0.2,
                                width: widget._size.width * 0.18,
                                child: Stack(
                                  alignment: Alignment.center,
                                  children: [
                                    Image.memory(
                                      uint8list!,
                                      fit: BoxFit.cover,
                                      height: widget._size.width * 0.2,
                                      width: widget._size.width * 0.18,
                                    ),
                                    Container(
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: CustomColors.colorAccent,
                                      ),
                                      child: const Icon(
                                        Icons.play_arrow,
                                        color: CustomColors.colorWhite,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Image.asset(
                                "assets/images/default_thumbnail.png",
                                height: widget._size.width * 0.2,
                                width: widget._size.width * 0.18,
                                fit: BoxFit.cover,
                              ),
                      ),
                    ),
                  ),
        itemCount: widget._projectBasicDetails.projectimages!.length +
            widget._projectBasicDetails.projectvideos!.length,
      ),
    );
  }

  Widget _getTitleText(String title, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 2.0),
      child: Text(
        title,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: CustomColors.colorAccent,
            ),
      ),
    );
  }

  Widget _getValueText(String? value, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        value ?? "N/A",
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: CustomColors.colorGrayDark2,
              fontSize: 15.0,
            ),
      ),
    );
  }

  Widget _getContractorText(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 4.0),
      child: Text(
        "Only verified contractors are allowed.",
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              color: CustomColors.colorAccent,
              fontSize: 12.0,
            ),
      ),
    );
  }

  _getActionButtons(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          InkWell(
            onTap: () {
              if (!widget.enableActions) {
                Common.showToast("You cant delete this project now.");
                return;
              }
              askForDeleteConfirmation();
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Delete",
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: widget.enableActions
                          ? CustomColors.colorAccent
                          : CustomColors.colorGrayDark3,
                    ),
              ),
            ),
          ),
          const SizedBox(width: 10),
          PrimaryRaisedButton(
            "Edit project",
            () async {
              dynamic result = await Navigator.pushNamed(
                context,
                EditProjectScreen.routeName,
                arguments: {
                  "PROJECT_MODEL": widget._projectBasicDetails,
                },
              );
              if (result != null && (result is bool) && result) {
                widget.onProjectEdited();
              }
            },
            onDisablePressed: () {
              Common.showToast("You cant edit this project now.");
            },
            size: widget._size,
            color: CustomColors.colorAccent,
            isDisabled: !widget.enableActions,
            leadingIcon: "assets/images/icon_edit.png",
          ),
        ],
      ),
    );
  }

  void getVideoThumbnail() async {
    if (widget._projectBasicDetails.projectvideos?.isNotEmpty ?? false) {
      uint8list = await VideoThumbnail.thumbnailData(
        video: widget._projectBasicDetails.projectvideos?[0].video ?? "",
        imageFormat: ImageFormat.JPEG,
        maxWidth: 128,
        quality: 25,
      );
    }

    setState(() {});
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void askForDeleteConfirmation() {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "Cancel",
      "Delete",
      "Are you sure you want to delete this project?",
      () {
        Navigator.pop(context);
      },
      () {
        Navigator.pop(context);
        widget.deleteProjectClick();
      },
    );
  }
}
