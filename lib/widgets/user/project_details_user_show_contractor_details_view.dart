import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../providers/rating_provider.dart';
import '../../providers/user_listings_provider.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/user/rate_contractor_popup.dart';

class ProjectDetailsUserShowContractorDetailsView extends StatelessWidget {
  final Size _size;
  final ProjectModel projectModel;
  final VoidCallback refreshData;

  const ProjectDetailsUserShowContractorDetailsView(
    this._size, {
    required this.projectModel,
    required this.refreshData,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(bottom: 10.0),
      child: Material(
        elevation: 1,
        color: CustomColors.colorWhite,
        borderRadius: BorderRadius.circular(5),
        child: Container(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: (projectModel.providerDetails?.image?.isEmpty ??
                            true)
                        ? Container(
                            color: CustomColors.colorPrimary.withOpacity(0.15),
                            height: _size.width * 0.2,
                            width: _size.width * 0.18,
                            child: Image.asset(
                              "assets/images/icon_user.png",
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl: projectModel.providerDetails?.image ?? "",
                            fit: BoxFit.cover,
                            height: _size.width * 0.2,
                            width: _size.width * 0.18,
                            placeholder: (context, url) => Image.asset(
                              "assets/images/no_pic_image.jpeg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, error) => Container(
                              padding: const EdgeInsets.all(4),
                              color: CustomColors.colorAccent.withOpacity(0.1),
                              height: _size.width * 0.2,
                              width: _size.width * 0.18,
                              child: const Icon(
                                Icons.error,
                              ),
                            ),
                          ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          getNameRow(context),
                          const SizedBox(height: 10),
                          getActionView(context),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              if (projectModel.projectStatus == Constants.PROJECT_PAID ||
                  projectModel.projectStatus ==
                      Constants.PROJECT_PAID_REVIEWED_BY_USER)
                ...getPaymentDetailsView(context),
              if (projectModel.projectStatus ==
                  Constants.PROJECT_PAID_REVIEWED_BY_USER)
                ...getRatingDetailsView(context, projectModel),
            ],
          ),
        ),
      ),
    );
  }

  Widget getNameRow(BuildContext context) {
    return Row(
      children: [
        Flexible(
          fit: FlexFit.loose,
          child: Text(
            "${projectModel.providerDetails?.firstName ?? ""} ${projectModel.providerDetails?.lastName ?? ""}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontSize: 18.0,
                ),
          ),
        ),
        if (projectModel.providerDetails?.providerVerified != null) ...[
          const SizedBox(width: 8),
          Image.asset(
            (projectModel.providerDetails?.providerVerified ?? false)
                ? "assets/images/icon_verified.png"
                : "assets/images/icon_not_verified.png",
            height: 15,
            width: 15,
          ),
          const SizedBox(width: 8),
        ]
      ],
    );
  }

  getActionView(BuildContext context) {
    if (projectModel.projectStatus == Constants.PROJECT_COMPLETED) {
      return Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(child: Container()),
          const SizedBox(width: 10),
          Consumer<UserListingProvider>(
            builder: (context, value, child) => Expanded(
              child: getButton(
                "Pay \$${projectModel.amount}",
                context,
                () async {
                  dynamic response = await Common.startPaymentProcessForProject(
                    context,
                    "${projectModel.projectId}",
                    value,
                  );

                  if (response is bool && response) {
                    value.updatePaidStatusLocally("${projectModel.projectId}");
                    refreshData();
                  }
                },
              ),
            ),
          ),
        ],
      );
    } else if (projectModel.projectStatus == Constants.PROJECT_PAID) {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            getPaidButton(context),
            const SizedBox(width: 10),
            getButton(
              "Leave review",
              context,
              () {
                _showLeaveReview(context, projectModel);
              },
            ),
          ],
        ),
      );
    } else if (projectModel.projectStatus ==
        Constants.PROJECT_PAID_REVIEWED_BY_USER) {
      return Row(
        children: [
          getPaidButton(context),
        ],
      );
    }
  }

  _showLeaveReview(BuildContext context, ProjectModel model) async {
    dynamic response = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return ChangeNotifierProvider(
          create: (context) => RatingProvider(),
          child: RateContractorPopup(
            projectId: "${model.projectId}",
          ),
        );
      },
    );

    if (response != null && (response is double)) {
      model.setRatingLocally(response);
      refreshData();

      UserListingProvider userListingProvider =
          Provider.of(context, listen: false);
      userListingProvider.updateRatingAndStatusLocally(
        "${projectModel.projectId}",
        response,
      );
    }
  }

  Widget getPaidButton(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 8.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: CustomColors.colorGreen,
          width: 1,
        ),
      ),
      child: Row(
        children: [
          const Icon(
            Icons.done,
            color: CustomColors.colorGreen,
            size: 15,
          ),
          const SizedBox(width: 5),
          Text(
            "Paid \$${projectModel.amount}",
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  fontSize: 13.0,
                  color: CustomColors.colorGreen,
                ),
          ),
        ],
      ),
    );
  }

  Widget getButton(
    String text,
    BuildContext context,
    VoidCallback click,
  ) {
    return Material(
      color: CustomColors.colorAccent,
      borderRadius: BorderRadius.circular(5),
      elevation: 1,
      child: InkWell(
        onTap: () {
          click();
        },
        child: Ink(
          padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 8.0),
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                CustomColors.colorAccent,
                CustomColors.colorRed,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: BorderRadius.all(
              Radius.circular(4.0),
            ),
          ),
          child: Text(
            text,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  fontSize: 14.0,
                ),
          ),
        ),
      ),
    );
  }

  getPaymentDetailsView(BuildContext context) {
    return [
      const SizedBox(height: 10),
      _getTitleText("Payment Details", context),
      Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Transaction Id:",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 15.0,
                ),
          ),
          const SizedBox(width: 4),
          Expanded(
            child: Text(
              projectModel.paymentDetails?.paymentId ?? "N/A",
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontSize: 14.0,
                    color: CustomColors.colorPrimary,
                  ),
            ),
          ),
        ],
      ),
      Row(
        children: [
          Text(
            "Transaction date:",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 15.0,
                ),
          ),
          const SizedBox(width: 4),
          Expanded(
            child: Text(
              Common.getConvertedDate(
                projectModel.paymentDetails?.paymentDate ?? "",
                Constants.DATE_FORMAT_4,
                Constants.DATE_FORMAT_3,
                isUtc: true,
              ),
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontSize: 15.0,
                    color: CustomColors.colorPrimary,
                  ),
            ),
          ),
        ],
      ),
      const SizedBox(height: 10),
    ];
  }

  getRatingDetailsView(BuildContext context, ProjectModel projectModel) {
    return [
      _getTitleText("Review Details", context),
      const SizedBox(height: 5),
      Align(
        alignment: Alignment.center,
        child: RatingBarIndicator(
          rating: projectModel.rating?.toDouble() ?? 0.0,
          itemBuilder: (context, index) => const Icon(
            Icons.star,
            color: CustomColors.colorPrimary,
          ),
          itemCount: 5,
          itemSize: 30.0,
          direction: Axis.horizontal,
        ),
      ),
      const SizedBox(height: 5),
      Text(
        projectModel.review ?? "",
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              fontSize: 15.0,
            ),
      ),
      const SizedBox(height: 10),
    ];
  }

  _getTitleText(String title, BuildContext context) {
    return Text(
      title,
      style: Theme.of(context).textTheme.headline2!.copyWith(
            fontSize: 16.0,
          ),
    );
  }
}
