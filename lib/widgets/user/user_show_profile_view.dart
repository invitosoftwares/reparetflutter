import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/change_password.dart';
import '../../widgets/common/my_outlined_button.dart';
import '../../widgets/common/neumorphic_view.dart';
import '../../widgets/common/show_neumorphic_details.dart';

class UserShowProfileView extends StatefulWidget {
  final VoidCallback? _onEditClicked;

  UserShowProfileView(this._onEditClicked);

  @override
  State<UserShowProfileView> createState() => _UserShowProfileViewState();
}

class _UserShowProfileViewState extends State<UserShowProfileView> {
  late Size _screenSize;
  bool isFirst = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    // if (isFirst) {
    //   isFirst = false;
    //   AuthProvider authProvider =
    //       Provider.of<AuthProvider>(context, listen: false);
    //   authProvider.userModel?.image =
    //       await UploadFileS3.getImageUrl(authProvider.userModel?.image);
    //   setState(() {});
    // }
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(_screenSize.width * 0.05),
      child: Consumer<AuthProvider>(
        builder: (context, authValue, child) => SingleChildScrollView(
          child: Column(
            children: [
              _getEditButton(),
              SizedBox(height: _screenSize.width * 0.05),
              _getUserPicView(authValue.userModel?.image ?? ""),
              SizedBox(height: _screenSize.width * 0.1),
              ShowNeumorphicDetails(
                "${authValue.userModel?.firstName} ${authValue.userModel?.lastName}",
                icon: "assets/images/icon_name.png",
              ),
              SizedBox(height: _screenSize.width * 0.02),
              Visibility(
                visible: false,
                child: ShowNeumorphicDetails(
                  "${authValue.userModel?.phone}",
                  icon: "assets/images/icon_phone.png",
                ),
              ),
              SizedBox(height: _screenSize.width * 0.05),
              _getChangePasswordButton(context),
              SizedBox(height: _screenSize.width * 0.05),
            ],
          ),
        ),
      ),
    );
  }

  _getEditButton() {
    return Align(
      alignment: Alignment.centerRight,
      child: NeumorphicContainer(
        onPressed: () {
          if (widget._onEditClicked != null) widget._onEditClicked!();
        },
        child: MyOutlinedButton(
          "Edit",
          icon: "assets/images/icon_edit.png",
        ),
      ),
    );
  }

  _getChangePasswordButton(BuildContext context) {
    return NeumorphicContainer(
      onPressed: () {
        showModalBottomSheet<void>(
          context: context,
          builder: (context) => Padding(
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: ChangePasswordView(Constants.USER),
          ),
          isScrollControlled: true,
          backgroundColor: Colors.transparent,
        );
      },
      child: MyOutlinedButton(
        "Change password",
        isExpanded: true,
      ),
    );
  }

  _getUserPicView(String image) {
    Widget child = (image.isEmpty)
        ? Container(
            width: _screenSize.width * 0.35,
            decoration: const BoxDecoration(
              color: CustomColors.colorGrayLight2,
              shape: BoxShape.circle,
            ),
            child: Image.asset(
              "assets/images/icon_user.png",
              color: CustomColors.colorAccent,
            ),
          )
        : ClipRRect(
            borderRadius: BorderRadius.circular(_screenSize.width * 0.175),
            child: FadeInImage.assetNetwork(
              placeholder: "assets/images/profile-user.png",
              image: image,
              width: _screenSize.width * 0.35,
              height: _screenSize.width * 0.35,
              fit: BoxFit.cover,
              imageErrorBuilder: (BuildContext context, Object exception,
                  StackTrace? stackTrace) {
                return Image.asset("assets/images/profile-user.png");
              },
            ),
            //     CachedNetworkImage(
            //   width: _screenSize.width * 0.35,
            //   height: _screenSize.width * 0.35,
            //   fit: BoxFit.cover,
            //   imageUrl: image,
            //   errorWidget: (context, url, error) => Container(
            //     padding: const EdgeInsets.all(4),
            //     color: CustomColors.colorAccent.withOpacity(0.1),
            //     width: _screenSize.width * 0.35,
            //     height: _screenSize.width * 0.35,
            //     child: const Icon(
            //       Icons.error,
            //     ),
            //   ),
            // ),
          );
    return NeumorphicContainer(
      child: child,
      radius: _screenSize.width * 0.175,
      onPressed: () {},
    );
  }
}
