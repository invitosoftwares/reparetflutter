import 'dart:async';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/user_listings_provider.dart';
import '../../screens/user/project_details_for_user_screen.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/no_listings_searched.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/user/item_completed_listing_user.dart';
import '../../widgets/user/user_no_listing_view.dart';

class UserCompletedListingsData extends StatefulWidget {
  final Size _size;
  final VoidCallback newProjectClick;
  final StreamController queryStreamController;

  const UserCompletedListingsData(
    this._size, {
    Key? key,
    required this.newProjectClick,
    required this.queryStreamController,
  }) : super(key: key);

  @override
  State<UserCompletedListingsData> createState() =>
      _UserCompletedListingsDataState();
}

class _UserCompletedListingsDataState extends State<UserCompletedListingsData> {
  int _loaderType = 0;
  bool _isFirstTime = true;
  UserListingProvider? _userListingProvider;
  int _page = 0;
  bool _isNoInternet = false;
  String _searchQuery = "";

  @override
  void dispose() {
    super.dispose();
    widget.queryStreamController.stream.listen((event) {}).cancel();
  }

  @override
  initState() {
    super.initState();
    widget.queryStreamController.stream.listen((event) {
      if (_searchQuery == event) {
        return;
      }
      _page = 0;
      if (event == null || (event is! String) || (event).isEmpty) {
        _searchQuery = "";
      } else {
        _searchQuery = event;
      }
      _getAllCompletedProjects();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _userListingProvider = Provider.of<UserListingProvider>(
        context,
        listen: false,
      );

      _loaderType = _userListingProvider!.allCompletedProjects.isEmpty ? 1 : 0;
      _page = 0;
      _getAllCompletedProjects(
        showLoader: _userListingProvider!.allCompletedProjects.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllCompletedProjects(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<UserListingProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllCompletedProjects(showLoader: false);
                      },
                      child: (provider.allCompletedProjects.isEmpty &&
                              _searchQuery.isEmpty)
                          ? UserNoListingView(widget.newProjectClick)
                          : (provider.allCompletedProjects.isEmpty &&
                                  _searchQuery.isNotEmpty)
                              ? const NoListingsSearched(
                                  "assets/images/empty_search_user_listings.png",
                                )
                              : ListView.builder(
                                  itemBuilder: (context, index) {
                                    if (index ==
                                            provider.allCompletedProjects
                                                    .length -
                                                min(
                                                    3,
                                                    provider
                                                        .allCompletedProjects
                                                        .length) &&
                                        provider.hasMoreCompletedListings &&
                                        _loaderType == 0) {
                                      _page++;
                                      _getAllCompletedProjects();
                                    }
                                    if (index ==
                                            provider
                                                .allCompletedProjects.length &&
                                        provider.hasMoreCompletedListings) {
                                      return Container();
                                    }
                                    return Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: ChangeNotifierProvider.value(
                                        value: provider
                                            .allCompletedProjects[index],
                                        child: ItemCompletedListingUser(
                                          widget._size,
                                          onPaymentDone: () {
                                            updatePaidStatusLocally(
                                                "${provider.allCompletedProjects[index].projectId}");
                                          },
                                          click: () async {
                                            Navigator.pushNamed(
                                              context,
                                              ProjectDetailsForUserScreen
                                                  .routeName,
                                              arguments: {
                                                "PROJECT_ID":
                                                    "${provider.allCompletedProjects[index].projectId}",
                                                "PROVIDER":
                                                    _userListingProvider,
                                              },
                                            );
                                          },
                                          userListingProvider:
                                              _userListingProvider!,
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount:
                                      provider.allCompletedProjects.length,
                                ),
                    ),
                  ),
        if (_loaderType == 2)
          const Padding(
            padding: EdgeInsets.only(bottom: 8.0),
            child: SizedBox(
              height: 10,
              width: 10,
              child: CircularProgressIndicator(
                color: CustomColors.colorAccent,
                strokeWidth: 2,
              ),
            ),
          ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllCompletedProjects({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _userListingProvider!.getAllCompletedProjects(
        context,
        "$_page",
        _searchQuery,
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }

  updatePaidStatusLocally(String projectId) {
    _userListingProvider!.updatePaidStatusLocally(projectId);
  }
}
