import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/categories_provider.dart';
import '../../utils/custom_colors.dart';

class SelectProjectCategoryPopup extends StatelessWidget {
  final Function _onSelected;

  const SelectProjectCategoryPopup(
    this._onSelected, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Center(
      child: SizedBox(
        width: size.width * 0.75,
        child: Material(
          elevation: 5,
          borderRadius: BorderRadius.circular(5),
          color: CustomColors.colorGrayLight2,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(
                  horizontal: 5,
                  vertical: 10,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: CustomColors.colorAccent,
                ),
                child: const Text(
                  "Select project category (optional)",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: CustomColors.colorGrayLight3,
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0,
                  ),
                ),
              ),
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: size.height * 0.7,
                ),
                child: Consumer<CategoriesProvider>(
                  builder: (context, value, child) => ListView.builder(
                    shrinkWrap: true,
                    itemBuilder: (context, index) => buildItem(index, context),
                    itemCount: value.allCategories.length,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildItem(int index, BuildContext context) {
    return Consumer<CategoriesProvider>(
      builder: (context, value, child) => InkWell(
        onTap: () {
          _onSelected(value.allCategories[index]);
          Navigator.pop(context);
        },
        child: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  value.allCategories[index].toString(),
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(fontSize: 15),
                ),
              ),
              if (index != value.allCategories.length)
                Container(
                  width: double.infinity,
                  margin: const EdgeInsets.symmetric(horizontal: 5),
                  height: 1,
                  color: CustomColors.colorGrayDark,
                ),
            ],
          ),
        ),
      ),
    );
  }
}
