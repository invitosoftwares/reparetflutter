import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../models/project_model.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class ItemInviteProject extends StatelessWidget {
  final Size _size;

  const ItemInviteProject(this._size, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProjectModel>(
      builder: (context, model, child) => Card(
        elevation: 1,
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: IntrinsicHeight(
            child: Row(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: CachedNetworkImage(
                    imageUrl: model.projectimages?[0].image ?? "",
                    fit: BoxFit.cover,
                    height: _size.width * 0.16875,
                    width: _size.width * 0.15,
                    placeholder: (context, url) => Image.asset(
                      "assets/images/no_pic_image.jpeg",
                      fit: BoxFit.cover,
                    ),
                    errorWidget: (context, url, error) => Container(
                      padding: const EdgeInsets.all(4),
                      color: CustomColors.colorAccent.withOpacity(0.1),
                      height: _size.width * 0.16875,
                      width: _size.width * 0.15,
                      child: const Icon(
                        Icons.error,
                      ),
                    ),
                  ),
                ),
                const SizedBox(width: 8),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 4.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          model.projectTitle ?? "",
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: CustomColors.colorAccent,
                                    fontSize: 18.0,
                                  ),
                        ),
                        const SizedBox(height: 5),
                        Row(
                          children: [
                            Image.asset(
                              "assets/images/icon_date_range.png",
                              height: 10,
                              width: 10,
                            ),
                            const SizedBox(width: 4),
                            Text(
                              Common.getConvertedDate(
                                model.projectTime ?? "",
                                Constants.DATE_FORMAT_4,
                                Constants.DATE_FORMAT_3,
                                isUtc: true,
                              ),
                              style: const TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
