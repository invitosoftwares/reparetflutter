import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/bottom_bar_with_indicator.dart';

class UserDashboardNavigationBar extends StatefulWidget {
  final Function(int) _onTap;
  final int _currentIndex;
  final Size _screenSize;

  UserDashboardNavigationBar(this._onTap, this._currentIndex, this._screenSize);

  @override
  _UserDashboardNavigationBarState createState() =>
      _UserDashboardNavigationBarState();
}

class _UserDashboardNavigationBarState
    extends State<UserDashboardNavigationBar> {
  @override
  Widget build(BuildContext context) {
    final List<TitledNavigationBarItem> items = [
      TitledNavigationBarItem("assets/images/icon_plus.png", "Project", 0),
      TitledNavigationBarItem("assets/images/icon_listings.png", "Listings", 0),
      TitledNavigationBarItem("assets/images/icon_chat.png", "Chat", 0),
      TitledNavigationBarItem("assets/images/icon_search.png", "Contractor", 0),
    ];

    return TitledBottomNavigationBar(
      onTap: widget._onTap,
      items: items,
      currentIndex: widget._currentIndex,
      screenSize: widget._screenSize,
      selectedColor: CustomColors.colorAccent,
      unSelectedColor: CustomColors.colorGrayDark2,
      showIndicator: true,
    );
  }
}
