import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import '../../models/project_chats_parent_model.dart';
import '../../providers/project_chats_provider.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/project_list_shimmer.dart';

class ProjectDetailsNewShowAllMessages extends StatefulWidget {
  final Size _size;
  final String projectId;

  const ProjectDetailsNewShowAllMessages(
    this._size, {
    Key? key,
    required this.projectId,
  }) : super(key: key);

  @override
  State<ProjectDetailsNewShowAllMessages> createState() =>
      _ProjectDetailsNewShowAllMessagesState();
}

class _ProjectDetailsNewShowAllMessagesState
    extends State<ProjectDetailsNewShowAllMessages> {
  ProjectChatsProvider? _projectChatsProvider;
  int _loaderType = 0;
  bool _isFirstTime = true;
  int _page = 0;
  bool _isNoInternet = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _projectChatsProvider = Provider.of<ProjectChatsProvider>(
        context,
        listen: false,
      );

      _loaderType = _projectChatsProvider!.allChats.isEmpty ? 1 : 0;
      _page = 0;

      _getAllChats(
        showLoader: _projectChatsProvider!.allChats.isEmpty,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        _isNoInternet
            ? NoInternetView(
                () => _getAllChats(),
              )
            : (_loaderType == 1)
                ? const ProjectListShimmer()
                : Consumer<ProjectChatsProvider>(
                    builder: (context, provider, child) => RefreshIndicator(
                      color: CustomColors.colorAccent,
                      onRefresh: () async {
                        if (_loaderType != 0) return;
                        _page = 0;
                        await _getAllChats(showLoader: false);
                      },
                      child: (provider.allChats.isEmpty)
                          ? Align(
                              alignment: Alignment.center,
                              child: InkWell(
                                onTap: () {
                                  _page = 0;
                                  _getAllChats();
                                },
                                child: Text(
                                  "No chats yet! Tap to retry.",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(fontWeight: FontWeight.bold),
                                ),
                              ),
                            )
                          : ListView.builder(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 2.0),
                              itemBuilder: (context, index) {
                                if (index ==
                                        provider.allChats.length -
                                            min(3, provider.allChats.length) &&
                                    provider.hasMore &&
                                    _loaderType == 0) {
                                  _page++;
                                  _getAllChats();
                                }
                                if (index == provider.allChats.length &&
                                    provider.hasMore) {
                                  return Container();
                                }
                                return _getMessageView(
                                  context,
                                  provider.allChats[index],
                                );
                              },
                              itemCount: provider.allChats.length,
                            ),
                    ),
                  ),
        if (_loaderType == 2)
          const Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 8.0),
              child: SizedBox(
                height: 10,
                width: 10,
                child: CircularProgressIndicator(
                  color: CustomColors.colorAccent,
                  strokeWidth: 2,
                ),
              ),
            ),
          ),
      ],
    );
  }

  _getMessageView(BuildContext context, ProjectChat model) {
    return InkWell(
      onTap: () async {
        String consumerId =
            (await Common.getUserModelFromSharedPreferences())?.uuid ?? "";
        Common.getChatScreen(
          context,
          consumerId: consumerId,
          providerId: model.providerId ?? "",
          projectId: int.parse(widget.projectId),
          name: model.name ?? "",
        );
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: Material(
          elevation: 1,
          color: CustomColors.colorWhite,
          borderRadius: BorderRadius.circular(5),
          child: Container(
            padding: const EdgeInsets.all(4.0),
            child: IntrinsicHeight(
              child: Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(5),
                    child: (model.image?.isEmpty ?? true)
                        ? Container(
                            color: CustomColors.colorAccent.withOpacity(0.15),
                            height: widget._size.width * 0.15,
                            width: widget._size.width * 0.13,
                            child: Image.asset(
                              "assets/images/icon_user.png",
                            ),
                          )
                        : CachedNetworkImage(
                            imageUrl: model.image!,
                            fit: BoxFit.cover,
                            height: widget._size.width * 0.15,
                            width: widget._size.width * 0.13,
                            placeholder: (context, url) => Image.asset(
                              "assets/images/no_pic_image.jpeg",
                              fit: BoxFit.cover,
                            ),
                            errorWidget: (context, url, error) => Container(
                              padding: const EdgeInsets.all(4),
                              color: CustomColors.colorAccent.withOpacity(0.1),
                              height: widget._size.width * 0.15,
                              width: widget._size.width * 0.13,
                              child: const Icon(
                                Icons.error,
                              ),
                            ),
                          ),
                  ),
                  const SizedBox(width: 8),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 6.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          getNameRow(context, model),
                          getMessageRow(context, model),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getNameRow(BuildContext context, ProjectChat model) {
    int count = model.unreadCount ?? 0;
    return Row(
      children: [
        Expanded(
          child: Text(
            model.name ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontSize: 18.0,
                ),
          ),
        ),
        const SizedBox(width: 8),
        if (count != 0)
          Container(
            padding: EdgeInsets.all(count < 10 ? 6 : 4),
            decoration: const BoxDecoration(
              color: CustomColors.colorPrimary,
              shape: BoxShape.circle,
            ),
            child: Text(
              (count < 10 ? "$count" : "9+"),
              style: const TextStyle(
                color: Colors.white,
                height: 1,
                fontSize: 8,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        const SizedBox(width: 8),
      ],
    );
  }

  Widget getMessageRow(BuildContext context, ProjectChat model) {
    return Row(
      children: [
        Expanded(
          child: Text(
            model.lastMessage ?? "",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: CustomColors.colorGrayDark3,
                  fontSize: 12.0,
                ),
          ),
        ),
        const SizedBox(width: 8),
        Text(
          Common.getConvertedDate(
            model.lastMessageAt ?? "",
            Constants.DATE_FORMAT_4,
            Constants.DATE_FORMAT_6,
            isUtc: true,
          ),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: CustomColors.colorGrayDark3,
                fontSize: 10.0,
              ),
        ),
        const SizedBox(width: 8),
      ],
    );
  }

  Widget getRatingBar(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        RatingBarIndicator(
          rating: 4.5,
          itemBuilder: (context, index) => const Icon(
            Icons.star,
            color: CustomColors.colorPrimary,
          ),
          itemCount: 5,
          itemSize: 17.0,
          direction: Axis.horizontal,
        ),
        const SizedBox(width: 5),
        Text(
          "(45)",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: CustomColors.colorGrayDark3,
                fontSize: 12.0,
              ),
        ),
      ],
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getAllChats({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _projectChatsProvider!.getProjectChats(
        context,
        widget.projectId,
        "$_page",
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }
}
