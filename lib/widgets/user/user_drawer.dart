import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';
import '../../utils/custom_colors.dart';

class UserDrawer extends StatelessWidget {
  final Size _screenSize;

  static final int PROFILE = 0;
  static final int FAVOURITES = 1;
  static final int PAYMENT_METHODS = 2;
  static final int PRIVACY_POLITY = 3;
  static final int SUPPORT = 4;
  static final int LOGOUT = 5;
  static final int DELETE_ACCOUNT = 6;

  final Function _onClicked;

  UserDrawer(this._screenSize, this._onClicked);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: CustomColors.colorDarkDrawerBackground,
      ),
      child: BackdropFilter(
        filter: ImageFilter.blur(
          sigmaX: 3,
          sigmaY: 3,
        ),
        child: Drawer(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: _screenSize.width * 0.05),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    SizedBox(height: _screenSize.height * 0.05),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Settings",
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    fontWeight: FontWeight.bold,
                                  ),
                        ),
                        IconButton(
                          icon: Image.asset("assets/images/icon_cross.png"),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                    getYellowTextOptions("Profile",
                        "assets/images/icon_user.png", PROFILE, context),
                    getYellowTextOptions("Favourites",
                        "assets/images/icon_heart.png", FAVOURITES, context),
                    Visibility(
                      visible: false,
                      child: getYellowTextOptions(
                          "Payment Methods",
                          "assets/images/icon_card.png",
                          PAYMENT_METHODS,
                          context),
                    ),
                  ],
                ),
                Column(
                  children: [
                    getWhiteTextOptions(
                      "Delete Account",
                      DELETE_ACCOUNT,
                      context,
                    ),
                    getWhiteTextOptions(
                        "Privacy Policy", PRIVACY_POLITY, context),
                    getWhiteTextOptions("Support", SUPPORT, context),
                    getWhiteTextOptions("Log Out", LOGOUT, context),
                    getBottomView(context)
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget getYellowTextOptions(
      String text, String image, int option, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
        _onClicked(option);
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Row(
          children: [
            Image.asset(
              image,
              color: CustomColors.colorAccent,
              height: 18,
              width: 18,
            ),
            SizedBox(width: 15),
            Expanded(
              child: Text(
                text,
                style: Theme.of(context)
                    .textTheme
                    .headline1!
                    .copyWith(color: CustomColors.colorAccent),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getWhiteTextOptions(String text, int option, BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
        _onClicked(option);
      },
      child: Padding(
        padding: const EdgeInsets.only(bottom: 20.0),
        child: Container(
          width: double.infinity,
          child: Text(
            text,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorGrayLight2,
                  fontSize: 18,
                ),
          ),
        ),
      ),
    );
  }

  Widget getBottomView(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Stack(
        alignment: Alignment.bottomRight,
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: FutureBuilder(
              builder: (context, AsyncSnapshot<PackageInfo> snapshot) => Text(
                "REPARET${snapshot.connectionState == ConnectionState.done && snapshot.data != null ? ""
                    " | App ver. ${snapshot.data?.version ?? ""}+${snapshot.data?.buildNumber ?? ""}" : ""}",
                style: Theme.of(context).textTheme.headline1!.copyWith(
                      color: CustomColors.colorGrayLight2,
                      fontWeight: FontWeight.normal,
                      fontSize: 12,
                    ),
              ),
              future: PackageInfo.fromPlatform(),
            ),
          ),
          Image.asset(
            "assets/images/small_logo_without_name.png",
            width: 40,
            height: 40,
          )
        ],
      ),
    );
  }
}
