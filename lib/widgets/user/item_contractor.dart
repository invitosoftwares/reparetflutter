import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';

import '../../models/search_providers_parent_model.dart';
import '../../screens/user/contractor_profile_screen.dart';
import '../../utils/custom_colors.dart';

class ItemContractorSearch extends StatelessWidget {
  final Size _size;
  final VoidCallback toggleFavourite;

  const ItemContractorSearch(
    this._size, {
    required this.toggleFavourite,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<ProviderModel>(
      builder: (context, model, child) => InkWell(
        onTap: () {
          Navigator.pushNamed(
            context,
            ContractorProfileScreen.routeName,
            arguments: {
              "PROVIDER_ID":
                  model.providerId == null ? "" : "${model.providerId}",
            },
          );
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Material(
            elevation: 1,
            color: CustomColors.colorWhite,
            borderRadius: BorderRadius.circular(5),
            child: Container(
              padding: const EdgeInsets.all(4.0),
              child: IntrinsicHeight(
                child: Row(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(5),
                      child: (model.image == null || model.image!.isEmpty)
                          ? Image.asset(
                              "assets/images/no_pic_image.jpeg",
                              height: _size.width * 0.225,
                              width: _size.width * 0.2,
                              fit: BoxFit.cover,
                            )
                          : CachedNetworkImage(
                              imageUrl: model.image ?? "",
                              height: _size.width * 0.225,
                              width: _size.width * 0.2,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Image.asset(
                                "assets/images/no_pic_image.jpeg",
                                fit: BoxFit.cover,
                              ),
                              errorWidget: (context, url, error) => Container(
                                padding: const EdgeInsets.all(4),
                                color:
                                    CustomColors.colorAccent.withOpacity(0.1),
                                height: _size.width * 0.225,
                                width: _size.width * 0.2,
                                child: const Icon(
                                  Icons.error,
                                ),
                              ),
                            ),
                    ),
                    const SizedBox(width: 8),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 6.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            getNameRow(context, model),
                            getRatingBar(context, model),
                            getServicesView(context, model),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getNameRow(BuildContext context, ProviderModel model) {
    return Row(
      children: [
        Expanded(
          child: Text(
            "${model.firstName} ${model.lastName}",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontSize: 18.0,
                ),
          ),
        ),
        InkWell(
          onTap: () {
            toggleFavourite();
          },
          borderRadius: BorderRadius.circular(20),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Image.asset(
              (model.favourite ?? false)
                  ? "assets/images/icon_heart_filled.png"
                  : "assets/images/icon_heart_empty.png",
              color: CustomColors.colorPrimary,
              height: 15,
              width: 15,
            ),
          ),
        ),
        const SizedBox(width: 8),
      ],
    );
  }

  Widget getServicesView(BuildContext context, ProviderModel model) {
    return Row(
      children: [
        Expanded(
          child: Text(
            model.categories?.join(", ") ?? "",
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: CustomColors.colorGrayDark3,
                  fontSize: 11.0,
                ),
          ),
        ),
        if (model.providerVerified != null)
          Padding(
            padding: const EdgeInsets.only(right: 5.0),
            child: Row(
              children: [
                Text(
                  (model.providerVerified ?? false)
                      ? "Verified"
                      : "Not verified",
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: (model.providerVerified ?? false)
                            ? CustomColors.colorGreen
                            : CustomColors.colorRed2,
                        fontSize: 11.0,
                      ),
                ),
                const SizedBox(width: 5),
                Image.asset(
                  (model.providerVerified ?? false)
                      ? "assets/images/icon_verified.png"
                      : "assets/images/icon_not_verified.png",
                  height: 15,
                  width: 15,
                ),
              ],
            ),
          ),
        const SizedBox(width: 8),
      ],
    );
  }

  Widget getRatingBar(BuildContext context, ProviderModel model) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        RatingBarIndicator(
          rating: model.rating == null
              ? 0.0
              : double.parse(model.rating.toString()),
          itemBuilder: (context, index) => const Icon(
            Icons.star,
            color: CustomColors.colorPrimary,
          ),
          itemCount: 5,
          itemSize: 17.0,
          direction: Axis.horizontal,
        ),
        const SizedBox(width: 5),
        Text(
          "(${model.totalRatings ?? 0})",
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                color: CustomColors.colorGrayDark3,
                fontSize: 12.0,
              ),
        ),
      ],
    );
  }
}
