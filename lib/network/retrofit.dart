import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'retrofit.g.dart';

@RestApi(baseUrl: "https://dev2.invitocreatives.com:3002/api/v1/")
abstract class Retrofit {
  factory Retrofit(Dio dio) = _Retrofit;

  @POST("/auth/login")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> loginAPI(
    @Field("email") String email,
    @Field("password") String password,
    @Field("device_id") String deviceId,
    @Field("device_token") String deviceToken,
    @Field("device_type") String deviceType, //1 => android, 2 => IOS
  );

  @POST("/auth/user/register")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> registerUserAPI(
    @Field("email") String email,
    @Field("first_name") String firstName,
    @Field("last_name") String lastName,
    @Field("password") String password,
    @Field("country_code") String countryCode,
    @Field("phone") String phone,
    @Field("image") String image,
  );

  @POST("/auth/provider/register")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> registerContractorAPI(
    @Field("email") String email,
    @Field("first_name") String firstName,
    @Field("last_name") String lastName,
    @Field("password") String password,
    @Field("country_code") String countryCode,
    @Field("phone") String phone,
    @Field("image") String image,
    @Field("company_name") String companyName,
    @Field("lat") String lat,
    @Field("lng") String lng,
    @Field("location") String location,
  );

  @POST("/auth/forgotpassword")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> forgotPasswordAPI(
    @Field("email") String email,
  );

  @POST("/auth/changepassword")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> changePasswordAPI(
    @Field("password") String password,
    @Field("new_password") String newPassword,
  );

  @POST("/common/delete-user-account")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> deleteAccountAPI(
    @Field("password") String password,
  );

  @POST("/user/payments/delete-cards")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> deleteCard(
    @Field("pm_id") String pmId,
  );

  @POST("/user/project/payment/confirm")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> confirmProjectPayment(
    @Field("project_id") String projectId,
    @Field("payment_id") String paymentId,
    @Field("payment_data") String paymentData,
  );

  @POST("/user/project")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> createProject(
    @Field("project_title") String projectTitle,
    @Field("project_description") String projectDescription,
    @Field("category_id") String categoryId,
    @Field("address") String address,
    @Field("lat") String lat,
    @Field("lng") String lng,
    /*@Field("project_date") String projectDate,
    @Field("project_time") String projectTime,*/
    @Field("allow_verified_only") String allowVerifiedOnly,
    @Field("videos") String videos,
    @Field("images") String images,
  );

  @PUT("/user/project")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> editProject(
    @Field("project_id") String projectId,
    @Field("project_title") String projectTitle,
    @Field("project_description") String projectDescription,
    @Field("category_id") String categoryId,
    @Field("address") String address,
    @Field("lat") String lat,
    @Field("lng") String lng,
    /*@Field("project_date") String projectDate,
    @Field("project_time") String projectTime,*/
    @Field("allow_verified_only") String allowVerifiedOnly,
    @Field("videos") String videos,
    @Field("images") String images,
  );

  @POST("/user/project/details")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> getUserProjectDetails(
    @Field("project_id") String projectId,
  );

  @POST("/provider/project/owner-details")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> getOwnerDetails(
    @Field("project_id") String projectId,
  );

  @POST("/provider/project/details")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> getContractorProjectDetails(
    @Field("project_id") String projectId,
  );

  @POST("/provider/project/bid")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> makeBid(
    @Field("project_id") String projectId,
    @Field("amount") String amount,
  );

  @POST("/user/project/bids")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> getProjectBids(
    @Field("project_id") String projectId,
    @Field("page") String page,
  );

  @POST("/user/project/chats")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> getProjectChats(
    @Field("project_id") String projectId,
    @Field("page") String page,
  );

  @POST("/user/bid/confirm")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> confirmBid(
    @Field("bid_id") String bidId,
  );

  @POST("/user/provider/details")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> getProviderDetails(
    @Field("provider_id") String providerId,
  );

  @POST("/provider/project/bid/cancel")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> cancelBid(
    @Field("project_id") String projectId,
  );

  @POST("/provider/project/complete")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> markAsDone(
    @Field("project_id") String projectId,
  );

  @POST("/user/project/payment")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> payForProject(
    @Field("project_id") String projectId,
    @Field("pm_id") String pmId,
  );

  @POST("/user/favourite")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> toggleFavourite(
    @Field("provider_id") String providerId,
  );

  @POST("/user/project/invitation")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> sendInvitation(
    @Field("project_id") String projectId,
    @Field("provider_id") String providerId,
  );

  @POST("/provider/withdraw")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> withdrawMoney(
    @Field("password") String password,
  );

  @POST("/user/review/add")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> addReviewByUser(
    @Field("project_id") String projectId,
    @Field("rating") String rating,
    @Field("review") String review,
  );

  @POST("/provider/review/add")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> addReviewByProvider(
    @Field("project_id") String projectId,
    @Field("rating") String rating,
    @Field("review") String review,
  );

  @PUT("/user/profile/update")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> updateUserProfile(
    @Field("first_name") String firstName,
    @Field("last_name") String lastName,
    @Field("phone") String phone,
    @Field("image") String image,
  );

  @PUT("/provider/profile/update")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> updateProviderProfile(
    @Field("first_name") String firstName,
    @Field("last_name") String lastName,
    @Field("phone") String phone,
    @Field("image") String image,
    @Field("categories") String categories,
    @Field("company_name") String companyName,
    @Field("lat") String lat,
    @Field("lng") String lng,
    @Field("location") String location,
    @Field("bio") String bio,
  );

  @PUT("/user/project")
  @FormUrlEncoded()
  Future<HttpResponse<dynamic>> updateProject(
    @Field("project_id") String projectId,
    @Field("project_title") String projectTitle,
    @Field("project_description") String projectDescription,
    @Field("category_id") String categoryId,
    @Field("address") String address,
    @Field("lat") String lat,
    @Field("lng") String lng,
    @Field("project_date") String projectDate,
    @Field("project_time") String projectTime,
    @Field("allow_verified_only") String allowVerifiedOnly,
    @Field("videos") String videos,
    @Field("images") String images,
  );

  @GET("/auth/logout")
  Future<HttpResponse<dynamic>> logoutAPI();

  @GET("/common/userdetails")
  Future<HttpResponse<dynamic>> getProfileDetails();

  @GET("/user/payments/create-setup-intent")
  Future<HttpResponse<dynamic>> createSetupIntent();

  @GET("/user/payments/get-cards")
  Future<HttpResponse<dynamic>> getPaymentMethods();

  @GET("/common/categories")
  Future<HttpResponse<dynamic>> getAllCategories();

  @GET("/provider/get-onboarding-url")
  Future<HttpResponse<dynamic>> getAccountLinkURL();

  @GET("/provider/login-link")
  Future<HttpResponse<dynamic>> getStripeLoginURL();

  @GET("/user/project")
  Future<HttpResponse<dynamic>> getUserProjects(
    @CancelRequest() CancelToken cancelToken,
    @Query("page") String page,
    @Query("status") String status,
    @Query("search") String search,
  );

  @GET("/provider/project")
  Future<HttpResponse<dynamic>> getProviderProjects(
    @CancelRequest() CancelToken cancelToken,
    @Query("page") String page,
    @Query("status") String status,
    @Query("search") String search,
  );

  @GET("/user/providers")
  Future<HttpResponse<dynamic>> searchProviders(
    @CancelRequest() CancelToken cancelToken,
    @Query("page") String page,
    @Query("search") String search,
  );

  @GET("/user/favourite/list")
  Future<HttpResponse<dynamic>> getFavouriteProviders(
    @CancelRequest() CancelToken cancelToken,
    @Query("page") String page,
  );

  @GET("/provider/project/invitations")
  Future<HttpResponse<dynamic>> getInvitations(
    @Query("page") String page,
  );

  @DELETE("/user/project")
  Future<HttpResponse<dynamic>> deleteProject(
    @Field("project_id") String projectId,
  );
}
