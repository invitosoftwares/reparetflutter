import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/user_model.dart';
import '../../network/retrofit.dart';
import '../../screens/common/login_screen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/shared_preferences.dart';

import '../providers/chat_provider.dart';

class BaseNetwork {
  late Retrofit retrofit;
  final String? contentType;
  final BuildContext _context;
  CancelToken? token;

  cancelCall() {
    if (token != null) token!.cancel("cancelled");
    token = CancelToken();
  }

  BaseNetwork(this._context, {this.contentType}) {
    token = CancelToken();
    final logging = LogInterceptor(
      request: true,
      requestHeader: true,
      requestBody: true,
      responseHeader: true,
      responseBody: true,
      error: true,
    );

    final baseOptions = BaseOptions(
      connectTimeout: 30000,
      receiveTimeout: 30000,
      followRedirects: true,
      validateStatus: (status) {
        return status == null ? false : status <= 500;
      },
    );

    Dio _dio = Dio(baseOptions);
    _dio.interceptors.add(logging);

    _dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          UserModel? userModel =
              await Common.getUserModelFromSharedPreferences();
          if (userModel == null) {
            handler.next(options);
            return;
          }
          print("token sent in api: ${userModel.token}");
          options.headers = {
            "Authorization": "Bearer ${userModel.token}",
            "content-type": contentType ?? "application/x-www-form-urlencoded",
          };
          handler.next(options);
        },
        onResponse: (response, handler) async {
          try {
            bool isUserLoggedIn =
                await MySharedPreferences.getBool(Constants.isUserLoggedIn);
            if (response.statusCode == 403 && isUserLoggedIn) {
              _continueToLogin();
            } else {
              return handler.next(response);
            }
          } catch (err) {
            print(err);
          }
        },
        onError: (error, handler) async {
          try {
            bool isUserLoggedIn =
                await MySharedPreferences.getBool(Constants.isUserLoggedIn);
            if (error.response != null &&
                error.response!.statusCode == 403 &&
                isUserLoggedIn) {
              _continueToLogin();
            } else {
              handler.next(error);
            }
          } catch (err) {
            Common.showToast(error.message);
          }
        },
      ),
    );

    retrofit = Retrofit(_dio);
  }

  _continueToLogin() async {
    print("SESSION EXPIRED, CONTINUE TO LOGIN");
    await MySharedPreferences.clearAll();
    await Provider.of<ChatProvider>(_context, listen: false).disconnect();
    Common.showToast(
      "Your session has expired.",
    );
    Navigator.pushNamedAndRemoveUntil(
      _context,
      LoginScreen.routeName,
      (route) => false,
    );
  }
}
