// To parse this JSON data, do
//
//     final projectPaymentModel = projectPaymentModelFromJson(jsonString);

import 'dart:convert';

ProjectPaymentModel projectPaymentModelFromJson(String str) =>
    ProjectPaymentModel.fromJson(json.decode(str));

String projectPaymentModelToJson(ProjectPaymentModel data) =>
    json.encode(data.toJson());

class ProjectPaymentModel {
  ProjectPaymentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  Data? data;

  factory ProjectPaymentModel.fromJson(Map<String, dynamic> json) =>
      ProjectPaymentModel(
        type: json["type"],
        message: json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data == null ? null : data!.toJson(),
      };
}

class Data {
  Data({
    this.paymentStatus,
    this.amount,
    this.clientSecret,
    this.paymentMethod,
  });

  bool? paymentStatus;
  int? amount;
  String? clientSecret;
  String? paymentMethod;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        paymentStatus: json["payment_status"],
        amount: json["amount"],
        clientSecret: json["client_secret"],
        paymentMethod: json["payment_method"],
      );

  Map<String, dynamic> toJson() => {
        "payment_status": paymentStatus,
        "amount": amount,
        "client_secret": clientSecret,
        "payment_method": paymentMethod,
      };
}
