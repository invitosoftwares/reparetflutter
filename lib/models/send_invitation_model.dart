// To parse this JSON data, do
//
//     final sendInvitationModel = sendInvitationModelFromJson(jsonString);

import 'dart:convert';

SendInvitationModel sendInvitationModelFromJson(String str) => SendInvitationModel.fromJson(json.decode(str));

String sendInvitationModelToJson(SendInvitationModel data) => json.encode(data.toJson());

class SendInvitationModel {
  SendInvitationModel({
    this.type,
    this.message,
  });

  String? type;
  String? message;

  factory SendInvitationModel.fromJson(Map<String, dynamic> json) => SendInvitationModel(
    type: json["type"],
    message: json["message"],
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "message": message,
  };
}
