// To parse this JSON data, do
//
//     final getListingsParentModel = getListingsParentModelFromJson(jsonString);

import 'dart:convert';

import '../../models/project_model.dart';

GetListingsParentModel getListingsParentModelFromJson(String str) =>
    GetListingsParentModel.fromJson(json.decode(str));

String getListingsParentModelToJson(GetListingsParentModel data) =>
    json.encode(data.toJson());

class GetListingsParentModel {
  GetListingsParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  Data? data;

  factory GetListingsParentModel.fromJson(Map<String, dynamic> json) =>
      GetListingsParentModel(
        type: json["type"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data?.toJson(),
      };
}

class Data {
  Data({
    this.page,
    this.pageSize,
    this.projects,
  });

  String? page;
  int? pageSize;
  List<ProjectModel>? projects;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        page: json["page"],
        pageSize: json["page_size"],
        projects: List<ProjectModel>.from(
            json["projects"].map((x) => ProjectModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "page_size": pageSize,
        "projects": List<dynamic>.from(
          projects!.map(
            (x) => x.toJson(),
          ),
        ),
      };
}
