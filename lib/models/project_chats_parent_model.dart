// To parse this JSON data, do
//
//     final projectChatsParentModel = projectChatsParentModelFromJson(jsonString);

import 'dart:convert';

ProjectChatsParentModel projectChatsParentModelFromJson(String str) =>
    ProjectChatsParentModel.fromJson(json.decode(str));

String projectChatsParentModelToJson(ProjectChatsParentModel data) =>
    json.encode(data.toJson());

class ProjectChatsParentModel {
  ProjectChatsParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  Data? data;

  factory ProjectChatsParentModel.fromJson(Map<String, dynamic> json) =>
      ProjectChatsParentModel(
        type: json["type"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.page,
    this.pageSize,
    this.projectChats,
  });

  String? page;
  int? pageSize;
  List<ProjectChat>? projectChats;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        page: json["page"],
        pageSize: json["page_size"],
        projectChats: List<ProjectChat>.from(
            json["project_chats"].map((x) => ProjectChat.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "page_size": pageSize,
        "project_chats":
            List<dynamic>.from(projectChats!.map((x) => x.toJson())),
      };
}

class ProjectChat {
  ProjectChat({
    this.chatId,
    this.name,
    this.image,
    this.providerId,
    this.lastMessage,
    this.lastMessageAt,
    this.unreadCount,
  });

  String? chatId;
  String? name;
  String? image;
  String? lastMessage;
  String? lastMessageAt;
  String? providerId;
  int? unreadCount;

  factory ProjectChat.fromJson(Map<String, dynamic> json) => ProjectChat(
        chatId: json["chat_id"],
        name: json["name"],
        image: json["image"],
        lastMessage: json["last_message"],
        lastMessageAt: json["last_message_at"],
        unreadCount: json["unread_count"],
        providerId: json["provider_id"],
      );

  Map<String, dynamic> toJson() => {
        "chat_id": chatId,
        "name": name,
        "image": image,
        "last_message": lastMessage,
        "last_message_at": lastMessageAt,
        "unread_count": unreadCount,
        "provider_id": providerId,
      };
}
