class TabModel {
  int type;
  bool isSelected;
  final String text;
  final String icon;
  final int badgeCount;

  TabModel(this.type, this.isSelected, this.text, this.icon, {this.badgeCount = 0});
}
