// To parse this JSON data, do
//
//     final getCategoriesParentModel = getCategoriesParentModelFromJson(jsonString);

import 'dart:convert';

GetCategoriesParentModel getCategoriesParentModelFromJson(String str) =>
    GetCategoriesParentModel.fromJson(json.decode(str));

String getCategoriesParentModelToJson(GetCategoriesParentModel data) =>
    json.encode(data.toJson());

class GetCategoriesParentModel {
  GetCategoriesParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  List<CategoryModel>? data;

  factory GetCategoriesParentModel.fromJson(Map<String, dynamic> json) =>
      GetCategoriesParentModel(
        type: json["type"],
        message: json["message"],
        data: List<CategoryModel>.from(
            json["data"].map((x) => CategoryModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data == null
            ? []
            : List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class CategoryModel {
  CategoryModel({
    this.id,
    this.name,
  });

  int? id;
  String? name;

  factory CategoryModel.fromJson(Map<String, dynamic> json) => CategoryModel(
        id: json["id"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
      };

  @override
  String toString() {
    return name ?? "";
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CategoryModel &&
          runtimeType == other.runtimeType &&
          id == other.id;

  @override
  int get hashCode => id.hashCode;
}
