// To parse this JSON data, do
//
//     final toggleFavouriteStatusModel = toggleFavouriteStatusModelFromJson(jsonString);

import 'dart:convert';

PaymentWithdrawalModel paymentWithdrawalModelFromJson(String str) =>
    PaymentWithdrawalModel.fromJson(json.decode(str));

String paymentWithdrawalModelToJson(PaymentWithdrawalModel data) =>
    json.encode(data.toJson());

class PaymentWithdrawalModel {
  PaymentWithdrawalModel({
    this.type,
    this.message,
  });

  String? type;
  String? message;

  factory PaymentWithdrawalModel.fromJson(Map<String, dynamic> json) =>
      PaymentWithdrawalModel(
        type: json["type"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
      };
}
