// To parse this JSON data, do
//
//     final projectBidsParentModel = projectBidsParentModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';

ProjectBidsParentModel projectBidsParentModelFromJson(String str) =>
    ProjectBidsParentModel.fromJson(json.decode(str));

String projectBidsParentModelToJson(ProjectBidsParentModel data) =>
    json.encode(data.toJson());

class ProjectBidsParentModel {
  ProjectBidsParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  Data? data;

  factory ProjectBidsParentModel.fromJson(Map<String, dynamic> json) =>
      ProjectBidsParentModel(
        type: json["type"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.projectBids,
  });

  List<ProjectBidModel>? projectBids;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        projectBids: List<ProjectBidModel>.from(
            json["project_bids"].map((x) => ProjectBidModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "project_bids": List<dynamic>.from(projectBids!.map((x) => x.toJson())),
      };
}

class ProjectBidModel with ChangeNotifier {
  ProjectBidModel({
    this.bidId,
    this.projectId,
    this.providerId,
    this.bidAmount,
    this.bidAccepted,
    this.createdAt,
    this.averageRating,
    this.totalRatings,
    this.providerImage,
    this.providerFirstName,
    this.providerLastName,
    this.providerVerified,
  });

  int? bidId;
  int? projectId;
  int? providerId;
  dynamic bidAmount;
  bool? bidAccepted;
  String? createdAt;
  dynamic averageRating;
  int? totalRatings;
  String? providerImage;
  String? providerFirstName;
  String? providerLastName;
  int? providerVerified;

  makeIsAccepted() {
    bidAccepted = true;
    notifyListeners();
  }

  factory ProjectBidModel.fromJson(Map<String, dynamic> json) =>
      ProjectBidModel(
        bidId: json["bid_id"],
        projectId: json["project_id"],
        providerId: json["provider_id"],
        bidAmount: json["bid_amount"],
        bidAccepted: json["bid_accepted"],
        createdAt: json["created_at"],
        averageRating: json["average_rating"],
        totalRatings: json["total_ratings"],
        providerImage: json["provider_image"],
        providerFirstName: json["provider_first_name"],
        providerLastName: json["provider_last_name"],
        providerVerified: json["provider_verified"],
      );

  Map<String, dynamic> toJson() => {
        "bid_id": bidId,
        "project_id": projectId,
        "provider_id": providerId,
        "bid_amount": bidAmount,
        "bid_accepted": bidAccepted,
        "created_at": createdAt,
        "average_rating": averageRating,
        "total_ratings": totalRatings,
        "provider_image": providerImage,
        "provider_first_name": providerFirstName,
        "provider_last_name": providerLastName,
        "provider_verified": providerVerified,
      };
}
