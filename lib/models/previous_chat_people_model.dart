// To parse this JSON data, do
//
//     final previousChatPeopleModel = previousChatPeopleModelFromJson(jsonString);

import 'dart:convert';

PreviousChatPeopleModel previousChatPeopleModelFromJson(String str) =>
    PreviousChatPeopleModel.fromJson(json.decode(str));

String previousChatPeopleModelToJson(PreviousChatPeopleModel data) =>
    json.encode(data.toJson());

class PreviousChatPeopleModel {
  PreviousChatPeopleModel({
    this.firstName,
    this.lastName,
    this.image,
    this.projectStatus,
    this.projectTitle,
    this.projectTime,
    this.projectId,
    this.chatId,
    this.consumerId,
    this.providerId,
    this.lastMessage,
    this.lastMessageAt,
    this.unread,
    this.hired,
  });

  String? firstName;
  String? lastName;
  String? image;
  int? projectStatus;
  String? projectTitle;
  String? projectTime;
  int? projectId;
  String? chatId;
  String? consumerId;
  String? providerId;
  String? lastMessage;
  String? lastMessageAt;
  int? unread;
  bool? hired;

  factory PreviousChatPeopleModel.fromJson(Map<String, dynamic> json) =>
      PreviousChatPeopleModel(
        firstName: json["first_name"],
        lastName: json["last_name"],
        image: json["image"],
        projectStatus: json["project_status"],
        projectTitle: json["project_title"],
        projectTime: json["project_time"],
        projectId: json["project_id"],
        chatId: json["chat_id"],
        consumerId: json["consumer_id"],
        providerId: json["provider_id"],
        lastMessage: json["last_message"],
        lastMessageAt: json["last_message_at"],
        unread: json["unread"],
        hired: json["hired"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "image": image,
        "project_status": projectStatus,
        "project_title": projectTitle,
        "project_time": projectTime,
        "project_id": projectId,
        "chat_id": chatId,
        "consumer_id": consumerId,
        "provider_id": providerId,
        "last_message": lastMessage,
        "last_message_at": lastMessageAt,
        "unread": unread,
        "hired": hired,
      };
}
