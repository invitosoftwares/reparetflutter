// To parse this JSON data, do
//
//     final setupIntentParentModel = setupIntentParentModelFromJson(jsonString);

import 'dart:convert';

SetupIntentParentModel setupIntentParentModelFromJson(String str) =>
    SetupIntentParentModel.fromJson(json.decode(str));

String setupIntentParentModelToJson(SetupIntentParentModel data) =>
    json.encode(data.toJson());

class SetupIntentParentModel {
  SetupIntentParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  Data? data;

  factory SetupIntentParentModel.fromJson(Map<String, dynamic> json) =>
      SetupIntentParentModel(
        type: json["type"] == null ? null : json["type"],
        message: json["message"] == null ? null : json["message"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "message": message == null ? null : message,
        "data": data == null ? null : data!.toJson(),
      };
}

class Data {
  Data({
    this.setupIntentId,
  });

  String? setupIntentId;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        setupIntentId:
            json["setup_intent_id"] == null ? null : json["setup_intent_id"],
      );

  Map<String, dynamic> toJson() => {
        "setup_intent_id": setupIntentId == null ? null : setupIntentId,
      };
}
