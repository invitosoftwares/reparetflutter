// To parse this JSON data, do
//
//     final searchProvidersParentModel = searchProvidersParentModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';

SearchProvidersParentModel searchProvidersParentModelFromJson(String str) =>
    SearchProvidersParentModel.fromJson(json.decode(str));

String searchProvidersParentModelToJson(SearchProvidersParentModel data) =>
    json.encode(data.toJson());

class SearchProvidersParentModel {
  SearchProvidersParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  List<ProviderModel>? data;

  factory SearchProvidersParentModel.fromJson(Map<String, dynamic> json) =>
      SearchProvidersParentModel(
        type: json["type"],
        message: json["message"],
        data: List<ProviderModel>.from(
            json["data"].map((x) => ProviderModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class ProviderModel with ChangeNotifier {
  ProviderModel({
    this.providerId,
    this.firstName,
    this.lastName,
    this.image,
    this.companyName,
    this.rating,
    this.totalRatings,
    this.location,
    this.providerVerified,
    this.favourite,
    this.categories,
  });

  int? providerId;
  String? firstName;
  String? lastName;
  String? image;
  String? companyName;
  dynamic rating;
  int? totalRatings;
  String? location;
  bool? providerVerified;
  bool? favourite;
  List<String>? categories;

  factory ProviderModel.fromJson(Map<String, dynamic> json) => ProviderModel(
        providerId: json["provider_id"],
        firstName: json["first_name"],
        lastName: json["last_name"],
        image: json["image"],
        companyName: json["company_name"],
        rating: json["rating"],
        totalRatings: json["total_ratings"],
        location: json["location"],
        providerVerified: json["provider_verified"],
        favourite: json["favourite"],
        categories: List<String>.from(json["categories"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "provider_id": providerId,
        "first_name": firstName,
        "last_name": lastName,
        "image": image,
        "company_name": companyName,
        "rating": rating,
        "total_ratings": totalRatings,
        "location": location,
        "provider_verified": providerVerified,
        "favourite": favourite,
        "categories": List<dynamic>.from(categories!.map((x) => x)),
      };

  updateFavouriteStatus() {
    if (favourite == null) return;
    favourite = !favourite!;
    notifyListeners();
  }
}
