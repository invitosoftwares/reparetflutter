// To parse this JSON data, do
//
//     final ownerDetailsParentModel = ownerDetailsParentModelFromJson(jsonString);

import 'dart:convert';

OwnerDetailsParentModel ownerDetailsParentModelFromJson(String str) => OwnerDetailsParentModel.fromJson(json.decode(str));

String ownerDetailsParentModelToJson(OwnerDetailsParentModel data) => json.encode(data.toJson());

class OwnerDetailsParentModel {
  OwnerDetailsParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  OwnerDetailsModel? data;

  factory OwnerDetailsParentModel.fromJson(Map<String, dynamic> json) => OwnerDetailsParentModel(
    type: json["type"],
    message: json["message"],
    data: OwnerDetailsModel.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "message": message,
    "data": data?.toJson(),
  };
}

class OwnerDetailsModel {
  OwnerDetailsModel({
    this.firstName,
    this.lastName,
    this.image,
    this.projectCount,
    this.completedProjectCount,
    this.totalSpent,
    this.reviewCount,
    this.rating,
  });

  String? firstName;
  String? lastName;
  String? image;
  int? projectCount;
  int? completedProjectCount;
  dynamic totalSpent;
  int? reviewCount;
  dynamic rating;

  factory OwnerDetailsModel.fromJson(Map<String, dynamic> json) => OwnerDetailsModel(
    firstName: json["first_name"],
    lastName: json["last_name"],
    image: json["image"],
    projectCount: json["project_count"],
    completedProjectCount: json["completed_project_count"],
    totalSpent: json["total_spent"],
    reviewCount: json["review_count"],
    rating: json["rating"],
  );

  Map<String, dynamic> toJson() => {
    "first_name": firstName,
    "last_name": lastName,
    "image": image,
    "project_count": projectCount,
    "completed_project_count": completedProjectCount,
    "total_spent": totalSpent,
    "review_count": reviewCount,
    "rating": rating,
  };
}
