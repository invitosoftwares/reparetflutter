// To parse this JSON data, do
//
//     final toggleFavouriteStatusModel = toggleFavouriteStatusModelFromJson(jsonString);

import 'dart:convert';

ToggleFavouriteStatusModel toggleFavouriteStatusModelFromJson(String str) =>
    ToggleFavouriteStatusModel.fromJson(json.decode(str));

String toggleFavouriteStatusModelToJson(ToggleFavouriteStatusModel data) =>
    json.encode(data.toJson());

class ToggleFavouriteStatusModel {
  ToggleFavouriteStatusModel({
    this.type,
    this.message,
  });

  String? type;
  String? message;

  factory ToggleFavouriteStatusModel.fromJson(Map<String, dynamic> json) =>
      ToggleFavouriteStatusModel(
        type: json["type"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
      };
}
