class FormFieldData {
  final bool isError;
  final String data;

  FormFieldData(this.isError, this.data);

  @override
  String toString() {
    return 'FormFieldData{isError: $isError, data: $data}';
  }
}
