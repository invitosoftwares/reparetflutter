// To parse this JSON data, do
//
//     final contractorDetailsModel = contractorDetailsModelFromJson(jsonString);

import 'dart:convert';

import 'package:flutter/material.dart';

ContractorDetailsModel contractorDetailsModelFromJson(String str) =>
    ContractorDetailsModel.fromJson(json.decode(str));

String contractorDetailsModelToJson(ContractorDetailsModel data) =>
    json.encode(data.toJson());

class ContractorDetailsModel {
  ContractorDetailsModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  ContractorDetailsDataModel? data;

  factory ContractorDetailsModel.fromJson(Map<String, dynamic> json) =>
      ContractorDetailsModel(
        type: json["type"],
        message: json["message"],
        data: ContractorDetailsDataModel.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data!.toJson(),
      };
}

class ContractorDetailsDataModel {
  ContractorDetailsDataModel({
    this.firstName,
    this.lastName,
    this.image,
    this.companyName,
    this.location,
    this.bio,
    this.reviewCount,
    this.rating,
    this.projects,
    this.speciality,
    this.completedProjectCount,
    this.providerVerified,
  });

  String? firstName;
  String? lastName;
  String? image;
  String? companyName;
  String? location;
  String? bio;
  int? reviewCount;
  dynamic rating;
  List<RecentProjectModel>? projects;
  List<String>? speciality;
  int? completedProjectCount;
  bool? providerVerified;

  factory ContractorDetailsDataModel.fromJson(Map<String, dynamic> json) =>
      ContractorDetailsDataModel(
        firstName: json["first_name"],
        lastName: json["last_name"],
        image: json["image"],
        companyName: json["company_name"],
        location: json["location"],
        bio: json["bio"],
        reviewCount: json["review_count"],
        rating: json["rating"],
        projects: List<RecentProjectModel>.from(
            json["projects"].map((x) => RecentProjectModel.fromJson(x))),
        speciality: List<String>.from(json["speciality"].map((x) => x)),
        completedProjectCount: json["completed_project_count"],
        providerVerified: json["provider_verified"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "image": image,
        "company_name": companyName,
        "location": location,
        "bio": bio,
        "review_count": reviewCount,
        "rating": rating,
        "projects": List<dynamic>.from(projects!.map((x) => x.toJson())),
        "speciality": List<dynamic>.from(speciality!.map((x) => x)),
        "completed_project_count": completedProjectCount,
        "provider_verified": providerVerified,
      };
}

class RecentProjectModel with ChangeNotifier {
  RecentProjectModel({
    this.projectTitle,
    this.projectDescription,
    this.projectTime,
    this.projectStatus,
    this.projectimages,
    this.projectReviews,
  });

  String? projectTitle;
  String? projectDescription;
  String? projectTime;
  int? projectStatus;
  List<Projectimage>? projectimages;
  ProjectReviews? projectReviews;

  factory RecentProjectModel.fromJson(Map<String, dynamic> json) =>
      RecentProjectModel(
        projectTitle: json["project_title"],
        projectDescription: json["project_description"],
        projectTime: json["project_time"],
        projectStatus: json["project_status"],
        projectimages: List<Projectimage>.from(
            json["projectimages"].map((x) => Projectimage.fromJson(x))),
        projectReviews: json["project_reviews"] == null
            ? null
            : ProjectReviews.fromJson(json["project_reviews"]),
      );

  Map<String, dynamic> toJson() => {
        "project_title": projectTitle,
        "project_description": projectDescription,
        "project_time": projectTime,
        "project_status": projectStatus,
        "projectimages":
            List<dynamic>.from(projectimages!.map((x) => x.toJson())),
        "project_reviews":
            projectReviews == null ? null : projectReviews!.toJson(),
      };
}

class ProjectReviews {
  ProjectReviews({
    this.rating,
  });

  dynamic rating;

  factory ProjectReviews.fromJson(Map<String, dynamic> json) => ProjectReviews(
        rating: json["rating"],
      );

  Map<String, dynamic> toJson() => {
        "rating": rating,
      };
}

class Projectimage {
  Projectimage({
    this.image,
  });

  String? image;

  factory Projectimage.fromJson(Map<String, dynamic> json) => Projectimage(
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
      };
}
