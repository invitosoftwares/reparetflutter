// To parse this JSON data, do
//
//     final projectDetailsParentModel = projectDetailsParentModelFromJson(jsonString);

import 'dart:convert';

import '../../models/project_model.dart';

ProjectDetailsParentModel projectDetailsParentModelFromJson(String str) => ProjectDetailsParentModel.fromJson(json.decode(str));

String projectDetailsParentModelToJson(ProjectDetailsParentModel data) => json.encode(data.toJson());

class ProjectDetailsParentModel {
  ProjectDetailsParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  ProjectModel? data;

  factory ProjectDetailsParentModel.fromJson(Map<String, dynamic> json) => ProjectDetailsParentModel(
    type: json["type"],
    message: json["message"],
    data: ProjectModel.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "type": type,
    "message": message,
    "data": data!.toJson(),
  };
}