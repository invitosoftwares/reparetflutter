// To parse this JSON data, do
//
//     final getAllCardsParentModel = getAllCardsParentModelFromJson(jsonString);

import 'dart:convert';

GetAllCardsParentModel getAllCardsParentModelFromJson(String str) =>
    GetAllCardsParentModel.fromJson(json.decode(str));

String getAllCardsParentModelToJson(GetAllCardsParentModel data) =>
    json.encode(data.toJson());

class GetAllCardsParentModel {
  GetAllCardsParentModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  List<CardModel>? data;

  factory GetAllCardsParentModel.fromJson(Map<String, dynamic> json) =>
      GetAllCardsParentModel(
        type: json["type"],
        message: json["message"],
        data: List<CardModel>.from(
            json["data"].map((x) => CardModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": List<dynamic>.from(data!.map((x) => x.toJson())),
      };
}

class CardModel {
  CardModel({
    this.pmId,
    this.country,
    this.expMonth,
    this.expYear,
    this.last4,
  });

  String? pmId;
  String? country;
  int? expMonth;
  int? expYear;
  String? last4;

  factory CardModel.fromJson(Map<String, dynamic> json) => CardModel(
        pmId: json["pm_id"],
        country: json["country"],
        expMonth: json["exp_month"],
        expYear: json["exp_year"],
        last4: json["last4"],
      );

  Map<String, dynamic> toJson() => {
        "pm_id": pmId,
        "country": country,
        "exp_month": expMonth,
        "exp_year": expYear,
        "last4": last4,
      };
}
