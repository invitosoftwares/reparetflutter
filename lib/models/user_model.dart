import '../../models/get_categories_parent_model.dart';

class UserModel {
  UserModel({
    this.firstName,
    this.lastName,
    this.email,
    this.uuid,
    this.countryCode,
    this.phone,
    this.image,
    this.role,
    this.token,
    this.bio,
    this.companyName,
    this.lat,
    this.lng,
    this.location,
    this.providerVerified,
    this.onboardingStatus,
    this.categories,
    this.walletBalance,
  });

  String? firstName;
  String? lastName;
  String? email;
  String? uuid;
  String? countryCode;
  String? phone;
  String? image;
  int? role;
  String? token;
  String? bio;
  String? companyName;
  String? lat;
  String? lng;
  String? location;
  bool? providerVerified;
  bool? onboardingStatus;
  List<CategoryModel>? categories;
  dynamic walletBalance;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        firstName: json["first_name"],
        lastName: json["last_name"],
        email: json["email"],
        uuid: json["uuid"],
        countryCode: json["country_code"],
        phone: json["phone"],
        image: json["image"],
        role: json["role"],
        token: json["api_token"],
        bio: json["bio"],
        companyName: json["company_name"],
        lat: json["lat"],
        lng: json["lng"],
        location: json["location"],
        onboardingStatus: json["onboarding_status"],
        walletBalance: json["wallet_balance"],
        providerVerified: json["provider_verified"],
        categories: json["categories"] == null
            ? null
            : List<CategoryModel>.from(
                json["categories"].map((x) => CategoryModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "email": email,
        "uuid": uuid,
        "country_code": countryCode,
        "phone": phone,
        "image": image,
        "role": role,
        "api_token": token,
        "bio": bio,
        "company_name": companyName,
        "lat": lat,
        "lng": lng,
        "location": location,
        "provider_verified": providerVerified,
        "onboarding_status": onboardingStatus,
        "wallet_balance": walletBalance,
        "categories": categories == null
            ? []
            : List<dynamic>.from(categories!.map((x) => x.toJson())),
      };

  @override
  String toString() {
    return 'UserModel{firstName: $firstName, lastName: $lastName, email: $email, '
        'uuid: $uuid, countryCode: $countryCode, phone: $phone, image: $image, role: $role,'
        ' token: $token, bio: $bio, companyName: $companyName,'
        ' lat: $lat, lng: $lng, location: $location, providerVerified: $providerVerified,'
        ' onboardingStatus: $onboardingStatus, categories: $categories, walletBalance: $walletBalance}';
  }
}
