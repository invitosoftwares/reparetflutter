// To parse this JSON data, do
//
//     final getStripeLoginUrlModel = getStripeLoginUrlModelFromJson(jsonString);

import 'dart:convert';

GetStripeLoginUrlModel getStripeLoginUrlModelFromJson(String str) =>
    GetStripeLoginUrlModel.fromJson(json.decode(str));

String getStripeLoginUrlModelToJson(GetStripeLoginUrlModel data) =>
    json.encode(data.toJson());

class GetStripeLoginUrlModel {
  GetStripeLoginUrlModel({
    this.type,
    this.message,
    this.data,
  });

  String? type;
  String? message;
  Data? data;

  factory GetStripeLoginUrlModel.fromJson(Map<String, dynamic> json) =>
      GetStripeLoginUrlModel(
        type: json["type"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "message": message,
        "data": data!.toJson(),
      };
}

class Data {
  Data({
    this.url,
  });

  String? url;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "url": url,
      };
}
