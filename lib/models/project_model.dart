import 'package:flutter/material.dart';
import '../../utils/constants.dart';
import '../widgets/common/upload_file_s3.dart';

class ProjectModel with ChangeNotifier {
  ProjectModel({
    this.projectId,
    this.consumerId,
    this.providerId,
    this.projectTitle,
    this.projectDescription,
    this.category,
    this.allowVerifiedOnly,
    this.categoryId,
    this.lat,
    this.lng,
    this.projectTime,
    this.address,
    this.projectStatus,
    this.projectDeleted,
    this.amount,
    this.createdAt,
    this.bidsCount,
    this.projectimages,
    this.projectvideos,
    this.providerDetails,
    this.unreadMessages,
    this.bidAmount,
    this.paymentDetails,
    this.rating,
    this.review,
    this.reviewAdded,
    this.consumerUuid,
    this.consumerFirstName,
    this.consumerLastName,
    this.consumerImage,
  }) {
//    getImagesAndVideos();
  }

  getImagesAndVideos() async {
    if (projectimages != null) {
      for (ProjectMedia media in projectimages!) {
        if (media.image != null && !(media.image!.contains("https"))) {
          String img = await UploadFileS3.getImageUrl(media.image!);
          if (!media.image!.contains("https")) {
            media.image = img;
          }
        }
      }
    }
    if (projectvideos != null) {
      for (ProjectMedia media in projectvideos!) {
        if (media.video != null && !(media.video!.contains("https"))) {
          String img = await UploadFileS3.getImageUrl(media.video!);
          if (!media.video!.contains("https")) {
            media.video = img;
          }
        }
      }
    }
    await Future.delayed(
      const Duration(seconds: 1),
    );
    notifyListeners();
  }

  int? projectId;
  int? consumerId;
  int? providerId;
  String? projectTitle;
  String? consumerUuid;
  String? projectDescription;
  String? category;
  int? allowVerifiedOnly;
  int? categoryId;
  String? lat;
  String? lng;
  String? projectTime;
  String? address;
  int? projectStatus;
  int? projectDeleted;
  dynamic amount;
  dynamic bidAmount;
  dynamic rating;
  String? review;
  String? createdAt;
  int? bidsCount;
  bool? reviewAdded;
  List<ProjectMedia>? projectimages;
  List<ProjectMedia>? projectvideos;
  int? unreadMessages;
  ProviderDetails? providerDetails;
  PaymentDetails? paymentDetails;
  String? consumerFirstName;
  String? consumerLastName;
  String? consumerImage;

  makeBidLocally(int bidAmount) {
    this.bidAmount = bidAmount;
    notifyListeners();
  }

  cancelBid() {
    bidAmount = null;
    notifyListeners();
  }

  markProjectAsPaid() {
    projectStatus = Constants.PROJECT_PAID;
    notifyListeners();
  }

  setRatingLocally(dynamic rating) {
    projectStatus = Constants.PROJECT_PAID_REVIEWED_BY_USER;
    this.rating = rating;
    reviewAdded = true;
    notifyListeners();
  }

  factory ProjectModel.fromJson(Map<String, dynamic> json) => ProjectModel(
        projectId: json["project_id"],
        consumerId: json["consumer_id"],
        consumerUuid: json["consumer_uuid"],
        providerId: json["provider_id"],
        projectTitle: json["project_title"],
        projectDescription: json["project_description"],
        allowVerifiedOnly: json["allow_verified_only"],
        categoryId: json["category_id"],
        lat: json["lat"],
        lng: json["lng"],
        address: json["address"],
        projectDeleted: json["project_deleted"],
        category: json["category"],
        projectTime: json["project_time"],
        projectStatus: json["project_status"],
        amount: json["amount"],
        bidAmount: json["bid_amount"],
        createdAt: json["createdAt"],
        bidsCount: json["bids_count"],
        rating: json["rating"],
        review: json["review"],
        reviewAdded: json["review_added"],
        paymentDetails: json["payment_details"] == null
            ? null
            : PaymentDetails.fromJson(json["payment_details"]),
        providerDetails: json["provider_details"] == null
            ? null
            : ProviderDetails.fromJson(json["provider_details"]),
        projectimages: List<ProjectMedia>.from(
          json["projectimages"].map(
            (x) => ProjectMedia.fromJson(x),
          ),
        ),
        projectvideos: (json["projectvideos"] == null)
            ? null
            : List<ProjectMedia>.from(
                json["projectvideos"].map(
                  (x) => ProjectMedia.fromJson(x),
                ),
              ),
        unreadMessages: json["unread_messages"],
        consumerFirstName: json["consumer_first_name"],
        consumerLastName: json["consumer_last_name"],
        consumerImage: json["consumer_image"],
      );

  Map<String, dynamic> toJson() => {
        "project_id": projectId,
        "consumer_id": consumerId,
        "consumer_uuid": consumerUuid,
        "provider_id": providerId,
        "project_description": projectDescription,
        "allow_verified_only": allowVerifiedOnly,
        "category_id": categoryId,
        "lat": lat,
        "lng": lng,
        "rating": rating,
        "review": review,
        "project_deleted": projectDeleted,
        "project_title": projectTitle,
        "category": category,
        "project_time": projectTime,
        "project_status": projectStatus,
        "amount": amount,
        "bid_amount": bidAmount,
        "createdAt": createdAt,
        "bids_count": bidsCount,
        "review_added": reviewAdded,
        "payment_details":
            paymentDetails == null ? null : paymentDetails!.toJson(),
        "provider_details":
            providerDetails == null ? null : providerDetails!.toJson(),
        "projectimages":
            List<dynamic>.from(projectimages!.map((x) => x.toJson())),
        "projectvideos": projectvideos != null
            ? List<dynamic>.from(projectvideos!.map((x) => x))
            : [],
        "unread_messages": unreadMessages,
        "consumer_first_name": consumerFirstName,
        "consumer_last_name": consumerLastName,
        "consumer_image": consumerImage,
      };

  @override
  String toString() {
    return 'ProjectModel{projectId: $projectId, consumerId: $consumerId, providerId: $providerId, projectTitle: $projectTitle, projectDescription: $projectDescription, category: $category, categoryId: $categoryId, allowVerifiedOnly: $allowVerifiedOnly, lat: $lat, lng: $lng, projectTime: $projectTime, address: $address, projectStatus: $projectStatus, projectDeleted: $projectDeleted, amount: $amount, bidAmount: $bidAmount, createdAt: $createdAt, bidsCount: $bidsCount, projectimages: $projectimages, projectvideos: $projectvideos, unreadMessages: $unreadMessages}';
  }
}

class ProjectMedia with ChangeNotifier {
  ProjectMedia({
    this.image,
    this.video,
  }) {
    //getImagesAndVideos();
  }

  getImagesAndVideos() async {
    if (image != null && !(image!.contains("https"))) {
      String img = await UploadFileS3.getImageUrl(image);
      image = img;
    }
    if (video != null && !(video!.contains("https"))) {
      String img = await UploadFileS3.getImageUrl(video);
      video = img;
    }
    await Future.delayed(
      const Duration(seconds: 1),
    );
    notifyListeners();
  }

  String? image;
  String? video;

  factory ProjectMedia.fromJson(Map<String, dynamic> json) => ProjectMedia(
        image: json["image"],
        video: json["video"],
      );

  Map<String, dynamic> toJson() => {
        "image": image,
        "video": video,
      };

  @override
  String toString() {
    return 'ProjectMedia{image: $image, video: $video}';
  }
}

class ProviderDetails with ChangeNotifier {
  ProviderDetails({
    this.firstName,
    this.lastName,
    this.image,
    this.providerVerified,
  });

  String? firstName;
  String? lastName;
  String? image;
  bool? providerVerified;

  factory ProviderDetails.fromJson(Map<String, dynamic> json) =>
      ProviderDetails(
        firstName: json["first_name"],
        lastName: json["last_name"],
        image: json["image"],
        providerVerified: json["provider_verified"],
      );

  Map<String, dynamic> toJson() => {
        "first_name": firstName,
        "last_name": lastName,
        "image": image,
        "provider_verified": providerVerified,
      };

  @override
  String toString() {
    return 'ProviderDetails{firstName: $firstName, lastName: $lastName, image: $image}';
  }
}

class PaymentDetails {
  PaymentDetails({
    this.paymentId,
    this.paymentDate,
  });

  String? paymentId;
  String? paymentDate;

  factory PaymentDetails.fromJson(Map<String, dynamic> json) => PaymentDetails(
        paymentId: json["payment_id"],
        paymentDate: json["payment_date"],
      );

  Map<String, dynamic> toJson() => {
        "payment_id": paymentId,
        "payment_date": paymentDate,
      };
}
