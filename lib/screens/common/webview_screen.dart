import 'dart:io';

import 'package:flutter/material.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewScreen extends StatefulWidget {
  static final String routeName = "WebViewScreen";

  @override
  _WebViewScreenState createState() => _WebViewScreenState();
}

class _WebViewScreenState extends State<WebViewScreen> {
  bool _isFirstTime = true;
  String _url = "";
  String _title = "";
  int _role = Constants.USER;
  bool _isLoading = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic> map =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;

      _url = map["URL"];
      _role = map["ROLE"];
      _title = map["TITLE"] ?? "";
    }
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        _role == Constants.USER
            ? CustomColors.colorAccent
            : CustomColors.colorPrimary,
        _screenSize,
        _title,
        isCenter: false,
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: _url,
            onPageStarted: (value) {
              setState(() {
                _isLoading = true;
              });
            },
            javascriptMode: JavascriptMode.unrestricted,
            onPageFinished: (value) {
              setState(() {
                _isLoading = false;
              });
            },
          ),
          if (_isLoading)
            SpinKitLoading(
              _role,
              false,
            ),
        ],
      ),
    );
  }
}
