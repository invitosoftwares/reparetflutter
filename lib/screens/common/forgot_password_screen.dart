import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/forgot_password_model.dart';
import '../../models/form_field_data.dart';
import '../../providers/auth_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/common/raised_button_primary.dart';

class ForgotPasswordScreen extends StatefulWidget {
  static final String routeName = "ForgotPasswordScreen";

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "EMAIL": null,
  };
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    var _customDecorators = CustomDecorators(context);

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: Container(),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Image.asset(
                "assets/images/login_background.png",
                width: double.infinity,
                fit: BoxFit.fill,
                height: _screenSize.height * 0.58,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: InkWell(
                            child: Image.asset(
                              "assets/images/icon_back.png",
                              height: _screenSize.height * 0.03,
                              width: _screenSize.height * 0.03,
                            ),
                            onTap: () => Navigator.pop(context)),
                      ),
                    ),
                    SizedBox(height: _screenSize.height * 0.1),
                    Image.asset(
                      "assets/images/forgot_password.png",
                      width: _screenSize.width * 0.5,
                      height: _screenSize.width * 0.5,
                    ),
                    SizedBox(height: _screenSize.height * 0.18),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: _screenSize.width * 0.05),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: _screenSize.width * 0.125),
                            child: Text(
                              "Enter your registered email so that we can send a new password for you to login",
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                          ),
                          SizedBox(height: _screenSize.height * 0.025),
                          getElevatedWidget(
                            TextFormField(
                              validator: (value) {
                                if (value == null || value.trim().isEmpty) {
                                  _data["EMAIL"] =
                                      FormFieldData(true, "Please enter email");
                                } else if (!Common.isValidEmail(value.trim())) {
                                  _data["EMAIL"] = FormFieldData(
                                      true, "Please enter some valid email");
                                } else {
                                  _data["EMAIL"] = FormFieldData(false, value);
                                }
                                return null;
                              },
                              style: Theme.of(context).textTheme.bodyText1,
                              keyboardType: TextInputType.emailAddress,
                              maxLines: 1,
                              textAlign: TextAlign.start,
                              cursorColor: Theme.of(context).primaryColor,
                              maxLength: 50,
                              inputFormatters: [
                                MyLengthLimitingTextInputFormatter(50),
                              ],
                              decoration: _customDecorators.getInputDecoration(
                                "Enter email",
                                prefixIconString:
                                    "assets/images/icon_email.png",
                              ),
                            ),
                          ),
                          SizedBox(height: 10),
                          Align(
                            alignment: Alignment.topRight,
                            child: PrimaryRaisedButton(
                              "Send OTP",
                              () {
                                _validateData(context);
                              },
                              size: _screenSize,
                              isLoading: _isLoading,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: _screenSize.height * 0.025),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getElevatedWidget(Widget widget) {
    return Material(
      elevation: 1,
      borderRadius: BorderRadius.all(Radius.circular(5)),
      child: widget,
    );
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);

    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    setState(() {
      _isLoading = true;
    });

    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      dynamic response = await authProvider.forgotPasswordApi(
        context,
        _data["EMAIL"]?.data ?? "",
      );

      if (response is ForgotPasswordModel) {
        AlertDialogs.showAlertDialogWithOk(context, response.message ?? "",
            dismiss: false, onOkayClicked: () => Navigator.pop(context));
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
