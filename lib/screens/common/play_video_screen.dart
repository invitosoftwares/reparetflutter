import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import 'package:video_player/video_player.dart';

class PlayVideoScreen extends StatefulWidget {
  static const String routeName = "PlayVideoScreen";

  const PlayVideoScreen({Key? key}) : super(key: key);

  @override
  _PlayVideoScreenState createState() => _PlayVideoScreenState();
}

class _PlayVideoScreenState extends State<PlayVideoScreen> {
  File? _videoFile;
  String? _videoUrl;
  bool _isFirstTime = true;
  Size? _screenSize;
  VideoPlayerController? _controller;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      _videoFile = data["VIDEO_FILE"];
      _videoUrl = data["VIDEO_URL"];

      if (_videoFile != null) {
        _controller = VideoPlayerController.file(_videoFile!)
          ..initialize().then((_) {
            _controller!.setLooping(true);
            setState(() {});
          });
      } else if (_videoUrl != null) {
        _controller = VideoPlayerController.network(_videoUrl!)
          ..initialize().then((_) {
            _controller!.setLooping(true);
            setState(() {});
          });
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (_controller != null) _controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Consumer<AuthProvider>(
      builder: (context, value, child) => Scaffold(
        backgroundColor: CustomColors.colorWhite,
        appBar: ToolbarWithBackAndText(
          (value.userModel!.role == Constants.USER)
              ? CustomColors.colorAccent
              : CustomColors.colorPrimary,
          _screenSize!,
          "Back",
          isCenter: false,
        ),
        body: ((_controller == null) || (!_controller!.value.isInitialized))
            ? _loader(
                color: (value.userModel!.role == Constants.USER)
                    ? CustomColors.colorAccent
                    : CustomColors.colorPrimary,
              )
            : Center(
                child: _controller!.value.isInitialized
                    ? Column(
                        children: [
                          Expanded(
                            child: Center(
                              child: AspectRatio(
                                aspectRatio: _controller!.value.aspectRatio,
                                child: VideoPlayer(
                                  _controller!,
                                ),
                              ),
                            ),
                          ),
                          if (_controller != null &&
                              _controller!.value.isInitialized)
                            _getVideoPlayerControllers(
                              color: (value.userModel!.role == Constants.USER)
                                  ? CustomColors.colorAccent
                                  : CustomColors.colorPrimary,
                            ),
                        ],
                      )
                    : Container(),
              ),
      ),
    );
  }

  _loader({
    @required Color? color,
  }) {
    return Center(
      child: SizedBox(
        height: 25,
        width: 25,
        child: CircularProgressIndicator(
          color: color ?? CustomColors.colorPrimary,
          strokeWidth: 2,
        ),
      ),
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Widget _getVideoPlayerControllers({
    @required Color? color,
  }) {
    return Padding(
      padding: const EdgeInsets.only(
        bottom: 5.0,
        top: 5.0,
        left: 5.0,
        right: 5.0,
      ),
      child: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: color,
        ),
        child: Row(
          children: [
            _getButton(
              false,
              color ?? CustomColors.colorPrimary,
            ),
            InkWell(
              onTap: () {
                _controller!.value.isPlaying
                    ? _controller!.pause()
                    : _controller!.play();
                setState(() {});
              },
              child: Icon(
                _controller!.value.isPlaying ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
                size: 30,
              ),
            ),
            _getButton(
              true,
              color ?? CustomColors.colorPrimary,
            ),
            _getVideoProgressIndicator(color: color),
          ],
        ),
      ),
    );
  }

  void _move({bool forward = true}) async {
    if (_controller == null) return;
    Duration? current = await _controller!.position;
    if (current == null) return;
    Duration newDuration = Duration(
      seconds: current.inSeconds + (forward ? 5 : -5),
    );
    _controller!.seekTo(newDuration);
  }

  Widget _getButton(bool isForward, Color color) {
    return InkWell(
      onTap: () {
        _move(forward: isForward);
      },
      child: Container(
        padding: const EdgeInsets.all(5),
        child: Icon(
          isForward ? Icons.forward_5 : Icons.replay_5,
          color: Colors.white,
          size: 20,
        ),
      ),
    );
  }

  _getVideoProgressIndicator({
    @required Color? color,
  }) {
    return Expanded(
      child: Row(
        children: [
          Expanded(
            child: VideoProgressIndicator(
              _controller!,
              padding: const EdgeInsets.only(bottom: 0),
              colors: VideoProgressColors(
                playedColor: Colors.white,
                bufferedColor: Colors.white.withOpacity(0.25),
              ),
              allowScrubbing: true,
            ),
          ),
        ],
      ),
    );
  }
}
