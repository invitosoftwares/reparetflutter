import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/user_model.dart';
import '../../providers/chat_provider.dart';
import '../../providers/contractor_project_details_provider.dart';
import '../../providers/user_project_details_provider.dart';
import '../../screens/contractor/project_details_for_contractor_screen.dart';
import '../../screens/user/project_details_for_user_screen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/shared_preferences.dart';
import '../../widgets/common/chat_input_widget.dart';
import '../../widgets/common/item_chat_message_others.dart';
import '../../widgets/common/item_chat_message_self.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';

class MainChatScreen extends StatefulWidget {
  static const String routeName = "MainChatScreen";

  const MainChatScreen({Key? key}) : super(key: key);

  @override
  _MainChatScreenState createState() => _MainChatScreenState();
}

class _MainChatScreenState extends State<MainChatScreen> {
  String? _otherPersonId;
  String? _projectId;
  String? _chatId;
  String? _myId;

  bool _isChatAllowed = true;

  Map<String, dynamic> args = {};
  ChatProvider? _chatProvider;
  bool _isFirstTime = true;

  bool _isUser = false;
  bool _isLoading = true;

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      args = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      _isUser = ((await Common.getUserModelFromSharedPreferences())?.role ??
              Constants.USER) ==
          Constants.USER;

      _otherPersonId = args["OTHER_USER_ID"];
      _projectId = "${args["PROJECT_ID"]}";
      _chatId = args["CHAT_ID"];
      _isChatAllowed = args["IS_CHAT_ALLOWED"] ?? true;

      UserModel? userModel = await Common.getUserModelFromSharedPreferences();
      _myId = "${userModel!.uuid}";

      _chatProvider = Provider.of<ChatProvider>(context, listen: false);
      _chatProvider!.turnOnSingleChatListeners(_chatId ?? "");

      MySharedPreferences.setString(
        Constants.currentChatWindowChatId,
        _chatId ?? "",
      );

      _getProjectDetails();
    }
  }

  @override
  void dispose() {
    _chatProvider!.isLoadingChat = true;
    _chatProvider!.turnOffSingleChatListeners();
    MySharedPreferences.clearSpecific(Constants.currentChatWindowChatId);
    super.dispose();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;

    return Consumer<ChatProvider>(
      builder: (context, value, child) => Scaffold(
        backgroundColor: CustomColors.colorGrayLight2,
        appBar: ToolbarWithBackAndText(
          _isUser ? CustomColors.colorAccent : CustomColors.colorPrimary,
          _screenSize,
          args["NAME"] ?? "",
          isCenter: false,
        ),
        body: Consumer<UserProjectDetailsProvider>(
          builder: (context, userProjectDetailsProvider, child) =>
              Consumer<ContractorProjectDetailsProvider>(
            builder: (context, contractorProjectDetailsProvider, child) =>
                Column(
              children: [
                const SizedBox(height: 4),
                if (!_isLoading)
                  InkWell(
                    onTap: () {
                      if (_isUser) {
                        Navigator.pushNamed(
                          context,
                          ProjectDetailsForUserScreen.routeName,
                          arguments: {
                            "PROJECT_ID": "$_projectId",
                          },
                        );
                      } else {
                        Navigator.pushNamed(
                          context,
                          ProjectDetailsForContractorScreen.routeName,
                          arguments: {
                            "PROJECT_ID": "$_projectId",
                          },
                        );
                      }
                    },
                    child: Container(
                      color: CustomColors.colorWhite,
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Image.asset(
                            "assets/images/icon_briefcase_filled.png",
                            color: _isUser
                                ? CustomColors.colorAccent
                                : CustomColors.colorPrimary,
                            height: 12,
                            width: 12,
                          ),
                          const SizedBox(width: 4),
                          Expanded(
                            child: Text(
                              _isUser
                                  ? (userProjectDetailsProvider
                                          .projectModel?.projectTitle ??
                                      "")
                                  : (contractorProjectDetailsProvider
                                          .projectModel?.projectTitle ??
                                      ""),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline1!
                                  .copyWith(
                                    color: CustomColors.colorGrayDark3,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12.0,
                                  ),
                            ),
                          ),
                          const SizedBox(width: 4),
                          Image.asset(
                            "assets/images/icon_date_range.png",
                            height: 10,
                            width: 10,
                          ),
                          const SizedBox(width: 4),
                          Text(
                            Common.getConvertedDate(
                              _isUser
                                  ? (userProjectDetailsProvider
                                          .projectModel?.projectTime ??
                                      "")
                                  : (contractorProjectDetailsProvider
                                          .projectModel?.projectTime ??
                                      ""),
                              Constants.DATE_FORMAT_4,
                              Constants.DATE_FORMAT_5,
                              isUtc: true,
                            ),
                            style: const TextStyle(
                              fontSize: 10,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                Expanded(
                  child: value.chatMessages.isEmpty
                      ? _getCenterText(
                          value.isLoadingChat
                              ? "Loading chat..."
                              : "Send your first message now...",
                        )
                      : ListView.builder(
                          padding: EdgeInsets.only(
                            left: _screenSize.width * 0.05,
                            top: _screenSize.width * 0.05,
                            bottom: _screenSize.width * 0.025,
                            right: _screenSize.width * 0.05,
                          ),
                          itemBuilder: (context, index) {
                            return value.chatMessages[index].senderId == _myId
                                ? ItemChatMessageSelf(
                                    value.chatMessages[index],
                                    _isUser,
                                  )
                                : ItemChatMessageOthers(
                                    value.chatMessages[index],
                                    _isUser,
                                  );
                          },
                          itemCount: value.chatMessages.length,
                          reverse: true,
                        ),
                ),
                _isChatAllowed
                    ? Padding(
                        padding: EdgeInsets.only(
                          left: _screenSize.width * 0.05,
                          bottom: _screenSize.width * 0.025,
                          right: _screenSize.width * 0.05,
                        ),
                        child: ChatInputWidget(
                          _otherPersonId ?? "",
                          _projectId ?? "",
                          _isUser,
                        ),
                      )
                    : Container(
                        height: 60,
                        width: double.infinity,
                        color: CustomColors.colorShimmerOverlay,
                        child: Center(
                          child: Text(
                            "You can not send messages to this chat anymore.",
                            textAlign: TextAlign.center,
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontSize: 16,
                                    ),
                          ),
                        ),
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getCenterText(String text) {
    return Center(
      child: Text(
        text,
        style: Theme.of(context).textTheme.bodyText1!.copyWith(
              height: 1.1,
              color: CustomColors.colorPrimary,
            ),
        textAlign: TextAlign.center,
      ),
    );
  }

  void _getProjectDetails() async {
    setState(() {
      _isLoading = true;
    });
    if (_isUser) {
      UserProjectDetailsProvider provider =
          Provider.of<UserProjectDetailsProvider>(context, listen: false);
      await provider.getProjectDetails(context, _projectId ?? "");
      setState(() {
        _isLoading = false;
      });
    } else {
      ContractorProjectDetailsProvider provider =
          Provider.of<ContractorProjectDetailsProvider>(context, listen: false);
      await provider.getProjectDetails(context, _projectId ?? "");
      setState(() {
        _isLoading = false;
      });
    }
  }
}
