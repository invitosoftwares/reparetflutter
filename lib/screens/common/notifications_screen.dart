import 'package:flutter/material.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/item_custom_notification.dart';
import '../../widgets/common/no_notifications_found_view.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';

class NotificationsScreen extends StatefulWidget {
  static final String routeName = "NotificationsScreen";

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  bool _isFirstTime = true;
  bool _isUser = false;
  List<NotificationModel> notifications = [];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      _isUser = data["IS_USER"];

      notifications.clear();
      notifications
          .add(NotificationModel("New Invitation", "Fix Roof", "4:02 pm"));
      notifications.add(
          NotificationModel("Payment received", "Tile replace", "4:02 pm"));
      notifications.add(NotificationModel(
          "New bid from joe root", "Broken window glass", "4:02 pm"));
      notifications.add(NotificationModel(
          "Joe root completed the project", "Broken window glass", "4:02 pm"));
    }
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        _isUser ? CustomColors.colorAccent : CustomColors.colorPrimary,
        _screenSize,
        "Notifications",
        isCenter: false,
      ),
      body: notifications.isEmpty
          ? Center(
              child: NoNotificationsFoundView(
                _isUser ? CustomColors.colorAccent : CustomColors.colorPrimary,
              ),
            )
          : ListView.builder(
              padding: EdgeInsets.only(
                left: _screenSize.width * 0.04,
                top: _screenSize.width * 0.04,
                bottom: _screenSize.width * 0.02,
                right: _screenSize.width * 0.04,
              ),
              itemBuilder: (context, index) =>
                  ItemCustomNotification(notifications[index], _isUser),
              itemCount: notifications.length,
            ),
    );
  }
}

class NotificationModel {
  String notificationTitle;
  String projectTitle;
  String time;

  NotificationModel(this.notificationTitle, this.projectTitle, this.time);
}
