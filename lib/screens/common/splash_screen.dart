import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../models/user_model.dart';
import '../../providers/auth_provider.dart';
import '../../providers/chat_provider.dart';
import '../../screens/common/login_screen.dart';
import '../../screens/contractor/contractor_dashboard_screen.dart';
import '../../screens/user/user_dashboard_sreen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = "SplashScreen";

  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _isFirstTime = true;

  @override
  void didChangeDependencies() {
    if (_isFirstTime) {
      checkLoginStatus();
      _isFirstTime = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Image.asset(
          "assets/images/splash.png",
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  void checkLoginStatus() async {
    if (await MySharedPreferences.getBool(Constants.isUserLoggedIn)) {
      await Provider.of<ChatProvider>(context, listen: false).connect();
      UserModel? userModel = await Common.getUserModelFromSharedPreferences();
      if (userModel == null) {
        Navigator.pushNamedAndRemoveUntil(
          context,
          LoginScreen.routeName,
          (route) => false,
        );
        return;
      }
      print(userModel);

      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      authProvider.userModel = userModel;
      // authProvider.userModel?.image =
      //     await UploadFileS3.getImageUrl(authProvider.userModel!.image);

      if (userModel.role == Constants.USER) {
        Navigator.pushNamedAndRemoveUntil(
          context,
          UserDashboardScreen.routeName,
          (route) => false,
        );
      } else if (userModel.role == Constants.CONTRACTOR) {
        Navigator.pushNamedAndRemoveUntil(
          context,
          ContractorDashboardScreen.routeName,
          (route) => false,
        );
      }
    } else {
      Navigator.pushNamedAndRemoveUntil(
        context,
        LoginScreen.routeName,
        (route) => false,
      );
    }
  }
}
