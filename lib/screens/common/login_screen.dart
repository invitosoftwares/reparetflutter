import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../models/user_model.dart';
import '../../providers/auth_provider.dart';
import '../../providers/chat_provider.dart';
import '../../screens/common/forgot_password_screen.dart';
import '../../screens/contractor/contractor_dashboard_screen.dart';
import '../../screens/contractor/contractor_signup_screen.dart';
import '../../screens/user/user_dashboard_sreen.dart';
import '../../screens/user/user_signup_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../utils/shared_preferences.dart';
import '../../widgets/common/password_text_form_field.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/signup_choose_register_type.dart';

class LoginScreen extends StatefulWidget {
  static final String routeName = "LoginScreen";

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "EMAIL": null,
    "PASSWORD": null,
  };
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    var _customDecorators = CustomDecorators(context);

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(0),
        child: Container(),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Stack(
            children: [
              Image.asset(
                "assets/images/login_background.png",
                width: double.infinity,
                fit: BoxFit.fill,
                height: _screenSize.height * 0.58,
              ),
              Container(
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(height: _screenSize.height * 0.15),
                    Image.asset(
                      "assets/images/logo.png",
                      width: _screenSize.width * 0.5,
                      height: _screenSize.width * 0.5,
                    ),
                    SizedBox(height: _screenSize.height * 0.18),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: _screenSize.width * 0.05),
                      child: Column(
                        children: [
                          getElevatedWidget(
                            TextFormField(
                              validator: (value) {
                                if (value == null || value.trim().isEmpty) {
                                  _data["EMAIL"] = FormFieldData(
                                      true, "Please enter email/username");
                                } else if (!Common.isValidEmail(value.trim())) {
                                  _data["EMAIL"] = FormFieldData(
                                      true, "Please enter some valid email");
                                } else {
                                  _data["EMAIL"] = FormFieldData(false, value);
                                }
                                return null;
                              },
                              style: Theme.of(context).textTheme.bodyText1,
                              keyboardType: TextInputType.emailAddress,
                              maxLines: 1,
                              textAlign: TextAlign.start,
                              cursorColor: Theme.of(context).primaryColor,
                              maxLength: 50,
                              inputFormatters: [
                                MyLengthLimitingTextInputFormatter(50),
                              ],
                              decoration: _customDecorators.getInputDecoration(
                                "Email/Username",
                                prefixIconString: "assets/images/icon_user.png",
                              ),
                            ),
                          ),
                          SizedBox(height: 8),
                          getElevatedWidget(
                            PasswordTextFormField(
                              validator: (value) {
                                if (value == null || value.trim().isEmpty) {
                                  _data["PASSWORD"] = FormFieldData(
                                      true, "Please enter password");
                                } else {
                                  _data["PASSWORD"] =
                                      FormFieldData(false, value);
                                }
                                return null;
                              },
                              placeholder: "Password",
                              prefixIconString: "assets/images/icon_key.png",
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: InkWell(
                              onTap: () => Navigator.pushNamed(
                                  context, ForgotPasswordScreen.routeName),
                              child: Padding(
                                padding: EdgeInsets.symmetric(
                                    vertical: 5.0, horizontal: 12.0),
                                child: Text(
                                  "Forgot Password?",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(
                                        color: CustomColors.colorPrimary,
                                      ),
                                ),
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: PrimaryRaisedButton(
                              "Login",
                              () {
                                _validateData(context);
                              },
                              size: _screenSize,
                              isLoading: _isLoading,
                              color: CustomColors.colorPrimary,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: _screenSize.height * 0.025),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "New User?",
                          style:
                              Theme.of(context).textTheme.bodyText1!.copyWith(
                                    fontWeight: FontWeight.w600,
                                  ),
                        ),
                        SizedBox(width: _screenSize.width * 0.01),
                        InkWell(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (context) => SignUpChooseRegisterType(
                                  _screenSize, (int type) {
                                if (type == SignUpChooseRegisterType.USER) {
                                  Navigator.pushNamed(
                                      context, UserSignUpScreen.routeName);
                                } else {
                                  Navigator.pushNamed(context,
                                      ContractorSignUpScreen.routeName);
                                }
                              }),
                            );
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical: 8.0),
                            child: Text(
                              "Create Account",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    fontWeight: FontWeight.w600,
                                    decoration: TextDecoration.underline,
                                    decorationColor:
                                        CustomColors.colorGrayDark2,
                                    shadows: [
                                      const Shadow(
                                          color: CustomColors.colorAccent,
                                          offset: Offset(0, -1))
                                    ],
                                    color: Colors.transparent,
                                  ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: _screenSize.height * 0.025),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Widget getElevatedWidget(Widget widget) {
    return Material(
      elevation: 1,
      borderRadius: BorderRadius.all(Radius.circular(5)),
      child: widget,
    );
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState?.validate();

    List<String> errors = Common.getErrors(_data);

    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    setState(() {
      _isLoading = true;
    });

    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      dynamic response = await authProvider.loginApi(
        context,
        _data["EMAIL"]?.data ?? "",
        _data["PASSWORD"]?.data ?? "",
      );

      if (response is UserModel) {
        await MySharedPreferences.setBool(Constants.isUserLoggedIn, true);
        await Common.setUserModelInSharedPreferences(response);
        await Provider.of<ChatProvider>(context, listen: false).connect();
        authProvider.userModel = response;

        if (response.role == Constants.USER) {
          Navigator.pushNamedAndRemoveUntil(
            context,
            UserDashboardScreen.routeName,
            (route) => false,
          );
        } else if (response.role == Constants.CONTRACTOR) {
          Navigator.pushNamedAndRemoveUntil(
            context,
            ContractorDashboardScreen.routeName,
            (route) => false,
          );
        }
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
