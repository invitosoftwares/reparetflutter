import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:photo_view/photo_view.dart';
import 'package:provider/provider.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';

class ImagesDetailsScreen extends StatefulWidget {
  static const String routeName = "ImagesDetailsScreen";

  const ImagesDetailsScreen({Key? key}) : super(key: key);

  @override
  _ImagesDetailsScreenState createState() => _ImagesDetailsScreenState();
}

class _ImagesDetailsScreenState extends State<ImagesDetailsScreen> {
  File? _imageFile;
  String? _imageUrl;
  bool _isFirstTime = true;
  Size? _screenSize;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      _imageFile = data["IMAGE_FILE"];
      _imageUrl = data["IMAGE_URL"];
    }
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Consumer<AuthProvider>(
      builder: (context, value, child) => Scaffold(
        backgroundColor: CustomColors.colorWhite,
        appBar: ToolbarWithBackAndText(
          (value.userModel!.role == Constants.USER)
              ? CustomColors.colorAccent
              : CustomColors.colorPrimary,
          _screenSize!,
          "Back",
          isCenter: false,
        ),
        body: Padding(
          padding: const EdgeInsets.only(
            right: 4,
            left: 4,
          ),
          child:
              (_imageFile == null && (_imageUrl == null || _imageUrl!.isEmpty))
                  ? Container(
                      padding: const EdgeInsets.all(10),
                      color: CustomColors.colorAccent.withOpacity(0.1),
                      child: Image.asset(
                        "assets/images/logo.png",
                        width: double.infinity,
                        fit: BoxFit.scaleDown,
                      ),
                    )
                  : (_imageFile != null)
                      ? PhotoView(
                          imageProvider: FileImage(_imageFile!),
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.transparent,
                          ),
                          loadingBuilder: (context, event) {
                            return _loader();
                          },
                        )
                      : PhotoView(
                          imageProvider: NetworkImage(_imageUrl ?? ""),
                          backgroundDecoration: const BoxDecoration(
                            color: Colors.transparent,
                          ),
                          loadingBuilder: (context, event) {
                            return _loader();
                          },
                        ),
        ),
      ),
    );
  }

  _loader() {
    return const Center(
      child: SizedBox(
        height: 25,
        width: 25,
        child: CircularProgressIndicator(
          color: CustomColors.colorPrimary,
          strokeWidth: 2,
        ),
      ),
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }
}
