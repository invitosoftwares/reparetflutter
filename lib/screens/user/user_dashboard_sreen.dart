import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/logout_model.dart';
import '../../models/project_model.dart';
import '../../providers/auth_provider.dart';
import '../../providers/chat_provider.dart';
import '../../providers/payment_methods_provider.dart';
import '../../providers/post_project_provider.dart';
import '../../providers/user_listings_provider.dart';
import '../../providers/user_search_contractors_provider.dart';
import '../../screens/common/login_screen.dart';
import '../../screens/common/notifications_screen.dart';
import '../../screens/common/webview_screen.dart';
import '../../screens/user/favourite_list_screen.dart';
import '../../screens/user/user_profile_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/shared_preferences.dart';
import '../../widgets/common/app_bar_searchable.dart';
import '../../widgets/common/chat_listing_view.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/user/post_new_project.dart';
import '../../widgets/user/search_contractors_view.dart';
import '../../widgets/user/user_dashboard_navigation_bar.dart';
import '../../widgets/user/user_drawer.dart';
import '../../widgets/user/user_listings_view.dart';

import 'manage_payment_methods_screen.dart';

class UserDashboardScreen extends StatefulWidget {
  static const String routeName = "UserDashboardScreen";

  static const int PROJECT = 0;
  static const int LISTINGS = 1;
  static const int CHAT = 2;
  static const int CONTRACTOR = 3;

  const UserDashboardScreen({Key? key}) : super(key: key);

  @override
  _UserDashboardScreenState createState() => _UserDashboardScreenState();
}

class _UserDashboardScreenState extends State<UserDashboardScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isFirstTime = true;
  int _currentView = UserDashboardScreen.LISTINGS;
  bool _isLoading = false;
  final StreamController<String> _querySearchController =
      StreamController<String>.broadcast();
  final StreamController<String> _queryClearController =
      StreamController<String>.broadcast();

  final List<String> _toolbarTitles = [
    "Post new project",
    "Projects",
    "Chats",
    "Search Contractor",
  ];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      Common.callProfileDetailsApi(context);
      Common.getAllCategories(context);
    }
  }

  @override
  void dispose() {
    super.dispose();
    _querySearchController.close();
    _queryClearController.close();
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    //Provider.of<ProjectModel>(context, listen: true);
    return Consumer<ProjectModel>(
      builder: (ctx, pModel, child) => Scaffold(
        key: _scaffoldKey,
        backgroundColor: CustomColors.colorGrayLight2,
        appBar: DashboardAppBarWithSearch(
          title: _toolbarTitles[_currentView],
          // notificationClick: _currentView == UserDashboardScreen.LISTINGS
          //     ? () => Navigator.pushNamed(context, NotificationsScreen.routeName,
          //         arguments: {"IS_USER": true})
          //     : null,
          // notificationCount: 5,
          sideMenuClick: () {
            _scaffoldKey.currentState!.openEndDrawer();
          },
          searchQuery: _currentView == UserDashboardScreen.LISTINGS
              ? (value) {
                  _querySearchController.add(value);
                }
              : null,
          mainColor: CustomColors.colorAccent,
          accentColor: CustomColors.colorPrimary,
          isLoading: _isLoading,
          clearQuery: _queryClearController,
        ),
        bottomNavigationBar: IntrinsicHeight(
          child: Stack(
            children: [
              UserDashboardNavigationBar(
                _onTabTapped,
                _currentView,
                _screenSize,
              ),
              if (_isLoading)
                InkWell(
                  onTap: () {},
                  child: Container(
                    color: CustomColors.colorBlack.withOpacity(0.15),
                  ),
                ),
            ],
          ),
        ),
        body: Stack(
          children: [
            _currentView == UserDashboardScreen.PROJECT
                ? MultiProvider(
                    providers: [
                      ChangeNotifierProvider(
                        create: (context) => PaymentMethodsProvider(),
                      ),
                      ChangeNotifierProvider(
                        create: (context) => PostProjectProvider(),
                      ),
                    ],
                    child: PostNewProject(_onProjectPosted),
                  )
                : _currentView == UserDashboardScreen.LISTINGS
                    ? ChangeNotifierProvider(
                        create: (context) => UserListingProvider(),
                        child: UserListingsView(
                          _screenSize,
                          newProjectClick: _onPostProject,
                          queryStreamController: _querySearchController,
                          onInternalTabChanged: () {
                            _queryClearController.add("1");
                          },
                        ),
                      )
                    : _currentView == UserDashboardScreen.CHAT
                        ? ChatListingView(true)
                        : _currentView == UserDashboardScreen.CONTRACTOR
                            ? const SearchContractorsView()
                            : Container(),
            if (_isLoading) SpinKitLoading(Constants.USER, false),
          ],
        ),
        endDrawer: UserDrawer(
          _screenSize,
          _onDrawerItemSelected,
        ),
      ),
    );
  }

  void _onDrawerItemSelected(int option) {
    if (option == UserDrawer.LOGOUT) {
      _confirmLogout();
    } else if (option == UserDrawer.FAVOURITES) {
      Navigator.pushNamed(context, FavouriteListScreen.routeName);
    } else if (option == UserDrawer.PROFILE) {
      Navigator.pushNamed(context, UserProfileScreen.routeName);
    } else if (option == UserDrawer.PAYMENT_METHODS) {
      Navigator.pushNamed(context, ManagePaymentMethodsScreen.routeName);
    } else if (option == UserDrawer.PRIVACY_POLITY) {
      Navigator.pushNamed(context, WebViewScreen.routeName, arguments: {
        "URL": Constants.PRIVACY_POLICY_URL,
        "ROLE": Constants.USER,
        "TITLE": "Privacy policy"
      });
    } else if (option == UserDrawer.SUPPORT) {
      Navigator.pushNamed(context, WebViewScreen.routeName, arguments: {
        "URL": Constants.SUPPORT_URL,
        "ROLE": Constants.USER,
        "TITLE": "Support"
      });
    } else if (option == UserDrawer.DELETE_ACCOUNT) {
      Common.deleteAccount(context);
    }
  }

  void _onTabTapped(int index) {
    _queryClearController.add("1");
    setState(() {
      _currentView = index;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _confirmLogout() async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "No",
      "Yes",
      "Are you sure you want to logout?",
      () => Navigator.pop(context),
      () {
        callLogout();
        Navigator.pop(context);
      },
    );
  }

  void callLogout() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    setState(() {
      _isLoading = true;
    });

    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      dynamic response = await authProvider.logoutApi(context);

      if (response is LogoutModel) {
        await MySharedPreferences.clearAll();
        await Provider.of<ChatProvider>(context, listen: false).disconnect();

        Navigator.pushNamedAndRemoveUntil(
          context,
          LoginScreen.routeName,
          (route) => false,
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (error) {
      print(error);
    }

    setState(() {
      _isLoading = false;
    });
  }

  _onProjectPosted() {
    setState(() {
      _currentView = UserDashboardScreen.LISTINGS;
    });
  }

  _onPostProject() {
    setState(() {
      _currentView = UserDashboardScreen.PROJECT;
    });
  }
}
