import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/contractor_details_model.dart';
import '../../providers/contractor_details_provider.dart';
import '../../providers/project_invitation_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/show_success_dialog.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/user/get_contractor_details_view.dart';
import '../../widgets/user/item_contractor_recent_projects.dart';
import '../../widgets/user/select_project_to_invite_popup.dart';

class ContractorProfileScreen extends StatefulWidget {
  static const String routeName = "ContractorProfileScreen";

  const ContractorProfileScreen({Key? key}) : super(key: key);

  @override
  State<ContractorProfileScreen> createState() =>
      _ContractorProfileScreenState();
}

class _ContractorProfileScreenState extends State<ContractorProfileScreen> {
  bool _showInvite = true;
  String? _providerId;
  bool _isFirstTime = true;
  ContractorDetailsProvider? _contractorDetailsProvider;
  bool _isLoading = true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _contractorDetailsProvider = Provider.of<ContractorDetailsProvider>(
        context,
        listen: false,
      );

      Map<String, dynamic>? data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;
      if (data != null) {
        if (data.containsKey("PROVIDER_ID")) {
          _providerId = data["PROVIDER_ID"];
        }
        if (data.containsKey("SHOW_INVITE")) _showInvite = data["SHOW_INVITE"];
      }
      _getProviderDetails();
    }
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorAccent,
        _screenSize,
        "Contractor's Profile",
        isCenter: false,
      ),
      body: _isLoading
          ? Container()
          : Consumer<ContractorDetailsProvider>(
              builder: (context, provider, child) => Padding(
                padding: EdgeInsets.all(_screenSize.width * 0.05),
                child: Column(
                  children: [
                    GetContractorDetailsView(
                      provider.contractorDetailsDataModel?.image ?? "",
                      "${provider.contractorDetailsDataModel?.firstName} ${provider.contractorDetailsDataModel?.lastName}",
                      provider.contractorDetailsDataModel?.speciality!
                              .join(", ") ??
                          "",
                      provider.contractorDetailsDataModel?.providerVerified ??
                          false,
                      provider.contractorDetailsDataModel?.rating ?? 0,
                      provider.contractorDetailsDataModel?.reviewCount ?? 0,
                      _screenSize,
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            const SizedBox(height: 5),
                            getTextWithIcon(
                              "Projects completed:",
                              "assets/images/icon_briefcase_filled.png",
                              context,
                              projects:
                                  "${provider.contractorDetailsDataModel?.completedProjectCount ?? 0}",
                            ),
                            getTextWithIcon(
                              provider.contractorDetailsDataModel?.location ??
                                  "N/A",
                              "assets/images/icon_location.png",
                              context,
                            ),
                            getTextWithIcon(
                              provider.contractorDetailsDataModel
                                      ?.companyName ??
                                  "N/A",
                              "assets/images/icon_company.png",
                              context,
                            ),
                            getTextWithIcon(
                              provider.contractorDetailsDataModel?.bio ?? "N/A",
                              "assets/images/icon_info.png",
                              context,
                            ),
                            const SizedBox(height: 5),
                            if (_showInvite)
                              Align(
                                alignment: Alignment.topRight,
                                child: PrimaryRaisedButton(
                                  "Invite",
                                  () {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return ChangeNotifierProvider(
                                          create: (context) =>
                                              ProjectInvitationProvider(),
                                          child: SelectProjectToInvitePopup(
                                            providerId: _providerId ?? "",
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  size: _screenSize,
                                  color: CustomColors.colorAccent,
                                  leadingIcon: "assets/images/icon_plus.png",
                                ),
                              ),
                            SizedBox(height: _screenSize.width * 0.05),
                            getProjectsTitle(context),
                            const SizedBox(height: 5),
                            getProjectsListing(_screenSize,
                                provider.contractorDetailsDataModel?.projects!),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }

  Widget getTextWithIcon(
    String text,
    String icon,
    BuildContext context, {
    String? projects,
  }) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: Image.asset(
              icon,
              color: CustomColors.colorAccent,
              height: 16,
              width: 16,
            ),
          ),
          const SizedBox(width: 10),
          Flexible(
            fit: FlexFit.loose,
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          if (projects != null) const SizedBox(width: 5),
          if (projects != null)
            Text(
              projects,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    fontWeight: FontWeight.w600,
                    color: CustomColors.colorAccent,
                  ),
            ),
        ],
      ),
    );
  }

  Widget getProjectsTitle(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Recent Projects",
          style: Theme.of(context).textTheme.headline1!.copyWith(
                color: CustomColors.colorGrayDark2,
                fontSize: 22.0,
              ),
        ),
        Container(
          height: 0.5,
          margin: const EdgeInsets.only(top: 4),
          width: double.infinity,
          color: CustomColors.colorAccent,
        ),
      ],
    );
  }

  getProjectsListing(Size _size, List<RecentProjectModel>? list) {
    if (list == null || list.isEmpty) {
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 25.0),
        child: Center(
          child: Text(
            "No recent projects found!",
            style: Theme.of(context).textTheme.headline1!.copyWith(
                  color: CustomColors.colorGrayDark2,
                  fontSize: 15.0,
                ),
          ),
        ),
      );
    }
    return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.only(top: 4.0),
        child: ChangeNotifierProvider.value(
          value: list[index],
          child: ItemContractorRecentProjects(
            _size,
          ),
        ),
      ),
      itemCount: list.length,
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  Future<dynamic> _getProviderDetails() async {
    if (_providerId == null || _providerId!.isEmpty) {
      await Future.delayed(Duration.zero);
      AlertDialogs.showAlertDialogWithOk(
        context,
        Constants.UNABLE_TO_PROCESS,
        dismiss: false,
        onOkayClicked: () {
          Navigator.pop(context);
        },
      );
      return;
    }
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed",
        dismiss: false,
        onOkayClicked: () {
          Navigator.pop(context);
        },
      );
      return;
    }

    Common.showLoaderDialog("Please wait...", context);

    try {
      var res = await _contractorDetailsProvider!.getContractorDetails(
        context,
        _providerId ?? "",
      );

      Navigator.pop(context);
      if (res is bool && res) {
        setState(() {
          _isLoading = false;
        });
      } else if (res is String) {
        AlertDialogs.showAlertDialogWithOk(context, res);
      }
    } catch (err) {
      print(err);
    }
  }
}
