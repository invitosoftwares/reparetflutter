import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/get_all_cards_parent_model.dart';
import '../../providers/payment_methods_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/user/bottom_sheet_enter_card.dart';

class ManagePaymentMethodsScreen extends StatefulWidget {
  static const String routeName = "ManagePaymentMethodsScreen";

  const ManagePaymentMethodsScreen({Key? key}) : super(key: key);

  @override
  _ManagePaymentMethodsScreenState createState() =>
      _ManagePaymentMethodsScreenState();
}

class _ManagePaymentMethodsScreenState
    extends State<ManagePaymentMethodsScreen> {
  Size? _screenSize;
  bool _isFirstTime = true;
  bool _isLoading = false;
  bool _forProjectPayment = false;
  late PaymentMethodsProvider provider;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      provider = Provider.of(context, listen: false);
      _getAllCards();

      Map<String, dynamic>? data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;
      if (data != null &&
          data.containsKey("PROJECT_PAYMENT") &&
          data["PROJECT_PAYMENT"]) {
        _forProjectPayment = true;
      }
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorAccent,
        _screenSize!,
        _forProjectPayment ? "Select Payment Method" : "Payment Methods",
        isCenter: false,
        onBackPressed: () {
          Navigator.pop(context);
        },
      ),
      body: _isLoading
          ? SpinKitLoading(
              Constants.USER,
              true,
            )
          : Consumer<PaymentMethodsProvider>(
              builder: (context, value, child) => value.allCards.isEmpty
                  ? _getNoCardView()
                  : _getPaymentMethodsList(value.allCards),
            ),
      floatingActionButton: _isLoading
          ? null
          : Consumer<PaymentMethodsProvider>(
              builder: (context, value, child) => value.allCards.isEmpty
                  ? Container()
                  : FloatingActionButton(
                      backgroundColor: CustomColors.colorPrimary,
                      onPressed: () {
                        _showEnterCardBottomSheet();
                      },
                      child: const Icon(
                        Icons.add,
                        color: CustomColors.colorWhite,
                      ),
                    ),
            ),
    );
  }

  Widget _getPaymentMethodsList(List<CardModel> allCards) {
    return ListView.builder(
      padding: const EdgeInsets.only(bottom: 80),
      itemCount: allCards.length,
      itemBuilder: (context, itemIndex) => Padding(
        padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0),
        child: InkWell(
          onTap: () {
            if (_forProjectPayment) {
              String pmId = allCards[itemIndex].pmId ?? "";
              if (pmId.isEmpty) {
                Common.showToast(Constants.UNABLE_TO_PROCESS);
                return;
              }
              Navigator.pop(context, pmId);
            }
          },
          child: Container(
            width: double.infinity,
            constraints: BoxConstraints(
              minHeight: _screenSize!.height * 0.2,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                colors: [
                  itemIndex % 2 == 0
                      ? CustomColors.colorAccent
                      : CustomColors.colorPrimary,
                  itemIndex % 2 == 0
                      ? CustomColors.colorAccent.withOpacity(0.75)
                      : CustomColors.colorPrimary.withOpacity(0.75),
                ],
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      const Icon(
                        Icons.credit_card,
                        color: CustomColors.colorWhite,
                      ),
                      const SizedBox(width: 10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "CARD NUMBER",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    fontSize: 10,
                                    height: 1,
                                    color: CustomColors.colorWhite,
                                  ),
                            ),
                            Text(
                              "xxxx xxxx xxxx ${allCards[itemIndex].last4}",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1!
                                  .copyWith(
                                    fontSize: 19,
                                    height: 1,
                                    color: CustomColors.colorWhite,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(width: 10),
                      if (!_forProjectPayment)
                        InkWell(
                          onTap: () {
                            _deleteCardConfirmation(
                              allCards[itemIndex].pmId!,
                            );
                          },
                          child: const Icon(
                            Icons.delete,
                            color: CustomColors.colorWhite,
                          ),
                        ),
                    ],
                  ),
                  Row(
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "EXPIRY DATE",
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontSize: 10,
                                      height: 1,
                                      color: CustomColors.colorWhite,
                                    ),
                          ),
                          Text(
                            "${allCards[itemIndex].expMonth}/${allCards[itemIndex].expYear}",
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontSize: 19,
                                      color: CustomColors.colorWhite,
                                      height: 1,
                                    ),
                          ),
                        ],
                      ),
                      Expanded(child: Container()),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "CVV",
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontSize: 10,
                                      height: 1,
                                      color: CustomColors.colorWhite,
                                    ),
                          ),
                          Text(
                            "xxx",
                            style:
                                Theme.of(context).textTheme.bodyText1!.copyWith(
                                      fontSize: 19,
                                      height: 1,
                                      color: CustomColors.colorWhite,
                                    ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getNoCardView() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "No saved cards",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 22.0,
                fontWeight: FontWeight.w600,
                color: CustomColors.colorAccent,
              ),
        ),
        Image.asset(
          "assets/images/empty_cards_list.png",
          width: double.infinity,
        ),
        FloatingActionButton(
          onPressed: () {
            _showEnterCardBottomSheet();
          },
          child: const Icon(
            Icons.add,
            color: CustomColors.colorWhite,
          ),
        ),
        const SizedBox(height: 10),
        Text(
          "Add new",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
              ),
        ),
      ],
    );
  }

  void _getAllCards({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    if (showLoader) {
      setState(() {
        _isLoading = true;
      });
    }

    try {
      await provider.getAllCards(context);
    } catch (e) {
      print(e);
    }

    if (showLoader) {
      setState(() {
        _isLoading = false;
      });
    }
  }

  _showEnterCardBottomSheet() async {
    dynamic res = await showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      builder: (context) => Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: ChangeNotifierProvider.value(
          value: provider,
          child: const BottomSheetEnterCard(),
        ),
      ),
    );

    if (res is bool && res) {
      _getAllCards(showLoader: false);
    }
  }

  _deleteCardConfirmation(String id) async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "Cancel",
      "Delete",
      "Are you sure you want to delete this card?",
      () {
        Navigator.pop(context);
      },
      () {
        Navigator.pop(context);
        _deleteCard(id);
      },
    );
  }

  void _deleteCard(String id) async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);

    try {
      dynamic response = await provider.deleteCard(context, id);
      Navigator.pop(context);
      if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }
  }
}
