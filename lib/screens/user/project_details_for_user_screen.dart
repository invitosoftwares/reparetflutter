import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../providers/user_listings_provider.dart';
import '../../providers/user_project_details_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/project_details_radio_button_tabs.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/user/get_project_basic_details_view.dart';
import '../../widgets/user/project_details_user_show_contractor_details_view.dart';
import '../../widgets/user/project_details_details_user_view.dart';
import '../../widgets/user/project_details_new_show_all_bids.dart';
import '../../widgets/user/project_details_new_show_all_messages.dart';

class ProjectDetailsForUserScreen extends StatefulWidget {
  static const String routeName = "ProjectDetailsForUserScreen";

  const ProjectDetailsForUserScreen({Key? key}) : super(key: key);

  @override
  _ProjectDetailsForUserScreenState createState() =>
      _ProjectDetailsForUserScreenState();
}

class _ProjectDetailsForUserScreenState
    extends State<ProjectDetailsForUserScreen> {
  bool _isFirstTime = true;
  String? _projectId;
  int _currentView = ProjectDetailsRadioButtonTabs.PROJECT_DETAILS;
  UserProjectDetailsProvider? _projectDetailsProvider;
  bool isBidConfirmed = false;
  bool isProjectEdited = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      _projectId = data["PROJECT_ID"];

      _projectDetailsProvider =
          Provider.of<UserProjectDetailsProvider>(context, listen: false);
      _getProjectDetails();
    }
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    Provider.of<ProjectModel>(context, listen: true);
    return WillPopScope(
      onWillPop: () async {
        Navigator.pop(context, {
          "BID_CONFIRMED": isBidConfirmed,
          "PROJECT_EDITED": isProjectEdited,
          "PROJECT_ID": _projectId ?? "",
        });
        return false;
      },
      child: Scaffold(
        backgroundColor: CustomColors.colorGrayLight2,
        appBar: ToolbarWithBackAndText(
          CustomColors.colorAccent,
          _screenSize,
          "Project Details",
          isCenter: false,
          onBackPressed: () {
            Navigator.pop(context, {
              "BID_CONFIRMED": isBidConfirmed,
              "PROJECT_EDITED": isProjectEdited,
              "PROJECT_ID": _projectId ?? "",
            });
          },
        ),
        body: Consumer<UserProjectDetailsProvider>(
          builder: (context, provider, child) => (provider.projectModel == null)
              ? Container()
              : Padding(
                  padding: EdgeInsets.only(
                    left: _screenSize.width * 0.05,
                    top: _screenSize.width * 0.05,
                    right: _screenSize.width * 0.05,
                  ),
                  child: Column(
                    children: [
                      GetProjectBasicDetailsView(
                        provider.projectModel?.projectimages![0].image ?? "",
                        provider.projectModel?.projectTitle ?? "",
                        provider.projectModel?.category ?? "",
                        Common.getConvertedDate(
                          provider.projectModel?.projectTime ?? "",
                          Constants.DATE_FORMAT_4,
                          Constants.DATE_FORMAT_5,
                          isUtc: true,
                        ),
                        Common.getConvertedDate(
                          provider.projectModel?.createdAt ?? "",
                          Constants.DATE_FORMAT_4,
                          Constants.DATE_FORMAT_5,
                          isUtc: true,
                        ),
                        provider.projectModel?.projectStatus ?? 0,
                        _screenSize,
                      ),
                      SizedBox(height: _screenSize.width * 0.04),
                      ProjectDetailsRadioButtonTabs(
                        (int type) {
                          setState(() {
                            _currentView = type;
                          });
                        },
                        CustomColors.colorPrimary,
                        ((provider.projectModel?.projectStatus ?? 0) ==
                                Constants.PROJECT_COMPLETED ||
                            (provider.projectModel?.projectStatus ?? 0) ==
                                Constants.PROJECT_PAID ||
                            (provider.projectModel?.projectStatus ?? 0) ==
                                Constants.PROJECT_PAID_REVIEWED_BY_USER),
                        badgeCountColor: CustomColors.colorAccent,
                        unreadMessagesCount:
                            provider.projectModel?.unreadMessages ?? 0,
                        bidCount:
                            ((provider.projectModel?.projectStatus ?? 0) ==
                                    Constants.PROJECT_POSTED)
                                ? (provider.projectModel?.bidsCount ?? 0)
                                : 0,
                      ),
                      Expanded(
                        child: Container(
                          width: double.infinity,
                          padding: const EdgeInsets.only(top: 5),
                          child: (_currentView ==
                                  ProjectDetailsRadioButtonTabs.PROJECT_DETAILS)
                              ? ProjectDetailsDetailsUserView(
                                  provider.projectModel!,
                                  _screenSize,
                                  enableActions:
                                      (provider.projectModel?.projectStatus ??
                                              0) ==
                                          Constants.PROJECT_POSTED,
                                  showActions: (provider.projectModel
                                                  ?.projectStatus ??
                                              0) ==
                                          Constants.PROJECT_POSTED ||
                                      (provider.projectModel?.projectStatus ??
                                              0) ==
                                          Constants.PROJECT_ACCEPTED,
                                  deleteProjectClick: () {
                                    _deleteProject();
                                  },
                                  onProjectEdited: () {
                                    isProjectEdited = true;
                                    _getProjectDetails();
                                  },
                                )
                              : (_currentView ==
                                      ProjectDetailsRadioButtonTabs.BIDS)
                                  ? ProjectDetailsNewShowAllBids(
                                      _screenSize,
                                      projectId:
                                          "${provider.projectModel?.projectId ?? ""}",
                                      onBidConfirmed: () {
                                        _getProjectDetails();
                                        isBidConfirmed = true;
                                      },
                                    )
                                  : (_currentView ==
                                          ProjectDetailsRadioButtonTabs
                                              .MESSAGES)
                                      ? ProjectDetailsNewShowAllMessages(
                                          _screenSize,
                                          projectId:
                                              "${provider.projectModel?.projectId ?? ""}",
                                        )
                                      : (_currentView ==
                                              ProjectDetailsRadioButtonTabs
                                                  .CONTRACTOR_DETAILS)
                                          ? ChangeNotifierProvider.value(
                                              value: Provider.of<
                                                      UserListingProvider>(
                                                  context,
                                                  listen: false),
                                              child:
                                                  ProjectDetailsUserShowContractorDetailsView(
                                                _screenSize,
                                                projectModel:
                                                    provider.projectModel!,
                                                refreshData: _getProjectDetails,
                                              ),
                                            )
                                          : Container(),
                        ),
                      ),
                    ],
                  ),
                ),
        ),
      ),
    );
  }

  void _getProjectDetails() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);
    try {
      dynamic response = await _projectDetailsProvider!.getProjectDetails(
        context,
        _projectId ?? "",
      );

      Navigator.pop(context);

      if ((response is bool) && response) {
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }
  }

  void _deleteProject() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed",
      );
      return;
    }

    Common.showLoaderDialog("Please wait...", context);
    try {
      dynamic response = await _projectDetailsProvider!.deleteProject(
        context,
        _projectId ?? "",
      );

      Navigator.pop(context);

      if ((response is bool) && response) {
        Navigator.pop(
          context,
          {
            "DELETE": true,
            "PROJECT_ID": _projectId ?? "",
          },
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }
}
