import 'package:flutter/material.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/user/user_edit_profile_view.dart';
import '../../widgets/user/user_show_profile_view.dart';

class UserProfileScreen extends StatefulWidget {
  static const String routeName = "ProfileScreen";

  const UserProfileScreen({Key? key}) : super(key: key);

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  Size? _screenSize;
  bool _isEdit = false;

  @override
  void initState() {
    super.initState();
//    Common.callProfileDetailsApi(context);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        if (!_isEdit) return true;
        setState(() {
          _isEdit = false;
        });
        return false;
      },
      child: Scaffold(
        backgroundColor: CustomColors.colorGrayLight2,
        appBar: ToolbarWithBackAndText(
          CustomColors.colorAccent,
          _screenSize!,
          _isEdit ? "Edit Profile" : "Profile",
          isCenter: false,
          onBackPressed: () {
            if (_isEdit) {
              setState(() {
                _isEdit = false;
              });
            } else {
              Navigator.pop(context);
            }
          },
        ),
        body: AnimatedSwitcher(
          duration: const Duration(milliseconds: 250),
          child: _isEdit
              ? Align(
                  alignment: Alignment.topCenter,
                  child: UserEditProfileView(
                    () {
                      setState(() {
                        _isEdit = false;
                      });
                    },
                  ),
                )
              : UserShowProfileView(
                  () {
                    setState(() {
                      _isEdit = true;
                    });
                  },
                ),
        ),
      ),
    );
  }
}
