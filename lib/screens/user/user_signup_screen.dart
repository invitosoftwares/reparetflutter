import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../models/user_register_model.dart';
import '../../providers/auth_provider.dart';
import '../../screens/common/webview_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/custom_decorators.dart';
import '../../utils/my_length_limiting.dart';
import '../../widgets/common/bottom_sheet_image_picker.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/common/password_text_form_field.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/signup_pick_image.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/common/upload_file_s3.dart';

class UserSignUpScreen extends StatefulWidget {
  static final String routeName = "UserSignUpScreen";

  @override
  _UserSignUpScreenState createState() => _UserSignUpScreenState();
}

class _UserSignUpScreenState extends State<UserSignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "FIRST_NAME": null,
    "LAST_NAME": null,
    "EMAIL": null,
    "PHONE": null,
    "PASSWORD": null,
  };
  bool _termsAccepted = false;
  File? _profileImageFile;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    var _customDecorators = CustomDecorators(context);

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
          CustomColors.colorAccent, _screenSize, "Sign Up"),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                bottom: _screenSize.height * 0.015,
                top: _screenSize.height * 0.07,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SignUpPickImage(
                    _screenSize,
                    CustomColors.colorAccent,
                    _showImagePickerModal,
                    file: _profileImageFile,
                  ),
                  SizedBox(height: _screenSize.height * 0.05),
                  Form(
                    key: _formKey,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: _screenSize.width * 0.05),
                      child: Column(
                        children: [
                          getElevatedWidget(
                            _customDecorators,
                            "First Name",
                            "assets/images/icon_name.png",
                            50,
                            CustomColors.colorAccent,
                            TextInputType.text,
                            (value) {
                              if (value == null || value.trim().isEmpty) {
                                _data["FIRST_NAME"] = FormFieldData(
                                    true, "Please enter first name");
                              } else {
                                _data["FIRST_NAME"] =
                                    FormFieldData(false, value);
                              }
                              return null;
                            },
                          ),
                          getElevatedWidget(
                            _customDecorators,
                            "Last Name",
                            "assets/images/icon_name.png",
                            50,
                            CustomColors.colorAccent,
                            TextInputType.text,
                            (value) {
                              if (value == null || value.trim().isEmpty) {
                                _data["LAST_NAME"] = FormFieldData(
                                    true, "Please enter last name");
                              } else {
                                _data["LAST_NAME"] =
                                    FormFieldData(false, value);
                              }
                              return null;
                            },
                          ),
                          getElevatedWidget(
                            _customDecorators,
                            "Enter email",
                            "assets/images/icon_email.png",
                            50,
                            CustomColors.colorAccent,
                            TextInputType.emailAddress,
                            (value) {
                              if (value == null || value.trim().isEmpty) {
                                _data["EMAIL"] =
                                    FormFieldData(true, "Please enter email");
                              } else if (!Common.isValidEmail(value.trim())) {
                                _data["EMAIL"] = FormFieldData(
                                    true, "Please enter some valid email");
                              } else {
                                _data["EMAIL"] = FormFieldData(false, value);
                              }
                              return null;
                            },
                          ),
                          Visibility(
                            visible: false,
                            child: getElevatedWidget(
                              _customDecorators,
                              "Phone number",
                              "assets/images/icon_phone.png",
                              10,
                              CustomColors.colorAccent,
                              TextInputType.number,
                              (value) {
                                if (value == null || value.trim().isEmpty) {
                                  _data["PHONE"] = FormFieldData(false, "");
                                } else if (value.trim().length < 10) {
                                  _data["PHONE"] = FormFieldData(
                                      true, "Please enter some valid phone");
                                } else {
                                  _data["PHONE"] = FormFieldData(false, value);
                                }
                                return null;
                              },
                            ),
                          ),
                          getElevatedWidget2(
                            PasswordTextFormField(
                              validator: (value) {
                                if (value == null || value.trim().isEmpty) {
                                  _data["PASSWORD"] = FormFieldData(
                                      true, "Please enter password");
                                } else if (value.trim().length < 6) {
                                  _data["PASSWORD"] = FormFieldData(true,
                                      "Password must be at least 6 characters long");
                                } else {
                                  _data["PASSWORD"] =
                                      FormFieldData(false, value);
                                }
                                return null;
                              },
                              placeholder: "Password",
                              prefixIconString: "assets/images/icon_key.png",
                              cursorColor: CustomColors.colorAccent,
                            ),
                          ),
                          CheckboxListTile(
                            contentPadding: EdgeInsets.all(0),
                            dense: true,
                            title: Row(
                              children: [
                                Text(
                                  "Accept",
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(fontSize: 13),
                                ),
                                SizedBox(width: 5),
                                InkWell(
                                  onTap: () {
                                    Navigator.pushNamed(
                                      context,
                                      WebViewScreen.routeName,
                                      arguments: {
                                        "URL":
                                            Constants.TERMS_AND_CONDITIONS_URL,
                                        "ROLE": Constants.USER,
                                      },
                                    );
                                  },
                                  child: Text(
                                    "Terms & Conditions",
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText1!
                                        .copyWith(
                                            fontSize: 13,
                                            color: CustomColors.colorAccent),
                                  ),
                                ),
                              ],
                            ),
                            value: _termsAccepted,
                            onChanged: (newVal) {
                              setState(() {
                                _termsAccepted = newVal ?? false;
                              });
                            },
                            controlAffinity: ListTileControlAffinity.leading,
                          ),
                          Align(
                            alignment: Alignment.topRight,
                            child: PrimaryRaisedButton(
                              "Sign Up",
                              () {
                                _validateData(context);
                              },
                              size: _screenSize,
                              color: CustomColors.colorAccent,
                              isLoading: _isLoading,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: _screenSize.height * 0.05),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Already a User?",
                        style: Theme.of(context).textTheme.bodyText1!.copyWith(
                              fontWeight: FontWeight.w600,
                            ),
                      ),
                      SizedBox(width: _screenSize.width * 0.01),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          "Sign in",
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1!
                              .copyWith(
                                fontWeight: FontWeight.w600,
                                decoration: TextDecoration.underline,
                                decorationColor: CustomColors.colorGrayDark2,
                                shadows: [
                                  Shadow(
                                      color: CustomColors.colorAccent,
                                      offset: Offset(0, -1))
                                ],
                                color: Colors.transparent,
                              ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          if (_isLoading)
            SpinKitLoading(
              Constants.USER,
              false,
            ),
        ],
      ),
    );
  }

  Widget getElevatedWidget(
    CustomDecorators _customDecorators,
    String hint,
    String icon,
    int maxLength,
    Color cursorColor,
    TextInputType textInputType,
    Function validator,
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Material(
        elevation: 1,
        borderRadius: BorderRadius.all(Radius.circular(5)),
        child: TextFormField(
          validator: (value) => validator(value),
          style: Theme.of(context).textTheme.bodyText1,
          keyboardType: textInputType,
          maxLines: 1,
          textAlign: TextAlign.start,
          cursorColor: cursorColor,
          maxLength: maxLength,
          inputFormatters: [
            MyLengthLimitingTextInputFormatter(maxLength),
            if (textInputType == TextInputType.number)
              FilteringTextInputFormatter.digitsOnly,
          ],
          decoration: _customDecorators.getInputDecoration(
            hint,
            prefixIconString: icon,
            prefixIconColor: cursorColor,
          ),
        ),
      ),
    );
  }

  Widget getElevatedWidget2(Widget widget) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Material(
        elevation: 1,
        borderRadius: const BorderRadius.all(Radius.circular(5)),
        child: widget,
      ),
    );
  }

  void _showImagePickerModal() async {
    File? pickedFile = await showModalBottomSheet<File>(
      context: context,
      builder: (context) => const BottomSheetImagePicker(
        color: CustomColors.colorAccent,
      ),
    );
    if (pickedFile != null) {
      setState(() {
        _profileImageFile = pickedFile;
      });
    }
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);
    if (!_termsAccepted) {
      errors.add("You must agree terms and conditions to proceed");
    }

    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    setState(() {
      _isLoading = true;
    });

    String? image = "";
    if (_profileImageFile != null) {
      image = await UploadFileS3(
        context,
        _profileImageFile!,
        null,
      ).uploadFile();
    }
    print("file -------------- " + (image ?? ""));

    try {
      final _authProvider = Provider.of<AuthProvider>(context, listen: false);
      dynamic response = await _authProvider.userRegisterAPI(
        context,
        _data["FIRST_NAME"]?.data ?? "",
        _data["LAST_NAME"]?.data ?? "",
        _data["EMAIL"]?.data ?? "",
        _data["PHONE"]?.data ?? "",
        _data["PASSWORD"]?.data ?? "",
        image ?? "",
      );

      if (response is UserRegisterModel) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response.message ?? "",
          dismiss: false,
          onOkayClicked: () {
            Navigator.pop(context);
          },
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (e) {
      print(e);
    }

    setState(() {
      _isLoading = false;
    });
  }
}
