import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../providers/post_project_provider.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/user/edit_project.dart';

class EditProjectScreen extends StatefulWidget {
  static const String routeName = "EditProjectScreen";

  const EditProjectScreen({Key? key}) : super(key: key);

  @override
  _EditProjectScreenState createState() => _EditProjectScreenState();
}

class _EditProjectScreenState extends State<EditProjectScreen> {
  bool _isFirstTime = true;
  ProjectModel? _projectModel;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic>? data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;
      if (data != null) {
        _projectModel = data["PROJECT_MODEL"];
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorAccent,
        _screenSize,
        "Edit project",
        isCenter: false,
      ),
      body: ChangeNotifierProvider(
        create: (context) => PostProjectProvider(),
        child: EditProject(
          onProjectEdited: () {
            Navigator.pop(context, true);
          },
          projectModel: _projectModel!,
        ),
      ),
    );
  }
}
