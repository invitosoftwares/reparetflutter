import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/user_search_contractors_provider.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/no_internet_view.dart';
import '../../widgets/common/project_list_shimmer.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/user/item_contractor.dart';
import '../../widgets/user/user_no_contractors_view.dart';

class FavouriteListScreen extends StatefulWidget {
  static const String routeName = "FavouriteListScreen";

  const FavouriteListScreen({Key? key}) : super(key: key);

  @override
  _FavouriteListScreenState createState() => _FavouriteListScreenState();
}

class _FavouriteListScreenState extends State<FavouriteListScreen> {
  Size? _screenSize;
  int _loaderType = 0;
  bool _isFirstTime = true;
  UserSearchContractorsProvider? _userSearchContractorsProvider;
  int _page = 0;
  bool _isNoInternet = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _userSearchContractorsProvider =
          Provider.of<UserSearchContractorsProvider>(
        context,
        listen: false,
      );

      _loaderType =
          _userSearchContractorsProvider!.allFavouriteContractors.isEmpty
              ? 1
              : 0;
      _page = 0;
      _getAllFavouriteContractors(
        showLoader:
            _userSearchContractorsProvider!.allFavouriteContractors.isEmpty,
      );
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorAccent,
        _screenSize!,
        "Favourites",
        isCenter: false,
      ),
      body: Padding(
        padding: EdgeInsets.all(_screenSize!.width * 0.035),
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            _isNoInternet
                ? NoInternetView(
                    () => _getAllFavouriteContractors(),
                  )
                : (_loaderType == 1)
                    ? const ProjectListShimmer()
                    : Consumer<UserSearchContractorsProvider>(
                        builder: (context, provider, child) => RefreshIndicator(
                          color: CustomColors.colorAccent,
                          onRefresh: () async {
                            if (_loaderType != 0) return;
                            _page = 0;
                            await _getAllFavouriteContractors(
                              showLoader: false,
                            );
                          },
                          child: (provider.allFavouriteContractors.isEmpty)
                              ? const UserNoContractorsView()
                              : ListView.builder(
                                  itemBuilder: (context, index) {
                                    if (index ==
                                            provider.allFavouriteContractors
                                                    .length -
                                                min(
                                                    3,
                                                    provider
                                                        .allFavouriteContractors
                                                        .length) &&
                                        provider.hasMoreFavouriteProviders &&
                                        _loaderType == 0) {
                                      _page++;
                                      _getAllFavouriteContractors();
                                    }
                                    if (index ==
                                            provider.allFavouriteContractors
                                                .length &&
                                        provider.hasMoreFavouriteProviders) {
                                      return Container();
                                    }
                                    return Padding(
                                      padding: const EdgeInsets.only(
                                        top: 4.0,
                                      ),
                                      child: ChangeNotifierProvider.value(
                                        value: provider
                                            .allFavouriteContractors[index],
                                        child: ItemContractorSearch(
                                          _screenSize!,
                                          toggleFavourite: () {
                                            _toggleFavouriteStatus(
                                              provider
                                                      .allFavouriteContractors[
                                                          index]
                                                      .providerId ??
                                                  0,
                                            );
                                          },
                                        ),
                                      ),
                                    );
                                  },
                                  itemCount:
                                      provider.allFavouriteContractors.length,
                                ),
                        ),
                      ),
            if (_loaderType == 2)
              const Padding(
                padding: EdgeInsets.only(bottom: 8.0),
                child: SizedBox(
                  height: 10,
                  width: 10,
                  child: CircularProgressIndicator(
                    color: CustomColors.colorAccent,
                    strokeWidth: 2,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }

  Future<dynamic> _getAllFavouriteContractors({bool showLoader = true}) async {
    if (!await Common.checkInternetConnection()) {
      setState(() {
        _isNoInternet = true;
      });
      return;
    }
    if (_isNoInternet) {
      setState(() {
        _isNoInternet = false;
      });
    }

    if (showLoader) {
      setState(() {
        _loaderType = _page == 0 ? 1 : 2;
      });
    }

    try {
      await _userSearchContractorsProvider!.getFavouriteProviders(
        context,
        "$_page",
      );
    } catch (err) {
      print(err);
    }

    if (_loaderType != 0) {
      setState(() {
        _loaderType = 0;
      });
    }
  }

  Future<dynamic> _toggleFavouriteStatus(int providerId) async {
    if (!await Common.checkInternetConnection()) {
      Common.showToast(
        "You are offline. Please make sure you have a stable internet connection to proceed",
      );
      return;
    }

    try {
      await _userSearchContractorsProvider!.toggleFavouriteStatus(
        context,
        providerId,
        fromFavouritesScreen: true,
      );
    } catch (err) {
      print(err);
    }
  }
}
