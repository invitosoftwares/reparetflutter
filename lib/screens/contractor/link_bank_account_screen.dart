import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/user_model.dart';
import '../../providers/auth_provider.dart';
import '../../providers/link_account_provider.dart';
import '../../screens/contractor/webview_screen_payment.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../widgets/common/get_spinkit_loading.dart';

class LinkBankAccountScreen extends StatefulWidget {
  static const String routeName = "LinkBankAccountScreen";

  const LinkBankAccountScreen({Key? key}) : super(key: key);

  @override
  _LinkBankAccountScreenState createState() => _LinkBankAccountScreenState();
}

class _LinkBankAccountScreenState extends State<LinkBankAccountScreen> {
  bool _isFirstTime = true;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _getUrlToProceed();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SpinKitLoading(
          Constants.CONTRACTOR,
          true,
        ),
      ),
    );
  }

  void _getUrlToProceed() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed",
          dismiss: false, onOkayClicked: () {
        Navigator.pop(context);
      });
      return;
    }

    LinkAccountProvider provider = Provider.of(context, listen: false);
    try {
      String url = await provider.getLinkAccountUrl(context);
      if (url is String) {
        dynamic res = await Navigator.pushNamed(
          context,
          WebViewScreenPayment.routeName,
          arguments: {
            "TITLE": "Back",
            "URL": url,
          },
        );
        if (res is bool) {
          if (res) {
            AuthProvider provider =
                Provider.of<AuthProvider>(context, listen: false);
            UserModel? userModel = provider.userModel;
            if (userModel != null) {
              userModel.onboardingStatus = true;
            }
            provider.userModel = userModel;
          }
        }
        Navigator.pop(context);
      }
    } catch (err) {
      print(err);
    }
  }
}
