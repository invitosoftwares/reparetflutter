import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/owner_details_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/contractor/get_user_details_view.dart';

class UserDetailsScreen extends StatefulWidget {
  static const String routeName = "UserDetailsScreen";

  const UserDetailsScreen({Key? key}) : super(key: key);

  @override
  _UserDetailsScreenState createState() => _UserDetailsScreenState();
}

class _UserDetailsScreenState extends State<UserDetailsScreen> {
  bool _isFirstTime = true;
  bool _isLoading = true;
  late Size _screenSize;
  String projectId = "";

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _isLoading = true;
      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      projectId = data["PROJECT_ID"];
      _getUserDetails();
    }
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.colorWhite,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorPrimary,
        _screenSize,
        "Project Owner Details",
        isCenter: false,
        onBackPressed: () {
          Navigator.pop(context);
        },
      ),
      body: _isLoading
          ? SpinKitLoading(
              Constants.CONTRACTOR,
              true,
            )
          : Consumer<OwnerDetailsProvider>(
              builder: (context, ownerModel, child) => Padding(
                padding: const EdgeInsets.fromLTRB(10.0, 12.0, 0.0, 0.0),
                child: Column(
                  children: [
                    GetUserDetailsView(
                      size: _screenSize,
                      rating: ownerModel.ownerModel?.rating ?? 0.0,
                      image: ownerModel.ownerModel?.image ?? "",
                      name:
                          "${ownerModel.ownerModel?.firstName ?? ""} ${ownerModel.ownerModel?.lastName ?? ""}",
                      totalReviews: ownerModel.ownerModel?.reviewCount ?? 0,
                    ),
                    const SizedBox(height: 8.0),
                    _getKeyValuePair(
                      "Projects posted",
                      "${ownerModel.ownerModel?.projectCount ?? "N/A"}",
                      "assets/images/icon_briefcase_filled.png",
                    ),
                    _getKeyValuePair(
                      "Projects completed",
                      "${ownerModel.ownerModel?.completedProjectCount ?? "N/A"}",
                      "assets/images/icon_tick.png",
                    ),
                    _getKeyValuePair(
                      "Money Spent",
                      "\$${ownerModel.ownerModel?.totalSpent ?? "N/A"}",
                      "assets/images/icon_card.png",
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget _getKeyValuePair(
    String key,
    String value,
    String icon,
  ) {
    return Padding(
      padding: const EdgeInsets.only(top: 2.0),
      child: Row(
        children: [
          Image.asset(
            icon,
            height: 16,
            width: 16,
            color: CustomColors.colorPrimary,
          ),
          const SizedBox(width: 6.0),
          Text(
            "$key : ",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 15,
                ),
          ),
          Text(
            value,
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  fontSize: 15,
                  color: CustomColors.colorPrimary,
                  fontWeight: FontWeight.w500,
                ),
          ),
        ],
      ),
    );
  }

  void _getUserDetails() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed",
        dismiss: false,
        onOkayClicked: () {
          Navigator.pop(context);
        },
      );
      return;
    }

    OwnerDetailsProvider provider =
        Provider.of<OwnerDetailsProvider>(context, listen: false);
    try {
      var res = await provider.getOwnerDetails(
        context,
        projectId,
      );

      setState(() {
        _isLoading = false;
      });

      if (res is String) {
        AlertDialogs.showAlertDialogWithOk(context, res);
      }
    } catch (err) {
      print(err);
    }
  }
}
