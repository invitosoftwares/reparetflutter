import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';

class WebViewScreenPayment extends StatefulWidget {
  static const String routeName = "WebViewScreenPayment";

  const WebViewScreenPayment({Key? key}) : super(key: key);

  @override
  _WebViewScreenPaymentState createState() => _WebViewScreenPaymentState();
}

class _WebViewScreenPaymentState extends State<WebViewScreenPayment> {
  bool _isFirstTime = true;
  String? title;
  String? url;
  bool _isLoading = false;
  InAppWebViewController? webViewController;
  late Size _screenSize;

  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
    android: AndroidInAppWebViewOptions(
      useHybridComposition: true,
    ),
  );

  @override
  void initState() {
    super.initState();
    _isLoading = true;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      _isLoading = true;
      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      title = data["TITLE"];
      url = data["URL"];
    }
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.colorWhite,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorPrimary,
        _screenSize,
        title ?? "",
        isCenter: false,
        onBackPressed: () async {
          if (webViewController != null &&
              (await webViewController!.canGoBack())) {
            webViewController!.goBack();
          } else {
            Navigator.pop(context);
          }
        },
      ),
      body: Stack(
        children: [
          InAppWebView(
            initialOptions: options,
            onWebViewCreated: (controller) {
              controller.addJavaScriptHandler(
                  handlerName: 'stripeResponseWithArgs',
                  callback: (args) {
                    if ((args is List) && args.isNotEmpty) {
                      if (args[0]["onboarding"]) {
                        Navigator.pop(context, true);
                      }
                    }
                    print(args);
                  });
              var url1 = Uri.parse(url!);
              controller.loadUrl(urlRequest: URLRequest(url: url1));
            },
            onLoadStop: (_, __) {
              setState(() {
                _isLoading = false;
              });
            },
          ),
          if (_isLoading)
            SpinKitLoading(
              Constants.CONTRACTOR,
              true,
            ),
        ],
      ),
    );
  }
}
