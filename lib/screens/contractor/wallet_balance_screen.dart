import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../models/payment_withdrawal_model.dart';
import '../../providers/auth_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/password_text_form_field.dart';
import '../../widgets/common/raised_button_primary.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';

class WalletBalanceScreen extends StatefulWidget {
  static const String routeName = "WalletBalanceScreen";

  const WalletBalanceScreen({Key? key}) : super(key: key);

  @override
  _WalletBalanceScreenState createState() => _WalletBalanceScreenState();
}

class _WalletBalanceScreenState extends State<WalletBalanceScreen> {
  late Size _screenSize;
  bool _isFirstTime = true;
  bool _showPassword = false;
  bool _isLoading = false;
  final _formKey = GlobalKey<FormState>();
  final Map<String, FormFieldData?> _data = {
    "CURRENT_PASSWORD": null,
  };

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;
      Common.callProfileDetailsApi(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorPrimary,
        _screenSize,
        "My Wallet",
        isCenter: false,
        onBackPressed: () {
          Navigator.pop(context);
        },
      ),
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: _screenSize.width * 0.15),
              Image.asset(
                "assets/images/wallet.png",
                height: _screenSize.width * 0.3,
                color: CustomColors.colorPrimary,
              ),
              const SizedBox(height: 25),
              Text(
                "You have",
                style: Theme.of(context).textTheme.headline1!.copyWith(
                      color: CustomColors.colorGrayDark3,
                      fontSize: 18,
                    ),
              ),
              Consumer<AuthProvider>(
                builder: (context, value, child) => Text(
                  "\$${value.userModel?.walletBalance ?? "0"}",
                  style: Theme.of(context).textTheme.headline1!.copyWith(
                        color: CustomColors.colorPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      ),
                ),
              ),
              Text(
                "in your wallet",
                style: Theme.of(context).textTheme.headline1!.copyWith(
                      color: CustomColors.colorGrayDark3,
                      fontSize: 18,
                    ),
              ),
              SizedBox(
                height: _screenSize.width * 0.5,
              ),
              if (_showPassword)
                Padding(
                  padding: EdgeInsets.fromLTRB(
                    _screenSize.width * 0.05,
                    0,
                    _screenSize.width * 0.05,
                    0,
                  ),
                  child: Column(
                    children: [
                      Text(
                        "We will need your password to verify you",
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                              color: CustomColors.colorGrayDark3,
                              fontSize: 15,
                            ),
                      ),
                      const SizedBox(height: 10),
                      Form(
                        key: _formKey,
                        child: getElevatedWidget(
                          PasswordTextFormField(
                            validator: (value) {
                              if (value == null || value.trim().isEmpty) {
                                _data["CURRENT_PASSWORD"] = FormFieldData(
                                    true, "Please enter your password");
                              } else {
                                _data["CURRENT_PASSWORD"] =
                                    FormFieldData(false, value);
                              }
                              return null;
                            },
                            placeholder: "Password",
                            prefixIconString: "assets/images/icon_key.png",
                            cursorColor: CustomColors.colorPrimary,
                          ),
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  ),
                ),
              Consumer<AuthProvider>(
                builder: (context, value, child) =>
                    ((value.userModel?.walletBalance ?? 0) == 0)
                        ? Container()
                        : PrimaryRaisedButton(
                            _showPassword ? "Proceed" : "Withdraw Money",
                            () {
                              if (!_showPassword) {
                                setState(() {
                                  _showPassword = true;
                                });
                                return;
                              }
                              _validateData(context);
                            },
                            size: _screenSize,
                            color: CustomColors.colorPrimary,
                            isLoading: _isLoading,
                          ),
              ),
              const SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  Widget getElevatedWidget(Widget widget) {
    return Material(
      elevation: 1,
      borderRadius: const BorderRadius.all(Radius.circular(5)),
      child: widget,
    );
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _validateData(BuildContext context) async {
    _formKey.currentState!.validate();

    List<String> errors = Common.getErrors(_data);
    if (errors.isNotEmpty) {
      AlertDialogs.showMultiStringAlertDialogWithOk(
        context,
        errors,
      );
      return;
    }

    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(
        context,
        "You are offline. Please make sure you have a stable internet connection to proceed.",
      );
      return;
    }

    setState(() {
      _isLoading = true;
    });

    try {
      AuthProvider provider = Provider.of<AuthProvider>(context, listen: false);
      dynamic response = await provider.withdrawMoney(
        context,
        _data["CURRENT_PASSWORD"]?.data ?? "",
      );

      setState(() {
        _isLoading = false;
      });

      if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      } else if (response is PaymentWithdrawalModel) {
        setState(() {
          _showPassword = false;
        });
        Common.showToast(response.message ?? "");
        Common.callProfileDetailsApi(context);
      }
    } catch (e) {
      print(e);
    }
  }
}
