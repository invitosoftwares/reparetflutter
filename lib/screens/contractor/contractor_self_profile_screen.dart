import 'package:flutter/material.dart';
import '../../utils/common.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/contractor/contractor_edit_profile_view.dart';
import '../../widgets/contractor/contractor_show_profile_view.dart';

class ContractorSelfProfileScreen extends StatefulWidget {
  static final String routeName = "ContractorSelfProfileScreen";

  @override
  _ContractorSelfProfileScreenState createState() =>
      _ContractorSelfProfileScreenState();
}

class _ContractorSelfProfileScreenState
    extends State<ContractorSelfProfileScreen> {
  late Size _screenSize;
  bool _isEdit = false;

  @override
  void initState() {
    super.initState();
    Common.callProfileDetailsApi(context);
    Common.getAllCategories(context);
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        if (!_isEdit) return true;
        setState(() {
          _isEdit = false;
        });
        return false;
      },
      child: Scaffold(
        backgroundColor: CustomColors.colorGrayLight2,
        appBar: ToolbarWithBackAndText(
          CustomColors.colorPrimary,
          _screenSize,
          _isEdit ? "Edit Profile" : "Profile",
          isCenter: false,
          onBackPressed: () {
            if (_isEdit) {
              setState(() {
                _isEdit = false;
              });
            } else {
              Navigator.pop(context);
            }
          },
        ),
        body: AnimatedSwitcher(
          duration: const Duration(milliseconds: 250),
          child: _isEdit
              ? Align(
                  alignment: Alignment.topCenter,
                  child: ContractorEditProfileView(
                    () {
                      setState(() {
                        _isEdit = false;
                      });
                    },
                  ),
                )
              : ContractorShowProfileView(
                  () {
                    setState(() {
                      _isEdit = true;
                    });
                  },
                ),
        ),
      ),
    );
  }
}
