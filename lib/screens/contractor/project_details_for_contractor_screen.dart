import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:provider/provider.dart';
import '../../models/project_model.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../providers/contractor_project_details_provider.dart';
import '../../providers/rating_provider.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../widgets/common/get_gradient_button_with_text.dart';
import '../../widgets/common/toolbar_custom_with_back_and_text.dart';
import '../../widgets/contractor/bid_popup.dart';
import '../../widgets/contractor/get_gradient_button_with_bid_icon.dart';
import '../../widgets/contractor/project_details_basic_details_contractor_view.dart';
import '../../widgets/contractor/rate_owner_popup.dart';
import '../../widgets/user/get_project_basic_details_view.dart';

class ProjectDetailsForContractorScreen extends StatefulWidget {
  static const String routeName = "ProjectDetailsForContractorScreen";

  const ProjectDetailsForContractorScreen({Key? key}) : super(key: key);

  @override
  _ProjectDetailsForContractorScreenState createState() =>
      _ProjectDetailsForContractorScreenState();
}

class _ProjectDetailsForContractorScreenState
    extends State<ProjectDetailsForContractorScreen> {
  bool _isFirstTime = true;
  String? _projectId;
  ContractorProjectDetailsProvider? _contractorProjectDetailsProvider;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      Map<String, dynamic> data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      _projectId = data["PROJECT_ID"];

      _contractorProjectDetailsProvider =
          Provider.of<ContractorProjectDetailsProvider>(context, listen: false);
      _getProjectDetails();
    }
  }

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    Provider.of<ProjectModel>(context, listen: true);

    return Scaffold(
      backgroundColor: CustomColors.colorWhite,
      appBar: ToolbarWithBackAndText(
        CustomColors.colorPrimary,
        _screenSize,
        "Project Details",
        isCenter: false,
      ),
      body: Consumer<ContractorProjectDetailsProvider>(
        builder: (context, provider, child) => (provider.projectModel == null)
            ? Container()
            : Padding(
                padding: EdgeInsets.only(
                  left: _screenSize.width * 0.05,
                  top: _screenSize.width * 0.05,
                  right: _screenSize.width * 0.05,
                ),
                child: Column(
                  children: [
                    GetProjectBasicDetailsView(
                      provider.projectModel?.projectimages![0].image ?? "",
                      provider.projectModel?.projectTitle ?? "",
                      provider.projectModel?.category ?? "",
                      Common.getConvertedDate(
                        provider.projectModel?.projectTime ?? "",
                        Constants.DATE_FORMAT_4,
                        Constants.DATE_FORMAT_5,
                        isUtc: true,
                      ),
                      Common.getConvertedDate(
                        provider.projectModel?.createdAt ?? "",
                        Constants.DATE_FORMAT_4,
                        Constants.DATE_FORMAT_5,
                        isUtc: true,
                      ),
                      provider.projectModel?.projectStatus ?? 0,
                      _screenSize,
                    ),
                    SizedBox(height: _screenSize.width * 0.02),
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            ProjectDetailsBasicDetailsContractorView(
                              _screenSize,
                              projectModel: provider.projectModel,
                            ),
                            _getActionView(provider.projectModel),
                            const SizedBox(height: 10),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  _getActionView(ProjectModel? projectModel) {
    if (projectModel == null) return Container();
    if ((projectModel.bidAmount ?? 0) == 0) {
      //means bid is not made yet
      return _getMessageAndMakeBid();
    } else if (((projectModel.bidAmount ?? 0) != 0) &&
        projectModel.projectStatus == Constants.PROJECT_POSTED) {
      //means bid has been made
      return _getCancelBidAndMessageView(projectModel);
    } else if (projectModel.projectStatus == Constants.PROJECT_ACCEPTED) {
      //means bid has been accepted
      return _getMessageAndMarkAsDoneView(projectModel);
    } else if (projectModel.projectStatus == Constants.PROJECT_COMPLETED) {
      //means project is completed but payment is pending
      return _getPaymentPendingView(projectModel);
    } else if (projectModel.projectStatus == Constants.PROJECT_PAID) {
      return _getPaymentReceivedView(projectModel);
    } else if (projectModel.projectStatus ==
        Constants.PROJECT_PAID_REVIEWED_BY_USER) {
      if (projectModel.reviewAdded ?? false) {
        return _getRatingDoneView(projectModel);
      } else {
        return _getPaymentReceivedView(projectModel);
      }
    }
    return Container();
  }

  _getBidAmountView({Widget? additionalText}) {
    return Row(
      children: [
        Text(
          "BID amount:",
          style: Theme.of(context).textTheme.bodyText1,
        ),
        const SizedBox(width: 5),
        Consumer<ContractorProjectDetailsProvider>(
          builder: (context, value, child) => Text(
            "\$${value.projectModel?.bidAmount ?? ""}",
            style: Theme.of(context).textTheme.bodyText1!.copyWith(
                  color: CustomColors.colorAccent,
                  fontWeight: FontWeight.bold,
                ),
          ),
        ),
        if (additionalText != null) ...[
          Expanded(child: Container()),
          additionalText,
        ],
      ],
    );
  }

  _getMessageAndMakeBid() {
    return Consumer<ContractorProjectDetailsProvider>(
      builder: (context, provider, child) => Row(
        children: [
          Expanded(child: Container()),
          getMessageView(provider.projectModel),
          const SizedBox(width: 10),
          Consumer<ContractorListingProvider>(
            builder: (context, contractorListingProvider, child) =>
                GradientButtonWithBidIcon(
              "Make a bid",
              () => showDialog(
                context: context,
                builder: (BuildContext context) {
                  return ChangeNotifierProvider.value(
                    value: contractorListingProvider,
                    child: BidPopup(
                      projectId: "${provider.projectModel?.projectId ?? ""}",
                      onBidAdded: (int value) {
                        ProjectModel model = provider.projectModel!;
                        model.makeBidLocally(value);
                        provider.projectModel = model;
                      },
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  _getCancelBidAndMessageView(ProjectModel projectModel) {
    return Column(
      children: [
        _getBidAmountView(),
        const SizedBox(height: 15),
        Row(
          children: [
            Expanded(
              child: Container(),
            ),
            InkWell(
              onTap: () {
                _confirmCancelBid("${projectModel.projectId}");
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Cancel bid",
                  style: Theme.of(context).textTheme.bodyText1!.copyWith(
                        color: CustomColors.colorAccent,
                        fontSize: 14.0,
                      ),
                ),
              ),
            ),
            const SizedBox(width: 10),
            getMessageView(projectModel),
          ],
        ),
      ],
    );
  }

  void _confirmCancelBid(String projectId) async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "No",
      "Yes",
      "Are you sure you want to cancel this bid?",
      () => Navigator.pop(context),
      () {
        Navigator.pop(context);
        _cancelBid(projectId);
      },
    );
  }

  _getMessageAndMarkAsDoneView(projectModel) {
    return Column(
      children: [
        _getBidAmountView(),
        const SizedBox(height: 15),
        Row(
          children: [
            Expanded(child: Container()),
            getMessageView(projectModel),
            const SizedBox(width: 10),
            GradientButtonWithText(
              "Mark as done",
              () {
                _confirmMarkAsDone(context);
              },
            ),
          ],
        ),
      ],
    );
  }

  void _confirmMarkAsDone(BuildContext context) async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "No",
      "Yes",
      "Are you sure you want to mark this project as done?",
      () => Navigator.pop(context),
      () {
        Navigator.pop(context);
        _markAsDone();
      },
    );
  }

  getMessageView(ProjectModel? projectModel) {
    return GradientButtonWithText(
      "Message",
      () async {
        String providerId =
            (await Common.getUserModelFromSharedPreferences())?.uuid ?? "";
        Common.getChatScreen(
          context,
          consumerId: projectModel?.consumerUuid ?? "",
          providerId: providerId,
          projectId: int.parse(_projectId ?? "0"),
          name:
              "${projectModel?.consumerFirstName ?? ""} ${projectModel?.consumerLastName ?? ""}",
        );
      },
    );
  }

  getLeaveReviewView(ProjectModel projectModel) {
    return GradientButtonWithText(
      "Leave a review",
      () async {
        dynamic response = await showDialog(
          context: context,
          builder: (BuildContext context) {
            return ChangeNotifierProvider(
              create: (context) => RatingProvider(),
              child: RateOwnerPopup(
                projectId: "${projectModel.projectId}",
              ),
            );
          },
        );

        if (response != null && (response is double)) {
          projectModel.setRatingLocally(response);
          Provider.of<ContractorListingProvider>(context, listen: false)
              .addReviewLocally("${projectModel.projectId ?? ""}", response);
          _getProjectDetails();
        }
      },
    );
  }

  _getPaymentPendingView(projectModel) {
    return Column(
      children: [
        _getBidAmountView(
          additionalText: Row(
            children: [
              const Icon(
                Icons.access_time_rounded,
                color: CustomColors.colorRed,
                size: 12.0,
              ),
              const SizedBox(width: 2),
              Text(
                "Payment pending",
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: CustomColors.colorRed,
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                    ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 15),
        Row(
          children: [
            Expanded(child: Container()),
            getMessageView(projectModel),
          ],
        ),
      ],
    );
  }

  _getPaymentReceivedView(ProjectModel projectModel) {
    return Column(
      children: [
        _getBidAmountView(
          additionalText: Row(
            children: [
              const Icon(
                Icons.done,
                color: CustomColors.colorGreen,
                size: 12.0,
              ),
              const SizedBox(width: 2),
              Text(
                "Payment received",
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: CustomColors.colorGreen,
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                    ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 15),
        Row(
          children: [
            Expanded(child: Container()),
            getLeaveReviewView(projectModel),
          ],
        ),
      ],
    );
  }

  _getRatingDoneView(ProjectModel projectModel) {
    return Column(
      children: [
        _getBidAmountView(
          additionalText: Row(
            children: [
              const Icon(
                Icons.done,
                color: CustomColors.colorGreen,
                size: 12.0,
              ),
              const SizedBox(width: 2),
              Text(
                "Payment received",
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: CustomColors.colorGreen,
                      fontWeight: FontWeight.w600,
                      fontSize: 12.0,
                    ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 5),
        Text(
          "Your Review",
          style: Theme.of(context).textTheme.headline2!.copyWith(
                fontSize: 16.0,
                color: CustomColors.colorPrimary,
              ),
        ),
        const SizedBox(height: 5),
        Align(
          alignment: Alignment.center,
          child: RatingBarIndicator(
            rating: projectModel.rating?.toDouble() ?? 0.0,
            itemBuilder: (context, index) => const Icon(
              Icons.star,
              color: CustomColors.colorAccent,
            ),
            itemCount: 5,
            itemSize: 30.0,
            direction: Axis.horizontal,
          ),
        ),
        const SizedBox(height: 5),
        Text(
          projectModel.review ?? "",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1!.copyWith(
                fontSize: 15.0,
              ),
        ),
      ],
    );
  }

  void _getProjectDetails() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);
    try {
      dynamic response =
          await _contractorProjectDetailsProvider!.getProjectDetails(
        context,
        _projectId ?? "",
      );

      Navigator.pop(context);

      if ((response is bool) && response) {
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }
  }

  void _cancelBid(String projectId) async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);
    try {
      dynamic response = await _contractorProjectDetailsProvider!.cancelBid(
        context,
        projectId,
      );

      Navigator.pop(context);

      if ((response is bool) && response) {
        //remove bid from current page
        ProjectModel model = _contractorProjectDetailsProvider!.projectModel!;
        model.cancelBid();
        _contractorProjectDetailsProvider!.projectModel = model;

        //remove bid from previous page
        Provider.of<ContractorListingProvider>(context, listen: false)
            .cancelBid(projectId);
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  void setState(VoidCallback fn) {
    if (mounted) super.setState(fn);
  }

  void _markAsDone() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed");
      return;
    }

    Common.showLoaderDialog("Please wait...", context);
    try {
      ContractorListingProvider contractorListingProvider =
          Provider.of<ContractorListingProvider>(context, listen: false);

      dynamic response = await contractorListingProvider.markProjectAsDone(
        context,
        _projectId ?? "",
      );

      Navigator.pop(context);
      if (response is bool && response) {
        Navigator.pop(context);
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (e) {
      print(e);
    }
  }
}
