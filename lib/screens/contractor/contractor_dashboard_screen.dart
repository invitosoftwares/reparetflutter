import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/get_stripe_login_url_model.dart';
import '../../models/logout_model.dart';
import '../../providers/auth_provider.dart';
import '../../providers/chat_provider.dart';
import '../../providers/link_account_provider.dart';
import '../../providers/contractor_listings_provider.dart';
import '../../screens/common/login_screen.dart';
import '../../screens/common/notifications_screen.dart';
import '../../screens/common/webview_screen.dart';
import '../../screens/contractor/contractor_self_profile_screen.dart';
import '../../screens/contractor/link_bank_account_screen.dart';
import '../../screens/contractor/wallet_balance_screen.dart';
import '../../screens/contractor/webview_screen_payment.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/shared_preferences.dart';
import '../../widgets/common/app_bar_searchable.dart';
import '../../widgets/common/chat_listing_view.dart';
import '../../widgets/common/get_spinkit_loading.dart';
import '../../widgets/contractor/contractor_dashboard_navigation_bar.dart';
import '../../widgets/contractor/contractor_drawer.dart';
import '../../widgets/contractor/contractor_listings_view.dart';
import '../../widgets/contractor/contractor_show_invitations_data.dart';

class ContractorDashboardScreen extends StatefulWidget {
  static final String routeName = "ContractorDashboardScreen";

  static const int PROJECT = 0;
  static const int INVITATIONS = 1;
  static const int CHAT = 2;

  @override
  _ContractorDashboardScreenState createState() =>
      _ContractorDashboardScreenState();
}

class _ContractorDashboardScreenState extends State<ContractorDashboardScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _isFirstTime = true;
  int _currentView = ContractorDashboardScreen.PROJECT;
  bool _isLoading = false;
  final StreamController<String> _querySearchController =
      StreamController<String>.broadcast();
  final StreamController<String> _queryClearController =
      StreamController<String>.broadcast();
  Map<String, dynamic>? data;

  @override
  void dispose() {
    super.dispose();
    _querySearchController.close();
    _queryClearController.close();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (_isFirstTime) {
      _isFirstTime = false;

      data =
          ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;
      if (data != null && data!["TAB"] != null) {
        _currentView = data!["TAB"];
      }

      Common.callProfileDetailsApi(context);
      Common.getAllCategories(context);
    }
  }

  final List<String> _toolbarTitles = [
    "Projects",
    "Invitations",
    "Chats",
  ];

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;

    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: CustomColors.colorGrayLight2,
      appBar: DashboardAppBarWithSearch(
        title: _toolbarTitles[_currentView],
        // notificationClick: _currentView == ContractorDashboardScreen.PROJECT
        //     ? () {
        //         Navigator.pushNamed(context, NotificationsScreen.routeName,
        //             arguments: {
        //               "IS_USER": false,
        //             });
        //       }
        //     : null,
        // notificationCount: 5,
        sideMenuClick: () {
          _scaffoldKey.currentState!.openEndDrawer();
        },
        searchQuery: _currentView == ContractorDashboardScreen.PROJECT
            ? (value) {
                _querySearchController.add(value);
              }
            : null,
        mainColor: CustomColors.colorPrimary,
        accentColor: CustomColors.colorAccent,
        isLoading: _isLoading,
        clearQuery: _queryClearController,
      ),
      bottomNavigationBar: IntrinsicHeight(
        child: Stack(
          children: [
            ContractorDashboardNavigationBar(
              _onTabTapped,
              _currentView,
              _screenSize,
            ),
            if (_isLoading)
              InkWell(
                onTap: () {},
                child: Container(
                  color: CustomColors.colorBlack.withOpacity(0.15),
                ),
              ),
          ],
        ),
      ),
      body: ChangeNotifierProvider(
        create: (context) => ContractorListingProvider(),
        child: Stack(
          children: [
            (_currentView == ContractorDashboardScreen.PROJECT)
                ? ContractorListingsView(
                    _screenSize,
                    queryStreamController: _querySearchController,
                    onInternalTabChanged: () {
                      _queryClearController.add("1");
                    },
                  )
                : (_currentView == ContractorDashboardScreen.INVITATIONS)
                    ? ContractorShowInvitationsData(_screenSize)
                    : (_currentView == ContractorDashboardScreen.CHAT)
                        ? const ChatListingView(false)
                        : Container(),
            if (_isLoading) SpinKitLoading(Constants.CONTRACTOR, false),
          ],
        ),
      ),
      endDrawer: ContractorDrawer(
        _screenSize,
        _onDrawerItemSelected,
      ),
    );
  }

  void _onTabTapped(int index) {
    _queryClearController.add("1");
    setState(() {
      _currentView = index;
    });
  }

  @override
  void setState(fn) {
    if (mounted) super.setState(fn);
  }

  void _onDrawerItemSelected(int option) {
    if (option == ContractorDrawer.LOGOUT) {
      _confirmLogout();
    } else if (option == ContractorDrawer.PROFILE) {
      Navigator.pushNamed(context, ContractorSelfProfileScreen.routeName);
    } else if (option == ContractorDrawer.PRIVACY_POLITY) {
      Navigator.pushNamed(context, WebViewScreen.routeName, arguments: {
        "URL": Constants.PRIVACY_POLICY_URL,
        "ROLE": Constants.CONTRACTOR,
        "TITLE": "Privacy policy"
      });
    } else if (option == ContractorDrawer.SUPPORT) {
      Navigator.pushNamed(context, WebViewScreen.routeName, arguments: {
        "URL": Constants.SUPPORT_URL,
        "ROLE": Constants.CONTRACTOR,
        "TITLE": "Support"
      });
    } else if (option == ContractorDrawer.ADD_BANK_ACCOUNT) {
      Navigator.pushNamed(context, LinkBankAccountScreen.routeName);
    } else if (option == ContractorDrawer.MANAGE_BANK_ACCOUNT) {
      _getLoginLink();
    } else if (option == ContractorDrawer.WALLET) {
      Navigator.pushNamed(context, WalletBalanceScreen.routeName);
    } else if (option == ContractorDrawer.VERIFICATION) {
      Common.launchUrl(Constants.verificationUrl);
    } else if (option == ContractorDrawer.DELETE_ACCOUNT) {
      Common.deleteAccount(context);
    }
  }

  void _confirmLogout() async {
    AlertDialogs.showAlertDialogWithTwoButtons(
      context,
      "No",
      "Yes",
      "Are you sure you want to logout?",
      () => Navigator.pop(context),
      () {
        callLogout();
        Navigator.pop(context);
      },
    );
  }

  void callLogout() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed.");
      return;
    }

    setState(() {
      _isLoading = true;
    });

    try {
      final authProvider = Provider.of<AuthProvider>(context, listen: false);
      dynamic response = await authProvider.logoutApi(context);

      if (response is LogoutModel) {
        await MySharedPreferences.clearAll();
        await Provider.of<ChatProvider>(context, listen: false).disconnect();

        Navigator.pushNamedAndRemoveUntil(
          context,
          LoginScreen.routeName,
          (route) => false,
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          response,
        );
      }
    } catch (error) {
      print(error);
    }

    setState(() {
      _isLoading = false;
    });
  }

  _getLoginLink() async {
    if (!await Common.checkInternetConnection()) {
      AlertDialogs.showAlertDialogWithOk(context,
          "You are offline. Please make sure you have a stable internet connection to proceed",
          dismiss: false, onOkayClicked: () {
        Navigator.pop(context);
      });
      return;
    }
    Common.showLoaderDialog("Please wait...", context);
    LinkAccountProvider provider = Provider.of(context, listen: false);
    try {
      var response = await provider.getStripeLoginUrl(context);
      Navigator.pop(context);
      if (response is GetStripeLoginUrlModel) {
        Navigator.pushNamed(
          context,
          WebViewScreenPayment.routeName,
          arguments: {
            "TITLE": "Profile",
            "URL": response.data!.url,
          },
        );
      } else if (response is String) {
        AlertDialogs.showAlertDialogWithOk(context, response);
      }
    } catch (err) {
      print(err);
    }
  }
}
