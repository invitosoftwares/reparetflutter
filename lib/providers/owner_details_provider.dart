import 'package:flutter/material.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../models/owner_details_parent_model.dart';
import '../../network/base_network.dart';
import '../../utils/common.dart';

class OwnerDetailsProvider with ChangeNotifier {
  OwnerDetailsModel? _ownerModel;

  OwnerDetailsModel? get ownerModel => _ownerModel;

  set ownerModel(OwnerDetailsModel? value) {
    _ownerModel = value;
    notifyListeners();
  }

  Future<dynamic> getOwnerDetails(
    BuildContext context,
    String projectId,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.getOwnerDetails(projectId);

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          final model = OwnerDetailsParentModel.fromJson(responseBean.data);
          ownerModel = model.data!;
          ownerModel?.image = await UploadFileS3.getImageUrl(ownerModel?.image);
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }
}
