import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:reparet/models/project_model.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../models/forgot_password_model.dart';
import '../../models/login_model.dart';
import '../../models/logout_model.dart';
import '../../models/payment_withdrawal_model.dart';
import '../../models/user_model.dart';
import '../../models/user_register_model.dart';
import '../../network/base_network.dart';
import '../../providers/user_search_contractors_provider.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  UserModel? _userModel;

  UserModel? get userModel => _userModel;

  set userModel(UserModel? value) {
    _userModel = null;
    _userModel = value;
    notifyListeners();
  }

  Future<dynamic> loginApi(
    BuildContext context,
    String email,
    String password,
  ) async {
    try {
      String deviceId = "";
      String deviceType = Platform.isAndroid ? "1" : "2";
      String devicePushToken =
          await MySharedPreferences.getString(Constants.pushNotificationToken);

      if (devicePushToken.isEmpty) {
        devicePushToken = "dummy token";
      }

      final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
      try {
        if (Platform.isAndroid) {
          var build = await deviceInfoPlugin.androidInfo;
          deviceId = build.androidId;
        } else if (Platform.isIOS) {
          var data = await deviceInfoPlugin.iosInfo;
          deviceId = data.identifierForVendor;
        }
      } on PlatformException {
        print('Failed to get platform version');
      }

      var responseBean = await BaseNetwork(context).retrofit.loginAPI(
            email,
            password,
            deviceId,
            devicePushToken,
            deviceType,
          );

      if (responseBean.response.statusCode == 200) {
        final model = LoginModel.fromJson(responseBean.data);
        if (model.type == "success" && model.data != null) {
          return model.data;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> userRegisterAPI(
    BuildContext context,
    String firstName,
    String lastName,
    String email,
    String phone,
    String password,
    String image,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.registerUserAPI(
            email,
            firstName,
            lastName,
            password,
            "+1",
            phone,
            image,
          );

      if (responseBean.response.statusCode == 200) {
        final model = UserRegisterModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> contractorRegisterAPI(
    BuildContext context,
    String firstName,
    String lastName,
    String email,
    String phone,
    String password,
    String image,
    String companyName,
    String lat,
    String lng,
    String location,
  ) async {
    try {
      var responseBean =
          await BaseNetwork(context).retrofit.registerContractorAPI(
                email,
                firstName,
                lastName,
                password,
                "+1",
                phone,
                image,
                companyName,
                lat,
                lng,
                location,
              );

      if (responseBean.response.statusCode == 200) {
        final model = UserRegisterModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> forgotPasswordApi(
    BuildContext context,
    String email,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.forgotPasswordAPI(
            email,
          );

      if (responseBean.response.statusCode == 200) {
        final model = ForgotPasswordModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> changePasswordApi(
    BuildContext context,
    String password,
    String newPassword,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.changePasswordAPI(
            password,
            newPassword,
          );

      if (responseBean.response.statusCode == 200) {
        final model = ForgotPasswordModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> deleteAccountAPI(
    BuildContext context,
    String password,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.deleteAccountAPI(
            password,
          );

      if (responseBean.data["message"] != null) {
        return responseBean.data["message"];
      }
      return Constants.UNABLE_TO_PROCESS;
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> logoutApi(BuildContext context) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.logoutAPI();

      if (responseBean.response.statusCode == 200) {
        final model = LogoutModel.fromJson(responseBean.data);
        if (model.type == "success") {
          UserSearchContractorsProvider provider =
              Provider.of<UserSearchContractorsProvider>(
            context,
            listen: false,
          );
          provider.allFavouriteContractors.clear();
          provider.allContractors.clear();
          notifyListeners();

          userModel = null;
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> getProfileDetailsApi(BuildContext context) async {
    try {
      var responseBean =
          await BaseNetwork(context).retrofit.getProfileDetails();

      if (responseBean.response.statusCode == 200) {
        final model = LoginModel.fromJson(responseBean.data);
        if (model.type == "success") {
          model.data?.image = await UploadFileS3.getImageUrl(model.data?.image);
          userModel = model.data;
          await Common.setUserModelInSharedPreferences(model.data!);
          return model.data;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> withdrawMoney(
    BuildContext context,
    String password,
  ) async {
    try {
      var responseBean =
          await BaseNetwork(context).retrofit.withdrawMoney(password);

      if (responseBean.response.statusCode == 200) {
        final model = PaymentWithdrawalModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> userUpdateProfileAPI(
    BuildContext context,
    String firstName,
    String lastName,
    String phone,
    String image,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.updateUserProfile(
            firstName,
            lastName,
            phone,
            image,
          );

      if (responseBean.response.statusCode == 200) {
        final model = LoginModel.fromJson(responseBean.data);
        if (model.type == "success") {
          model.data?.image = await UploadFileS3.getImageUrl(model.data?.image);
          userModel = model.data;
          await Common.setUserModelInSharedPreferences(model.data!);
          return model.data;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> contractorUpdateProfileAPI(
    BuildContext context,
    String firstName,
    String lastName,
    String phone,
    String image,
    String categories,
    String companyName,
    String lat,
    String lng,
    String location,
    String bio,
  ) async {
    try {
      var responseBean =
          await BaseNetwork(context).retrofit.updateProviderProfile(
                firstName,
                lastName,
                phone,
                image,
                categories,
                companyName,
                lat,
                lng,
                location,
                bio,
              );

      if (responseBean.response.statusCode == 200) {
        final model = LoginModel.fromJson(responseBean.data);
        if (model.type == "success") {
          model.data?.image = await UploadFileS3.getImageUrl(model.data?.image);
          userModel = model.data;
          await Common.setUserModelInSharedPreferences(model.data!);
          return model.data;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  getImagesAndVideosUrl(ProjectModel projectModel) async {
    if (projectModel.projectimages != null) {
      for (ProjectMedia media in projectModel.projectimages!) {
        if (media.image != null && !(media.image!.contains("https"))) {
          String img = await UploadFileS3.getImageUrl(media.image!);
          media.image = img;
        }
      }
    }
    if (projectModel.projectvideos != null) {
      for (ProjectMedia media in projectModel.projectvideos!) {
        if (media.video != null && !(media.video!.contains("https"))) {
          String img = await UploadFileS3.getImageUrl(media.video!);
          media.video = img;
        }
      }
    }
    notifyListeners();
  }
}
