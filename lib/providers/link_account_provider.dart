import 'package:flutter/material.dart';
import '../../models/get_stripe_login_url_model.dart';
import '../../network/base_network.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';

class LinkAccountProvider with ChangeNotifier {
  Future<dynamic> getLinkAccountUrl(
    BuildContext context,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.getAccountLinkURL();

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          return responseBean.response.data["data"]["url"];
        } else {
          AlertDialogs.showAlertDialogWithOk(
            context,
            Common.errorMessage(responseBean),
            onOkayClicked: () {
              Navigator.pop(context);
            },
            dismiss: false,
          );
          return null;
        }
      } else {
        AlertDialogs.showAlertDialogWithOk(
          context,
          Common.errorMessage(responseBean),
          onOkayClicked: () {
            Navigator.pop(context);
          },
          dismiss: false,
        );
        return null;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  Future<dynamic> getStripeLoginUrl(
    BuildContext context,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.getStripeLoginURL();

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          final model =
              GetStripeLoginUrlModel.fromJson(responseBean.response.data);
          return model;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }
}
