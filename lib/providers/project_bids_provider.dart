import 'package:flutter/material.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../models/project_bids_parent_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';

class ProjectBidsProvider with ChangeNotifier {
  bool hasMore = false;
  final List<ProjectBidModel> _allBids = [];

  List<ProjectBidModel> get allBids => _allBids;

  Future<dynamic> getProjectBids(
    BuildContext context,
    String projectId,
    String page,
  ) async {
    try {
      int perPage = 10;

      var responseBean = await BaseNetwork(context).retrofit.getProjectBids(
            projectId,
            page,
          );

      if (responseBean.response.statusCode == 200) {
        final model = ProjectBidsParentModel.fromJson(responseBean.data);
        for (ProjectBidModel projectBidModel in model.data?.projectBids ?? []) {
          projectBidModel.providerImage =
              await UploadFileS3.getImageUrl(projectBidModel.providerImage);
        }
        if (model.type == "success") {
          if (page == "0") {
            _allBids.clear();
          }

          if (model.data!.projectBids!.isEmpty) {
            hasMore = false;
            notifyListeners();
            return;
          }

          hasMore = model.data!.projectBids!.length == perPage;
          allBids.addAll(model.data?.projectBids ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> confirmBid(
    BuildContext context,
    int bidId,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.confirmBid(
            "$bidId",
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          allBids.removeWhere((element) => element.bidId != bidId);
          allBids[0].makeIsAccepted();
          notifyListeners();
          return true;
        } else {
          if (responseBean.data["message"] != null) {
            return responseBean.data["message"];
          }
          return Constants.UNABLE_TO_PROCESS;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
