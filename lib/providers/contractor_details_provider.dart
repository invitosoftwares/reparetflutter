import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../models/contractor_details_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';

class ContractorDetailsProvider with ChangeNotifier {
  ContractorDetailsDataModel? _contractorDetailsDataModel;

  ContractorDetailsDataModel? get contractorDetailsDataModel =>
      _contractorDetailsDataModel;

  set contractorDetailsDataModel(ContractorDetailsDataModel? value) {
    _contractorDetailsDataModel = value;
    notifyListeners();
  }

  Future<dynamic> getContractorDetails(
    BuildContext context,
    String contractorId,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.getProviderDetails(
            contractorId,
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          final model = ContractorDetailsModel.fromJson(responseBean.data);
          contractorDetailsDataModel = model.data;
          return true;
        } else {
          if (responseBean.data["message"] != null) {
            return responseBean.data["message"];
          }
          return Constants.UNABLE_TO_PROCESS;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
