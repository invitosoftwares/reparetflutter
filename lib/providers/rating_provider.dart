import 'package:flutter/material.dart';
import '../../network/base_network.dart';
import '../../utils/common.dart';

class RatingProvider with ChangeNotifier {
  Future<dynamic> rateContractorByUser(
    BuildContext context,
    String projectId,
    String rating,
    String review,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.addReviewByUser(
            projectId,
            rating,
            review,
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  Future<dynamic> rateUserByContractor(
    BuildContext context,
    String projectId,
    String rating,
    String review,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.addReviewByProvider(
            projectId,
            rating,
            review,
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }
}
