import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reparet/providers/auth_provider.dart';
import '../../models/project_details_parent_model.dart';
import '../../models/project_model.dart';
import '../../network/base_network.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../widgets/common/upload_file_s3.dart';

class UserProjectDetailsProvider with ChangeNotifier {
  late BuildContext context;
  ProjectModel? _projectModel;

  ProjectModel? get projectModel => _projectModel;

  set projectModel(ProjectModel? value) {
    _projectModel = value;
    getImagesAndVideosUrl();
  }

  getImagesAndVideosUrl() async {
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    await authProvider.getImagesAndVideosUrl(_projectModel!);
    notifyListeners();
  }

  Future<dynamic> getProjectDetails(
    BuildContext context,
    String projectId,
  ) async {
    try {
      this.context = context;
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.getUserProjectDetails(projectId);

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          final model = ProjectDetailsParentModel.fromJson(responseBean.data);
          projectModel = model.data!;
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  Future<dynamic> deleteProject(
    BuildContext context,
    String projectId,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.deleteProject(projectId);

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }
}
