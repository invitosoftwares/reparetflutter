import 'package:flutter/material.dart';
import '../../models/get_all_cards_parent_model.dart';
import '../../models/setup_intent_parent_model.dart';
import '../../network/base_network.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';

class PaymentMethodsProvider with ChangeNotifier {
  final List<CardModel> _allCards = [];

  List<CardModel> get allCards => _allCards;

  Future<dynamic> getAllCards(
    BuildContext context,
  ) async {
    try {
      var responseBean =
          await BaseNetwork(context).retrofit.getPaymentMethods();

      if (responseBean.response.statusCode == 200) {
        final model = GetAllCardsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          _allCards.clear();
          _allCards.addAll(model.data ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> getSetupIntent(
    BuildContext context,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.createSetupIntent();

      if (responseBean.response.statusCode == 200) {
        final model = SetupIntentParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return model;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  Future<dynamic> deleteCard(
    BuildContext context,
    String id,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.deleteCard(id);

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          Common.showToast(responseBean.data["message"]);
          _allCards.removeWhere((element) => element.pmId == id);
          notifyListeners();
        } else {
          if (responseBean.data["message"] != null) {
            return responseBean.data["message"];
          }
          return Constants.UNABLE_TO_PROCESS;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }
}
