import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../../models/chat_message_model.dart';
import '../../models/previous_chat_people_model.dart';
import '../../models/user_model.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/shared_preferences.dart';
import 'package:socket_io_client/socket_io_client.dart';

class ChatProvider with ChangeNotifier {
  Socket? _socket;

  final List<ChatMessageModel> _chatMessages = [];

  List<ChatMessageModel> get chatMessages => _chatMessages;

  final List<PreviousChatPeopleModel> _chatPeople = [];

  List<PreviousChatPeopleModel> get chatPeople => _chatPeople;

  bool isLoadingChat = true;

  Future disconnect() async {
    if (_socket != null) {
      print("socket disconnect");
      return _socket!.disconnect();
    }
  }

  Future<void> connect() async {
    _socket = io(
      Constants.SOCKET_URL,
      OptionBuilder()
          .setTransports(['websocket'])
          .enableForceNewConnection()
          .build(),
    );
    _socket!.onConnect((_) {
      print('connect');
      sendConnectedUserId();
    });
    _socket!.onDisconnect((_) => print('disconnect'));
  }

  void sendConnectedUserId() async {
    UserModel? userModel = await Common.getUserModelFromSharedPreferences();
    String pushToken =
        await MySharedPreferences.getString(Constants.pushNotificationToken);
    if (userModel == null) return;

    String deviceId = "";
    final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceId = build.androidId;
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceId = data.identifierForVendor;
      }
    } on PlatformException {
      print('Failed to get platform version');
    }

    var data = {
      "bearer_token": "${userModel.token}",
      "device_token": pushToken,
      "device_id": deviceId,
      "device_type": Platform.isAndroid ? "1" : "2",
    };

    if (_socket!.hasListeners(Constants.SAVE_SOCKET_ID_RESPONSE)) {
      _socket!.off(Constants.SAVE_SOCKET_ID_RESPONSE);
    }
    _socket!.on(Constants.SAVE_SOCKET_ID_RESPONSE, (data) {
      print("data response connect-user-response $data");
      turnOnChatPeopleListeners();
    });

    _socket!.emit(Constants.SAVE_SOCKET_ID, [
      data,
    ]);
    print("data sent $data");
  }

  void turnOnSingleChatListeners(String chatId) {
    _receivePreviousMessagesListener(chatId: chatId);
    receivePreviousMessagesEmitter(chatId: chatId);
    _receiveSingleMessage(chatId: chatId);
  }

  void turnOnChatPeopleListeners() {
    _receiveChatPeopleListListener();
    _receiveChatPeopleListEmitter();
  }

  _receivePreviousMessagesListener({String? chatId}) async {
    if (_socket == null || !_socket!.connected) return;

    if (_socket!.hasListeners(Constants.CHAT_MESSAGES_LISTENER)) {
      _socket!.off(Constants.CHAT_MESSAGES_LISTENER);
    }
    _socket!.on(Constants.CHAT_MESSAGES_LISTENER, (event) async {
      print(event);
      List<ChatMessageModel> list = List<ChatMessageModel>.from(
        event.map(
          (x) => ChatMessageModel.fromJson(x),
        ),
      );
      if (list.isEmpty) {
        isLoadingChat = false;
        notifyListeners();
        return;
      }

      _chatMessages.clear();
      _chatMessages.addAll(list);

      _readMessageEmitter(
        chatId: chatId,
      );

      isLoadingChat = false;
      notifyListeners();
    });
  }

  getChatDetails({
    required String consumerId,
    required String providerId,
    required int projectId,
    required Function callBack,
  }) async {
    if (_socket == null || !_socket!.connected) {
      Common.showToast(
        "We are unable to proceed at the moment. Please try again later.",
      );
      return;
    }

    if (_socket!.hasListeners(Constants.GET_CHAT_DETAILS)) {
      _socket!.off(Constants.GET_CHAT_DETAILS);
    }

    _socket!.on(Constants.GET_CHAT_DETAILS, (data) {
      _socket!.off(Constants.GET_CHAT_DETAILS);
      print("${Constants.GET_CHAT_DETAILS} $data");
      callBack(data);
    });

    String? token =
        (await Common.getUserModelFromSharedPreferences())?.token ?? "";
    if (token == null) {
      Common.showToast(
        "We are unable to send messages at the moment. Please try again later.",
      );
      return;
    }

    var data = {
      "consumer_id": consumerId,
      "provider_id": providerId,
      "project_id": projectId,
      "bearer_token": token,
    };
    _socket!.emit(
      Constants.GET_CHAT_DETAILS_EMIT,
      [data],
    );
    print("data for getting chat details $data");
  }

  receivePreviousMessagesEmitter({String? chatId}) async {
    if (_socket == null || !_socket!.connected) return;
    String? token =
        (await Common.getUserModelFromSharedPreferences())?.token ?? "";
    if (token == null) {
      Common.showToast(
        "We are unable to send messages at the moment. Please try again later.",
      );
      return;
    }

    var data = {
      "chat_id": chatId,
      "bearer_token": token,
    };
    _socket!.emit(
      Constants.GET_CHAT_MESSAGES,
      [data],
    );
    print("data for getting previous messages $data");
  }

  _receiveChatPeopleListListener() async {
    if (_socket == null || !_socket!.connected) return;

    if (_socket!.hasListeners(Constants.CHAT_PEOPLE_LIST)) {
      _socket!.off(Constants.CHAT_PEOPLE_LIST);
    }
    _socket!.on(Constants.CHAT_PEOPLE_LIST, (event) async {
      print(event);
      List<PreviousChatPeopleModel> list = List<PreviousChatPeopleModel>.from(
        event.map(
          (x) => PreviousChatPeopleModel.fromJson(x),
        ),
      );
      if (list.isEmpty) {
        notifyListeners();
        return;
      }

      _chatPeople.clear();
      _chatPeople.addAll(list);

      notifyListeners();
    });
  }

  _receiveChatPeopleListEmitter() async {
    if (_socket == null || !_socket!.connected) return;
    String? token =
        (await Common.getUserModelFromSharedPreferences())?.token ?? "";
    if (token == null) {
      Common.showToast(
        "We are unable to send messages at the moment. Please try again later.",
      );
      return;
    }

    var data = {
      "bearer_token": token,
    };
    _socket!.emit(
      Constants.GET_CHAT_PEOPLE_LIST,
      [data],
    );
    print("data for getting chat people $data");
  }

  _readMessageEmitter({String? chatId}) async {
    if (_socket == null || !_socket!.connected) return;
    String? token =
        (await Common.getUserModelFromSharedPreferences())?.token ?? "";
    if (token == null) {
      Common.showToast(
        "We are unable to send messages at the moment. Please try again later.",
      );
      return;
    }

    var data = {
      "chat_id": chatId,
      "bearer_token": token,
    };
    _socket!.emit(
      Constants.MARK_READ,
      [data],
    );
    print("data for getting chat people $data");
  }

  void _receiveSingleMessage({String? chatId}) async {
    print("chatId $chatId here");
    if (_socket == null || !_socket!.connected) return;

    if (_socket!.hasListeners(Constants.NEW_MESSAGE)) {
      _socket!.off(Constants.NEW_MESSAGE);
    }
    _socket!.on(Constants.NEW_MESSAGE, (event) {
      print("event gg $event");
      if (event is Map<String, dynamic>) {
        ChatMessageModel chatMessageModel = ChatMessageModel.fromJson(event);
        if (chatMessageModel.chatId == chatId ||
            chatId == null ||
            chatId.isEmpty) {
          newMessageReceived(chatMessageModel, chatId ?? "");

          if (chatId == null || chatId.isEmpty) {
            _receiveSingleMessage(chatId: chatMessageModel.chatId);
          }
        }
      }
    });
  }

  void newMessageReceived(ChatMessageModel value, String chatId) async {
    _chatMessages.insert(0, value);

    UserModel? userModel = await Common.getUserModelFromSharedPreferences();
    if (value.senderId != (userModel?.uuid ?? "")) {
      _readMessageEmitter(chatId: chatId);
    }

    notifyListeners();
  }

  void sendNewMessage(
    String message,
    String receiverId,
    String projectId,
  ) async {
    String? token =
        (await Common.getUserModelFromSharedPreferences())?.token ?? "";
    if (token == null) {
      Common.showToast(
        "We are unable to send messages at the moment. Please try again later.",
      );
      return;
    }
    var data = {
      "bearer_token": token,
      "message_to": receiverId,
      "message": message,
      "project_id": projectId,
    };
    if (_socket != null && _socket!.connected) {
      _socket!.emit(
        Constants.SEND_SINGLE_MESSAGE,
        [data],
      );
      print("sending new message with $data");
    } else {
      Common.showToast(
        "We are unable to send messages at the moment. Please try again later.",
      );
    }
  }

  void turnOffSingleChatListeners() async {
    _chatMessages.clear();
    if (_socket!.hasListeners(Constants.NEW_MESSAGE)) {
      _socket!.off(Constants.NEW_MESSAGE);
    }
    if (_socket!.hasListeners(Constants.CHAT_MESSAGES_LISTENER)) {
      _socket!.off(Constants.CHAT_MESSAGES_LISTENER);
    }
  }

  void turnOffChatPeopleListeners() async {
    if (_socket!.hasListeners(Constants.CHAT_PEOPLE_LIST)) {
      _socket!.off(Constants.CHAT_PEOPLE_LIST);
    }
  }
}
