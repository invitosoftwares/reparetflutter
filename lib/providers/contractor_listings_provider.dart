import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reparet/providers/auth_provider.dart';
import '../../models/get_listings_parent_model.dart';
import '../../models/project_model.dart';
import '../../network/base_network.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import 'package:collection/collection.dart';

class ContractorListingProvider with ChangeNotifier {
  BaseNetwork? _baseNetwork;

  bool hasMoreNewProjects = false;
  final List<ProjectModel> _allNewProjects = [];

  List<ProjectModel> get allNewProjects => _allNewProjects;

  Future<dynamic> getAllNewProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getProviderProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_POSTED}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allNewProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreNewProjects = false;
            notifyListeners();
            return;
          }

          hasMoreNewProjects = model.data!.projects!.length == perPage;
          allNewProjects.addAll(model.data?.projects ?? []);
          AuthProvider authProvider =
              Provider.of<AuthProvider>(context, listen: false);
          for (ProjectModel projectModel in allNewProjects) {
            await authProvider.getImagesAndVideosUrl(projectModel);
          }
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> makeBid(
    BuildContext context,
    String projectId,
    String bidAmount,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.makeBid(
            projectId,
            bidAmount,
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          _allNewProjects
              .firstWhere((element) => "${element.projectId}" == projectId)
              .makeBidLocally(int.parse(bidAmount));
          if (_allInvitations.isNotEmpty) {
            ProjectModel? model = _allInvitations.firstWhereOrNull(
                (element) => "${element.projectId}" == projectId);
            if (model != null) model.makeBidLocally(int.parse(bidAmount));
          }
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  cancelBid(String projectId) {
    _allNewProjects
        .firstWhere((element) => "${element.projectId}" == projectId)
        .cancelBid();

    if (_allInvitations.isNotEmpty) {
      ProjectModel? model = _allInvitations
          .firstWhereOrNull((element) => "${element.projectId}" == projectId);
      if (model != null) model.cancelBid();
    }
  }

  addReviewLocally(String projectId, dynamic rating) {
    _allCompletedProjects
        .firstWhere((element) => "${element.projectId}" == projectId)
        .setRatingLocally(rating);
  }

  //ongoing projects
  bool hasMoreOngoingProjects = false;
  final List<ProjectModel> _allOngoingProjects = [];

  List<ProjectModel> get allOngoingProjects => _allOngoingProjects;

  Future<dynamic> getAllOngoingProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getProviderProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_ACCEPTED}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allOngoingProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreOngoingProjects = false;
            notifyListeners();
            return;
          }

          hasMoreOngoingProjects = model.data!.projects!.length == perPage;
          allOngoingProjects.addAll(model.data?.projects ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> markProjectAsDone(
    BuildContext context,
    String projectId,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.markAsDone(projectId);

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          _allOngoingProjects
              .removeWhere((element) => "${element.projectId}" == projectId);
          notifyListeners();
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  //completed projects
  bool hasMoreCompletedProjects = false;
  final List<ProjectModel> _allCompletedProjects = [];

  List<ProjectModel> get allCompletedProjects => _allCompletedProjects;

  Future<dynamic> getAllCompletedProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getProviderProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_COMPLETED},${Constants.PROJECT_PAID},${Constants.PROJECT_PAID_REVIEWED_BY_USER}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allCompletedProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreCompletedProjects = false;
            notifyListeners();
            return;
          }

          hasMoreCompletedProjects = model.data!.projects!.length == perPage;
          allCompletedProjects.addAll(model.data?.projects ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  //invitations
  bool hasMoreInvitations = false;
  final List<ProjectModel> _allInvitations = [];

  List<ProjectModel> get allInvitations => _allInvitations;

  Future<dynamic> getAllInvitations(
    BuildContext context,
    String page,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getInvitations(
        page,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allInvitations.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreInvitations = false;
            notifyListeners();
            return;
          }

          hasMoreInvitations = model.data!.projects!.length == perPage;
          allInvitations.addAll(model.data?.projects ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
