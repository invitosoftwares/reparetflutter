import 'package:flutter/material.dart';
import '../../models/search_providers_parent_model.dart';
import '../../models/toggle_favourite_status_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';
import 'package:collection/collection.dart';

class UserSearchContractorsProvider with ChangeNotifier {
  BaseNetwork? _baseNetwork;

  bool hasMoreProviders = false;
  final List<ProviderModel> _allContractors = [];

  List<ProviderModel> get allContractors => _allContractors;

  Future<dynamic> searchProviders(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.searchProviders(
        _baseNetwork!.token!,
        page,
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = SearchProvidersParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allContractors.clear();
          }

          if (model.data!.isEmpty) {
            hasMoreProviders = false;
            notifyListeners();
            return;
          }

          hasMoreProviders = model.data!.length == perPage;
          allContractors.addAll(model.data ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> toggleFavouriteStatus(
    BuildContext context,
    int providerId, {
    bool fromFavouritesScreen = false,
  }) async {
    try {
      _baseNetwork ??= BaseNetwork(context);
      toggleFavouriteStatusLocally(providerId);

      allFavouriteContractors
          .removeWhere((element) => element.providerId == providerId);
      notifyListeners();

      var responseBean = await _baseNetwork!.retrofit.toggleFavourite(
        "$providerId",
      );

      if (responseBean.response.statusCode == 200) {
        final model = ToggleFavouriteStatusModel.fromJson(responseBean.data);
        if (model.type != "success") {
          toggleFavouriteStatusLocally(providerId);
        }
      } else {
        toggleFavouriteStatusLocally(providerId);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      toggleFavouriteStatusLocally(providerId);
    }
  }

  toggleFavouriteStatusLocally(int providerId) {
    ProviderModel? model = allContractors
        .firstWhereOrNull((element) => element.providerId == providerId);
    if (model != null) {
      model.updateFavouriteStatus();
    }
  }

  //favourite contractors list
  bool hasMoreFavouriteProviders = false;
  final List<ProviderModel> _allFavouriteContractors = [];

  List<ProviderModel> get allFavouriteContractors => _allFavouriteContractors;

  Future<dynamic> getFavouriteProviders(
    BuildContext context,
    String page,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getFavouriteProviders(
        _baseNetwork!.token!,
        page,
      );

      if (responseBean.response.statusCode == 200) {
        final model = SearchProvidersParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allFavouriteContractors.clear();
          }

          if (model.data!.isEmpty) {
            hasMoreFavouriteProviders = false;
            notifyListeners();
            return;
          }

          hasMoreFavouriteProviders = model.data!.length == perPage;
          allFavouriteContractors.addAll(model.data ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
