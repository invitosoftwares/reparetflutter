import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:reparet/providers/auth_provider.dart';
import '../../models/get_listings_parent_model.dart';
import '../../models/project_model.dart';
import '../../models/project_payment_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';
import 'package:collection/collection.dart';

class UserListingProvider with ChangeNotifier {
  BaseNetwork? _baseNetwork;

  bool hasMoreNewListings = false;
  final List<ProjectModel> _allNewProjects = [];

  List<ProjectModel> get allNewProjects => _allNewProjects;

  Future<dynamic> getAllNewProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getUserProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_POSTED}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allNewProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreNewListings = false;
            notifyListeners();
            return;
          }

          hasMoreNewListings = model.data!.projects!.length == perPage;
          allNewProjects.addAll(model.data?.projects ?? []);
          AuthProvider authProvider =
              Provider.of<AuthProvider>(context, listen: false);
          for (ProjectModel projectModel in allNewProjects) {
            await authProvider.getImagesAndVideosUrl(projectModel);
          }
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  deleteNewProjectLocally(String id) {
    allNewProjects.removeWhere((element) => "${element.projectId}" == id);
    notifyListeners();
  }

  // ongoing listings data
  bool hasMoreOngoingListings = false;
  final List<ProjectModel> _allOngoingProjects = [];

  List<ProjectModel> get allOngoingProjects => _allOngoingProjects;

  Future<dynamic> getAllOngoingProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getUserProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_ACCEPTED}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allOngoingProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreOngoingListings = false;
            notifyListeners();
            return;
          }

          hasMoreOngoingListings = model.data!.projects!.length == perPage;
          allOngoingProjects.addAll(model.data?.projects ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  // completed listings data
  bool hasMoreCompletedListings = false;
  final List<ProjectModel> _allCompletedProjects = [];

  List<ProjectModel> get allCompletedProjects => _allCompletedProjects;

  Future<dynamic> getAllCompletedProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getUserProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_COMPLETED},${Constants.PROJECT_PAID},${Constants.PROJECT_PAID_REVIEWED_BY_USER}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allCompletedProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreCompletedListings = false;
            notifyListeners();
            return;
          }

          hasMoreCompletedListings = model.data!.projects!.length == perPage;
          allCompletedProjects.addAll(model.data?.projects ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> payForProject(
    BuildContext context,
    String projectId,
    String pmId,
  ) async {
    try {
      _baseNetwork ??= BaseNetwork(context);

      var responseBean = await _baseNetwork!.retrofit.payForProject(
        projectId,
        pmId,
      );

      if (responseBean.response.statusCode == 200) {
        final model = ProjectPaymentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (model.data != null && (model.data?.paymentStatus ?? false)) {
            return true;
          } else {
            return model;
          }
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  updatePaidStatusLocally(String projectId) {
    ProjectModel? projectModel = allCompletedProjects
        .firstWhereOrNull((element) => "${element.projectId}" == projectId);
    if (projectModel != null) {
      projectModel.markProjectAsPaid();
    }
  }

  updateRatingAndStatusLocally(String projectId, dynamic rating) {
    ProjectModel? projectModel = allCompletedProjects
        .firstWhereOrNull((element) => "${element.projectId}" == projectId);
    if (projectModel != null) {
      projectModel.setRatingLocally(rating);
    }
  }

  Future<dynamic> confirmPaymentForProject(
    BuildContext context,
    String projectId,
    String paymentId,
    String paymentRawData,
  ) async {
    try {
      _baseNetwork ??= BaseNetwork(context);

      var responseBean = await _baseNetwork!.retrofit.confirmProjectPayment(
        projectId,
        paymentId,
        paymentRawData,
      );

      if (responseBean.response.statusCode == 200) {
        final model = ProjectPaymentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (model.data != null && (model.data?.paymentStatus ?? false)) {
            return true;
          } else {
            return model;
          }
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
