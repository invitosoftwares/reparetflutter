import 'package:flutter/material.dart';
import '../../models/get_stripe_login_url_model.dart';
import '../../network/base_network.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';

class PostProjectProvider with ChangeNotifier {
  Future<dynamic> postProject(
    BuildContext context,
    String projectTitle,
    String projectDescription,
    String categoryId,
    String address,
    String lat,
    String lng,
    /*String projectDate,
    String projectTime,*/
    String allowUnverified,
    String videos,
    String images,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.createProject(
            projectTitle,
            projectDescription,
            categoryId,
            address,
            lat,
            lng,
            /*projectDate,
            projectTime,*/
            allowUnverified,
            videos,
            images,
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }

  Future<dynamic> editProject(
    BuildContext context,
    String projectId,
    String projectTitle,
    String projectDescription,
    String categoryId,
    String address,
    String lat,
    String lng,
    /*String projectDate,
    String projectTime,*/
    String allowUnverified,
    String videos,
    String images,
  ) async {
    try {
      var responseBean = await BaseNetwork(
        context,
      ).retrofit.editProject(
            projectId,
            projectTitle,
            projectDescription,
            categoryId,
            address,
            lat,
            lng,
            /*projectDate,
            projectTime,*/
            allowUnverified,
            videos,
            images,
          );

      if (responseBean.response.statusCode == 200) {
        if (responseBean.response.data["type"] == "success") {
          return true;
        } else {
          return Common.errorMessage(responseBean);
        }
      } else {
        return Common.errorMessage(responseBean);
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return "We are unable to fetch your data, please try again later.";
    }
  }
}
