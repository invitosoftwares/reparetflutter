import 'package:flutter/material.dart';
import '../../models/get_categories_parent_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';

class CategoriesProvider with ChangeNotifier {
  final List<CategoryModel> _allCategories = [];

  List<CategoryModel> get allCategories => _allCategories;

  Future<dynamic> getAllCategories(
    BuildContext context,
  ) async {
    try {
      var responseBean = await BaseNetwork(context).retrofit.getAllCategories();

      if (responseBean.response.statusCode == 200) {
        final model = GetCategoriesParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          _allCategories.clear();
          _allCategories.addAll(model.data ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
