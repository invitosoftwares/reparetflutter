import 'package:flutter/material.dart';
import 'package:reparet/widgets/common/upload_file_s3.dart';
import '../../models/project_chats_parent_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';

class ProjectChatsProvider with ChangeNotifier {
  bool hasMore = false;
  final List<ProjectChat> _allChats = [];

  List<ProjectChat> get allChats => _allChats;

  Future<dynamic> getProjectChats(
    BuildContext context,
    String projectId,
    String page,
  ) async {
    try {
      int perPage = 10;

      var responseBean = await BaseNetwork(context).retrofit.getProjectChats(
            projectId,
            page,
          );

      if (responseBean.response.statusCode == 200) {
        final model = ProjectChatsParentModel.fromJson(responseBean.data);

        if (model.type == "success") {
          if (page == "0") {
            _allChats.clear();
          }

          if (model.data!.projectChats!.isEmpty) {
            hasMore = false;
            notifyListeners();
            return;
          }

          hasMore = model.data!.projectChats!.length == perPage;
          allChats.addAll(model.data?.projectChats ?? []);
          for (ProjectChat chat in allChats) {
            chat.image = await UploadFileS3.getImageUrl(chat.image);
          }
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
