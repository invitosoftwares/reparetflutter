import 'package:flutter/material.dart';
import '../../models/get_listings_parent_model.dart';
import '../../models/project_model.dart';
import '../../models/send_invitation_model.dart';
import '../../network/base_network.dart';
import '../../utils/constants.dart';

class ProjectInvitationProvider with ChangeNotifier {
  BaseNetwork? _baseNetwork;

  bool hasMoreNewListings = false;
  final List<ProjectModel> _allNewProjects = [];

  List<ProjectModel> get allNewProjects => _allNewProjects;

  Future<dynamic> getAllNewProjects(
    BuildContext context,
    String page,
    String search,
  ) async {
    try {
      int perPage = 10;
      if (_baseNetwork == null) {
        _baseNetwork = BaseNetwork(context);
      } else {
        _baseNetwork!.cancelCall();
      }

      var responseBean = await _baseNetwork!.retrofit.getUserProjects(
        _baseNetwork!.token!,
        page,
        "${Constants.PROJECT_POSTED}",
        search,
      );

      if (responseBean.response.statusCode == 200) {
        final model = GetListingsParentModel.fromJson(responseBean.data);
        if (model.type == "success") {
          if (page == "0") {
            allNewProjects.clear();
          }

          if (model.data!.projects!.isEmpty) {
            hasMoreNewListings = false;
            notifyListeners();
            return;
          }

          hasMoreNewListings = model.data!.projects!.length == perPage;
          allNewProjects.addAll(model.data?.projects ?? []);
          notifyListeners();
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }

  Future<dynamic> sendInvitation(
    BuildContext context,
    String projectId,
    String providerId,
  ) async {
    try {
      _baseNetwork ??= BaseNetwork(context);

      var responseBean = await _baseNetwork!.retrofit.sendInvitation(
        projectId,
        providerId,
      );

      if (responseBean.response.statusCode == 200) {
        final model = SendInvitationModel.fromJson(responseBean.data);
        if (model.type == "success") {
          return true;
        } else {
          return model.message;
        }
      } else {
        if (responseBean.data["message"] != null) {
          return responseBean.data["message"];
        }
        return Constants.UNABLE_TO_PROCESS;
      }
    } catch (error) {
      print(
          "------------------------------------------------------------------------------------------\n$error\n------------------------------------------------------------------------------------------");
      return Constants.UNABLE_TO_PROCESS;
    }
  }
}
