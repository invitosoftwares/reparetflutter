import 'package:permission_handler/permission_handler.dart';

class MyPermissionHandler {
  Future<bool> checkPermissions(List<Permission> permissions) async {
    for (Permission permission in permissions) {
      if (await permission.status != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }

  Future<bool> requestPermissions(List<Permission> permissions) async {
    Map<Permission, PermissionStatus> statuses = await [
      ...permissions,
    ].request();

    for (PermissionStatus status in statuses.values) {
      if (status != PermissionStatus.granted) {
        return false;
      }
    }
    return true;
  }
}
