//import 'package:aws_s3/aws_s3.dart';

class Constants {
  static String isUserLoggedIn = "isUserLoggedIn";
  static String userModel = "userModel";
  static String pushNotificationToken = "pushNotificationToken";
  static String currentChatWindowChatId = "currentChatWindowChatId";

  static int USER = 1;
  static int CONTRACTOR = 2;

  static String UNABLE_TO_PROCESS =
      "We are unable to process your request, please try again later.";

  static const DATE_FORMAT_1 =
      "MM-dd-yyyy"; //do not use in project, used to send to the backend
  static const DATE_FORMAT_2 = "HH:mm:ss";
  static const DATE_FORMAT_3 = "dd-MM-yyyy";
  static const DATE_FORMAT_4 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
  static const DATE_FORMAT_5 = "dd-MM-yyyy hh:mm a";
  static const DATE_FORMAT_6 = "hh:mm a";

  static const String FOLDER_PATH = "images";
  static const String POOL_ID =
      "ap-south-1:1e59724f-94d9-48d2-822f-d921c97c8e50";
  static const String BUCKET_NAME = "yellowishs3bucket";
  static const String UPLOADED_URL_PREFIX =
      "https://yellowishs3bucket.s3.amazonaws.com/images/";
  //static const Regions REGION = Regions.AP_SOUTH_1;

  static String TERMS_AND_CONDITIONS_URL = "https://www.reparet.com/terms";
  static String PRIVACY_POLICY_URL = "https://www.reparet.com/terms";
  static String SUPPORT_URL = "https://www.reparet.com/terms";

  static String stripePublishableKey =
      "pk_test_51HIT93Hg7UzqC5bOV3DpOOQUWYvSMDrNkz7Z8h1Sh7vE45HJ8PjJehgP3NSXaGxsxdN397yt7PmWQEp83OOv8RAp00LxIDMDfl";
  //  "pk_live_AAwCgESfZVm6JtruHYQeshkg"; // on vipulbansalkkp@gmail.com

  static String GOOGLE_API_KEY_MAPS =
      "AIzaSyDcfOUOt5Wr_ACHzpJV4RoWFXr070gsR3w"; //provided by vipul on 9th Jul, 2022

  static String verificationUrl =
      "https://trueme.goodhire.com/custom-link/f1d730e7-8b61-4a37-a23d-17d993674531";

  //project status
  static int PROJECT_POSTED = 1;
  static int PROJECT_ACCEPTED = 2;
  static int PROJECT_COMPLETED = 3;
  static int PROJECT_PAID = 4;
  static int PROJECT_PAID_REVIEWED_BY_USER = 5;

  //sockets
  static const String SOCKET_URL = "https://dev2.invitocreatives.com:3002";

  static const String SAVE_SOCKET_ID = "connect-user";
  static const String SAVE_SOCKET_ID_RESPONSE = "connect-user-response";

  static const String SEND_SINGLE_MESSAGE = "send-message";
  static const String NEW_MESSAGE = "new-message";

  static const String GET_CHAT_MESSAGES = "get-chat-messages";
  static const String CHAT_MESSAGES_LISTENER = "chat-messages";

  static const String GET_CHAT_PEOPLE_LIST = "get-chat-list";
  static const String CHAT_PEOPLE_LIST = "chat-list";

  static const String MARK_READ = "mark-read";

  static const String GET_CHAT_DETAILS_EMIT = "get-chat-id";
  static const String GET_CHAT_DETAILS = "chat-id";
}
