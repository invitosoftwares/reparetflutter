import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../providers/chat_provider.dart';
import '../../screens/common/main_chat_screen.dart';
import '../../widgets/common/delete_account_password_view.dart';
import 'package:stripe_sdk/stripe_sdk.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import '../../models/form_field_data.dart';
import '../../models/project_payment_model.dart';
import '../../models/user_model.dart';
import '../../place_picker/entities/location_result.dart';
import '../../place_picker/widgets/place_picker.dart';
import '../../providers/auth_provider.dart';
import '../../providers/categories_provider.dart';
import '../../providers/user_listings_provider.dart';
import '../../screens/user/manage_payment_methods_screen.dart';
import '../../utils/alert_dialogs.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';
import '../../utils/my_permission_handler.dart';
import '../../utils/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

class Common {
  static Future<bool> checkInternetConnection() async {
    try {
      ConnectivityResult result = await Connectivity().checkConnectivity();
      return result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi;
    } catch (e) {
      print("checkInternetConnection()" + e.toString());
      return false;
    }
  }

  static Future launchUrl(String url) async {
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(url);
    } else {
      showToast("No app found to open verification link.");
    }
  }

  static void deleteAccount(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (context) => Padding(
        padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).viewInsets.bottom,
        ),
        child: DeleteAccountPasswordView(Constants.CONTRACTOR),
      ),
      isScrollControlled: true,
      backgroundColor: Colors.transparent,
    );
  }

  static bool isValidEmail(String email) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern);
    return regex.hasMatch(email);
  }

  static Future<void> setUserModelInSharedPreferences(
      UserModel userModel) async {
    return await MySharedPreferences.setString(
        Constants.userModel, json.encode(userModel.toJson()));
  }

  static Future<UserModel?> getUserModelFromSharedPreferences() async {
    String data = await MySharedPreferences.getString(Constants.userModel);
    if (data.isEmpty) {
      return null;
    } else {
      return UserModel.fromJson(json.decode(data));
    }
  }

  static void showToast(String text) {
    Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 2,
      backgroundColor: CustomColors.colorGrayDark,
      textColor: Colors.white,
    );
  }

  static RouteTransitionsBuilder getRouteTransitionBuilder() {
    return (context, animation, secondaryAnimation, child) {
      var begin = const Offset(1.0, 0.0);
      var end = Offset.zero;
      var curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    };
  }

  static List<String> getErrors(Map<String, FormFieldData?> _data) {
    List<String> errors = [];
    errors.clear();
    for (var element in _data.values) {
      if (element != null && element.isError) errors.add(element.data);
    }
    return errors;
  }

  static Future<Position?> getCurrentLocation() async {
    List<Permission> allPermissions = [
      Permission.location,
    ];

    MyPermissionHandler myPermissionHandler = MyPermissionHandler();
    if (!await myPermissionHandler.checkPermissions(allPermissions)) {
      bool status = await myPermissionHandler.requestPermissions(
        allPermissions,
      );
      if (status) {
        return _pickLocation();
      } else {
        Common.showToast("Requested permissions are required to proceed.");
        openAppSettings();
        return null;
      }
    } else {
      return _pickLocation();
    }
  }

  static Future<Position?> _pickLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);

      return position;
    } catch (err) {
      print(err);
    }
  }

  static Future<String?> getAddressFromLatLng(Position _currentPosition) async {
    try {
      List<Placemark> placeMarks = await placemarkFromCoordinates(
        _currentPosition.latitude,
        _currentPosition.longitude,
      );
      Placemark place = placeMarks[0];
      print(place);
      String address =
          "${place.name}, ${place.street}, ${place.locality}, ${place.subLocality}, ${place.administrativeArea}, "
          "${place.subAdministrativeArea}, ${place.country}, ${place.postalCode}";
      print(address);
      return address;
    } catch (e) {
      print(e);
    }
  }

  static void callProfileDetailsApi(BuildContext context) {
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    authProvider.getProfileDetailsApi(context);
  }

  static void getAllCategories(BuildContext context) {
    CategoriesProvider categoriesProvider =
        Provider.of<CategoriesProvider>(context, listen: false);
    categoriesProvider.getAllCategories(context);
  }

  static void showLoaderDialog(String text, BuildContext context) {
    AuthProvider authProvider =
        Provider.of<AuthProvider>(context, listen: false);
    AlertDialog alert = AlertDialog(
      content: Row(
        children: [
          SizedBox(
            height: 25,
            width: 25,
            child: CircularProgressIndicator(
              color: ((authProvider.userModel?.role ?? Constants.USER) ==
                      Constants.USER)
                  ? CustomColors.colorAccent
                  : CustomColors.colorPrimary,
              strokeWidth: 2,
            ),
          ),
          Container(
            margin: const EdgeInsets.only(left: 10),
            child: Text(
              text,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: ((authProvider.userModel?.role ?? Constants.USER) ==
                            Constants.USER)
                        ? CustomColors.colorAccent
                        : CustomColors.colorPrimary,
                  ),
            ),
          ),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  static String errorMessage(responseBean) {
    String error =
        "We are unable to process your request, please try again later.";
    if (responseBean == null) return error;
    if (responseBean.response.data["message"] != null ||
        responseBean.response.data["message"] != "") {
      error = responseBean.response.data["message"];
    }
    return error;
  }

  static Future<LocationResult?> checkLocationPermission(
      BuildContext context) async {
    List<Permission> allPermissions = [
      Permission.location,
    ];

    MyPermissionHandler myPermissionHandler = MyPermissionHandler();
    if (!await myPermissionHandler.checkPermissions(allPermissions)) {
      bool status =
          await myPermissionHandler.requestPermissions(allPermissions);
      if (status) {
        return showPlacePicker(context);
      } else {
        Common.showToast("Requested permissions are required to proceed.");
        openAppSettings();
        return null;
      }
    } else {
      return showPlacePicker(context);
    }
  }

  static Future<LocationResult?> showPlacePicker(BuildContext context) async {
    LocationResult? result = await Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => PlacePicker(Constants.GOOGLE_API_KEY_MAPS),
      ),
    );
    return result;
  }

  static String getConvertedDate(
    String date,
    String inputFormat,
    String outputFormat, {
    bool isUtc = false,
  }) {
    if (date.isEmpty) return "";

    DateFormat inputDateFormat = DateFormat(inputFormat);
    DateFormat outputDateFormat = DateFormat(outputFormat);
    try {
      DateTime dateTime = inputDateFormat.parse(date, isUtc).toLocal();
      if (dateTime == null) return "";
      String result = outputDateFormat.format(dateTime);
      return result;
    } catch (err) {
      print(err);
    }
    return "";
  }

  static Future<dynamic> startPaymentProcessForProject(
    BuildContext context,
    String projectId,
    UserListingProvider userListingProvider,
  ) async {
    // dynamic data = await Navigator.pushNamed(
    //   context,
    //   ManagePaymentMethodsScreen.routeName,
    //   arguments: {
    //     "PROJECT_PAYMENT": true,
    //   },
    // );
    dynamic data = "pm_1L1p7qHg7UzqC5bOtH9jw27A";
    if (data != null && (data is String) && data.isNotEmpty) {
      if (!await Common.checkInternetConnection()) {
        AlertDialogs.showAlertDialogWithOk(
          context,
          "You are offline. Please make sure you have a stable internet connection to proceed",
        );
        return;
      }

      Common.showLoaderDialog("Please wait...", context);

      try {
        dynamic response = await userListingProvider.payForProject(
          context,
          projectId,
          data,
        );

        if (response is String) {
          Navigator.pop(context);
          AlertDialogs.showAlertDialogWithOk(context, response);
        } else if (response is bool && response) {
          Navigator.pop(context);
          return true;
        } else if (response is ProjectPaymentModel) {
          dynamic res = await start3DSec(
            response.data?.clientSecret ?? "",
            data,
            projectId,
            context,
            userListingProvider,
          );
          Navigator.pop(context);
          if (res is bool && res) {
            return true;
          } else if (res is String) {
            AlertDialogs.showAlertDialogWithOk(context, res);
          }
        } else {
          Navigator.pop(context);
        }
      } catch (e) {
        Navigator.pop(context);
        print(e);
      }
    }
  }

  static Future start3DSec(
    String clientSecret,
    String pmID,
    String projectId,
    BuildContext context,
    UserListingProvider userListingProvider,
  ) async {
    Map<String, dynamic>? paymentIntentRes = await confirmPayment3DSecure(
      context,
      clientSecret,
      pmID,
    );

    if (paymentIntentRes == null) {
      return "paymentIntentRes is null";
    }
    if (paymentIntentRes['status'] == 'failure') {
      return paymentIntentRes['error'] ??
          "Transaction was not completed at the moment.";
    }
    if (paymentIntentRes['status'] != 'succeeded') {
      return "Transaction cancelled.";
    }

    if (paymentIntentRes['status'] == 'succeeded') {
      String paymentId = "";
      String rawData = "";
      if (paymentIntentRes["id"] != null) {
        paymentId = paymentIntentRes["id"];
      }
      rawData = json.encode(paymentIntentRes);

      return userListingProvider.confirmPaymentForProject(
        context,
        projectId,
        paymentId,
        rawData,
      );
    }
  }

  static Future<Map<String, dynamic>?> confirmPayment3DSecure(
    BuildContext context,
    String clientSecret,
    String paymentMethodId,
  ) async {
    Map<String, dynamic>? paymentIntentRes_3dSecure;
    try {
      final Stripe stripe = Stripe(
        Constants.stripePublishableKey,
      );
      await stripe.confirmPayment(
        clientSecret,
        context,
        paymentMethodId: paymentMethodId,
      );
      paymentIntentRes_3dSecure = await stripe.api.retrievePaymentIntent(
        clientSecret,
      );
    } catch (e) {
      print("ERROR_ConfirmPayment3DSecure: $e");
      return {
        "error": "$e",
        "status": "failure",
      };
    }
    return paymentIntentRes_3dSecure;
  }

  static void getChatScreen(
    BuildContext context, {
    required String consumerId,
    required String providerId,
    required int projectId,
    required String name,
  }) {
    ChatProvider chatProvider = Provider.of<ChatProvider>(
      context,
      listen: false,
    );

    showLoaderDialog("Loading, please wait...", context);
    chatProvider.getChatDetails(
      consumerId: consumerId,
      providerId: providerId,
      projectId: projectId,
      callBack: (val) async {
        Navigator.pop(context);
        UserModel? userModel = await Common.getUserModelFromSharedPreferences();
        if (userModel == null) return;
        openChatScreen(
          context,
          name: name,
          projectId: projectId,
          chatId: val["chat_id"] ?? "",
          projectStatus: val["project_status"] ?? 1,
          otherUserId:
              userModel.role == Constants.USER ? providerId : consumerId,
        );
      },
    );
  }

  static void openChatScreen(
    BuildContext context, {
    required String otherUserId,
    required int projectId,
    required String chatId,
    required String name,
    required int projectStatus,
  }) {
    Navigator.pushNamed(
      context,
      MainChatScreen.routeName,
      arguments: {
        "OTHER_USER_ID": otherUserId,
        "PROJECT_ID": projectId,
        "CHAT_ID": chatId,
        "NAME": name,
        "IS_CHAT_ALLOWED":
            (projectStatus != Constants.PROJECT_PAID_REVIEWED_BY_USER &&
                projectStatus != Constants.PROJECT_PAID),
      },
    );
  }
}
