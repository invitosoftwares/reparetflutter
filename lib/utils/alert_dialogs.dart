import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../providers/auth_provider.dart';
import '../../utils/constants.dart';
import '../../utils/custom_colors.dart';

class AlertDialogs {
  static showAlertDialogWithTwoButtons(
    BuildContext context,
    String negativeText,
    String positiveText,
    String content,
    Function negative,
    Function positive, {
    bool dismiss = true,
  }) {
    Widget negativeButton = Expanded(
      child: TextButton(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 20),
          decoration: BoxDecoration(
            color: CustomColors.colorGrayDark2,
            borderRadius: BorderRadius.circular(5),
          ),
          width: double.infinity,
          child: Center(
            child: Text(
              negativeText,
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: CustomColors.colorWhite,
                    fontSize: 14.0,
                  ),
            ),
          ),
        ),
        onPressed: () {
          negative();
        },
      ),
    );
    Widget positiveButton = Consumer<AuthProvider>(
      builder: (context, value, child) => Expanded(
        child: TextButton(
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            decoration: BoxDecoration(
              color:
                  ((value.userModel?.role ?? Constants.USER) == Constants.USER)
                      ? CustomColors.colorAccent
                      : CustomColors.colorPrimary,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Center(
              child: Text(
                ("Yes"),
                style: Theme.of(context).textTheme.bodyText1!.copyWith(
                      color: CustomColors.colorWhite,
                      fontSize: 14.0,
                    ),
              ),
            ),
          ),
          onPressed: () {
            positive();
          },
        ),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 5),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              content,
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(fontSize: 14.0),
            ),
          ),
          const SizedBox(height: 20),
          Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              negativeButton,
              positiveButton,
            ],
          ),
        ],
      ),
      contentPadding: const EdgeInsets.all(10),
    );

    showDialog(
      context: context,
      barrierDismissible: dismiss,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async {
            return dismiss;
          },
          child: alert,
        );
      },
    );
  }

  static showAlertDialogWithOk(
    BuildContext context,
    String content, {
    bool dismiss = true,
    VoidCallback? onOkayClicked,
  }) {
    Widget button = Consumer<AuthProvider>(
      builder: (context, value, child) => Center(
        child: TextButton(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            decoration: BoxDecoration(
              color:
                  ((value.userModel?.role ?? Constants.USER) == Constants.USER)
                      ? CustomColors.colorAccent
                      : CustomColors.colorPrimary,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Text(
              ("OK"),
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: CustomColors.colorWhite,
                    fontSize: 14.0,
                  ),
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
            if (!dismiss && onOkayClicked != null) onOkayClicked();
          },
        ),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              content,
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .bodyText1!
                  .copyWith(fontSize: 14.0),
            ),
          ),
          const SizedBox(height: 20),
          button,
        ],
      ),
      contentPadding: const EdgeInsets.all(10),
    );

    showDialog(
      context: context,
      barrierDismissible: dismiss,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async {
            return dismiss;
          },
          child: alert,
        );
      },
    );
  }

  static showMultiStringAlertDialogWithOk(
    BuildContext context,
    List<String> content, {
    bool dismiss = true,
    VoidCallback? onOkayClicked,
  }) {
    Widget button = Consumer<AuthProvider>(
      builder: (context, value, child) => Align(
        alignment: Alignment.centerRight,
        child: TextButton(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 20),
            decoration: BoxDecoration(
              color:
                  ((value.userModel?.role ?? Constants.USER) == Constants.USER)
                      ? CustomColors.colorAccent
                      : CustomColors.colorPrimary,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Text(
              ("OK"),
              style: Theme.of(context).textTheme.bodyText1!.copyWith(
                    color: CustomColors.colorWhite,
                    fontSize: 14.0,
                  ),
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
            if (!dismiss && onOkayClicked != null) onOkayClicked();
          },
        ),
      ),
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          ...content.map((e) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Consumer<AuthProvider>(
                      builder: (context, value, child) => Container(
                        margin: const EdgeInsets.only(top: 7),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ((value.userModel?.role ?? Constants.USER) ==
                                  Constants.USER)
                              ? CustomColors.colorAccent
                              : CustomColors.colorPrimary,
                        ),
                        width: 5,
                        height: 5,
                      ),
                    ),
                    const SizedBox(width: 5),
                    Expanded(
                      child: Text(
                        e,
                        textAlign: TextAlign.start,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1!
                            .copyWith(fontSize: 14.0),
                      ),
                    ),
                  ],
                ),
              )),
          const SizedBox(height: 10),
          button,
        ],
      ),
      contentPadding: const EdgeInsets.all(10),
    );

    showDialog(
      context: context,
      barrierDismissible: dismiss,
      builder: (BuildContext context) {
        return WillPopScope(
          onWillPop: () async {
            return dismiss;
          },
          child: alert,
        );
      },
    );
  }
}
