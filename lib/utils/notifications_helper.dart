import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import '../../models/user_model.dart';
import '../../screens/common/main_chat_screen.dart';
import '../../screens/contractor/project_details_for_contractor_screen.dart';
import '../../screens/user/project_details_for_user_screen.dart';
import '../../utils/common.dart';
import '../../utils/constants.dart';
import '../../utils/shared_preferences.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
}

class NotificationsHelper {
  late FirebaseMessaging messaging;
  late GlobalKey<NavigatorState> navigatorKey;

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  setNavigatorKey(GlobalKey<NavigatorState> navigatorKey) {
    this.navigatorKey = navigatorKey;
  }

  initialize() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    final IOSInitializationSettings initializationSettingsIOS =
        IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification: (
        int id,
        String? title,
        String? body,
        String? payload,
      ) async {},
    );

    const MacOSInitializationSettings initializationSettingsMacOS =
        MacOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
    );

    final InitializationSettings initializationSettings =
        InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
      macOS: initializationSettingsMacOS,
    );

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (String? payload) async {
        if (payload != null) {
          print('notification payload: $payload');
          parseDataAndProceedToScreen(
            jsonDecode(payload),
          );
        }
      },
    );

    messaging = FirebaseMessaging.instance;
    _generateToken();
    _requestNotificationsPermissions();
    _listenForNotifications();
  }

  _generateToken() {
    messaging.getToken().then((value) async {
      print("push token is: $value");
      if (value != null) {
        await MySharedPreferences.setString(
            Constants.pushNotificationToken, value);
        print(
            "value saved ${await MySharedPreferences.getString(Constants.pushNotificationToken)}");
      }
    });
  }

  void _requestNotificationsPermissions() {
    messaging.requestPermission(
      alert: true,
      badge: true,
      sound: true,
      provisional: true,
    );

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );

    flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            MacOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
  }

  Future<void> showNotification(
    String title,
    String body,
    Map<String, dynamic> payload,
  ) async {
    var androidPlatformChannelSpecifics = const AndroidNotificationDetails(
      'channel-01',
      'channel name',
      channelDescription: 'channel description',
      importance: Importance.max,
      priority: Priority.max,
      ticker: 'test ticker',
    );

    var platformChannelSpecifics =
        NotificationDetails(android: androidPlatformChannelSpecifics);
    print("i am here $title $body $payload");
    await flutterLocalNotificationsPlugin.show(
      0,
      title,
      body,
      platformChannelSpecifics,
      payload: jsonEncode(payload),
    );
  }

  void _listenForNotifications() {
    MySharedPreferences.clearSpecific(Constants.currentChatWindowChatId);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) async {
      RemoteNotification? notification = message.notification;
      Map<String, dynamic> data = message.data;
      if (notification == null) {
        return;
      }
      print(
          "message onMessage.listen: ${notification.body} ${notification.title} $data");
      if (data["type"] == "NEW-MESSAGE") {
        String? spChatId = (await MySharedPreferences.getString(
            Constants.currentChatWindowChatId));
        if (spChatId == data["chat_id"]) {
          return;
        }
      }
      showNotification(notification.title ?? "", notification.body ?? "", data);
    });
    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage? message) {
      if (message != null) {
        RemoteNotification? notification = message.notification;
        Map<String, dynamic> data = message.data;
        print(
            "FirebaseMessaging.instance.getInitialMessage ${notification?.body} ${notification?.title} $data");
        parseDataAndProceedToScreen(data, delay: true);
      }
    });
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage? message) {
      if (message != null) {
        RemoteNotification? notification = message.notification;
        Map<String, dynamic> data = message.data;
        print(
            "A new onMessageOpenedApp event was published!  ${notification?.body} ${notification?.title} $data");
        parseDataAndProceedToScreen(data, delay: true);
      }
    });
  }

  parseDataAndProceedToScreen(
    Map<String, dynamic> data, {
    bool delay = false,
  }) async {
    if (navigatorKey.currentState == null) {
      print("navigator key current state is null");
      return;
    }

    if (!(await MySharedPreferences.getBool(Constants.isUserLoggedIn))) {
      return;
    }

    if (delay) {
      await Future.delayed(const Duration(seconds: 1));
    }

    String routeName = "";
    bool clearAllScreens = false;
    Map<String, dynamic> args = {};
    String action = data["type"];

    if (action == "BID-ADDED") {
      clearAllScreens = false;
      routeName = ProjectDetailsForUserScreen.routeName;
      args = {
        "PROJECT_ID": data["project_id"] ?? "",
      };
    } else if (action == "BID-REMOVED") {
      clearAllScreens = false;
      routeName = ProjectDetailsForUserScreen.routeName;
      args = {
        "PROJECT_ID": data["project_id"] ?? "",
      };
    } else if (action == "BID-ACCEPTED") {
      clearAllScreens = false;
      routeName = ProjectDetailsForContractorScreen.routeName;
      args = {
        "PROJECT_ID": data["project_id"] ?? "",
      };
    } else if (action == "PROJECT-PAID") {
      clearAllScreens = false;
      routeName = ProjectDetailsForContractorScreen.routeName;
      args = {
        "PROJECT_ID": data["project_id"] ?? "",
      };
    } else if (action == "PROJECT-COMPLETED") {
      clearAllScreens = false;
      routeName = ProjectDetailsForUserScreen.routeName;
      args = {
        "PROJECT_ID": data["project_id"] ?? "",
      };
    } else if (action == "NEW-MESSAGE") {
      clearAllScreens = false;
      routeName = MainChatScreen.routeName;

      UserModel? userModel = await Common.getUserModelFromSharedPreferences();
      if (userModel == null) return;

      args = {
        "OTHER_USER_ID": userModel.role == Constants.USER
            ? data["provider_id"]
            : data["consumer_id"],
        "PROJECT_ID": data["project_id"],
        "CHAT_ID": data["chat_id"],
        "NAME": data["name"],
        "IMAGE": data["image"],
        "IS_CHAT_ALLOWED": (data["project_status"] !=
                Constants.PROJECT_PAID_REVIEWED_BY_USER &&
            data["project_status"] != Constants.PROJECT_PAID),
      };
    } else {
      print("Unknown action found");
      return;
    }

    if (clearAllScreens) {
      navigatorKey.currentState!.pushNamedAndRemoveUntil(
        routeName,
        (route) => false,
        arguments: args,
      );
    } else {
      navigatorKey.currentState!.pushNamed(
        routeName,
        arguments: args,
      );
    }
  }
}
