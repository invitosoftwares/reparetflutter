import 'package:flutter/material.dart';

import 'custom_colors.dart';

class CustomDecorators {
  var _border;
  BuildContext _context;

  CustomDecorators(this._context) {
    _border = OutlineInputBorder(
      borderSide: BorderSide(
        color: CustomColors.colorWhite,
      ),
    );
  }

  InputDecoration getInputDecoration(
    String text, {
    String? prefixIconString,
    Color? prefixIconColor,
    bool isPrefixStringText = false,
    EdgeInsetsGeometry? contentPadding,
  }) {
    return InputDecoration(
      counterText: "",
      isDense: true,
      contentPadding: contentPadding != null
          ? contentPadding
          : EdgeInsets.symmetric(vertical: 14, horizontal: 2),
      filled: true,
      fillColor: CustomColors.colorWhite,
      hintText: text,
      hintStyle: Theme.of(_context).textTheme.bodyText1!.copyWith(
            color: CustomColors.colorGrayDark,
          ),
      enabledBorder: _border,
      errorBorder: _border,
      focusedErrorBorder: _border,
      focusedBorder: _border,
      prefixIcon: prefixIconString != null
          ? Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12.0),
              child: isPrefixStringText
                  ? Text(
                      "$prefixIconString",
                      style: TextStyle(
                        color: prefixIconColor != null
                            ? prefixIconColor
                            : CustomColors.colorPrimary,
                        fontWeight: FontWeight.bold,
                      ),
                    )
                  : Image.asset(
                      prefixIconString,
                      fit: BoxFit.fill,
                      color: prefixIconColor != null
                          ? prefixIconColor
                          : CustomColors.colorPrimary,
                    ),
            )
          : null,
      prefixIconConstraints: BoxConstraints(
        maxHeight: isPrefixStringText ? 25 : 15,
        maxWidth: isPrefixStringText ? 30 : 40,
      ),
    );
  }

  InputDecoration getInputDecorationPasswordToggle(
    String text,
    VoidCallback onPressed,
    bool isVisible, {
    String? prefixIconString,
    Color? prefixIconColor,
  }) {
    return getInputDecoration(
      text,
      prefixIconString: prefixIconString,
      prefixIconColor: prefixIconColor,
    ).copyWith(
      suffixIcon: IconButton(
        icon: Icon(
          isVisible ? Icons.visibility_off : Icons.visibility,
          color: CustomColors.colorGrayDark,
          size: 18,
        ),
        onPressed: onPressed,
      ),
    );
  }

  InputDecoration getInputDecorationMultiline(
    String text, {
    String? prefixIconString,
    Color? prefixIconColor,
    int maxLines = 1,
    bool isPrefixStringText = false,
    EdgeInsetsGeometry? contentPadding,
  }) {
    return getInputDecoration(
      text,
      prefixIconString: prefixIconString,
      isPrefixStringText: isPrefixStringText,
      prefixIconColor: prefixIconColor,
      contentPadding: contentPadding,
    ).copyWith(
      prefixIconConstraints: BoxConstraints(
        maxHeight: (maxLines * 20).toDouble(),
        maxWidth: 38,
      ),
      prefixIcon: prefixIconString != null
          ? Container(
              height: double.infinity,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 12.0),
                child: isPrefixStringText
                    ? Text(
                        "$prefixIconString",
                        style: TextStyle(
                          color: prefixIconColor != null
                              ? prefixIconColor
                              : CustomColors.colorPrimary,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    : Column(
                        children: [
                          Image.asset(
                            prefixIconString,
                            fit: BoxFit.fill,
                            color: prefixIconColor != null
                                ? prefixIconColor
                                : CustomColors.colorPrimary,
                          ),
                        ],
                      ),
              ),
            )
          : null,
    );
  }
}
