import 'package:flutter/material.dart';

class CustomColors {
  static const Color colorPrimary = Color(0xFF008081);
  static const Color colorAccent = Color(0xFFF7941D);
  static const Color colorWhite = Color(0xFFFFFFFF);
  static const Color colorGrayLight = Color(0x0000000F);
  static const Color colorGrayLight2 = Color(0xFFEFF1ED);
  static const Color colorGrayLight3 = Color(0xFFF8F8F8);
  static const Color colorGrayDark = Color(0x4D4D4D73);
  static const Color colorGrayDark2 = Color(0xFF4D4D4D);
  static const Color colorGrayDark3 = Color(0xFF929292);
  static const Color colorDarkDrawerBackground = Color(0xA8000000);
  static const Color colorBlue = Color(0xFF00BFC1);
  static const Color colorBlue2 = Color(0xFF1DA0F7);
  static const Color colorGreen = Color(0xFF12D626);
  static const Color colorRed = Color(0xFFFF5700);
  static const Color colorRed2 = Color(0xFFFF0000);
  static const Color colorBlack = Color(0xFF000000);

  static const Color colorShimmerBase = Color(0xFFE2E2E2);
  static const Color colorShimmerOverlay = Color(0xFFF8F8F8);
}
