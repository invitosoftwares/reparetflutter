import UIKit
import Flutter
import Firebase
import GoogleMaps
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GMSServices.provideAPIKey("AIzaSyDcfOUOt5Wr_ACHzpJV4RoWFXr070gsR3w")

    FirebaseApp.configure()
    GeneratedPluginRegistrant.register(with: self)

        if #available(iOS 10.0, *) {
          UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        }

    application.registerForRemoteNotifications()
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
  override func application(_ application: UIApplication,
    didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        print("deviceToken");
        print(deviceToken);

            Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)
             super.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
        }
}

