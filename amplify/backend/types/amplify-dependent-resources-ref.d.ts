export type AmplifyDependentResourcesAttributes = {
    "auth": {
        "reparet8918d7fb": {
            "IdentityPoolId": "string",
            "IdentityPoolName": "string",
            "UserPoolId": "string",
            "UserPoolArn": "string",
            "UserPoolName": "string",
            "AppClientIDWeb": "string",
            "AppClientID": "string"
        }
    },
    "storage": {
        "s35d2e9364": {
            "BucketName": "string",
            "Region": "string"
        }
    }
}